package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface SurnameListItemClickListener {
    void onItemClick(View v, int pos);
}
