package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface GroupMembersItemClickListener {
    void onItemClick(View v, int pos);
}
