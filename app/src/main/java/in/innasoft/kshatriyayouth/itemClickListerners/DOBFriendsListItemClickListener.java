package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface DOBFriendsListItemClickListener {
    void onItemClick(View v, int pos);
}
