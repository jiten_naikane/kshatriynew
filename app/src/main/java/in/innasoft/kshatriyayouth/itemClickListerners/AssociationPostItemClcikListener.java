package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

/**
 * Created by Devolper on 22-Aug-17.
 */


public interface AssociationPostItemClcikListener {
    void onItemClick(View v, int pos);
}
