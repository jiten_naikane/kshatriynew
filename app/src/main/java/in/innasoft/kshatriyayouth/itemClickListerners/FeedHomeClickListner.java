package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface FeedHomeClickListner {
    void onItemClick(View v, int pos);
}
