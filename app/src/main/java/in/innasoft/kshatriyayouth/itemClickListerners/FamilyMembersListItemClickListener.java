package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface FamilyMembersListItemClickListener {
    void onItemClick(View v, int pos);
}
