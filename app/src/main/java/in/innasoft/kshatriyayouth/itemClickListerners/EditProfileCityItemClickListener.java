package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface EditProfileCityItemClickListener {
    void onItemClick(View v, int pos);
}
