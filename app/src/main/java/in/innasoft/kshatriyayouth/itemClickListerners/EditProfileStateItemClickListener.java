package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface EditProfileStateItemClickListener {
    void onItemClick(View v, int pos);
}
