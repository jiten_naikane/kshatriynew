package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface EditProfileCountryItemClickListener {
    void onItemClick(View v, int pos);
}
