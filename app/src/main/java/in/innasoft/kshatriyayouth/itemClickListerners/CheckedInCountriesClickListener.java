package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

/**
 * Created by purushotham on 27/1/17.
 */

public interface CheckedInCountriesClickListener {
    void onItemClick(View v, int pos);
}
