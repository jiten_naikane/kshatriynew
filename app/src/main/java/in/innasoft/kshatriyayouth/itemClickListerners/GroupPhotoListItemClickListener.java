package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface GroupPhotoListItemClickListener {
    void onItemClick(View v, int pos);
}
