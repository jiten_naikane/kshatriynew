package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface SelectedImageItemClickListener2 {
    void onItemClick(View v, int pos);
}
