package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface GroupVideoListItemClickListener {
    void onItemClick(View v, int pos);
}
