package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface InformationDirectoryItemClickListener {
    void onItemClick(View v, int pos);
}
