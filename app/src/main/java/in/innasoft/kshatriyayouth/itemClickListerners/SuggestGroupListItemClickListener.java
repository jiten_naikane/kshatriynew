package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface SuggestGroupListItemClickListener {
    void onItemClick(View v, int pos);
}
