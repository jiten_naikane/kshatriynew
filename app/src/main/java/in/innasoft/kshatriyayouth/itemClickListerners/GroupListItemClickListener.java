package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface GroupListItemClickListener {
    void onItemClick(View v, int pos);
}
