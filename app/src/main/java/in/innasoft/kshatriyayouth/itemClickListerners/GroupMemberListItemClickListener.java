package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface GroupMemberListItemClickListener {
    void onItemClick(View v, int pos);
}
