package in.innasoft.kshatriyayouth.itemClickListerners;

import android.view.View;

public interface AreaListItemClickListener {
    void onItemClick(View v, int pos);
}
