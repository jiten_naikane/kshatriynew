package in.innasoft.kshatriyayouth.utilities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.innasoft.kshatriyayouth.fragments.AssociationAboutFragment;
import in.innasoft.kshatriyayouth.fragments.AssociationEventFragment;
import in.innasoft.kshatriyayouth.fragments.AssociationPeopleFragment;
import in.innasoft.kshatriyayouth.fragments.PostAssociationFragment;

/**
 * Created by Devolper on 21-Aug-17.
 */

public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                AssociationAboutFragment about = new AssociationAboutFragment();
                return about;
            case 1:
                PostAssociationFragment post = new PostAssociationFragment();
                return post;
            case 2:
                AssociationEventFragment event = new AssociationEventFragment();
                return event;
            case 3:
                AssociationPeopleFragment people = new AssociationPeopleFragment();
                return people;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}