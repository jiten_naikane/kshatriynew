package in.innasoft.kshatriyayouth.utilities;

public class AppUrls {

    public static String BASE_URL = "http://kshatriyayouth.com/";

//public static String BASE_URL = "http://192.168.1.28/ksy/";

    //public static String BASE_URL_HTTPS = "https://kshatriyayouth.com/";

// http://localhost/ksy/application/views/pages/vpb-group-photos/

    public static String BASE_IMAGE_URL = "http://kshatriyayouth.com/application/views/pages/photos/"; /////687a400e43c59ac7291eff910819af36.jpg

//public static String BASE_IMAGE_URL = "http://192.168.1.40/ksy/application/views/pages/photos/"; /////687a400e43c59ac7291eff910819af36.jpg

// public static String BASE_IMAGE_URL = "http://192.168.1.40/ksy/application/views/pages/photos/"; /////687a400e43c59ac7291eff910819af36.jpg


    public static String BASE_IMAGE_URL_GROUP_LIST = "http://kshatriyayouth.com/application/views/pages/vpb-group-photos/";
//public static String BASE_IMAGE_URL_GROUP_LIST = "http://192.168.1.40/ksy/application/views/pages/vpb-group-photos/";

    public static String BASE_IMAGE_URL_GROUP_ALL_PHOTOS = "http://kshatriyayouth.com/application/views/pages/vpb-wall-photos/";
//public static String BASE_IMAGE_URL_GROUP_ALL_PHOTOS = "http://192.168.1.40/ksy/application/views/pages/vpb-wall-photos/";

// public static String BASE_IMAGE_LOCAL = "http://192.168.1.40/ksy/";

    public static String SURNAME = "getsurname";
    public static String ADD_SURNAME = "addsurname";
    public static String REGISTRATION = "registar";
    public static String ACCOUNT_VERIFICATION = "verifyotp";
    public static String LOGIN = "userlogin";
    public static String RESET_PASSWORD = "reset_pwd";
    public static String RE_SEND_OTP = "resendotp";
    public static String FORGOT_PASSWORD = "forgotpwd";
    public static String CHANGE_PASSWORD = "changePassword";
    public static String UPDATE_USER_PIC = "profile_pic";
    public static String COUNTRY_LIST = "country_app";
    public static String STATE_LIST = "state_app/";
    public static String CITY_LIST = "city_app/";
    public static String USER_FEED = "feed_user";
    public static String ASSOCIATIONS = "associations_app";
    public static String ASSOCIATIONS_DETAIL = "associations_details";
    public static String ASSOCIATIONS_POST = "associations_posts";
    public static String ASSOCIATIONS_EVENT = "associations_events";
    public static String ASSOCIATIONS_PEOPLE = "associations_people";
    public static String ASSOCIATIONS_FOLLOWING = "associations_following";
    public static String ASSOCIATIONS_FOLLOW = "association_follow";
    public static String ASSOCIATIONS_UNFOLLOW = "association_unfollow";
    public static String CATEGORY_LIST = "category_app";
    public static String JOBS = "jobs_app";
    public static String JOBS_DETAILS = "job_details";
    public static String BLOG = "blogs_app";
    public static String BLOG_DETAILS = "blog_details";
    public static String INFORMATION_DIRECTORY = "information_directory_app";
    public static String INFORMATION_DETAILS = "information_details";
    public static String BUSINESS_DIRECTORY = "business_directory_app";
    public static String BUSINESS_DETAILS = "business_details";
    public static String AREA_DETAIL = "area_app";
    public static String CAUSES = "causes_app";
    public static String CAUSES_DETAILS = "causes_details";
    public static String DONATION_ADD = "donation_app";
    public static String ANNOUNCEMENT = "announcements_app";
    public static String ANNOUNCEMENT_DETAIL = "announcements_details";
    public static String FEEDBACK = "feedback_app";
    public static String ABOUT_USER = "about_user";
    public static String SAVE_ABOUT_USER = "save_about_user";
    public static String GROUP_MEMBERS = "group_members";
    public static String GROUP_CREATION = "save_group";
    public static String GROUP_WISE_MEMBER_LIST = "group_members_list";
    public static String GROUP_LIST = "groups_list";
    public static String GROUP_PHOTOS = "group_photos";
    public static String GROUP_VIDEOS = "group_videos";
    public static String GROUP_EDIT = "groups_info_id";
    public static String GROUP_DELETE = "groups_delete_id";
    public static String GROUP_REPORT = "post_report";
    public static String UPLOAD_RESUME = "upload_resume_app";
    public static String POST_USER_FEED = "post_user_feed";
    public static String FRIENDS_LIST = "friends_list";
    public static String FRIENDS_REQUEST_LIST = "friend_request_list";
    public static String ADD_FRIEND_REQUEST = "add_friend_request";
    public static String ACCEPT_FRIEND_REQUEST = "accept_friend_request";
    public static String UNFRIEND_REQUEST = "unfriend_request";
    public static String REJECT_FRIEND_REQUEST = "reject_friend_request";
    public static String CANCEL_FRIEND_REQUEST = "cancelFriendRequest";
    public static String NOTIFICATIONS_LIST = "notification_list/";
    public static String GROUP_MAIN_FEED = "group_main_feed";
    public static String GROUP_POST_PHOTOS = "group_post_photos";
    public static String GROUP_POST_LIKECOMMENT = "group_post_likecomment";
    public static String GROUP_POST_COMMENT_LIKECOMMENT = "user_post_comment_likecomment";
    public static String USER_POST_COMMENT = "user_post_comment";
    public static String USER_POST_COMMENT_LIKES = "user_post_comment_likes";
    public static String USER_POST_COMMENT_COMMENTS = "user_post_comment_comments";
    public static String USER_POST_LIKES = "user_post_likes";
    public static String USER_POST_EDIT = "user_post_edit";
    public static String USER_POST_UNLIKES = "user_post_unlikes";
    public static String USER_MAIN_FEED = "user_main_feed";
    public static String USER_POST_HIDE = "user_post_hide";
    public static String USER_POST_UNHIDE = "user_post_unhide";
    public static String USER_POST_DELETE = "user_post_delete";
    public static String POST_EDIT_INFO = "post_edit_info";
    public static String USER_COMMENT_EDIT = "user_comment_edit";
    public static String USER_COMMENT_DELETE = "user_comment_delete";
    public static String USER_EDIT_INFO = "comment_edit_info";
    public static String USER_COMMENT_REPLY_EDIT = "user_comment_reply_edit";
    public static String USER_COMMENT_REPLY_DELETE = "user_comment_reply_delete";
    public static String USER_COMMENT_REPLY_EDIT_INFO = "comment_reply_edit_info";
    public static String USER_DELETE_INFO = "user_details_info";
    public static String COMMETN_UNDERCOMMENT_LIKES = "comment_undercomment_likes";
    public static String USER_POST_COMMENT_UNDERCOMMENT_UNLIKES = "user_post_comment_unlikes";
    public static String COMMENT_UNDER_COMMENT_UNLIKES = "comment_undercomment_unlikes";
    public static String USER_PROFILE_INFO_LIST = "user_profile_info_list";
    public static String USER_GROUP_PHOTOS = "user_group_photos";
    public static String USER_GROUP_VIDEOS = "user_group_videos";
    public static String USER_GROUP_LIST = "user_group_list";
    public static String UPDATE_GROUP = "edit_group";
    public static String FRIENDS_LIST_NEW = "friends_list_new";
    public static String USER_FRIEND_LIST = "user_friends_list";
    public static String SUGGEST_GROUP_LIST = "suggest_group_list";
    public static String JOIN_GROUP = "join_group";
    public static String DELETE_JOIN_GROUP = "delete_join_group";
    public static String ACCEPT_JOIN_GROUP = "accept_join_group";
    public static String NOTIFICATION_COUNT = "notification_list_count";
    public static String NOTIFICATION_SET_READ = "notification_set_read";
    public static String POST_EDIT_INFO_LIST = "post_edit_info_list";
    public static String UPDATE_SHARE = "update_share";
    public static String GROUP_POST_LIKES_LIST = "group_post_like_list";

    public static String DATE_OF_BIRTH_WHISHES = "user_friends_birthdays";


    //////////////////////////////////////////
    public static final String urlUpload = "http://kshatriyayouth.com/upload_img_path";
    public static final int REQCODE = 100;
    public static final String imageList = "imageList";
    public static final String imageName = "name";


}

