package in.innasoft.kshatriyayouth.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

import in.innasoft.kshatriyayouth.activities.LoginPage;
import in.innasoft.kshatriyayouth.activities.SplashScreen;

public class UserSessionManager {

    SharedPreferences pref, preferences;
    Editor editor, editor2;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREFER_NAME = "sricoachingcenter";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String KEY_ACCSES = "access_key";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_SURNAME = "user_surname";
    public static final String USER_PROFILE_ID = "user_profile_id";
    public static final String USER_MEMBERSHIP = "membership";
    public static final String PROFILE_PIC_URL = "user_profile_pic_url";
    public static final String INTRO_SLIDE = "intorslide";
    public static final String ISFIRTTIMEAPP = "isfirsttime";
    public static final String LOGINTYPE = "login_type";
    public static final String NOTIFICATION_STATUS = "notification_status";
    public static final String USER_DEVICE_ID = "user_device_id";


    public UserSessionManager(Context context) {
        this._context = context;
        preferences = context.getSharedPreferences(INTRO_SLIDE, PRIVATE_MODE);
        editor2 = preferences.edit();

        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createUserLoginSession(String userId, String userName, String mobile, String email, String surname, String user_profile_id, String photo_url, String membership, String device_id) {

        editor.putBoolean(IS_USER_LOGIN, true);
        //   editor.putString(KEY_ACCSES, key);
        editor.putString(USER_ID, userId);
        editor.putString(USER_NAME, userName);
        editor.putString(USER_MOBILE, mobile);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_SURNAME, surname);
        editor.putString(USER_PROFILE_ID, user_profile_id);
        editor.putString(PROFILE_PIC_URL, photo_url);
        editor.putString(USER_MEMBERSHIP, membership);
        editor.putString(USER_DEVICE_ID, device_id);
        //   editor.putString(LOGINTYPE, login_type);
        //   editor.putString(NOTIFICATION_STATUS, notification_status);
        editor.commit();
    }


    public void createIsFirstTimeAppLunch() {
        editor2.putBoolean(ISFIRTTIMEAPP, true);
        editor2.commit();
    }

    public boolean checkIsFirstTime() {

        if (!this.isFirstTimeAppLunch()) {

            Intent i = new Intent(_context, SplashScreen.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

            return true;
        }
        return false;
    }

    public boolean checkLogin() {

        if (!this.isUserLoggedIn()) {

            Intent i = new Intent(_context, LoginPage.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            return true;
        }
        return false;
    }

    public void updateProfileInfo(String name, String email, String mobile) {
        editor.putString(USER_NAME, name);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_MOBILE, mobile);

        editor.commit();
    }

    public void updateNotificationStatus(String noti_status) {
        editor.putString(NOTIFICATION_STATUS, noti_status);


        editor.commit();
    }

    public void updateProfilePicInfo(String profilePic) {
        editor.putString(PROFILE_PIC_URL, profilePic);


        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        //    user.put(KEY_ACCSES, pref.getString(KEY_ACCSES, null));
        user.put(USER_ID, pref.getString(USER_ID, "0"));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(USER_MOBILE, pref.getString(USER_MOBILE, null));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));
        user.put(USER_SURNAME, pref.getString(USER_SURNAME, null));
        user.put(USER_PROFILE_ID, pref.getString(USER_PROFILE_ID, null));
        user.put(PROFILE_PIC_URL, pref.getString(PROFILE_PIC_URL, null));
        user.put(USER_MEMBERSHIP, pref.getString(USER_MEMBERSHIP, null));
        user.put(USER_DEVICE_ID, pref.getString(USER_DEVICE_ID, null));
        //   user.put(PROFILE_PIC_URL, pref.getString(PROFILE_PIC_URL, null));
        //    user.put(LOGINTYPE, pref.getString(LOGINTYPE, null));
        //    user.put(NOTIFICATION_STATUS, pref.getString(NOTIFICATION_STATUS, null));
        return user;
    }


    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, LoginPage.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    public boolean isFirstTimeAppLunch() {
        return preferences.getBoolean(ISFIRTTIMEAPP, false);
    }


}