package in.innasoft.kshatriyayouth.utilities;

import android.app.Application;


public class MyApp extends Application {

    @Override
    public void onCreate() {
        TypefaceUtil.overrideFont(getApplicationContext(), "MONOSPACE1", "fonts/Roboto-Light.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
    }
}
