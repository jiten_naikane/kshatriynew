package in.innasoft.kshatriyayouth.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.CommentsActivity;
import in.innasoft.kshatriyayouth.activities.EditPostActivity;
import in.innasoft.kshatriyayouth.activities.FeedAllImagesActivity;
import in.innasoft.kshatriyayouth.activities.GroupVideoPostActivity;
import in.innasoft.kshatriyayouth.activities.LikeMembersDisplayActivity;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.activities.SharePostActivity;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.fragments.GroupDiscussionFragment;
import in.innasoft.kshatriyayouth.holders.FeedHomeHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.models.HomeFeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FeedImageView;
import in.innasoft.kshatriyayouth.utilities.FilePath;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

import static in.innasoft.kshatriyayouth.app.AppController.TAG;


public class GroupFeedHomeAdapter extends RecyclerView.Adapter<FeedHomeHolder> {
    public ArrayList<HomeFeedItem> feeditem;
    public GroupDiscussionFragment context;
    LayoutInflater li;
    int resource;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    String category_id;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    BottomSheetDialog bottomSheetDialog;
    String user_name = "";
    String profile_pic_url = "";
    //    Typeface typeface;
    ImageView image_view;
    UserSessionManager sessionManager;
    Drawable d;
    EditText commenttext;
    ImageView send_image, select_image;
    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    private Uri fileUri;
    private String selectedFilePath;
    private boolean checkInternet;
    //////////////////////////////
    private ListView listView;
    private NotificationsListAdapter listAdapter;
    String like_style = "";
    private String URL_FEED = AppUrls.BASE_URL + AppUrls.GROUP_POST_LIKECOMMENT;
    /////////////////////////////////////
    int defaultPageNo = 1;


    public GroupFeedHomeAdapter(ArrayList<HomeFeedItem> feeditem, GroupDiscussionFragment context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;
        sessionManager = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        user_name = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        Log.d("dfjkdfbndfjkb", "hhghgh" + profile_pic_url);

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public FeedHomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        FeedHomeHolder slh = new FeedHomeHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final FeedHomeHolder holder, final int position) {
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        holder.name.setText(Html.fromHtml(feeditem.get(position).getName()));


//        holder.likes.setTypeface(typeface);
//        holder.shares.setTypeface(typeface);
//        holder.post_status.setTypeface(typeface);
        holder.post_status.setText(feeditem.get(position).getSecurity());

//        holder.like_text.setTypeface(typeface);
//        holder.coment_text.setTypeface(typeface);
//        holder.share_text.setTypeface(typeface);
//        holder.seemore.setTypeface(typeface);
//        holder.name.setTypeface(typeface);
//        holder.unhide.setTypeface(typeface);
        if (!feeditem.get(position).getHiddenitem().equals("0")) {
            holder.main_feed_layout.setVisibility(View.GONE);
            holder.unhide.setVisibility(View.VISIBLE);
            holder.unhide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = feeditem.get(position).getId();
                    postUnHide(id);
                }
            });

        } else {
            holder.main_feed_layout.setVisibility(View.VISIBLE);
            holder.unhide.setVisibility(View.GONE);
        }
        if (!feeditem.get(position).getPost_edit_count().equals("0")) {
            holder.edited.setVisibility(View.VISIBLE);
            holder.edited.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context.getActivity(), EditPostActivity.class);
                    intent.putExtra("id", feeditem.get(position).getId());
                    intent.putExtra("sub_id", "0");
                    intent.putExtra("type", "post");
                    context.getActivity().startActivity(intent);
                }
            });
        } else {
            holder.edited.setVisibility(View.GONE);
        }
        int count = Integer.parseInt(feeditem.get(position).getPhoto_list_count());
        if (count > 1) {
            holder.seemore.setVisibility(View.VISIBLE);
            holder.name.setText(Html.fromHtml(feeditem.get(position).getName() + " added " + feeditem.get(position).getPhoto_list_count() + " photos"));

        } else {
            holder.seemore.setVisibility(View.GONE);
            holder.name.setText(Html.fromHtml(feeditem.get(position).getName()));
        }
        // Converting timestamp into x ago format
        /*CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
				Long.parseLong(item.getTimeStamp()),
				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);*/
        holder.timestamp.setText(Html.fromHtml(feeditem.get(position).getDate()));
        if (!feeditem.get(position).getPost_likes_count().equals("0")) {
            holder.likes.setText(feeditem.get(position).getPost_likes_count() + " Likes");
        } else {
            holder.likes.setVisibility(View.GONE);
        }
        if (!feeditem.get(position).getPost_comments_count().equals("0")) {
            holder.shares.setText(feeditem.get(position).getPost_comments_count() + " Comments");
        } else {
            holder.shares.setVisibility(View.GONE);
        }

//        holder.timestamp.setTypeface(typeface);

        // Chcek for empty status message
        if (!TextUtils.isEmpty(feeditem.get(position).getPost())) {

            Log.d("sdjsdffbbk", feeditem.get(position).getPost());
            SpannableString ss = new SpannableString("");
            if (feeditem.get(position).getPost().contains(":blushing-angel:")) {


                d = context.getActivity().getResources().getDrawable(R.drawable.angel);
                Drawable img = context.getResources().getDrawable(R.drawable.angel);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "angel" + "</b> "));
                String value = feeditem.get(position).getPost().replace(":blushing-angel:", "");
                //  holder.statusMsg.setText(Html.fromHtml(value));

            } else if (feeditem.get(position).getPost().contains("o.O")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.confused);
                Drawable img = context.getResources().getDrawable(R.drawable.confused);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "confused" + "</b> "));

            } else if (feeditem.get(position).getPost().contains("B|")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.cool);

                Drawable img = context.getResources().getDrawable(R.drawable.cool);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "cool" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":cry:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.cry);
                Drawable img = context.getResources().getDrawable(R.drawable.cry);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "cry" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":tea-cup:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.cup_of_tea);
                Drawable img = context.getResources().getDrawable(R.drawable.cup_of_tea);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "cup of tea" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":laughing-devil:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.devil);
                Drawable img = context.getResources().getDrawable(R.drawable.devil);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "devil" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":(")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.frown);
                Drawable img = context.getResources().getDrawable(R.drawable.frown);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "cry" + "</b> "));

            } else if (feeditem.get(position).getPost().contains("B)")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.glasses);
                Drawable img = context.getResources().getDrawable(R.drawable.glasses);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "glasses" + "</b> "));
            } else if (feeditem.get(position).getPost().contains(":lve:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.heart);
                Drawable img = context.getResources().getDrawable(R.drawable.heart);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "love" + "</b> "));

            } else if (feeditem.get(position).getPost().contains("^_^")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.kiki);
                Drawable img = context.getResources().getDrawable(R.drawable.kiki);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "hehe" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":kiss:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.kiss);
                Drawable img = context.getResources().getDrawable(R.drawable.kiss);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "kiss" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":v")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.pacman);
                Drawable img = context.getResources().getDrawable(R.drawable.pacman);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "packman" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":(")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.sadj);
                Drawable img = context.getResources().getDrawable(R.drawable.sadj);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "sad" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":)")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.shock);
                Drawable img = context.getResources().getDrawable(R.drawable.shock);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "shock" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":unsure:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.unsure);
                Drawable img = context.getResources().getDrawable(R.drawable.unsure);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "unsure" + "</b> "));

            } else if (feeditem.get(position).getPost().contains(":grumpy:")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.grumpy);
                Drawable img = context.getResources().getDrawable(R.drawable.grumpy);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "grumpy" + "</b> "));

            } else if (feeditem.get(position).getPost().contains("(wink)")) {
                d = context.getActivity().getResources().getDrawable(R.drawable.wink);
                Drawable img = context.getResources().getDrawable(R.drawable.wink);
                img.setBounds(0, 0, 60, 60);
                holder.statusMsg.setCompoundDrawables(img, null, null, null);
                holder.statusMsg.append(Html.fromHtml(" feeling " + "<b>" + "wink" + "</b> "));

            } else {
                holder.statusMsg.setText(Html.fromHtml(feeditem.get(position).getPost()));
            }
            holder.statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            holder.statusMsg.setVisibility(View.GONE);
        }

        // Checking for null feed url
        if (feeditem.get(position).getImage_one() != null) {
            holder.url.setText(Html.fromHtml("<a href=\"" + feeditem.get(position).getImage_one() + "\">"
                    + feeditem.get(position).getImage_one() + "</a> "));
//            holder. url.setTypeface(typeface);
            // Making url clickable
            holder.url.setMovementMethod(LinkMovementMethod.getInstance());
            holder.url.setVisibility(View.GONE);

        } else {

            // url is null, remove from the view
            holder.url.setVisibility(View.GONE);
        }
        if (feeditem.get(position).getVideo_list_url().equals("")) {
            holder.webView.setVisibility(View.GONE);
            holder.thumbnail_image.setVisibility(View.GONE);
        } else {
            holder.thumbnail_image.setVisibility(View.VISIBLE);
            holder.name.setText(Html.fromHtml(feeditem.get(position).getName() + " added Video"));
            Picasso.with(context.getActivity())
                    .load(feeditem.get(position).getVideo_list_url())
                    .placeholder(R.drawable.thumbnail_default)
                    .into(holder.thumbnail_image);
            holder.webView.setVisibility(View.GONE);

        }
        holder.thumbnail_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context.getActivity());
                if (checkInternet) {

                    String videopostUrl = feeditem.get(position).getVideo_list_url();

                    if (videopostUrl.equals("notfound") || videopostUrl.equals("null")) {
                        Snackbar snackbar = Snackbar
                                .make(context.getView(), "Currently not available..!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        Intent intent = new Intent(context.getActivity(), GroupVideoPostActivity.class);
                        // String video[] = videopostUrl.split("=");
                        // String video_id = video[1];
                        intent.putExtra("Video_Url", videopostUrl);
                        context.startActivity(intent);

                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(context.getView(), "Please check your Internet connection....!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
        /////////////////////////////////////////////
        if (!feeditem.get(position).getShared_info_count().equals("0")) {
            holder.shared_post.setVisibility(View.GONE);
            holder.shared_post_layout.setVisibility(View.VISIBLE);
            holder.shared_name.setText(feeditem.get(position).getShared_info_name() + " " + feeditem.get(position).getShared_info_surname() + " shared " + feeditem.get(position).getName() + " 's post ");
            holder.shared_display_text.setText(feeditem.get(position).getShared_info_post());
            holder.name.append("  shared " + feeditem.get(position).getShared_info_name() + " " + feeditem.get(position).getShared_info_surname() + "'s Post");
            holder.shared_profilePic.setImageUrl(feeditem.get(position).getShared_info_photo(), imageLoader);
            holder.shared_profilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                    intent.putExtra("user_id", feeditem.get(position).getShared_info_username());
                    context.getActivity().startActivity(intent);
                }
            });
            holder.shared_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                    intent.putExtra("user_id", feeditem.get(position).getShared_info_username());
                    context.getActivity().startActivity(intent);
                }
            });

           /* holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                    intent.putExtra("user_id",feeditem.get(position).getUsername());
                    context.getActivity().startActivity(intent);
                }
            });*/
        } else {
            holder.shared_post.setVisibility(View.GONE);
            holder.shared_post_layout.setVisibility(View.GONE);
        }/////////////////////////////
//       Log.d("sjdhvksdvgf",""+feeditem.get(position).getVideo_list_url().trim().replace("///","/"));
        String video_url = "" + feeditem.get(position).getVideo_list_url();
        // user profile pic
        holder.profilePic.setImageUrl(feeditem.get(position).getPhoto(), imageLoader);
        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
            }
        });
        if (video_url.equals("null") || video_url == null) {
            holder.webView.setVisibility(View.GONE);
        } else {
            holder.webView.setVisibility(View.VISIBLE);
            holder.webView.setFocusable(true);
            holder.webView.setFocusableInTouchMode(true);
            holder.webView.getSettings().setJavaScriptEnabled(true);
            holder.webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            holder.webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            holder.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            holder.webView.getSettings().setDomStorageEnabled(true);
            holder.webView.getSettings().setDatabaseEnabled(true);
            holder.webView.getSettings().setAppCacheEnabled(true);
            holder.webView.setWebViewClient(new WebViewClient());
            holder.webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

            String html = video_url.replace("///", "/");
            Log.d("sjkdavhsv", "" + html);
            //  String html = "<html><body><iframe class=\"youtube-player\" type=\"text/html\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/MSTkgCBDqKw\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
            //  String html = "<html><body><iframe class=\"youtube-player\" type=\"text/html\" width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/MSTkgCBDqKw\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
            String mime = "text/html";
            String encoding = "utf-8";
            holder.webView.loadData(html, mime, encoding);


        }

       /* Picasso.with(context.getActivity())
                .load(feeditem.get(position).getImage_two())
                .into(holder.feedImageView);*/
        // Feed image
        Log.d("images_", "" + feeditem.get(position).getImage_two());
        if (feeditem.get(position).getImage_one().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS) && feeditem.get(position).getImage_two().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS) && feeditem.get(position).getImage_three().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {

        }
        if (!feeditem.get(position).getImage_one().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {
           /* holder. feedImageView.setVisibility(View.VISIBLE);
            holder. feedImageView1.setVisibility(View.GONE);
            holder. feedImageView2.setVisibility(View.GONE);*/
            holder.feedImageView.setImageUrl(feeditem.get(position).getImage_one(), imageLoader);

            holder.feedImageView
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });

        }
        if (!feeditem.get(position).getImage_one().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS) && !feeditem.get(position).getImage_two().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {
          /*  holder. feedImageView.setVisibility(View.VISIBLE);
            holder. feedImageView1.setVisibility(View.VISIBLE);
            holder. feedImageView2.setVisibility(View.GONE);*/
            holder.feedImageView.setImageUrl(feeditem.get(position).getImage_one(), imageLoader);

            holder.feedImageView
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
            holder.feedImageView1.setImageUrl(feeditem.get(position).getImage_two(), imageLoader);

            holder.feedImageView1
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });


        }
        if (!feeditem.get(position).getImage_one().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS) && !feeditem.get(position).getImage_two().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS) && !feeditem.get(position).getImage_three().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {
           /* holder. feedImageView.setVisibility(View.VISIBLE);
            holder. feedImageView1.setVisibility(View.VISIBLE);
            holder. feedImageView2.setVisibility(View.VISIBLE);*/
            holder.feedImageView.setImageUrl(feeditem.get(position).getImage_one(), imageLoader);

            holder.feedImageView
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
            holder.feedImageView1.setImageUrl(feeditem.get(position).getImage_two(), imageLoader);

            holder.feedImageView1
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
            holder.feedImageView2.setImageUrl(feeditem.get(position).getImage_three(), imageLoader);


            holder.feedImageView2
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });

        }


        holder.sharelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showPictureialog();
                Intent intent = new Intent(context.getActivity(), SharePostActivity.class);
                intent.putExtra("post_id", feeditem.get(position).getId());
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);


            }
        });
        holder.shares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showPictureialog();
                Intent intent = new Intent(context.getActivity(), CommentsActivity.class);
                intent.putExtra("id", feeditem.get(position).getId());
                intent.putExtra("friend", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);


            }
        });
        holder.likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showPictureialog();
                if (!feeditem.get(position).getPost_likes_count().equals("0")) {
                    Intent intent = new Intent(context.getActivity(), LikeMembersDisplayActivity.class);
                    intent.putExtra("post_id", feeditem.get(position).getId());
                    intent.putExtra("user_id", user_name);
                    context.startActivity(intent);
                }


            }
        });
        holder.chatlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.hideshow_text.setVisibility(View.GONE);

                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                Intent intent = new Intent(context.getActivity(), CommentsActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("friend", friend);
                context.getActivity().startActivity(intent);
                // showBottomSheet(id);

            }
        });
        holder.post_hideorvisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  showPictureialogforPostVisibleorHide();
                if (user_name.equals(feeditem.get(position).getUsername())) {
                    CharSequence[] items = {"Edit Post", "Delete"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            if (item == 0) {
                                final String id = feeditem.get(position).getId();
                                final String name = feeditem.get(position).getPost();
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        context.getActivity());
                                LayoutInflater inflater = (LayoutInflater) context.getActivity()
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = inflater.inflate(R.layout.dialog_demo, null);
                                alertDialogBuilder.setView(view);
                                alertDialogBuilder.setCancelable(false);
                                final AlertDialog dialog1 = alertDialogBuilder.create();
                                dialog1.show();
                                final EditText report_edit_text = (EditText) view.findViewById(R.id.report_edit_text);
                                report_edit_text.setText(name);
                                final TextView text = (TextView) view.findViewById(R.id.text);
                                text.setText("EDIT POST");
                                Button dialog_report = (Button) view.findViewById(R.id.dialog_report);
                                dialog_report.setText("Save");
                                Button dialog_cancel = (Button) view.findViewById(R.id.dialog_cancel);
                                final String edt_text = report_edit_text.getText().toString();
                                dialog_report.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!report_edit_text.getText().toString().equals(name)) {
                                            editPost(id, report_edit_text.getText().toString(), name);
                                            dialog1.cancel();
                                        }
                                    }
                                });

                                dialog_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog1.cancel();
                                    }
                                });
                            } else if (item == 1) {
                                String id = feeditem.get(position).getId();
                                postDelete(id);
                            }
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
                    wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
                    wmlp.x = 100;   //x position
                    wmlp.y = 100;   //y position

                    dialog.show();

                } else {
                    CharSequence[] items = {"Hide Post", "Report"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            if (item == 0) {
                                String id = feeditem.get(position).getId();
                                postHide(id);
                            } else if (item == 1) {
                                final String id = feeditem.get(position).getId();
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        context.getActivity());
                                LayoutInflater inflater = (LayoutInflater) context.getActivity()
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = inflater.inflate(R.layout.dialog_demo, null);
                                alertDialogBuilder.setView(view);
                                alertDialogBuilder.setCancelable(false);
                                final AlertDialog dialog1 = alertDialogBuilder.create();
                                dialog1.show();
                                final EditText report_edit_text = (EditText) view.findViewById(R.id.report_edit_text);
                                Button dialog_report = (Button) view.findViewById(R.id.dialog_report);
                                Button dialog_cancel = (Button) view.findViewById(R.id.dialog_cancel);
                                final String edt_text = report_edit_text.getText().toString();
                                dialog_report.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!report_edit_text.getText().toString().equals("")) {
                                            reportPost(id, report_edit_text.getText().toString());
                                            dialog1.cancel();
                                        }
                                    }
                                });

                                dialog_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog1.cancel();
                                    }
                                });

                            }
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
                    wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
                    wmlp.x = 100;   //x position
                    wmlp.y = 100;   //y position

                    dialog.show();
                }

            }
        });
        /*if (feeditem.get(position).getUser_like_status().equals("1")){
            int tabIconColor = ContextCompat.getColor(context.getActivity(), R.color.colorPrimary);

            holder. like_text.setTextColor(Color.parseColor("#FF9F06"));
            holder.like_image.setImageResource(R.drawable.like_icon_color);
           // setTextViewDrawableColor(holder. like_text, R.color.colorPrimary);
            holder. like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = feeditem.get(position).getId();
                    postDisLikes(id);

                }
            });

        }else{
            int tabIconColor = ContextCompat.getColor(context.getActivity(), R.color.black);
            holder. like_text.setTextColor(Color.parseColor("#000000"));
            holder.like_image.setImageResource(R.drawable.like_icon);
           // setTextViewDrawableColor(holder. like_text, R.color.black);
            holder. like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id,friend);

                }
            });
        }*/
        if (feeditem.get(position).getUser_like_status().equals("1")) {
            int tabIconColor = ContextCompat.getColor(context.getActivity(), R.color.colorPrimary);
            if (!feeditem.get(position).getUser_like_status_type().equals("null") || feeditem.get(position).getUser_like_status_type() != null)
                if (feeditem.get(position).getUser_like_status_type().equals("Like")) {
                    holder.like_text.setText("Like");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.like_pop);
                } else if (feeditem.get(position).getUser_like_status_type().equals("Love")) {
                    holder.like_text.setText("Love");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.heart_pop);
                } else if (feeditem.get(position).getUser_like_status_type().equals("Haha")) {
                    holder.like_text.setText("Haha");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.laugh_pop);
                } else if (feeditem.get(position).getUser_like_status_type().equals("Angry")) {
                    holder.like_text.setText("Angry");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.angry_pop);
                } else if (feeditem.get(position).getUser_like_status_type().equals("Wow")) {
                    holder.like_text.setText("Wow");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.surprise_pop);
                } else if (feeditem.get(position).getUser_like_status_type().equals("Sad")) {
                    holder.like_text.setText("Sad");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.sad_pop);
                }
            // setTextViewDrawableColor(holder. like_text, R.color.colorPrimary);
            holder.like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = feeditem.get(position).getId();

                    postDisLikes(id);

                }
            });
            /*if (feeditem.get(position).getUser_like_status_type().equals("Like")) {
                holder.like_text.setText("Like");
                holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                holder.like_image.setImageResource(R.drawable.like_icon_color);
                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                postLikes(id,friend);
            }else if (feeditem.get(position).getUser_like_status_type().equals("Love")){
                holder.like_text.setText("Love");
                holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                holder.like_image.setImageResource(R.drawable.heart_pop);
                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                postLikes(id,friend);
            }else if (feeditem.get(position).getUser_like_status_type().equals("Haha")){
                holder.like_text.setText("Haha");
                holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                holder.like_image.setImageResource(R.drawable.laugh_pop);
                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                postLikes(id,friend);
            }else if (feeditem.get(position).getUser_like_status_type().equals("Angry")){
                holder.like_text.setText("Angry");
                holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                holder.like_image.setImageResource(R.drawable.angry_pop);
                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                postLikes(id,friend);
            }else if (feeditem.get(position).getUser_like_status_type().equals("Wow")){
                holder.like_text.setText("Wow");
                holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                holder.like_image.setImageResource(R.drawable.surprise_pop);
                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                postLikes(id,friend);
            }else if (feeditem.get(position).getUser_like_status_type().equals("Sad")){
                holder.like_text.setText("Sad");
                holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                holder.like_image.setImageResource(R.drawable.sad_pop);
                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                String id = feeditem.get(position).getId();
                String friend = feeditem.get(position).getUsername();
                postLikes(id,friend);
            }*/
            holder.like_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.like_text.setText("Like");
                    like_style = "Like";
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.like_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.love_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.like_text.setText("Love");
                    like_style = "Love";
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.heart_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.haha_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Haha";
                    holder.like_text.setText("Haha");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.laugh_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.wow_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Wow";
                    holder.like_text.setText("Wow");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.surprise_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.sad_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Sad";
                    holder.like_text.setText("Sad");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.sad_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.angry_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Angry";
                    holder.like_text.setText("Angry");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.angry_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });

        } else {
            int tabIconColor = ContextCompat.getColor(context.getActivity(), R.color.black);
            holder.like_text.setTextColor(Color.parseColor("#000000"));
            holder.like_image.setImageResource(R.drawable.like_icon);
            // setTextViewDrawableColor(holder. like_text, R.color.black);
            holder.like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Like";
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);

                }
            });
            holder.like_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.like_text.setText("Like");
                    like_style = "Like";
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.like_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.love_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.like_text.setText("Love");
                    like_style = "Love";
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.heart_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.haha_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Haha";
                    holder.like_text.setText("Haha");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.laugh_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.wow_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Wow";
                    holder.like_text.setText("Wow");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.surprise_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.sad_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Sad";
                    holder.like_text.setText("Sad");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.sad_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
            holder.angry_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_style = "Angry";
                    holder.like_text.setText("Angry");
                    holder.like_text.setTextColor(Color.parseColor("#FF9F06"));
                    holder.like_image.setImageResource(R.drawable.angry_pop);
                    holder.like_catogery_layout.setVisibility(View.GONE);
                    holder.likecommentsharelayout.setVisibility(View.VISIBLE);
                    String id = feeditem.get(position).getId();
                    String friend = feeditem.get(position).getUsername();
                    postLikes(id, friend);
                }
            });
        }

        holder.like_catogery_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);

            }
        });
        holder.feed_view_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);

            }
        });
        holder.like_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                holder.hideshow_text.setVisibility(View.GONE);
                holder.like_catogery_layout.setVisibility(View.VISIBLE);
                holder.likecommentsharelayout.setVisibility(View.INVISIBLE);
                return false;
            }
        });
        holder.feedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), FeedAllImagesActivity.class);
                intent.putExtra("id", feeditem.get(position).getId());
                context.startActivity(intent);
            }
        });


        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {

                holder.like_catogery_layout.setVisibility(View.GONE);
                holder.likecommentsharelayout.setVisibility(View.VISIBLE);


            }
        });

    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showBottomSheet(final String id) {
        bottomSheetDialog = new BottomSheetDialog(context.getActivity());

        View view = context.getActivity().getLayoutInflater().inflate(R.layout.content_dialog_bottom_sheet, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
        listView = (ListView) view.findViewById(R.id.list);
        image_view = (ImageView) view.findViewById(R.id.image_view1);
        commenttext = (EditText) view.findViewById(R.id.commenttext);
        send_image = (ImageView) view.findViewById(R.id.send_image);
        select_image = (ImageView) view.findViewById(R.id.select_image);
        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(checkInternet)
                {
                    final boolean result = ImagePermissions.checkPermission(context.getActivity());
                    final Dialog mBottomSheetDialog = new Dialog(context.getActivity(), R.style.MaterialDialogSheet);
               *//* TextView dialog_title = (TextView) mBottomSheetDialog.findViewById(R.id.dialog_title);
//                dialog_title.setTypeface(typeface);*//*
                    mBottomSheetDialog.setContentView(R.layout.custom_dialog_image_selection); // your custom view.
                    mBottomSheetDialog.setCancelable(true);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                    mBottomSheetDialog.show();

                    TextView take_photo_text = (TextView) mBottomSheetDialog.findViewById(R.id.take_photo_text);
//                    take_photo_text.setTypeface(typeface);
                    take_photo_text.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Toast.makeText(context.getActivity(),"take photo",Toast.LENGTH_LONG).show();
                            if (result)
                            {
                                if (ContextCompat.checkSelfPermission(context.getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                {
                                    if (getFromPref(context.getActivity(), ALLOW_KEY)) {
                                        showSettingsAlert();
                                    } else if (ContextCompat.checkSelfPermission(context.getActivity(),
                                            android.Manifest.permission.CAMERA)

                                            != PackageManager.PERMISSION_GRANTED) {

                                        // Should we show an explanation?
                                        if (ActivityCompat.shouldShowRequestPermissionRationale(context.getActivity(),
                                                android.Manifest.permission.CAMERA)) {
                                            showAlert();
                                        } else {
                                            // No explanation needed, we can request the permission.
                                            ActivityCompat.requestPermissions(context.getActivity(),
                                                    new String[]{android.Manifest.permission.CAMERA},
                                                    CAMERA_REQUEST_IMAGE);
                                        }
                                    }
                                }
                                else
                                {
                                    cameraIntent();
                                    mBottomSheetDialog.dismiss();
                                }

                            }


                        }
                    });

                    TextView select_from_gallery = (TextView) mBottomSheetDialog.findViewById(R.id.select_from_gallery);
//                    select_from_gallery.setTypeface(typeface);
                    select_from_gallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            Toast.makeText(context.getActivity(),"gallry",Toast.LENGTH_LONG).show();
                        *//*if (result) {

                        }*//*
                            galleryIntent();
                            mBottomSheetDialog.dismiss();


                        }
                    });

                    mBottomSheetDialog.show();
                }
                else
                {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }*/
            }
        });

        send_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!commenttext.getText().toString().equals("")) {
                    postComment(id);
                } else {
                    Toast.makeText(context.getActivity(), "Please write some text", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Picasso.with(context.getActivity())
                .load(profile_pic_url)
                .error(R.drawable.normal_bg)
                .into(image_view);
        feedItems = new ArrayList<FeedItem>();

        listAdapter = new NotificationsListAdapter(context.getActivity(), feedItems);
        listView.setAdapter(listAdapter);
        // We first check for cached request
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL_FEED + "/" + id, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    private void parseJsonFeed(JSONObject response) {
        feedItems.clear();
        try {
            JSONObject feedObject = response.getJSONObject("data");
            JSONArray feedArray = feedObject.getJSONArray("post_comments");
            JSONArray feedArray_likes = feedObject.getJSONArray("post_likes");
            Log.d("sdjvkbbkvsdfbvsjk", "" + feedArray);

            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setId(feedObj.getInt("id"));
                item.setId_comment(feedObj.getString("id"));
                item.setName(feedObj.getString("name") + " " + feedObj.getString("surname"));
                item.setTimeStamp(feedObj.getString("comment"));
                item.setComment_count(feedObj.getString("comment_count"));
                item.setTime(feedObj.getString("date"));
                String profile_pic = AppUrls.BASE_IMAGE_URL + feedObj.getString("photo");
                item.setProfilePic(profile_pic);
                String image = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + feedObj.getString("images");
                item.setImge(image);

               /* // Image might be null sometimes
                String image = feedObj.isNull("jukebox_images") ? null : feedObj
                        .getString("jukebox_images");
                item.setImge(image);
                item.setStatus(feedObj.getString("movietype"));



                // url might be null sometimes
                String feedUrl = feedObj.isNull("still") ? null : feedObj
                        .getString("still");
                item.setUrl(feedUrl);*/
                Log.d("image", image);
                feedItems.add(item);
            }
           /* for (int i = 0; i < feedArray_likes.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray_likes.get(i);
                FeedItem item = new FeedItem();

                item.setUser_name(feedObj.getString("username"));


                feedItems.add(item);
            }*/

            // notify data changes to list adapater
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(context.getActivity().getColor(color), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    private void postComment(final String post_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    commenttext.setText("");
                                    Cache cache = AppController.getInstance().getRequestQueue().getCache();
                                    Cache.Entry entry = cache.get(URL_FEED);
                                    if (entry != null) {
                                        // fetch the data from cache
                                        try {
                                            String data = new String(entry.data, "UTF-8");
                                            try {
                                                parseJsonFeed(new JSONObject(data));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        // making fresh volley request and getting json
                                        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                                URL_FEED + "/" + post_id, null, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                VolleyLog.d(TAG, "Response: " + response.toString());
                                                if (response != null) {
                                                    parseJsonFeed(response);
                                                }
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                            }
                                        });

                                        // Adding request to volley request queue
                                        AppController.getInstance().addToRequestQueue(jsonReq);
                                    }

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                    params.put("comment", commenttext.getText().toString());
                    params.put("userfile ", "");
                    params.put("friend", "10823861019183948");
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postLikes(final String post_id, final String friend) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_LIKES;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);
                                    // ((HomeFragment) context).notifyDatas();

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10300")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                    params.put("type", like_style);
                    params.put("friend", friend);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postDisLikes(final String post_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_UNLIKES;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10300")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                   /* params.put("type", "Like");
                    params.put("friend", "10823861019183948");*/
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        context.getActivity().startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST_IMAGE);
    }

    private void cameraIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Toast.makeText(ProfileActivity.this, "GRANTED", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        ContentValues values = new ContentValues(1);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
        fileUri = context.getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        context.getActivity().startActivityForResult(intent, CAMERA_REQUEST_IMAGE);
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(context.getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        context.getActivity().finish();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(context.getActivity(),
                                new String[]{android.Manifest.permission.CAMERA},
                                CAMERA_REQUEST_IMAGE);
                    }
                });
        alertDialog.show();
    }

    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(context.getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(context.getActivity());
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {
                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_REQUEST_IMAGE) {
                try {
                    onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void onCaptureImageResult(Intent data) throws IOException {


        selectedFilePath = FilePath.getPath(context.getActivity(), fileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                correctBmp = getResizedBitmap(correctBmp, 200);
                select_image.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();


            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }


    }

    private void onSelectFromGalleryResult(Intent data) throws IOException {
        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(context.getActivity(), selectedFileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {
            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                correctBmp = getResizedBitmap(correctBmp, 200);
                select_image.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();


            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void postDelete(final String post_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_DELETE;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    // params.put("username", user_name);
                   /* params.put("type", "Like");
                    params.put("friend", "10823861019183948");*/
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postHide(final String post_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_HIDE;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                    params.put("type", "post");
                    //  params.put("friend", "10823861019183948");*/
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void reportPost(final String post_id, final String edit_text_text) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_HIDE;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                    params.put("comment", edit_text_text);
                    //  params.put("friend", "10823861019183948");*/
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void editPost(final String post_id, final String edit_text_text, final String name) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_EDIT;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                    params.put("post_old", name);
                    params.put("post_new", edit_text_text);
                    params.put("page_owner", user_name);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postUnHide(final String post_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_UNHIDE;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    ((GroupDiscussionFragment) context).homeFeedItem(defaultPageNo);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("username", user_name);
                    params.put("type", "post");
                    //  params.put("friend", "10823861019183948");*/
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
}
