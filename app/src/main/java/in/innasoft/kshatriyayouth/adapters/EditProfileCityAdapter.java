package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.EditProfileActivity;
import in.innasoft.kshatriyayouth.filters.EditProfileCustomFilterForCityList;
import in.innasoft.kshatriyayouth.holders.EditProfileCityHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.EditProfileCityItemClickListener;
import in.innasoft.kshatriyayouth.models.CityModel;


public class EditProfileCityAdapter extends RecyclerView.Adapter<EditProfileCityHolder> implements Filterable {
    public ArrayList<CityModel> cityModels, filterList;
    public EditProfileActivity context;
    EditProfileCustomFilterForCityList filter;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public EditProfileCityAdapter(ArrayList<CityModel> cityModels, EditProfileActivity context, int resource) {
        this.cityModels = cityModels;
        this.context = context;
        this.resource = resource;
        this.filterList = cityModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new EditProfileCustomFilterForCityList(filterList, this);
        }

        return filter;
    }

    @Override
    public EditProfileCityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        EditProfileCityHolder slh = new EditProfileCityHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(EditProfileCityHolder holder, final int position) {

//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

//        holder.city_name_txt.setTypeface(typeface);
        holder.city_name_txt.setText(Html.fromHtml(cityModels.get(position).getName()));

        holder.setItemClickListener(new EditProfileCityItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setCityName(cityModels.get(pos).getName(), cityModels.get(pos).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.cityModels.size();
    }
}
