package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.EditProfileActivity;
import in.innasoft.kshatriyayouth.filters.EditProfileCustomFilterForStateList;
import in.innasoft.kshatriyayouth.holders.EditProfileStateHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.EditProfileStateItemClickListener;
import in.innasoft.kshatriyayouth.models.StateModel;


public class EditProfileStateAdapter extends RecyclerView.Adapter<EditProfileStateHolder> implements Filterable {
    public ArrayList<StateModel> stateModels, filterList;
    public EditProfileActivity context;
    EditProfileCustomFilterForStateList filter;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public EditProfileStateAdapter(ArrayList<StateModel> stateModels, EditProfileActivity context, int resource) {
        this.stateModels = stateModels;
        this.context = context;
        this.resource = resource;
        this.filterList = stateModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new EditProfileCustomFilterForStateList(filterList, this);
        }

        return filter;
    }

    @Override
    public EditProfileStateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        EditProfileStateHolder slh = new EditProfileStateHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(EditProfileStateHolder holder, final int position) {

//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

//        holder.state_name_txt.setTypeface(typeface);
        holder.state_name_txt.setText(Html.fromHtml(stateModels.get(position).getName()));

        holder.setItemClickListener(new EditProfileStateItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setStateName(stateModels.get(pos).getName(), stateModels.get(pos).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.stateModels.size();
    }
}
