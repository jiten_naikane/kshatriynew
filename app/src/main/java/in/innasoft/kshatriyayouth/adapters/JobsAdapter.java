package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.JobsDetailActivity;
import in.innasoft.kshatriyayouth.activities.ProfileJobsActivity;
import in.innasoft.kshatriyayouth.holders.JobsHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.JobsModel;


public class JobsAdapter extends RecyclerView.Adapter<JobsHolder> {
    public ArrayList<JobsModel> feeditem;
    public ProfileJobsActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public JobsAdapter(ArrayList<JobsModel> feeditem, ProfileJobsActivity context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public JobsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        JobsHolder slh = new JobsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final JobsHolder holder, final int position) {
        String title = firstLetterCaps(feeditem.get(position).getName());
        holder.job_title_text.setText(Html.fromHtml(title));
        holder.job_title_text.setTypeface(typeface);

        holder.job_experience_text.setText(Html.fromHtml(feeditem.get(position).getExperience()));
        holder.job_experience_text.setTypeface(typeface);

        holder.job_skills_text.setText(Html.fromHtml(feeditem.get(position).getSkills()));
        holder.job_skills_text.setTypeface(typeface);

        String postedby = feeditem.get(position).getAssociations_name();
        if (postedby.equals("null") || postedby.equals("")) {
            holder.postedby_text.setVisibility(View.GONE);
        }

        holder.postedby_text.setText(Html.fromHtml(postedby));
        holder.postedby_text.setTypeface(typeface);
        holder.postedby_title.setTypeface(typeface);

        holder.job_salary_text.setText(Html.fromHtml(feeditem.get(position).getSalary()));
        holder.job_salary_text.setTypeface(typeface);

        String job_created_time = feeditem.get(position).getCreate_date_time();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dispTime = "";

        try {

            Date oldDate = dateFormat.parse(job_created_time);
            System.out.println(oldDate);

            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.before(currentDate)) {

                Log.e("oldDate", "is previous date");
                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                        + " hours: " + hours + " days: " + days);

                if (days == 0) {
                    if (hours == 0) {
                        if (minutes == 0) {
                            dispTime = seconds + " sec ago";
                        } else {
                            dispTime = minutes + " Mins ago";
                        }
                    } else {
                        dispTime = hours + " Hours ago";
                    }
                } else {
                    dispTime = days + " Days ago";
                }
            }

        } catch (ParseException e) {

            e.printStackTrace();
        }
        //holder.createddateandtime_text.setText(dispTime);
        holder.createddateandtime_text.setText(feeditem.get(position).getCreate_date_time());
        holder.createddateandtime_text.setTypeface(typeface);

        Picasso.with(context)
                .load(feeditem.get(position).getImage())
                .placeholder(R.drawable.no_images_found)
                .into(holder.job_image_iv);

        Log.d("ahsdbvjchbsd", feeditem.get(position).getImage());
        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent job = new Intent(context, JobsDetailActivity.class);
                job.putExtra("JOB_ID", feeditem.get(position).getId());
                job.putExtra("JOBNAME", feeditem.get(position).getName());

                context.startActivity(job);

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }


}
