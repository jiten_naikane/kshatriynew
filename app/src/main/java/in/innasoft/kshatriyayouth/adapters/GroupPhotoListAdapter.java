package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.GroupMemberImagesActivity;
import in.innasoft.kshatriyayouth.fragments.GroupPhotosFragment;
import in.innasoft.kshatriyayouth.holders.GroupPhotoListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupPhotoListItemClickListener;
import in.innasoft.kshatriyayouth.models.GroupPhotoListModel;


public class GroupPhotoListAdapter extends RecyclerView.Adapter<GroupPhotoListHolder> {
    public ArrayList<GroupPhotoListModel> gphotoListItem;
    public GroupPhotosFragment context;
    LayoutInflater li;
    int resource;
    //    Typeface typeface;
    ArrayList<String> imgArray = new ArrayList<String>();

    public GroupPhotoListAdapter(ArrayList<GroupPhotoListModel> gphotoListItem, GroupPhotosFragment context, int resource) {
        this.gphotoListItem = gphotoListItem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

    }

    @Override
    public GroupPhotoListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        GroupPhotoListHolder slh = new GroupPhotoListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GroupPhotoListHolder holder, final int position) {

        Picasso.with(context.getActivity())
                .load(gphotoListItem.get(position).getPhoto_link())
                .placeholder(R.drawable.user_profile)
                .into(holder.photo_image_iv);
        Log.d("ahsdbvjchbsd", gphotoListItem.get(position).getPhoto_link());


        holder.setItemClickListener(new GroupPhotoListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                imgArray.clear();
                for (int i = 0; i < gphotoListItem.size(); i++) {
                    imgArray.add(gphotoListItem.get(i).getPhoto_link());
                }
                Log.d("ARRYALISTPRINTTT", imgArray.toString());
                Intent intent = new Intent(context.getActivity(), GroupMemberImagesActivity.class);
                intent.putExtra("gphoto_list", gphotoListItem.get(pos));
                intent.putExtra("modelsize", gphotoListItem.size() + "");
                intent.putExtra("position", pos + "");
                intent.putStringArrayListExtra("image_arraylist", imgArray);
                // Toast.makeText(context.getActivity(), "Hi "+imgArray.size(), Toast.LENGTH_SHORT).show();
                context.startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {
        return this.gphotoListItem.size();

    }


}
