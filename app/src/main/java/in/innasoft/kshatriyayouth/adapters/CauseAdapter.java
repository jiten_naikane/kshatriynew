package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.CauseDetailActivity;
import in.innasoft.kshatriyayouth.fragments.Cause;
import in.innasoft.kshatriyayouth.holders.CauseHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.CauseModel;


public class CauseAdapter extends RecyclerView.Adapter<CauseHolder> {
    public ArrayList<CauseModel> feeditem;
    public Cause context;
    LayoutInflater li;
    int resource;

    //     Typeface typeface;
    public CauseAdapter(ArrayList<CauseModel> feeditem, Cause context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public CauseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        CauseHolder slh = new CauseHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CauseHolder holder, final int position) {


        holder.description.setText(Html.fromHtml(feeditem.get(position).getDescription()));
//        holder.description.setTypeface(typeface);

        holder.createddataandtime.setText(Html.fromHtml(feeditem.get(position).getCreate_date_time()));
//        holder.createddataandtime.setTypeface(typeface);

        holder.charity_name.setText(Html.fromHtml(feeditem.get(position).getName()));
//        holder.charity_name.setTypeface(typeface);

        holder.donation_amount.setText(Html.fromHtml(feeditem.get(position).getAmount()));
//        holder.donation_amount.setTypeface(typeface);

        holder.location.setText(Html.fromHtml(feeditem.get(position).getLocation()));
//        holder.location.setTypeface(typeface);

//        holder.postedby.setTypeface(typeface);

        if (feeditem.get(position).getAssociations_name().equals(null) || feeditem.get(position).getAssociations_name().equals("null") ||
                feeditem.get(position).getAssociations_name().equals("")) {
            holder.textView2.setVisibility(View.GONE);
            holder.postedby.setVisibility(View.GONE);
            holder.dot_txt.setVisibility(View.GONE);
        } else {
            holder.postedby.setText(Html.fromHtml(feeditem.get(position).getAssociations_name()));
        }

        Picasso.with(context.getActivity())
                .load(feeditem.get(position).getImage())
                .into(holder.image);
        Log.d("ahsdbvjchbsd", feeditem.get(position).getImage());
        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent allAssoction = new Intent(context.getActivity(), CauseDetailActivity.class);
                allAssoction.putExtra("CAUSE_ID", feeditem.get(pos).getId());
                allAssoction.putExtra("CAUSE_NAME", feeditem.get(pos).getName());
                allAssoction.putExtra("CAUSE_IMAGE", feeditem.get(pos).getImage());
                Log.d("CAUSEIDDD+IMAGE:", feeditem.get(pos).getId() + ":::" + feeditem.get(pos).getImage() + "::" + feeditem.get(pos).getImage());
                context.startActivity(allAssoction);


            }
        });

    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

}
