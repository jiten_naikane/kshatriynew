package in.innasoft.kshatriyayouth.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.filters.CustomFilterForSuggestGroup;
import in.innasoft.kshatriyayouth.fragments.SuggestGroupFragment;
import in.innasoft.kshatriyayouth.holders.SuggestGroupHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.SuggestGroupListItemClickListener;
import in.innasoft.kshatriyayouth.models.SuggestGroupModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class SuggestGroupListAdapter extends RecyclerView.Adapter<SuggestGroupHolder> implements Filterable {
    public ArrayList<SuggestGroupModel> suggestgroupitem, filterList;
    public SuggestGroupFragment context;
    LayoutInflater li;
    CustomFilterForSuggestGroup filter;
    ProgressDialog progressDialog;
    int resource;
    private boolean checkInternet;
    //    Typeface typeface;
    UserSessionManager session;
    String userid = "", user_profile_id = "";
    String group_status, delete_group_id;
    int defaultPageNo = 1;


    public SuggestGroupListAdapter(ArrayList<SuggestGroupModel> suggestgroupitem, SuggestGroupFragment context, int resource) {
        this.suggestgroupitem = suggestgroupitem;
        this.filterList = suggestgroupitem;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
    }

    @Override
    public SuggestGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SuggestGroupHolder slh = new SuggestGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final SuggestGroupHolder holder, final int position) {

        progressDialog = new ProgressDialog(context.getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        String groupnmame = suggestgroupitem.get(position).getGroup_name().substring(0, 1).toUpperCase() + suggestgroupitem.get(position).getGroup_name().substring(1);
        holder.suggest_group_name.setText(Html.fromHtml(groupnmame));
//        holder.suggest_group_name.setTypeface(typeface);


        holder.suggest_group_members_count.setText(Html.fromHtml(suggestgroupitem.get(position).getGroup_count()) + "  members");
//        holder.suggest_group_members_count.setTypeface(typeface);

        Picasso.with(context.getActivity())
                .load(suggestgroupitem.get(position).getGroup_picture())
                .placeholder(R.drawable.user_profile)
                .into(holder.suggest_group_list_img_view);

        group_status = suggestgroupitem.get(position).getGroup_status();
        if (group_status.equals("0")) {
            holder.suggesst_join_button.setText("Join");
            holder.suggesst_join_button.setVisibility(View.VISIBLE);
            holder.textDelete.setVisibility(View.GONE);
            holder.suggesst_join_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String group_id = suggestgroupitem.get(position).getGroup_id();
                    String group_manager_id = suggestgroupitem.get(position).getGroup_manager();

                    join(group_id, group_manager_id); //JOIN GROUP BUTTON METHOD

                }
            });

        } else {
            holder.suggesst_join_button.setVisibility(View.VISIBLE);
//            holder.suggesst_join_button.setTypeface(typeface);
            holder.suggesst_join_button.setText("Request sent");
            holder.textDelete.setVisibility(View.VISIBLE);
            holder.suggesst_join_button.setClickable(false);

        }


//        holder.textDelete.setTypeface(typeface);
        holder.textDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete_group_id = suggestgroupitem.get(position).getGroup_id();
                deleterequest(delete_group_id);   ////DELETE GROUP BUTTON METHOD
            }
        });


        holder.setItemClickListener(new SuggestGroupListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }


    @Override
    public int getItemCount() {
        return this.suggestgroupitem.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForSuggestGroup(filterList, this);
        }

        return filter;
    }

    public void join(final String group_id, final String group_manager_id) {
        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String urldelete = AppUrls.BASE_URL + AppUrls.JOIN_GROUP;
            Log.d("joinrequesturl:", urldelete);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.JOIN_GROUP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("joinrequestresp", response);
                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equalsIgnoreCase("10100")) {
                                    suggestgroupitem.clear();
                                    Toast.makeText(context.getActivity(), "Request Sent Successfully..!", Toast.LENGTH_SHORT).show();
                                    ((SuggestGroupFragment) context).getAllSuggestedGroup(defaultPageNo);

                                    progressDialog.dismiss();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context.getActivity(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("group_id", group_id);
                    params.put("group_manager", group_manager_id);
                    params.put("username", user_profile_id);

                    Log.d("joinrequestPARAM:", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleterequest(final String delete_group_id) {
        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String urldelete = AppUrls.BASE_URL + AppUrls.JOIN_GROUP;
            Log.d("deleterequesturl:", urldelete);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.DELETE_JOIN_GROUP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("deleterequestresp", response);
                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equalsIgnoreCase("10100")) {
                                    suggestgroupitem.clear();
                                    Toast.makeText(context.getActivity(), "Request Deleted Succesfully..!", Toast.LENGTH_SHORT).show();
                                    ((SuggestGroupFragment) context).getAllSuggestedGroup(defaultPageNo);


                                    progressDialog.dismiss();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context.getActivity(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("group_id", delete_group_id);
                    params.put("username", user_profile_id);

                    Log.d("deleterequestPARAM:", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }
}
