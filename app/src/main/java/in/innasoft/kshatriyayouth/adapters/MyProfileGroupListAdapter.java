package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.GroupDetailActivity;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.filters.CustomFilterforGroupList;
import in.innasoft.kshatriyayouth.holders.GroupListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupListItemClickListener;
import in.innasoft.kshatriyayouth.models.GroupListModel;


public class MyProfileGroupListAdapter extends RecyclerView.Adapter<GroupListHolder> {
    public ArrayList<GroupListModel> groupListItem, filterList;
    public MyProfileActivity context;
    CustomFilterforGroupList filter;
    LayoutInflater li;
    int resource;

//    Typeface typeface;

    public MyProfileGroupListAdapter(ArrayList<GroupListModel> groupListItem, MyProfileActivity context, int resource) {
        this.groupListItem = groupListItem;
        this.context = context;
        this.resource = resource;
        this.filterList = groupListItem;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public GroupListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        GroupListHolder slh = new GroupListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GroupListHolder holder, final int position) {


        String pack_name = groupListItem.get(position).getGroup_name().substring(0, 1).toUpperCase() + groupListItem.get(position).getGroup_name().substring(1);
        holder.group_name.setText(Html.fromHtml(pack_name));
//        holder.group_name.setTypeface(typeface);

        holder.members_count.setText(Html.fromHtml(groupListItem.get(position).getGroup_count() + "  members"));
//        holder.members_count.setTypeface(typeface);


        Picasso.with(context)
                .load(groupListItem.get(position).getGroup_picture())
                .placeholder(R.drawable.no_images_found)
                .into(holder.group_list_img_view);
        Log.d("ahsdbvjchbsd", groupListItem.get(position).getGroup_picture());

//        holder.button_join_group.setTypeface(typeface);

        holder.button_join_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        holder.setItemClickListener(new GroupListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent groupdetail = new Intent(context, GroupDetailActivity.class);
                groupdetail.putExtra("GROUP_ID", groupListItem.get(position).getGroup_id());
                groupdetail.putExtra("GROUP_MANAGER", groupListItem.get(position).getGroup_manager());
                groupdetail.putExtra("GROUP_NAME", groupListItem.get(position).getGroup_name());
                groupdetail.putExtra("GROUP_IMAGE", groupListItem.get(position).getGroup_picture());
                groupdetail.putExtra("GROUP_TYPE", groupListItem.get(position).getGroup_type());
                context.startActivity(groupdetail);

            }
        });

    }

    @Override
    public int getItemCount() {
        // Log.d("sdfaadsfdsf",groupListItem.toString());
        return this.groupListItem.size();

    }


}
