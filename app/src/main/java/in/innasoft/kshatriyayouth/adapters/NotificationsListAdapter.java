package in.innasoft.kshatriyayouth.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.CommentsActivity;
import in.innasoft.kshatriyayouth.activities.EditPostActivity;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.activities.SubCommentsActivity;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

import static in.innasoft.kshatriyayouth.app.AppController.TAG;


public class NotificationsListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private List<FeedItem> feedItems1;
    //    Typeface typeface;
    ImageView image_view;
    private Sub_NotificationsListAdapter listAdapter;
    BottomSheetDialog bottomSheetDialog;
    private ListView listView;
    EditText commenttext;
    ImageView send_image, select_image;
    String profile_pic_url = "";
    String user_name = "";
    UserSessionManager sessionManager;
    TextView like;
    String user_profile_id = "";
    private boolean checkInternet;
    private String URL_FEED = AppUrls.BASE_URL + AppUrls.GROUP_POST_COMMENT_LIKECOMMENT;

    public NotificationsListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
//		typeface = Typeface.createFromAsset(activity.getAssets(), activity.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        sessionManager = new UserSessionManager(activity);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        user_name = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        Log.d("dfjkdfbndfjkb", "hhghgh" + profile_pic_url);
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.main_notifications_list_item, null);


        feedItems1 = new ArrayList<FeedItem>();
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView time = (TextView) convertView
                .findViewById(R.id.time);
        TextView viewreplys = (TextView) convertView
                .findViewById(R.id.viewreplys);

        like = (TextView) convertView
                .findViewById(R.id.like);

        TextView reply = (TextView) convertView
                .findViewById(R.id.reply);

        TextView edited = (TextView) convertView
                .findViewById(R.id.edited);

        ImageView profilePic = (ImageView) convertView
                .findViewById(R.id.profilePic);
        ImageView comment_image = (ImageView) convertView
                .findViewById(R.id.comment_image);
        /*listView = (NonScrollListView) convertView. findViewById(R.id.list);
		feedItems1 = new ArrayList<FeedItem>();
		listAdapter = new SubNotificationsListAdapter(activity, feedItems1);
		listView.setAdapter(listAdapter);
		Cache cache = AppController.getInstance().getRequestQueue().getCache();
		Cache.Entry entry = cache.get(URL_FEED);
		if (entry != null) {
			// fetch the data from cache
			try {
				String data = new String(entry.data, "UTF-8");
				try {
					parseJsonFeed(new JSONObject(data));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		} else {
			// making fresh volley request and getting json
			JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
					URL_FEED+"/"+"426", null, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					VolleyLog.d(TAG, "Response: " + response.toString());
					if (response != null) {
						parseJsonFeed(response);
					}
				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					VolleyLog.d(TAG, "Error: " + error.getMessage());
				}
			});

			// Adding request to volley request queue
			AppController.getInstance().addToRequestQueue(jsonReq);
		}*/
        final FeedItem item = feedItems.get(position);
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MyProfileActivity.class);
                intent.putExtra("user_id", item.getUsername());
                activity.startActivity(intent);
            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MyProfileActivity.class);
                intent.putExtra("user_id", item.getUsername());
                activity.startActivity(intent);
            }
        });
        if (!item.getComment_edit_count().equals("0")) {
            edited.setVisibility(View.VISIBLE);
            edited.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, EditPostActivity.class);
                    String ids = ((Activity) activity).getIntent().getExtras().getString("id");
                    intent.putExtra("id", ids);
                    intent.putExtra("sub_id", feedItems.get(position).getId_comment());
                    intent.putExtra("type", "comment");
                    activity.startActivity(intent);
                }
            });
        } else {
            edited.setVisibility(View.GONE);
        }
        name.setText(item.getName());
        time.setText(item.getTime());
        //user_profile_id = item.getUser_name();
//		name.setTypeface(typeface);
        if (!item.getProfilePic().equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(activity)
                    .load(item.getProfilePic())
                    .into(profilePic);
        }
        if (!item.getImge().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {
            comment_image.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(item.getImge())
                    .into(comment_image);
        } else {
            comment_image.setVisibility(View.GONE);
        }
        // Converting timestamp into x ago format
	/*	CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
				Long.parseLong(item.getTimeStamp()),
				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);*/
        timestamp.setText(item.getTimeStamp());
//		timestamp.setTypeface(typeface);
        if (!item.getComment_count().equals("0")) {
            viewreplys.setVisibility(View.VISIBLE);

        } else {
            viewreplys.setVisibility(View.GONE);
        }
        viewreplys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String id = feedItems.get(position).getId_comment();
                    Log.d("sdhbvhsduiv", id);
                    Intent intent = new Intent(activity, SubCommentsActivity.class);
                    intent.putExtra("id", id);
                    activity.startActivity(intent);
                    //showBottomSheet(id);
                } catch (Exception e) {

                }

            }
        });
        if (feedItems.get(position).getUser_like_status().equals("0")) {
            like.setTextColor(Color.BLACK);
            like.setText("Like");
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like.setTextColor(Color.BLUE);
                    String id = feedItems.get(position).getId_comment();
                    String username = feedItems.get(position).getUsername();
                    postLikes(id, username);
                }
            });
        } else {
            like.setTextColor(Color.BLUE);
            like.setText("Liked");
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    like.setTextColor(Color.BLACK);

                    String id = feedItems.get(position).getId_comment();
                    String username = feedItems.get(position).getUsername();
                    postDisLikes(id);
                }
            });
        }


        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String id = feedItems.get(position).getId_comment();
                    String friends = feedItems.get(position).getUsername();
                    Intent intent = new Intent(activity, SubCommentsActivity.class);
                    intent.putExtra("id", id);
                    intent.putExtra("friends", friends);
                    activity.startActivity(intent);
                    //showBottomSheet(id);
                } catch (Exception e) {

                }

            }
        });


        return convertView;
    }

    private void parseJsonFeed(JSONObject response) {
        try {
            JSONObject feedObject = response.getJSONObject("data");
            JSONArray feedArray = feedObject.getJSONArray("comments_list");
            Log.d("sdfvgjbdfjkbg", "" + feedObject);
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setId_comment(feedObj.getString("id"));
                item.setName(feedObj.getString("name") + " " + feedObj.getString("surname"));
                item.setTimeStamp(feedObj.getString("reply"));
                String profile_pic = AppUrls.BASE_IMAGE_URL + feedObj.getString("photo");
                item.setProfilePic(profile_pic);
                item.setTime(feedObj.getString("date"));

                String image = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + feedObj.getString("images");
                item.setImge(image);


                feedItems1.add(item);
            }

            // notify data changes to list adapater
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showBottomSheet(final String id) {

        bottomSheetDialog = new BottomSheetDialog(activity);

        View view = activity.getLayoutInflater().inflate(R.layout.content_dialog_bottom_sheet, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
        listView = (ListView) view.findViewById(R.id.list);
        image_view = (ImageView) view.findViewById(R.id.image_view1);
        commenttext = (EditText) view.findViewById(R.id.commenttext);
        send_image = (ImageView) view.findViewById(R.id.send_image);
        select_image = (ImageView) view.findViewById(R.id.select_image);
        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(checkInternet)
                {
                    final boolean result = ImagePermissions.checkPermission(activity);
                    final Dialog mBottomSheetDialog = new Dialog(activity, R.style.MaterialDialogSheet);
               *//* TextView dialog_title = (TextView) mBottomSheetDialog.findViewById(R.id.dialog_title);
                dialog_title.setTypeface(typeface);*//*
                    mBottomSheetDialog.setContentView(R.layout.custom_dialog_image_selection); // your custom view.
                    mBottomSheetDialog.setCancelable(true);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                    mBottomSheetDialog.show();

                    TextView take_photo_text = (TextView) mBottomSheetDialog.findViewById(R.id.take_photo_text);
                    take_photo_text.setTypeface(typeface);
                    take_photo_text.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Toast.makeText(activity,"take photo",Toast.LENGTH_LONG).show();
                            if (result)
                            {
                                if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                                {
                                    if (getFromPref(activity, ALLOW_KEY)) {
                                        showSettingsAlert();
                                    } else if (ContextCompat.checkSelfPermission(activity,
                                            android.Manifest.permission.CAMERA)

                                            != PackageManager.PERMISSION_GRANTED) {

                                        // Should we show an explanation?
                                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                                android.Manifest.permission.CAMERA)) {
                                            showAlert();
                                        } else {
                                            // No explanation needed, we can request the permission.
                                            ActivityCompat.requestPermissions(activity,
                                                    new String[]{android.Manifest.permission.CAMERA},
                                                    CAMERA_REQUEST_IMAGE);
                                        }
                                    }
                                }
                                else
                                {
                                    cameraIntent();
                                    mBottomSheetDialog.dismiss();
                                }

                            }


                        }
                    });

                    TextView select_from_gallery = (TextView) mBottomSheetDialog.findViewById(R.id.select_from_gallery);
                    select_from_gallery.setTypeface(typeface);
                    select_from_gallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            Toast.makeText(activity,"gallry",Toast.LENGTH_LONG).show();
                        *//*if (result) {

                        }*//*
                            galleryIntent();
                            mBottomSheetDialog.dismiss();


                        }
                    });

                    mBottomSheetDialog.show();
                }
                else
                {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }*/
            }
        });
        send_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!commenttext.getText().toString().equals("")) {
                    String friend = ((Activity) activity).getIntent().getExtras().getString("friend");
                    postComment(id, friend);
                } else {
                    Toast.makeText(activity, "Please write some text", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Picasso.with(activity)
                .load(profile_pic_url)
                .error(R.drawable.normal_bg)
                .into(image_view);
        feedItems = new ArrayList<FeedItem>();

        listAdapter = new Sub_NotificationsListAdapter(activity, feedItems1);
        listView.setAdapter(listAdapter);
        // We first check for cached request
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL_FEED + "/" + id, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    private void postLikes(final String post_id, final String username) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(activity);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_LIKES;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    Toast.makeText(activity, "Liked", Toast.LENGTH_SHORT).show();
                                    String ids = ((Activity) activity).getIntent().getExtras().getString("id");
                                    ((CommentsActivity) activity).getCommentsRefresh(ids, user_name);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", post_id);
                    params.put("username", user_name);
                    params.put("friend", username);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(activity, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postDisLikes(final String id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(activity);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_UNDERCOMMENT_UNLIKES;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    Toast.makeText(activity, "DisLiked", Toast.LENGTH_SHORT).show();
                                    String ids = ((Activity) activity).getIntent().getExtras().getString("id");
                                    ((CommentsActivity) activity).getCommentsRefresh(ids, user_name);


                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");


                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);


                                    }


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", id);
                    params.put("username", user_name);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(activity, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postComment(final String post_id, final String friend) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(activity);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    commenttext.setText("");
                                    listAdapter = new Sub_NotificationsListAdapter(activity, feedItems1);
                                    listView.setAdapter(listAdapter);
                                    //((FeedHomeAdapter) activity).homeFeedItem();
                                    Cache cache = AppController.getInstance().getRequestQueue().getCache();
                                    Cache.Entry entry = cache.get(URL_FEED);
                                    if (entry != null) {
                                        // fetch the data from cache
                                        try {
                                            String data = new String(entry.data, "UTF-8");
                                            try {
                                                parseJsonFeed(new JSONObject(data));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        // making fresh volley request and getting json
                                        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                                URL_FEED + "/" + post_id, null, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                VolleyLog.d(TAG, "Response: " + response.toString());
                                                if (response != null) {
                                                    parseJsonFeed(response);
                                                }
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                            }
                                        });

                                        // Adding request to volley request queue
                                        AppController.getInstance().addToRequestQueue(jsonReq);
                                    }

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", post_id);
                    params.put("username", user_name);
                    params.put("reply", commenttext.getText().toString());
                    params.put("userfile ", "");
                    params.put("friend", friend);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(activity, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

}
