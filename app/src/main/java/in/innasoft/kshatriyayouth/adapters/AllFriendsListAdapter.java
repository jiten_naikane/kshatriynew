package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.fragments.FriendsFragment;
import in.innasoft.kshatriyayouth.holders.AllFriendsHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.AllFriendsModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class AllFriendsListAdapter extends RecyclerView.Adapter<AllFriendsHolder> {
    public ArrayList<AllFriendsModel> feeditem;
    public FriendsFragment context;
    LayoutInflater li;
    int resource;
    //    Typeface typeface;
    String friend_name = "";
    private boolean checkInternet;
    UserSessionManager session;
    String userid = "", user_profile_id = "", membership = "";
    int defaultPageNo = 1;
    private long mLastClickTime = 0;

    public AllFriendsListAdapter(ArrayList<AllFriendsModel> feeditem, FriendsFragment context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        membership = userDetails.get(UserSessionManager.USER_MEMBERSHIP);
    }

    @Override
    public AllFriendsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllFriendsHolder slh = new AllFriendsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllFriendsHolder holder, final int position) {

//        holder.name.setTypeface(typeface);
        // String name = feeditem.get(position).getName().substring(0, 1).toUpperCase()+ feeditem.get(position).getName().substring(1);
        holder.name.setText(feeditem.get(position).getName());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
            }
        });
//        holder.timestamp.setTypeface(typeface);
        try {
            String timestamp = feeditem.get(position).getSurname().substring(0, 1).toUpperCase() + feeditem.get(position).getSurname().substring(1);
            holder.timestamp.setText((Html.fromHtml(timestamp)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //holder.timestamp.setText(Html.fromHtml(feeditem.get(position).getSurname()));

        //    holder.confirm.setText("Add Friend");
//        holder.confirm.setTypeface(typeface);
        // holder.delete.setText("Remove");
        Log.d("bcghcscv", feeditem.get(position).getRequeststatus() + "-------" + feeditem.get(position).getFriendstatus());
        if (feeditem.get(position).getRequeststatus().equals("null") && feeditem.get(position).getFriendstatus().equals("null") && feeditem.get(position).getSentbystatus().equals("null")) {
            holder.confirm.setText("Add Friend");
            holder.delete.setVisibility(View.INVISIBLE);
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 5000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    friend_name = feeditem.get(position).getUsername();
                    sendFriendRequest();
                }
            });
        } else if (!feeditem.get(position).getRequeststatus().equals("null") && feeditem.get(position).getFriendstatus().equals("null") && feeditem.get(position).getSentbystatus().equals("null")) {
            holder.confirm.setText("Confirm");
            holder.confirm.setBackgroundResource(R.drawable.background_for_friendslist_thick);
            holder.delete.setText("Reject");
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend_name = feeditem.get(position).getUsername();
                    accseptFriendRequest();
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend_name = feeditem.get(position).getUsername();
                    rejectFriend();
                }
            });

        } else if (feeditem.get(position).getRequeststatus().equals("null") && feeditem.get(position).getFriendstatus().equals("null") && !feeditem.get(position).getSentbystatus().equals("null")) {
            holder.confirm.setText("Cancel Request");
            holder.confirm.setTextColor(context.getResources().getColor(R.color.gray));
            holder.confirm.setBackgroundResource(R.color.white);
            holder.delete.setVisibility(View.INVISIBLE);
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelFriendRequest();
                }
            });

        } else if (feeditem.get(position).getRequeststatus().equals("null") && !feeditem.get(position).getFriendstatus().equals("null") && feeditem.get(position).getSentbystatus().equals("null")) {
            holder.confirm.setText("Unfriend");
            holder.confirm.setTextColor(context.getResources().getColor(R.color.black));
            holder.confirm.setBackgroundResource(R.drawable.background_for_friendsdelete);
            holder.delete.setVisibility(View.INVISIBLE);
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend_name = feeditem.get(position).getUsername();
                    unFriendRequest();
                }
            });
        }
//        holder.delete.setTypeface(typeface);
        Picasso.with(context.getActivity())
                .load(feeditem.get(position).getPhoto())
                .into(holder.profilePic);
        Log.d("ahsdbvjchbsd", feeditem.get(position).getPhoto());
        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
            }
        });

        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
              /*  Intent announceDetail=new Intent(context.getActivity(), ProfileAnnouncementDetailActivity.class);
                announceDetail.putExtra("DETAIL_ID",feeditem.get(position).getId());
                context.startActivity(announceDetail);*/

            }
        });

    }

    private void cancelFriendRequest() {
        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.CANCEL_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    feeditem.clear();
                                    ((FriendsFragment) context).feedItemFriendRequests();
                                    Toast.makeText(context.getActivity(), "Request has been Cancelled..!", Toast.LENGTH_SHORT).show();

                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {
                                    Toast.makeText(context.getActivity(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    params.put("friend_user_name", friend_name);
                    Log.d("CANCELPARAM::", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void sendFriendRequest() {

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ADD_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    //((FriendsFragment) context).feedItemFriendRequests(user_profile_id,defaultPageNo);
                                    ((FriendsFragment) context).feedItemFriendRequests();
                                    // context.feedItemFriendRequests();

                                    //   JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {

                                    Toast.makeText(context.getActivity(), "Request has been Sent..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void accseptFriendRequest() {

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ACCEPT_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    //((FriendsFragment) context).feedItemFriendRequests(user_profile_id,defaultPageNo);
                                    ((FriendsFragment) context).feedItemFriendRequests();
                                    // context.feedItemFriendRequests();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void unFriendRequest() {

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.UNFRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    feeditem.clear();
                                    //((FriendsFragment) context).feedItemFriendRequests(user_profile_id,defaultPageNo);
                                    ((FriendsFragment) context).feedItemFriendRequests();
                                    //  context.feedItemFriendRequests();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void rejectFriend() {

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.REJECT_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    Toast.makeText(context.getActivity(), "Request has been Rejected..!", Toast.LENGTH_SHORT).show();
                                    feeditem.clear();
                                    //((FriendsFragment) context).feedItemFriendRequests(user_profile_id,defaultPageNo);
                                    ((FriendsFragment) context).feedItemFriendRequests();
                                    //  context.feedItemFriendRequests();
                                    //  JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {
                                    Toast.makeText(context.getActivity(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

}
