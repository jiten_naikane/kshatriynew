package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.LikeMembersDisplayActivity;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.filters.CustomFilterForSearching;
import in.innasoft.kshatriyayouth.holders.SearchingHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.SearchingModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class LikeMembersDisplayAdapter extends RecyclerView.Adapter<SearchingHolder> {
    public ArrayList<SearchingModel> feeditem, filterList;
    public LikeMembersDisplayActivity context;
    LayoutInflater li;
    CustomFilterForSearching filter;
    int resource;
    //    Typeface typeface;
    UserSessionManager session;
    private boolean checkInternet;
    String user_profile_id = "", pack_name;
    String friend_name = "";


    public LikeMembersDisplayAdapter(ArrayList<SearchingModel> feeditem, LikeMembersDisplayActivity context, int resource) {
        this.feeditem = feeditem;
        this.filterList = feeditem;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
    }

    @Override
    public SearchingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SearchingHolder slh = new SearchingHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final SearchingHolder holder, final int position) {


        pack_name = feeditem.get(position).getName().substring(0, 1).toUpperCase() + feeditem.get(position).getName().substring(1);
        holder.name.setText(Html.fromHtml(pack_name));
//        holder.name.setTypeface(typeface);

        holder.confirm.setVisibility(View.VISIBLE);
        String user_id = feeditem.get(position).getUsername();
        if (user_profile_id.equals(user_id)) {
            holder.confirm.setVisibility(View.GONE);
        }
        Log.d("dfgnsdfgdf", "" + user_profile_id + "  --------  " + feeditem.get(position).getUsername());
        if (feeditem.get(position).getRequeststatus().equals("null") && feeditem.get(position).getFriendstatus().equals("null") && feeditem.get(position).getSentbystatus().equals("null")) {
//
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend_name = feeditem.get(position).getUsername();
                    sendFriendRequest();
                }
            });
        } else if (!feeditem.get(position).getRequeststatus().equals("null") && feeditem.get(position).getFriendstatus().equals("null") && feeditem.get(position).getSentbystatus().equals("null")) {
            if (user_profile_id == feeditem.get(position).getUsername()) {

                holder.confirm.setVisibility(View.GONE);
            } else {
                holder.confirm.setText("Add Friend");
            }
            //  holder.confirm.setBackgroundResource(R.drawable.background_for_friendslist_thick);
            //holder.delete.setText("Reject");
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend_name = feeditem.get(position).getUsername();
                    accseptFriendRequest();
                }
            });

//15064134292291667
        } else if (feeditem.get(position).getRequeststatus().equals("null") && feeditem.get(position).getFriendstatus().equals("null") && !feeditem.get(position).getSentbystatus().equals("null")) {
            holder.confirm.setText("Request Sent");
/*
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    //  accseptFriendRequest();
                }
            });
*/

        } else if (feeditem.get(position).getRequeststatus().equals("null") && !feeditem.get(position).getFriendstatus().equals("null") && feeditem.get(position).getSentbystatus().equals("null")) {
            holder.confirm.setText("Unfriend");
            // holder.confirm.setBackgroundResource(R.drawable.background_for_friendslist_thick);
//            holder.delete.setVisibility(View.INVISIBLE);
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend_name = feeditem.get(position).getUsername();
                    unFriendRequest();

                }
            });
        }


        Picasso.with(context)
                .load(feeditem.get(position).getPhoto())
                .placeholder(R.drawable.user_profile)
                .into(holder.profilePic);




        /*holder.delete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });*/


        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent intent = new Intent(context, MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }


    /*@Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter=new CustomFilterForSearching(filterList,this);
        }

        return filter;
    }
*/
    private void sendFriendRequest() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ADD_FRIEND_REQUEST;
            Log.d("SENDFRENDREQURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SENDFRENDREQRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    ((LikeMembersDisplayActivity) context).feedItem();

                                    //   JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    //   Toast.makeText(context,""+jsonObject1,Toast.LENGTH_LONG).show();
                                }

                                if (responceCode.equals("10200")) {

                                    Toast.makeText(context, "Try Again..", Toast.LENGTH_LONG).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("SENDFRENDREQPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void accseptFriendRequest() {

        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ACCEPT_FRIEND_REQUEST;
            Log.d("ACCEPTURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACCEPTEDRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    //((FriendsFragment) context).feedItemFriendRequests();
                                    ((LikeMembersDisplayActivity) context).feedItem();
                                    //  JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("ACCEPTPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void rejectFriend() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.REJECT_FRIEND_REQUEST;
            Log.d("REJECTEDURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("REJECTEDRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    // ((FriendsFragment) context).feedItemFriendRequests();
                                    ((LikeMembersDisplayActivity) context).feedItem();
                                    //   JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("REJECTPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void unFriendRequest() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.UNFRIEND_REQUEST;
            Log.d("UNFREINDURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("UNFREINDRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    ((LikeMembersDisplayActivity) context).feedItem();

                                    // JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    /*  JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                       }
*/
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", friend_name);
                    Log.d("UNFREINDPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


}
