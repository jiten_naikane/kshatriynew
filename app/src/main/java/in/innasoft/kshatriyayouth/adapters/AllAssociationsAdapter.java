package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.AssociationDetailActivity;
import in.innasoft.kshatriyayouth.fragments.AllAssociations;
import in.innasoft.kshatriyayouth.holders.AllAssociationsHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.AllAssociationsModel;


public class AllAssociationsAdapter extends RecyclerView.Adapter<AllAssociationsHolder> {
    public ArrayList<AllAssociationsModel> feeditem;
    public AllAssociations context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public AllAssociationsAdapter(ArrayList<AllAssociationsModel> feeditem, AllAssociations context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

    }

    @Override
    public AllAssociationsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllAssociationsHolder slh = new AllAssociationsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllAssociationsHolder holder, final int position) {

        holder.title.setText(Html.fromHtml(feeditem.get(position).getName()));
//        holder.title.setTypeface(typeface);

        holder.description.setText(Html.fromHtml(feeditem.get(position).getDescription()));
//        holder.description.setTypeface(typeface);

        Picasso.with(context.getActivity())
                .load(feeditem.get(position).getImage())
                .placeholder(R.drawable.no_images_found)
                .into(holder.image);
        Log.d("ahsdbvjchbsd", feeditem.get(position).getImage());


        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent allAssoction = new Intent(context.getActivity(), AssociationDetailActivity.class);
                allAssoction.putExtra("ASSOCIATION_ID", feeditem.get(pos).getId());
                allAssoction.putExtra("ASSOCIATION_NAME", feeditem.get(pos).getName());
                allAssoction.putExtra("ASSOCIATION_IMAGE", feeditem.get(pos).getImage());
                Log.d("IDDD+IMAGE:", feeditem.get(pos).getId() + ":::" + feeditem.get(pos).getImage() + "::" + feeditem.get(pos).getImage());
                context.startActivity(allAssoction);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

}
