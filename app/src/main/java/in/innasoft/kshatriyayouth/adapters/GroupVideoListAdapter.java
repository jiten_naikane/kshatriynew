package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.GroupVideoPostActivity;
import in.innasoft.kshatriyayouth.fragments.GroupVideosFragment;
import in.innasoft.kshatriyayouth.holders.GroupVideoListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupVideoListItemClickListener;
import in.innasoft.kshatriyayouth.models.GroupVideoListModel;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;


public class GroupVideoListAdapter extends RecyclerView.Adapter<GroupVideoListHolder> {
    public ArrayList<GroupVideoListModel> gvideoListItem;
    public GroupVideosFragment context;
    LayoutInflater li;
    int resource;
    //    Typeface typeface;
    private boolean checkInternet;

    public GroupVideoListAdapter(ArrayList<GroupVideoListModel> gvideoListItem, GroupVideosFragment context, int resource) {
        this.gvideoListItem = gvideoListItem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

    }

    @Override
    public GroupVideoListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        GroupVideoListHolder slh = new GroupVideoListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GroupVideoListHolder holder, final int position) {

        // String surname =firstLetterCaps(gmemberListItem.get(position).getSurname());

//        holder.video_title.setTypeface(typeface);
        holder.video_title.setText(Html.fromHtml(gvideoListItem.get(position).getTitle()));

        String video_url = gvideoListItem.get(position).getUrl();
        Log.d("URLLLLLL:", video_url);

        Picasso.with(context.getActivity())
                .load(gvideoListItem.get(position).getUrl())
                .placeholder(R.drawable.thumbnail_default)
                .into(holder.img_defult_videothumbnail);

        holder.setItemClickListener(new GroupVideoListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                checkInternet = NetworkChecking.isConnected(context.getActivity());
                if (checkInternet) {

                    String videopostUrl = gvideoListItem.get(pos).getUrl();

                    if (videopostUrl.equals("notfound") || videopostUrl.equals("null")) {
                        Snackbar snackbar = Snackbar
                                .make(context.getView(), "Currently not available..!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        Intent intent = new Intent(context.getActivity(), GroupVideoPostActivity.class);
                        String video[] = videopostUrl.split("=");
                        String video_id = video[1];
                        intent.putExtra("Video_Url", video_id);
                        context.startActivity(intent);

                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(context.getView(), "Please check your Internet connection....!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

               /* Intent i_video=new Intent(context.getActivity(),GroupVideoPostActivity.class);
                String video[] = jukeboxUrl.split("=");
                String video_id = video[1];
                i_video.putExtra("Video_Url",gvideoListItem.get(position).getUrl());
                context.startActivity(i_video);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.gvideoListItem.size();

    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }


}
