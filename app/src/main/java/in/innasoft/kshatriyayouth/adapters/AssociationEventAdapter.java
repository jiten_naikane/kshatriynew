package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.fragments.AssociationEventFragment;
import in.innasoft.kshatriyayouth.holders.AssociationEventHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.AssociationEventItemClcikListener;
import in.innasoft.kshatriyayouth.models.Association_EventModel;

/**
 * Created by Devolper on 22-Aug-17.
 */


public class AssociationEventAdapter extends RecyclerView.Adapter<AssociationEventHolder> {

    public ArrayList<Association_EventModel> asseventlistModels;
    AssociationEventFragment context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public AssociationEventAdapter(ArrayList<Association_EventModel> asseventlistModels, AssociationEventFragment context, int resource) {
        this.asseventlistModels = asseventlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
    }

    @Override
    public AssociationEventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AssociationEventHolder slh = new AssociationEventHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AssociationEventHolder holder, final int position) {


//        holder.asso_event_title.setTypeface(typeface);
        String asso_event_title = asseventlistModels.get(position).getName();//.substring(0, 1).toUpperCase() ;
        holder.asso_event_title.setText(Html.fromHtml(asso_event_title));

//  holder.description.setText(Html.fromHtml(feeditem.get(position).getDescription()));

//        holder.asso_event_descrption.setTypeface(typeface);
        String asso_event_descrption = asseventlistModels.get(position).getDescription();//.substring(0, 1).toUpperCase() ;
        holder.asso_event_descrption.setText(Html.fromHtml(asso_event_descrption));

//        holder.asso_event_venue.setTypeface(typeface);
        String asso_event_venue = asseventlistModels.get(position).getVenue();//.substring(0, 1).toUpperCase() ;
        holder.asso_event_venue.setText(Html.fromHtml(asso_event_venue));

//        holder.asso_event_start_date.setTypeface(typeface);
        String asso_event_start_date = asseventlistModels.get(position).getStart_date();//.substring(0, 1).toUpperCase() ;
        holder.asso_event_start_date.setText(Html.fromHtml(asso_event_start_date));

//        holder.asso_event_end_date.setTypeface(typeface);
        String asso_event_end_date = asseventlistModels.get(position).getEnd_date();//.substring(0, 1).toUpperCase() ;
        holder.asso_event_end_date.setText(Html.fromHtml(asso_event_end_date));


        Picasso.with(context.getActivity())
                .load(asseventlistModels.get(position).getImage())
                .placeholder(R.drawable.app_logo_new)
                .into(holder.asso_event_img);

        holder.setItemClickListener(new AssociationEventItemClcikListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });


    }

    @Override
    public int getItemCount() {
        //notifyDataSetChanged();
        return this.asseventlistModels.size();
    }


}

