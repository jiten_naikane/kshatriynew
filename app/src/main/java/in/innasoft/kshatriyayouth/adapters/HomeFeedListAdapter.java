package in.innasoft.kshatriyayouth.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialog;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.utilities.FeedImageView;
import in.innasoft.kshatriyayouth.utilities.NonScrollListView;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

import static in.innasoft.kshatriyayouth.app.AppController.TAG;


public class HomeFeedListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    BottomSheetDialog bottomSheetDialog;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    int num = 1;
    String profile_pic_url = "";
//    Typeface typeface;

    UserSessionManager sessionManager;
    ///////////////////////////////////////////////////
    private NonScrollListView listView;
    private NotificationsListAdapter listAdapter;
    ImageView image_view;
    private String URL_FEED = "http://learningslot.in/srikanth/movielist";

    ///////////////////////////////////////////////////

    public HomeFeedListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
        sessionManager = new UserSessionManager(activity);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        Log.d("dfjkdfbndfjkb", "hhghgh" + profile_pic_url);
//		typeface = Typeface.createFromAsset(activity.getAssets(), activity.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.feed_item, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
/////////////////////////////////

        ///////////////////
        TextView name = (TextView) convertView.findViewById(R.id.name);
        ImageView post_hideorvisible = (ImageView) convertView.findViewById(R.id.post_hideorvisible);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) convertView
                .findViewById(R.id.txtStatusMsg);
        RelativeLayout chatlayout = (RelativeLayout) convertView
                .findViewById(R.id.chatlayout);
        RelativeLayout sharelayout = (RelativeLayout) convertView
                .findViewById(R.id.sharelayout);
        TextView url = (TextView) convertView.findViewById(R.id.txtUrl);
        NetworkImageView profilePic = (NetworkImageView) convertView
                .findViewById(R.id.profilePic);
        FeedImageView feedImageView = (FeedImageView) convertView
                .findViewById(R.id.feedImage1);

        FeedItem item = feedItems.get(position);

        name.setText(item.getName());
//        name.setTypeface(typeface);

        // Converting timestamp into x ago format
    /*	CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
				Long.parseLong(item.getTimeStamp()),
				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);*/
        timestamp.setText(item.getTimeStamp());
//		timestamp.setTypeface(typeface);
        // Chcek for empty status message
        if (!TextUtils.isEmpty(item.getStatus())) {
            statusMsg.setText(item.getStatus());
//			statusMsg.setTypeface(typeface);
            statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }

        // Checking for null feed url
        if (item.getUrl() != null) {
            url.setText(Html.fromHtml("<a href=\"" + item.getUrl() + "\">"
                    + item.getUrl() + "</a> "));
//			url.setTypeface(typeface);
            // Making url clickable
            url.setMovementMethod(LinkMovementMethod.getInstance());
            url.setVisibility(View.GONE);
        } else {
            // url is null, remove from the view
            url.setVisibility(View.GONE);
        }

        // user profile pic
        profilePic.setImageUrl(item.getProfilePic(), imageLoader);

        // Feed image
        if (item.getImge() != null) {
            feedImageView.setImageUrl(item.getImge(), imageLoader);
            feedImageView.setVisibility(View.VISIBLE);
            feedImageView
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
        } else {
            feedImageView.setVisibility(View.GONE);
        }
        sharelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureialog();


            }
        });
        chatlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showBottomSheet();

            }
        });
        post_hideorvisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPictureialogforPostVisibleorHide();

            }
        });
        return convertView;
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showBottomSheet() {
        bottomSheetDialog = new BottomSheetDialog(activity);

        View view = activity.getLayoutInflater().inflate(R.layout.content_dialog_bottom_sheet, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
        listView = (NonScrollListView) view.findViewById(R.id.list);
        image_view = (ImageView) view.findViewById(R.id.image_view1);


        Toast.makeText(activity, "Hi", Toast.LENGTH_SHORT).show();
        Picasso.with(activity)
                .load(profile_pic_url)
                .error(R.drawable.normal_bg)
                .into(image_view);
        feedItems = new ArrayList<FeedItem>();

        listAdapter = new NotificationsListAdapter(activity, feedItems);
        listView.setAdapter(listAdapter);
        // We first check for cached request
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL_FEED, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    private void parseJsonFeed(JSONObject response) {
        try {
            JSONObject feedObject = response.getJSONObject("data");
            JSONArray feedArray = feedObject.getJSONArray("records1");

            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setId(feedObj.getInt("id"));
                item.setName(feedObj.getString("movie_name"));

                // Image might be null sometimes
                String image = feedObj.isNull("jukebox_images") ? null : feedObj
                        .getString("jukebox_images");
                item.setImge(image);
                item.setStatus(feedObj.getString("movietype"));
                item.setProfilePic(feedObj.getString("poster"));
                item.setTimeStamp(feedObj.getString("movielength"));

                // url might be null sometimes
                String feedUrl = feedObj.isNull("still") ? null : feedObj
                        .getString("still");
                item.setUrl(feedUrl);

                feedItems.add(item);
            }

            // notify data changes to list adapater
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
