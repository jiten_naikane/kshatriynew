package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.fragments.PostAssociationFragment;
import in.innasoft.kshatriyayouth.holders.AssociationPostHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.AssociationPostItemClcikListener;
import in.innasoft.kshatriyayouth.models.Association_PostModel;

/**
 * Created by Devolper on 22-Aug-17.
 */


public class AssociationPostAdapter extends RecyclerView.Adapter<AssociationPostHolder> {

    public ArrayList<Association_PostModel> asspostlistModels;
    PostAssociationFragment context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public AssociationPostAdapter(ArrayList<Association_PostModel> asspostlistModels, PostAssociationFragment context, int resource) {
        this.asspostlistModels = asspostlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
    }

    @Override
    public AssociationPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AssociationPostHolder slh = new AssociationPostHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AssociationPostHolder holder, final int position) {

//        holder.asso_post_title.setTypeface(typeface);
        String asso_post_title = asspostlistModels.get(position).getName();//.substring(0, 1).toUpperCase() ;
        holder.asso_post_title.setText(Html.fromHtml(asso_post_title));


//        holder.asso_post_descrption.setTypeface(typeface);
        String asso_post_descrption = asspostlistModels.get(position).getDescription();//.substring(0, 1).toUpperCase() ;
        holder.asso_post_descrption.setText(Html.fromHtml(asso_post_descrption));


        Picasso.with(context.getActivity())
                .load(asspostlistModels.get(position).getImage())
                .placeholder(R.drawable.app_logo_new)
                .into(holder.asso_post_img);

        holder.setItemClickListener(new AssociationPostItemClcikListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });


    }

    @Override
    public int getItemCount() {
        //notifyDataSetChanged();
        return this.asspostlistModels.size();
    }


}

