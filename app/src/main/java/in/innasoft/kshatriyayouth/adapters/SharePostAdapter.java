package in.innasoft.kshatriyayouth.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.EditPostFeedItem;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class SharePostAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<EditPostFeedItem> feedItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    //    Typeface typeface;
    ImageView send_image, select_image;
    String profile_pic_url = "";
    String user_name = "";
    UserSessionManager sessionManager;
    TextView like;
    String user_profile_id = "";

    private boolean checkInternet;

    public SharePostAdapter(Activity activity, List<EditPostFeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
//		typeface = Typeface.createFromAsset(activity.getAssets(), activity.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        sessionManager = new UserSessionManager(activity);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        user_name = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        Log.d("dfjkdfbndfjkb", "hhghgh" + profile_pic_url);
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.edit_post_list_item, null);


        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView post = (TextView) convertView
                .findViewById(R.id.post);
        TextView time = (TextView) convertView
                .findViewById(R.id.time);

        ImageView profilePic = (ImageView) convertView
                .findViewById(R.id.profilePic);


        final EditPostFeedItem item = feedItems.get(position);
        name.setText(item.getName());
//		name.setTypeface(typeface);

        time.setText(item.getDate());

        Picasso.with(activity)
                .load(profile_pic_url)
                .into(profilePic);

        post.setText(item.getPost());
//		post.setTypeface(typeface);

        return convertView;
    }


}
