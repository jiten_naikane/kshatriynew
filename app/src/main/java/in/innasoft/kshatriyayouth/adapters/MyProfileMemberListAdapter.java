package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.holders.GroupMemberListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupMemberListItemClickListener;
import in.innasoft.kshatriyayouth.models.GroupMembersListModel;


public class MyProfileMemberListAdapter extends RecyclerView.Adapter<GroupMemberListHolder> {
    public ArrayList<GroupMembersListModel> gmemberListItem;
    public MyProfileActivity context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public MyProfileMemberListAdapter(ArrayList<GroupMembersListModel> gmemberListItem, MyProfileActivity context, int resource) {
        this.gmemberListItem = gmemberListItem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

    }

    @Override
    public GroupMemberListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        GroupMemberListHolder slh = new GroupMemberListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GroupMemberListHolder holder, final int position) {

        String surname = firstLetterCaps(gmemberListItem.get(position).getSurname());

        //String member_name_text=Html.fromHtml(gmemberListItem.get(position).getName();


        holder.member_name_text.setText(firstLetterCaps(gmemberListItem.get(position).getName()) + " " + surname);
//        holder.member_name_text.setTypeface(typeface);


        Picasso.with(context)
                .load(gmemberListItem.get(position).getPhoto())
                .placeholder(R.drawable.user_profile)
                .into(holder.member_image_iv);
        Log.d("ahsdbvjchbsd", gmemberListItem.get(position).getPhoto());


        holder.setItemClickListener(new GroupMemberListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent intent = new Intent(context, MyProfileActivity.class);
                intent.putExtra("user_id", gmemberListItem.get(pos).getUsername());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.gmemberListItem.size();

    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }


}
