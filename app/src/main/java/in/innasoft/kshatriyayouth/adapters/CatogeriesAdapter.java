package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.ProfileJobsActivity;
import in.innasoft.kshatriyayouth.holders.CatogeriesHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.CatogeriesModel;


public class CatogeriesAdapter extends RecyclerView.Adapter<CatogeriesHolder> {
    public ArrayList<CatogeriesModel> feeditem;
    public ProfileJobsActivity context;
    LayoutInflater li;
    int resource;
    int defaultPageNo = 1;
    //int selectedPosition = -1;
    int selectedPosition = 0;
    //    Typeface typeface;
    View titleline;

    public CatogeriesAdapter(ArrayList<CatogeriesModel> feeditem, ProfileJobsActivity context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public CatogeriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        CatogeriesHolder slh = new CatogeriesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CatogeriesHolder holder, final int position) {
        if (feeditem.get(position).getId().equals("0")) {
            holder.title.setTextColor(Color.parseColor("#FF9F06"));
            holder.titleline.setBackgroundColor(Color.parseColor("#FF9F06"));
        }
        if (selectedPosition == position) {
//            holder.title.setBackgroundColor(Color.parseColor("#FF9F06"));

            holder.title.setTextColor(Color.parseColor("#FF9F06"));
            holder.titleline.setBackgroundColor(Color.parseColor("#FF9F06"));
        } else {
//            holder.title.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.title.setTextColor(Color.parseColor("#666666"));
            holder.titleline.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        holder.title.setText(Html.fromHtml(feeditem.get(position).getName()));
//        holder.title.setTypeface(typeface);
        final String id = feeditem.get(position).getId();
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
                if (context instanceof ProfileJobsActivity) {
                    ((ProfileJobsActivity) context).feedItem(id, defaultPageNo);
                }
            }
        });
        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();

    }
}
