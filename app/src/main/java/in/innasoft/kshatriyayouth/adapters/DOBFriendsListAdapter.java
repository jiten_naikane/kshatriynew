package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.RegisterPage;
import in.innasoft.kshatriyayouth.holders.DOBFriendsListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.DOBFriendsListItemClickListener;
import in.innasoft.kshatriyayouth.models.DOBModel;


public class DOBFriendsListAdapter extends RecyclerView.Adapter<DOBFriendsListHolder> {

    public ArrayList<DOBModel> dob_friendArrayList;
    public RegisterPage context;
    LayoutInflater li;
    int resource;
    String dob_friend_name;


    public DOBFriendsListAdapter(ArrayList<DOBModel> dob_friendArrayList, RegisterPage context, int resource) {
        this.dob_friendArrayList = dob_friendArrayList;

        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public DOBFriendsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        DOBFriendsListHolder slh = new DOBFriendsListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(DOBFriendsListHolder holder, int position) {

        dob_friend_name = dob_friendArrayList.get(position).getName().substring(0, 1).toUpperCase() + dob_friendArrayList.get(position).getName().substring(1);
        holder.name_dob_friends.setText(Html.fromHtml(dob_friend_name));

        Picasso.with(context)
                .load(dob_friendArrayList.get(position).getPhoto())
                .placeholder(R.drawable.user_profile)
                .into(holder.imge_dob_friends);

        holder.setItemClickListener(new DOBFriendsListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.dob_friendArrayList.size();
    }
}
