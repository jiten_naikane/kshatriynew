package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.ProfileAnnouncementDetailActivity;
import in.innasoft.kshatriyayouth.activities.ProfileAnnouncementsActivity;
import in.innasoft.kshatriyayouth.holders.AnnouncementHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.AnnouncementModel;


public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementHolder> {
    public ArrayList<AnnouncementModel> feeditem;
    public ProfileAnnouncementsActivity context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public AnnouncementAdapter(ArrayList<AnnouncementModel> feeditem, ProfileAnnouncementsActivity context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public AnnouncementHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AnnouncementHolder slh = new AnnouncementHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AnnouncementHolder holder, final int position) {
        holder.createddateandtime.setText(Html.fromHtml(feeditem.get(position).getCreate_date_time()));
//        holder.createddateandtime.setTypeface(typeface);
        holder.title.setText(Html.fromHtml(feeditem.get(position).getName()));
//        holder.title.setTypeface(typeface);
        holder.description.setText(Html.fromHtml(feeditem.get(position).getDescription()));
//        holder.description.setTypeface(typeface);

        Picasso.with(context)
                .load(feeditem.get(position).getImage())
                .into(holder.image);
        Log.d("ahsdbvjchbsd", feeditem.get(position).getImage());

        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent announceDetail = new Intent(context, ProfileAnnouncementDetailActivity.class);
                announceDetail.putExtra("DETAIL_ID", feeditem.get(position).getId());
                context.startActivity(announceDetail);

            }
        });


    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

}
