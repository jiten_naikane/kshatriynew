package in.innasoft.kshatriyayouth.adapters;


import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.PostStatusActivity;
import in.innasoft.kshatriyayouth.models.SearchingModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class MultiCompletTagFriendAdapter extends ArrayAdapter implements Filterable {
    private ArrayList<SearchingModel> mCountry;

    UserSessionManager session;
    String user_profile_id;
    PostStatusActivity context;
    String TAG_FRIEND_URL = AppUrls.BASE_URL + AppUrls.FRIENDS_LIST_NEW;
    String val;

    public MultiCompletTagFriendAdapter(PostStatusActivity context, int resource, ArrayList<SearchingModel> mCountry) {
        super(context, resource);
        this.context = context;
        mCountry = new ArrayList<>();
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);


    }

    @Override
    public int getCount() {
        return mCountry.size();
    }


    @Override
    public String getItem(int position) {
        // Log.d("dfdfdf",mCountry.get(position).getName().toString()+"-"+mCountry.get(position).getUsername().toString());
        //   Log.d("dfdfdf",mCountry.get(position).getUsername().toString());
        //  val=mCountry.get(position).getUsername().toString();
        return mCountry.get(position).getName().toString() + "-" + mCountry.get(position).getUsername().toString();

    }


    @NonNull
    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    try {
                        //get data from the web
                        String term = constraint.toString();
                        Log.d("TERM", "TERM " + term);
                        mCountry = new DownloadCountry().execute(term).get();
                    } catch (Exception e) {
                        Log.d("HUS", "EXCEPTION " + e);
                    }
                    filterResults.values = mCountry;
                    filterResults.count = mCountry.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.row_multiselect_members, parent, false);
        Log.d("CALLLLLL", "CALLLLL");

        //get Country
        SearchingModel contry = mCountry.get(position);

        TextView auto_tv = (TextView) view.findViewById(R.id.auto_tv);

        ImageView imge_multiselect = (ImageView) view.findViewById(R.id.imge_multiselect);

        auto_tv.setText(contry.getName());

        Picasso.with(getContext()).load(contry.getPhoto())
                .placeholder(R.drawable.user_profile)
                .fit().into(imge_multiselect);


        return view;
    }

    private class DownloadCountry extends AsyncTask<String, Void, ArrayList<SearchingModel>> {

        @Override
        protected ArrayList<SearchingModel> doInBackground(String... params) {
            String s = null;

            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            String Tag = "CHANTI";

            ArrayList<SearchingModel> feeditem = new ArrayList<>();
            try {
                URL url = new URL(TAG_FRIEND_URL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                //   conn.setDoInput(true);
                //   conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("user_profile_id", user_profile_id);

                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);


                writer.flush();
                writer.close();
                // os.close();

                //  conn.connect();

                Log.e(Tag, "File Sent, Response: " + String.valueOf(conn.getResponseCode()));
                int responceCode = conn.getResponseCode();
                Log.d("RESPONSECODE:", responceCode + "");
                if (responceCode == 200) {
                    InputStream is = conn.getInputStream();

                    // retrieve the response from server
                    int ch;

                    StringBuffer b = new StringBuffer();
                    while ((ch = is.read()) != -1) {
                        b.append((char) ch);
                    }
                    s = b.toString();
                    Log.d("SSSSSSSS:", s);
                    JSONObject jsonObject = new JSONObject(s);
                    Log.d("JSONResponse", s);
                    String statuscode = jsonObject.getString("status");
                    if (statuscode.equals("10100")) {
                        JSONObject jobj = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = jobj.getJSONArray("friendsData");

                        Log.d("JARRAY:", jsonArray.toString());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            //store the country name
                            SearchingModel item = new SearchingModel();
                            item.setName(jo.getString("name"));
                            String image = AppUrls.BASE_IMAGE_URL + jo.getString("photo");
                            Log.d("image_photo", image);
                            item.setPhoto(image);
                            item.setSurname(jo.getString("surname"));
                            item.setUsername(jo.getString("username"));
                            item.setFriendstatus(jo.getString("friendstatus"));
                            item.setRequeststatus(jo.getString("requeststatus"));
                            item.setSentbystatus(jo.getString("sentbystatus"));
                            feeditem.add(item);
                        }
                    }

                    os.close();
                }
                if (responceCode == 400) {
                    InputStream errorstream = conn.getErrorStream();
                    int ch;

                    StringBuffer b = new StringBuffer();
                    while ((ch = errorstream.read()) != -1) {
                        b.append((char) ch);
                    }
                    s = b.toString();
                    Log.d("*******Response", s);
                    os.close();
                }
            } catch (MalformedURLException ex) {
                Log.e(Tag, "URL error: " + ex.getMessage(), ex);
            } catch (IOException ioe) {
                Log.e(Tag, "IO error: " + ioe.getMessage(), ioe);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("COUNTRYLIST:", feeditem.toString());
            return feeditem;

        }
    }


}


