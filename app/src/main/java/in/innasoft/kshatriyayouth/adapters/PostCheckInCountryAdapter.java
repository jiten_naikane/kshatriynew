package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.PostStatusActivity;
import in.innasoft.kshatriyayouth.filters.PostCheckedInCustomFilterForCountries;
import in.innasoft.kshatriyayouth.holders.CheckedInCountriesHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.CheckedInCountriesClickListener;
import in.innasoft.kshatriyayouth.models.CheckInCountriesModel;


/**
 * Created by purushotham on 27/1/17.
 */

public class PostCheckInCountryAdapter extends RecyclerView.Adapter<CheckedInCountriesHolder> implements Filterable {
    public ArrayList<CheckInCountriesModel> checkInCountriesArrayList, filterList;
    public PostStatusActivity context;
    PostCheckedInCustomFilterForCountries filter;
    LayoutInflater li;
    int resource;
    Typeface type_lato;


    public PostCheckInCountryAdapter(ArrayList<CheckInCountriesModel> checkInCountriesArrayList, PostStatusActivity context, int resource) {
        this.checkInCountriesArrayList = checkInCountriesArrayList;
        this.filterList = checkInCountriesArrayList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new PostCheckedInCustomFilterForCountries(filterList, this);
        }

        return filter;
    }

    @Override
    public CheckedInCountriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutAerator = li.inflate(resource, parent, false);
        CheckedInCountriesHolder ah = new CheckedInCountriesHolder(layoutAerator);
        return ah;
    }

    @Override
    public void onBindViewHolder(CheckedInCountriesHolder holder, int position) {
        type_lato = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        holder.checkin_country_name_tv.setTypeface(type_lato);
        holder.checkin_country_name_tv.setText(checkInCountriesArrayList.get(position).getCountry_name());

        holder.setItemClickListener(new CheckedInCountriesClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                // Toast.makeText(context.getActivity(), ""+citiesModelArrayList.get(pos).getCity_name(), Toast.LENGTH_SHORT).show();
                //refreshMyList
                context.refreshMyList(checkInCountriesArrayList.get(pos).getCountry_name(), checkInCountriesArrayList.get(pos).getId());

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.checkInCountriesArrayList.size();
    }
}
