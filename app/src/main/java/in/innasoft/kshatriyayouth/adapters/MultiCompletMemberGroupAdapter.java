package in.innasoft.kshatriyayouth.adapters;


import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.GroupCreateActivityDialog;
import in.innasoft.kshatriyayouth.models.AutoCompletGroupCrationModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class MultiCompletMemberGroupAdapter extends ArrayAdapter implements Filterable {
    private ArrayList<AutoCompletGroupCrationModel> mCountry;

    UserSessionManager session;
    String user_profile_id;
    GroupCreateActivityDialog context;
    String COUNTRY_URL = AppUrls.BASE_URL + AppUrls.GROUP_MEMBERS;
    String val;

    public MultiCompletMemberGroupAdapter(GroupCreateActivityDialog context, int resource, ArrayList<AutoCompletGroupCrationModel> mCountry) {
        super(context, resource);
        this.context = context;
        mCountry = new ArrayList<>();
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);


    }

    @Override
    public int getCount() {
        return mCountry.size();

    }


    @Override
    public String getItem(int position) {
        // Log.d("dfdfdf",mCountry.get(position).getName().toString()+"-"+mCountry.get(position).getUsername().toString());
        //   Log.d("dfdfdf",mCountry.get(position).getUsername().toString());
        //  val=mCountry.get(position).getUsername().toString();
        return mCountry.get(position).getName().toString() + "-" + mCountry.get(position).getUsername().toString();


    }


    @NonNull
    @Override
    public Filter getFilter() {
        final Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    try {
                        //get data from the web
                        String term = constraint.toString();
                        Log.d("TERM", "TERM " + term);
                        mCountry = new GettingAllMembers().execute(term).get();


                    } catch (Exception e) {
                        Log.d("HUS", "EXCEPTION " + e);
                    }
                    filterResults.values = mCountry;
                    filterResults.count = mCountry.size();


                }

                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.row_multiselect_members, parent, false);
        Log.d("CALLLLLL", "CALLLLL");

        //get Country
        AutoCompletGroupCrationModel contry = mCountry.get(position);

        TextView auto_tv = (TextView) view.findViewById(R.id.auto_tv);

        ImageView imge_multiselect = (ImageView) view.findViewById(R.id.imge_multiselect);

        String user = contry.getName().substring(0, 1).toUpperCase() + contry.getName().substring(1);
        auto_tv.setText(user);
        // auto_tv.setText(contry.getName());

        Picasso.with(getContext()).load(contry.getPhoto())
                .placeholder(R.drawable.user_profile)
                .fit().into(imge_multiselect);

        return view;
    }

    private class GettingAllMembers extends AsyncTask<String, Void, ArrayList<AutoCompletGroupCrationModel>> {


        @Override
        protected ArrayList<AutoCompletGroupCrationModel> doInBackground(String... params) {
            String s = null;

            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            String Tag = "CHANTI";

            ArrayList<AutoCompletGroupCrationModel> countryList = new ArrayList<>();
            try {
                URL url = new URL(COUNTRY_URL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                //   conn.setDoInput(true);
                //   conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("user_profile_id", user_profile_id);

                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);


                writer.flush();
                writer.close();
                // os.close();

                //  conn.connect();

                Log.e(Tag, "File Sent, Response: " + String.valueOf(conn.getResponseCode()));
                int responceCode = conn.getResponseCode();
                Log.d("RESPONSECODE:", responceCode + "");
                if (responceCode == 200) {
                    InputStream is = conn.getInputStream();

                    // retrieve the response from server
                    int ch;

                    StringBuffer b = new StringBuffer();
                    while ((ch = is.read()) != -1) {
                        b.append((char) ch);
                    }
                    s = b.toString();
                    Log.d("SSSSSSSS:", s);
                    JSONObject jsonObject = new JSONObject(s);
                    Log.d("JSONResponse", s);
                    String statuscode = jsonObject.getString("status");
                    if (statuscode.equals("10100")) {
                        JSONObject jobj = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = jobj.getJSONArray("groupData");

                        Log.d("JARRAY:", jsonArray.toString());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            //store the country name
                            AutoCompletGroupCrationModel country = new AutoCompletGroupCrationModel();
                            country.setName(jo.getString("name"));
                            country.setSurname(jo.getString("surname"));
                            country.setUsername(jo.getString("username"));
                            country.setPhoto(AppUrls.BASE_IMAGE_URL + jo.getString("photo"));
                            countryList.add(country);


                        }


                    }


                    os.close();
                }
                if (responceCode == 400) {
                    InputStream errorstream = conn.getErrorStream();
                    int ch;

                    StringBuffer b = new StringBuffer();
                    while ((ch = errorstream.read()) != -1) {
                        b.append((char) ch);
                    }
                    s = b.toString();
                    Log.d("*******Response", s);
                    os.close();
                }
            } catch (MalformedURLException ex) {
                Log.e(Tag, "URL error: " + ex.getMessage(), ex);
            } catch (IOException ioe) {
                Log.e(Tag, "IO error: " + ioe.getMessage(), ioe);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("COUNTRYLIST:", countryList.toString());
            return countryList;


        }

    }


}


