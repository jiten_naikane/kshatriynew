package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.fragments.AssociationPeopleFragment;
import in.innasoft.kshatriyayouth.holders.AssociationPeopleHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.AssociationPeopleItemClcikListener;
import in.innasoft.kshatriyayouth.models.Association_PeopleModel;

/**
 * Created by Devolper on 22-Aug-17.
 */


public class AssociationPeopleAdapter extends RecyclerView.Adapter<AssociationPeopleHolder> {

    public ArrayList<Association_PeopleModel> asspeopletlistModels;
    AssociationPeopleFragment context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public AssociationPeopleAdapter(ArrayList<Association_PeopleModel> asspeopletlistModels, AssociationPeopleFragment context, int resource) {
        this.asspeopletlistModels = asspeopletlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
    }

    @Override
    public AssociationPeopleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AssociationPeopleHolder slh = new AssociationPeopleHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AssociationPeopleHolder holder, final int position) {

//        holder.people_name_text.setTypeface(typeface);
        String people_name_text = asspeopletlistModels.get(position).getName();
        holder.people_name_text.setText(people_name_text);


        String state = asspeopletlistModels.get(position).getState();


//        holder.people_city_state_text.setTypeface(typeface);
        String people_city_state_text = asspeopletlistModels.get(position).getCity();
        if (people_city_state_text.equals("null") || people_city_state_text.equals("") || state.equals("null") || state.equals("")) {
            holder.people_city_state_text.setVisibility(View.GONE);

        }
        holder.people_city_state_text.setText(Html.fromHtml(people_city_state_text) + ", " + state);


        Picasso.with(context.getActivity())
                .load(asspeopletlistModels.get(position).getProfile_pic())
                .placeholder(R.drawable.app_logo_new)
                .into(holder.people_profile_img);

        holder.setItemClickListener(new AssociationPeopleItemClcikListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });


    }

    @Override
    public int getItemCount() {
        //notifyDataSetChanged();
        return this.asspeopletlistModels.size();
    }


}

