package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.RegisterPage;
import in.innasoft.kshatriyayouth.filters.CustomFilterForSurnameList;
import in.innasoft.kshatriyayouth.holders.SurnameListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.SurnameListItemClickListener;
import in.innasoft.kshatriyayouth.models.SurnameListModel;

public class SurnameListAdapter extends RecyclerView.Adapter<SurnameListHolder> implements Filterable {

    public ArrayList<SurnameListModel> surnameArrayList, filterList;
    public RegisterPage context;
    CustomFilterForSurnameList filter;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public SurnameListAdapter(ArrayList<SurnameListModel> surnameArrayList, RegisterPage context, int resource) {
        this.surnameArrayList = surnameArrayList;
        this.filterList = surnameArrayList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForSurnameList(filterList, this);
        }

        return filter;
    }

    @Override
    public SurnameListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        SurnameListHolder slh = new SurnameListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(SurnameListHolder holder, int position) {

//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.montserrat_light));


//        holder.row_surname_txt.setTypeface(typeface);
        holder.row_surname_txt.setText(Html.fromHtml(surnameArrayList.get(position).surname));

        holder.setItemClickListener(new SurnameListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setSurName(surnameArrayList.get(pos).getSurname());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.surnameArrayList.size();
    }
}
