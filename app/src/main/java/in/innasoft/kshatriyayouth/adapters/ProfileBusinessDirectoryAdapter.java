package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.BusinessDirectoryDetailActivity;
import in.innasoft.kshatriyayouth.activities.ProfileBusinessDirectoryActivity;
import in.innasoft.kshatriyayouth.holders.BusinessDirectoryHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.BusinessDirectoryItemClcikListener;
import in.innasoft.kshatriyayouth.models.BusinessDirectoryModel;


public class ProfileBusinessDirectoryAdapter extends RecyclerView.Adapter<BusinessDirectoryHolder> {
    public ArrayList<BusinessDirectoryModel> feeditem;
    public ProfileBusinessDirectoryActivity context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public ProfileBusinessDirectoryAdapter(ArrayList<BusinessDirectoryModel> feeditem, ProfileBusinessDirectoryActivity context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public BusinessDirectoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        BusinessDirectoryHolder slh = new BusinessDirectoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final BusinessDirectoryHolder holder, final int position) {


        String b_title = feeditem.get(position).getName().substring(0, 1).toUpperCase() + feeditem.get(position).getName().substring(1);
        holder.bisiness_title.setText(Html.fromHtml(b_title));
//        holder.bisiness_title.setTypeface(typeface);

        holder.bisiness_location.setText(Html.fromHtml(feeditem.get(position).getLocation()));
//        holder.bisiness_location.setTypeface(typeface);

        holder.createddateandtime.setText(Html.fromHtml(feeditem.get(position).getCreate_date_time()));
//        holder.createddateandtime.setTypeface(typeface);


        Picasso.with(context)
                .load(feeditem.get(position).getImage())
                .error(R.drawable.no_images_found)
                .into(holder.business_image);
        Log.d("ahsdbvjchbsd", feeditem.get(position).getImage());


        holder.setItemClickListener(new BusinessDirectoryItemClcikListener() {
            @Override
            public void onItemClick(View v, int pos) {

                Intent businessDetail = new Intent(context, BusinessDirectoryDetailActivity.class);
                businessDetail.putExtra("BUSINESSDETAIL_ID", feeditem.get(position).getId());
                context.startActivity(businessDetail);

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

}
