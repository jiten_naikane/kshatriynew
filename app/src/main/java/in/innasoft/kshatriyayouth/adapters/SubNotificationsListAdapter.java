package in.innasoft.kshatriyayouth.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.EditPostActivity;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.activities.SubCommentsActivity;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class SubNotificationsListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    //    Typeface typeface;
    ImageView send_image, select_image;
    String profile_pic_url = "";
    String user_name = "";
    UserSessionManager sessionManager;
    TextView like;
    String user_profile_id = "";
    private boolean checkInternet;

    public SubNotificationsListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
//		typeface = Typeface.createFromAsset(activity.getAssets(), activity.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        sessionManager = new UserSessionManager(activity);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        user_name = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        Log.d("dfjkdfbndfjkb", "hhghgh" + profile_pic_url);
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.main_notifications_list_item, null);


        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        final TextView like = (TextView) convertView
                .findViewById(R.id.like);
        TextView time = (TextView) convertView
                .findViewById(R.id.time);
        TextView reply = (TextView) convertView
                .findViewById(R.id.reply);
        TextView edited = (TextView) convertView
                .findViewById(R.id.edited);
        ImageView profilePic = (ImageView) convertView
                .findViewById(R.id.profilePic);
        ImageView comment_image = (ImageView) convertView
                .findViewById(R.id.comment_image);
        ImageView dot = (ImageView) convertView
                .findViewById(R.id.dot);
        reply.setVisibility(View.GONE);
        dot.setVisibility(View.GONE);

        final FeedItem item = feedItems.get(position);
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MyProfileActivity.class);
                intent.putExtra("user_id", item.getUsername());
                activity.startActivity(intent);
            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MyProfileActivity.class);
                intent.putExtra("user_id", item.getUsername());
                activity.startActivity(intent);
            }
        });
        if (!item.getComment_edit_count().equals("0")) {
            edited.setVisibility(View.VISIBLE);
            edited.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, EditPostActivity.class);
                    String ids = ((Activity) activity).getIntent().getExtras().getString("id");
                    intent.putExtra("id", ids);
                    intent.putExtra("sub_id", feedItems.get(position).getId_comment());
                    intent.putExtra("type", "reply");
                    activity.startActivity(intent);
                }
            });
        } else {
            edited.setVisibility(View.GONE);
        }
        name.setText(item.getName());
//		name.setTypeface(typeface);

        time.setText(item.getTime());

//		name.setTypeface(typeface);
        if (!item.getProfilePic().equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(activity)
                    .load(item.getProfilePic())
                    .into(profilePic);
        }
        if (!item.getImge().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {
            comment_image.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(item.getImge())
                    .into(comment_image);
        } else {
            comment_image.setVisibility(View.GONE);
        }
        // Converting timestamp into x ago format
    /*	CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
				Long.parseLong(item.getTimeStamp()),
				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);*/
        timestamp.setText(item.getTimeStamp());
//		timestamp.setTypeface(typeface);
//		like.setTypeface(typeface);
        if (feedItems.get(position).getUser_like_status().equals("0")) {
            like.setText("Like");
            like.setTextColor(Color.BLACK);
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String reply_id = item.getId_comment();
                    String id = ((Activity) activity).getIntent().getExtras().getString("id");

                    String friend = item.getUsername();
                    postLikes(reply_id, id, friend);
                }
            });
        } else {
            like.setText("Liked");
            like.setTextColor(Color.BLUE);
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like.setTextColor(Color.BLACK);
                    String id = ((Activity) activity).getIntent().getExtras().getString("id");
                    String reply_id = feedItems.get(position).getId_comment();
                    String username = feedItems.get(position).getUsername();
                    postDisLikes(id, reply_id);
                }
            });
        }


        return convertView;
    }

    private void postLikes(final String post_id, final String id, final String friends) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(activity);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.COMMETN_UNDERCOMMENT_LIKES;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    Toast.makeText(activity, "Liked", Toast.LENGTH_SHORT).show();
									/*Intent lObjIntent = new Intent(activity, CommentsActivity.class);
                                    activity.startActivity(lObjIntent);
                                    activity.finish();*/
                                    //notifyDataSetChanged();
                                    ((SubCommentsActivity) activity).subComments(id, user_name);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", id);
                    params.put("reply_id", post_id);
                    params.put("username", user_name);
                    params.put("friend", friends);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(activity, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void postDisLikes(final String id, final String reply_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(activity);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.COMMENT_UNDER_COMMENT_UNLIKES;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    Toast.makeText(activity, "DisLiked", Toast.LENGTH_SHORT).show();
                                    ((SubCommentsActivity) activity).subComments(id, user_name);
/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", id);
                    params.put("reply_id", reply_id);
                    params.put("username", user_name);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(activity, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }


}
