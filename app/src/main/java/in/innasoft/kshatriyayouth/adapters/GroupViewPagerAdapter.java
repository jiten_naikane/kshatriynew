package in.innasoft.kshatriyayouth.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by purushotham on 28/2/17.
 */

public class GroupViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> gFragmentList = new ArrayList<>();
    private final List<String> gFragmentTitleList = new ArrayList<>();

    public GroupViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return gFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return gFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        gFragmentList.add(fragment);
        gFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return gFragmentTitleList.get(position);
    }

}