package in.innasoft.kshatriyayouth.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.InformationDirectoryDetailActivity;
import in.innasoft.kshatriyayouth.activities.ProfileInformationDirectoryActivity;
import in.innasoft.kshatriyayouth.holders.InformationDirectoryHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.InformationDirectoryItemClickListener;
import in.innasoft.kshatriyayouth.models.InformationDirectoryModel;


public class ProfileInformationDirectoryAdapter extends RecyclerView.Adapter<InformationDirectoryHolder> {
    public ArrayList<InformationDirectoryModel> info_directory_list;
    public ProfileInformationDirectoryActivity context;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public ProfileInformationDirectoryAdapter(ArrayList<InformationDirectoryModel> info_directory_list, ProfileInformationDirectoryActivity context, int resource) {
        this.info_directory_list = info_directory_list;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public InformationDirectoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        InformationDirectoryHolder slh = new InformationDirectoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final InformationDirectoryHolder holder, final int position) {
        holder.info_directory_title.setText(Html.fromHtml(info_directory_list.get(position).getName()));
//        holder.info_directory_title.setTypeface(typeface);

        holder.info_directory_occupation.setText(Html.fromHtml(info_directory_list.get(position).getOccupation()));
//        holder.info_directory_occupation.setTypeface(typeface);

        Picasso.with(context)
                .load(info_directory_list.get(position).getInfo_image())
                .error(R.drawable.no_images_found)
                .into(holder.info_image_iv);
        Log.d("ahsdbvjchbsd", info_directory_list.get(position).getInfo_image());

        holder.setItemClickListener(new InformationDirectoryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent info_detail = new Intent(context, InformationDirectoryDetailActivity.class);
                info_detail.putExtra("INFO_DIRECTROY_ID", info_directory_list.get(pos).getInfo_id());
                context.startActivity(info_detail);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.info_directory_list.size();
    }

    private void showPictureialog() {
        CharSequence[] items = {"Share Post", "Write Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

    private void showPictureialogforPostVisibleorHide() {
        CharSequence[] items = {"Hide Post", "Report Post"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                } else if (item == 1) {

                } else if (item == 2) {

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position

        dialog.show();


    }

}
