package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.FriendsBirthdayActivity;
import in.innasoft.kshatriyayouth.activities.PostStatusActivity;
import in.innasoft.kshatriyayouth.models.DOBModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

/**
 * Created by Devolper on 13-Oct-17.
 */

public class DOBFriendsVeiwPagerAdapter extends PagerAdapter {

    public ArrayList<DOBModel> dob_friendArrayList;
    public FriendsBirthdayActivity context;
    LayoutInflater li;
    int resource;
    String dob_friend_name;
    UserSessionManager sessionManager;
    String userid = "", user_profile_id = "", user_profile_name = "", profile_pic_url = "";
    private boolean checkInternet;
    public JSONObject jsonObject;
    String mesage = "Wish You a Many More Happy Returns of the Day";


    public DOBFriendsVeiwPagerAdapter(ArrayList<DOBModel> dob_friendArrayList, FriendsBirthdayActivity context) {
        this.dob_friendArrayList = dob_friendArrayList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        sessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();

        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        user_profile_name = userDetails.get(UserSessionManager.USER_NAME);
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        Log.d("USERID:", user_profile_id);
    }

    @Override
    public int getCount() {
        return dob_friendArrayList.size();

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        // Declare Variables
        TextView name_dob_friends;
        ImageView imge_dob_friends;
        Button button_send_wishes;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = li.inflate(R.layout.row_dob_friends_members, container, false);

        name_dob_friends = (TextView) itemView.findViewById(R.id.name_dob_friends);
        imge_dob_friends = (ImageView) itemView.findViewById(R.id.imge_dob_friends);
        button_send_wishes = (Button) itemView.findViewById(R.id.button_send_wishes);


        dob_friend_name = dob_friendArrayList.get(position).getName().substring(0, 1).toUpperCase() + dob_friendArrayList.get(position).getName().substring(1);
        name_dob_friends.setText(Html.fromHtml(dob_friend_name));

        Picasso.with(context)
                .load(dob_friendArrayList.get(position).getPhoto())
                .placeholder(R.drawable.user_profile)
                .into(imge_dob_friends);

        button_send_wishes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user_name = dob_friendArrayList.get(position).getUsername();
                Intent intent = new Intent(context, PostStatusActivity.class);
                intent.putExtra("name", user_profile_name);
                intent.putExtra("page_owner", user_name);
                intent.putExtra("photo_identifier", "wallfeeds");
                intent.putExtra("feeds", "wall");
                intent.putExtra("image", profile_pic_url);
                context.startActivity(intent);
                context.finish();
            }
        });


        ((ViewPager) container).addView(itemView);

        return itemView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }


    public void uploadPost(String user_friend_id) {
        checkInternet = NetworkChecking.isConnected(context);

        if (checkInternet) {
//
            try {
                jsonObject.put(AppUrls.imageName, "uploadimages");
                jsonObject.put("username", user_profile_id);
                jsonObject.put("page_owner", user_friend_id);
                jsonObject.put("page_id", "feeds");
                jsonObject.put("post", mesage);
                jsonObject.put("security", "public");
                jsonObject.put("photo_identifier", "wallfeeds");
                jsonObject.put(AppUrls.imageList, "");
                jsonObject.put("video_id", "");
                jsonObject.put("url", "");
                jsonObject.put("type", "youtube");
                jsonObject.put("title", "");
                jsonObject.put("description", "");
                jsonObject.put("keywords", "");
                jsonObject.put("video_identifier", "");
                jsonObject.put("location", "");
                jsonObject.put("friends", "");
                Log.d("Birthday_params", "SENDDONATION " + jsonObject.toString());
            } catch (JSONException e) {
                Log.e("JSONObject Here", e.toString());

            }

            String url = AppUrls.BASE_URL + AppUrls.POST_USER_FEED;

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.d("Birthday_params_resp", jsonObject.toString());
                            Intent intent = new Intent(context, MainActivity.class);
                            context.startActivity(intent);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("MessagefromserverERROR", volleyError.toString());
                    Toast.makeText(context, "Error Occurred", Toast.LENGTH_SHORT).show();
                    // dialog1.dismiss();
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(200 * 30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(context).add(jsonObjectRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
