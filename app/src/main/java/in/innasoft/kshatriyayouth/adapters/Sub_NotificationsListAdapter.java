package in.innasoft.kshatriyayouth.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class Sub_NotificationsListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private List<FeedItem> feedItems1;
    //    Typeface typeface;
    ImageView image_view;
    private SubNotificationsListAdapter listAdapter;
    BottomSheetDialog bottomSheetDialog;
    private ListView listView;
    EditText commenttext;
    ImageView send_image, select_image;
    private boolean checkInternet;
    String profile_pic_url = "";
    String user_name = "";
    UserSessionManager sessionManager;
    private String URL_FEED = AppUrls.BASE_URL + AppUrls.GROUP_POST_COMMENT_LIKECOMMENT;

    public Sub_NotificationsListAdapter(Activity activity, List<FeedItem> feedItems) {
        this.activity = activity;
        this.feedItems = feedItems;
//		typeface = Typeface.createFromAsset(activity.getAssets(), activity.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        sessionManager = new UserSessionManager(activity);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        user_name = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        Log.d("dfjkdfbndfjkb", "hhghgh" + profile_pic_url);
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.notifications_list_item, null);


        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView time = (TextView) convertView
                .findViewById(R.id.time);
        TextView viewreplys = (TextView) convertView
                .findViewById(R.id.viewreplys);

        ImageView profilePic = (ImageView) convertView
                .findViewById(R.id.profilePic);
        ImageView dot = (ImageView) convertView
                .findViewById(R.id.dot);
        ImageView comment_image = (ImageView) convertView
                .findViewById(R.id.comment_image);

		/*listView = (NonScrollListView) convertView. findViewById(R.id.list);
        feedItems1 = new ArrayList<FeedItem>();
		listAdapter = new SubNotificationsListAdapter(activity, feedItems1);
		listView.setAdapter(listAdapter);
		Cache cache = AppController.getInstance().getRequestQueue().getCache();
		Cache.Entry entry = cache.get(URL_FEED);
		if (entry != null) {
			// fetch the data from cache
			try {
				String data = new String(entry.data, "UTF-8");
				try {
					parseJsonFeed(new JSONObject(data));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		} else {
			// making fresh volley request and getting json
			JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
					URL_FEED+"/"+"426", null, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					VolleyLog.d(TAG, "Response: " + response.toString());
					if (response != null) {
						parseJsonFeed(response);
					}
				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					VolleyLog.d(TAG, "Error: " + error.getMessage());
				}
			});

			// Adding request to volley request queue
			AppController.getInstance().addToRequestQueue(jsonReq);
		}*/
        FeedItem item = feedItems.get(position);

        name.setText(item.getName());
        time.setText(item.getTime());
//		name.setTypeface(typeface);
        if (!item.getProfilePic().equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(activity)
                    .load(item.getProfilePic())
                    .into(profilePic);
        }
        if (!item.getImge().equals(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS)) {
            comment_image.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(item.getImge())
                    .into(comment_image);
        } else {
            comment_image.setVisibility(View.GONE);
        }
        // Converting timestamp into x ago format
	/*	CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
				Long.parseLong(item.getTimeStamp()),
				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);*/
        timestamp.setText(item.getTimeStamp());
//		timestamp.setTypeface(typeface);


        return convertView;
    }

    private void parseJsonFeed(JSONObject response) {
        try {
            JSONObject feedObject = response.getJSONObject("data");
            JSONArray feedArray = feedObject.getJSONArray("post_comments");

            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setId_comment(feedObj.getString("id"));
                item.setName(feedObj.getString("name") + " " + feedObj.getString("surname"));
                item.setTimeStamp(feedObj.getString("comment"));
                String profile_pic = AppUrls.BASE_IMAGE_URL + feedObj.getString("photo");
                item.setProfilePic(profile_pic);
                String image = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + feedObj.getString("images");
                item.setImge(image);

               /* // Image might be null sometimes
                String image = feedObj.isNull("jukebox_images") ? null : feedObj
                        .getString("jukebox_images");
                item.setImge(image);
                item.setStatus(feedObj.getString("movietype"));



                // url might be null sometimes
                String feedUrl = feedObj.isNull("still") ? null : feedObj
                        .getString("still");
                item.setUrl(feedUrl);*/
                Log.d("image", image);
                feedItems1.add(item);
            }

            // notify data changes to list adapater
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
