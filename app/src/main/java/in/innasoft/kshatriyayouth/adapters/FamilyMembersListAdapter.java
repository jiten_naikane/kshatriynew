package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.InformationDirectoryDetailActivity;
import in.innasoft.kshatriyayouth.holders.FamilyMembersListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FamilyMembersListItemClickListener;
import in.innasoft.kshatriyayouth.models.FamilyMembersModel;


public class FamilyMembersListAdapter extends RecyclerView.Adapter<FamilyMembersListHolder> {
    public ArrayList<FamilyMembersModel> familyMembersModels;
    public InformationDirectoryDetailActivity context;
    LayoutInflater li;
    int resource;

    //     Typeface typeface;
    public FamilyMembersListAdapter(ArrayList<FamilyMembersModel> familyMembersModels, InformationDirectoryDetailActivity context, int resource) {
        this.familyMembersModels = familyMembersModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public FamilyMembersListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        FamilyMembersListHolder slh = new FamilyMembersListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final FamilyMembersListHolder holder, final int position) {

//        holder.family_name.setTypeface(typeface);
//        holder.family_gender.setTypeface(typeface);
//        holder.family_age.setTypeface(typeface);
//        holder.family_education.setTypeface(typeface);
//        holder.family_contact.setTypeface(typeface);
//        holder.family_occupation.setTypeface(typeface);
        if (familyMembersModels.get(position).getName().equals("") || familyMembersModels.get(position).getName().equals("null")) {
            holder.family_name.setVisibility(View.GONE);
        } else {
            holder.family_name.setText(familyMembersModels.get(position).getName());
        }

        if (familyMembersModels.get(position).getGender().equals("") || familyMembersModels.get(position).getGender().equals("null")) {
            holder.family_gender.setVisibility(View.GONE);
        } else {
            holder.family_gender.setText(familyMembersModels.get(position).getGender());
        }

        if (familyMembersModels.get(position).getAge().equals("") || familyMembersModels.get(position).getAge().equals("null")) {
            holder.family_age.setVisibility(View.GONE);
        } else {
            holder.family_age.setText(familyMembersModels.get(position).getAge());
        }
        if (familyMembersModels.get(position).getEducation().equals("") || familyMembersModels.get(position).getEducation().equals("null")) {
            holder.family_education.setVisibility(View.GONE);
        } else {
            holder.family_education.setText(familyMembersModels.get(position).getEducation());
        }
        if (familyMembersModels.get(position).getContact_no().equals("") || familyMembersModels.get(position).getContact_no().equals("null")) {
            holder.family_contact.setVisibility(View.GONE);
        } else {
            holder.family_contact.setText(familyMembersModels.get(position).getContact_no());
        }
        if (familyMembersModels.get(position).getOccupation().equals("") || familyMembersModels.get(position).getOccupation().equals("null")) {
            holder.family_occupation.setVisibility(View.GONE);
        } else {
            holder.family_occupation.setText(familyMembersModels.get(position).getOccupation());
        }


        holder.setItemClickListener(new FamilyMembersListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return this.familyMembersModels.size();
    }


}
