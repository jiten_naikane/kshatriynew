package in.innasoft.kshatriyayouth.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.fragments.NotificationsFragment;
import in.innasoft.kshatriyayouth.holders.NotificationHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.models.NotificationsItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationHolder> {
    public ArrayList<NotificationsItem> feeditem;
    public NotificationsFragment context;
    LayoutInflater li;
    ProgressDialog progressDialog;
    private boolean checkInternet;
    int resource;
    //    Typeface typeface;
    String item_name;
    UserSessionManager session;
    String userid = "", user_profile_id = "";

    //liked_your_post

    public NotificationsAdapter(ArrayList<NotificationsItem> feeditem, NotificationsFragment context, int resource) {
        this.feeditem = feeditem;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        NotificationHolder slh = new NotificationHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final NotificationHolder holder, final int position) {
        progressDialog = new ProgressDialog(context.getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        item_name = feeditem.get(position).getItem_name();

        String only_name = feeditem.get(position).getName().substring(0, 1).toUpperCase() + feeditem.get(position).getName().substring(1);
        holder.name_surname.setText(Html.fromHtml(only_name + " " + feeditem.get(position).getSurname()));
//        holder.name_surname.setTypeface(typeface);
        holder.name_surname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
            }
        });

        holder.item_name.setText(feeditem.get(position).getItem_name());
//        holder.item_name.setTypeface(typeface);


        if (item_name.equals("sent_a_request_to_join_group")) {

            holder.group_name.setText("Named : " + feeditem.get(position).getGroup_name());
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("group");
            holder.item_name.setText("wants to join your");
        }

        if (item_name.equals("wants to join your") || item_name.equals("sent_a_request_to_join_group")) {
            holder.accepted_deny_ll.setVisibility(View.VISIBLE);
            holder.btn_accept_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String ccept_group_id = feeditem.get(position).getGroup_id();
                    acceptGroup(ccept_group_id);
                    holder.accepted_deny_ll.setVisibility(View.GONE);
                }
            });
            holder.btn_deny_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String deny_group_id = feeditem.get(position).getGroup_id();
                    denayGroup(deny_group_id);
                    holder.accepted_deny_ll.setVisibility(View.GONE);

                }
            });

        }
        if (item_name.equals("liked_your_post") || item_name.equals("liked_your_comment")) {
            holder.group_name.setVisibility(View.GONE);
            holder.like_button.setVisibility(View.VISIBLE);
            holder.item_name.setText("Your Post");
            if (feeditem.get(position).getPost_or_reply().equals("comment")) {
                holder.comment_name.setVisibility(View.VISIBLE);
                holder.comment_name.setText(feeditem.get(position).getComment_name());
            }
            if (feeditem.get(position).getPost_or_reply().equals("post")) {
                holder.post_name.setVisibility(View.VISIBLE);
                holder.post_name.setText(feeditem.get(position).getPost_name());
            }
        }


        if (item_name.equals("allowed_request_to_join_group")) {
            holder.accepted_deny_ll.setVisibility(View.GONE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.resolve_group.setVisibility(View.VISIBLE);

            holder.post_or_reply.setText("");
            holder.group_name.setText("Name: " + feeditem.get(position).getGroup_name());
            holder.item_name.setText("want to join your");
            holder.resolve_group.setText("This notification has been resolved");

        }

        if (item_name.equals("denied_request_to_join_group")) {
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("group");
            holder.group_name.setText("Name: " + feeditem.get(position).getGroup_name());
            holder.item_name.setText("Denied request to join group");

        }

        if (item_name.equals("added_you_to_a_new_group")) {
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("");
            holder.group_name.setText("Name: " + feeditem.get(position).getGroup_name());
            holder.item_name.setText("added you to a new group");

        }

        if (item_name.equals("group_edited")) {
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("");
            holder.group_name.setText("Name: " + feeditem.get(position).getGroup_name());
            holder.item_name.setText("Group Edited");

        }

        if (item_name.equals("commented_on_your_post")) {
            holder.group_name.setVisibility(View.GONE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("Comment");
            holder.item_name.setText("commented on your");
            if (feeditem.get(position).getPost_or_reply().equals("comment")) {
                holder.comment_name.setVisibility(View.VISIBLE);
                holder.comment_name.setText(feeditem.get(position).getComment_name());
            }
            if (feeditem.get(position).getPost_or_reply().equals("post")) {
                holder.post_name.setVisibility(View.VISIBLE);
                holder.post_name.setText(feeditem.get(position).getPost_name());
            }


        }

        //
        if (item_name.equals("shared_your_post")) {
            holder.group_name.setVisibility(View.GONE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("Post");
            holder.item_name.setText("shared your post");
            if (feeditem.get(position).getPost_or_reply().equals("post")) {
                holder.post_name.setVisibility(View.VISIBLE);
                holder.post_name.setText(feeditem.get(position).getPost_name());
            }
        }


        if (item_name.equals("replied_to_a_comment_on_your_page")) {
            holder.comment_name.setVisibility(View.VISIBLE);
            holder.group_name.setVisibility(View.GONE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("commnet");
            holder.item_name.setText("replied to a comment on your");
            holder.comment_name.setText(feeditem.get(position).getComment_name());
        }

        if (item_name.equals("added_you_to_a_new_group")) {
            holder.group_name.setVisibility(View.VISIBLE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("");
            holder.item_name.setText("Added you to new group");
        }

        if (item_name.equals("accepted_request")) {
            holder.comment_name.setVisibility(View.GONE);
            holder.group_name.setVisibility(View.GONE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("request");
            holder.item_name.setText("Accepted your friendship");
        }

        if (item_name.equals("has_left_the_group")) {
            holder.comment_name.setVisibility(View.GONE);
            holder.group_name.setVisibility(View.VISIBLE);
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("group");
            holder.item_name.setText("has Left the group");
        }

        if (item_name.contains("posted_in_a_group_you_are_in")) {
            holder.post_or_reply.setVisibility(View.VISIBLE);
            holder.group_name.setVisibility(View.VISIBLE);
            holder.post_or_reply.setText("" + " " + "you are in");
            holder.group_name.setText("Name: " + feeditem.get(position).getGroup_name());
            holder.item_name.setText("posted in a");
        }


        holder.timestamp.setText(Html.fromHtml(feeditem.get(position).getDate()));
//        holder.timestamp.setTypeface(typeface);

        Picasso.with(context.getActivity())
                .load(feeditem.get(position).getPhoto())
                .placeholder(R.drawable.user_profile)
                .into(holder.profilePic);
        Log.d("ahsdbvjchbsd", feeditem.get(position).getPhoto());
        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
            }
        });
        holder.setItemClickListener(new FeedHomeClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent intent = new Intent(context.getActivity(), MyProfileActivity.class);
                intent.putExtra("user_id", feeditem.get(position).getUsername());
                context.getActivity().startActivity(intent);
                 /* Intent homefeed=new Intent (context.getActivity(), MainActivity.class);
                  context.startActivity(homefeed);*/
            }
        });

    }

    private void acceptGroup(final String accept_grioupid) {

        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String urldelete = AppUrls.BASE_URL + AppUrls.ACCEPT_JOIN_GROUP;
            Log.d("ACCEPPARAMURL:", urldelete);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCEPT_JOIN_GROUP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASCEPTRESP", response);
                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equalsIgnoreCase("10100")) {
                                    Toast.makeText(context.getActivity(), "Request Sent Successfully..!", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context.getActivity(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("group_id", accept_grioupid);
                    params.put("group_manager", user_profile_id);
                    params.put("username", user_profile_id);

                    Log.d("ACCEPPARAM:", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void denayGroup(final String deny_grioupid) {
        checkInternet = NetworkChecking.isConnected(context.getActivity());
        if (checkInternet) {
            String urldelete = AppUrls.BASE_URL + AppUrls.JOIN_GROUP;
            Log.d("deleterequesturl:", urldelete);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.DELETE_JOIN_GROUP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("deleterequestresp", response);
                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equalsIgnoreCase("10100")) {
                                    Toast.makeText(context.getActivity(), "Request Rejected Succesfully..!", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context.getActivity(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("group_id", deny_grioupid);
                    params.put("username", user_profile_id);
                    Log.d("deleterequestPARAM:", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(context.getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.feeditem.size();
    }


}
