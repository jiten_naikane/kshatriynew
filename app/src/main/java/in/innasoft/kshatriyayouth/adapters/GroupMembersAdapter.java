package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.GroupCreateActivityDialog;
import in.innasoft.kshatriyayouth.filters.CustomFilterforGroupMembers;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupMembersItemClickListener;
import in.innasoft.kshatriyayouth.models.GroupMembersModel;

public class GroupMembersAdapter extends RecyclerView.Adapter<GroupMembersAdapter.GroupMembersHolder> implements Filterable {

    public ArrayList<GroupMembersModel> gmemberListItem, filterList;
    public GroupCreateActivityDialog context;
    CustomFilterforGroupMembers filter;
    LayoutInflater li;
    int resource;
    ArrayList<String> gm_ids = new ArrayList<String>();
    ArrayList<String> gm_names = new ArrayList<String>();

    public GroupMembersAdapter(ArrayList<GroupMembersModel> gmemberListItem, GroupCreateActivityDialog context, int resource) {
        this.gmemberListItem = gmemberListItem;
        this.context = context;
        this.resource = resource;
        this.filterList = gmemberListItem;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public GroupMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        GroupMembersHolder slh = new GroupMembersHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GroupMembersHolder holder, final int position) {
        holder.gm_name_txt.setText(gmemberListItem.get(position).getName() + gmemberListItem.get(position).getSurname());

        if (gmemberListItem.get(position).getPhoto().isEmpty()) {
            holder.gm_img.setImageResource(R.drawable.user_profile);
        } else {
            Picasso.with(context)
                    .load(gmemberListItem.get(position).getPhoto())
                    .placeholder(R.drawable.user_profile)
                    .into(holder.gm_img);
        }

        /*Picasso.with(context)
                .load(gmemberListItem.get(position).getPhoto())
                .placeholder(R.drawable.user_profile)
                .into(holder.gm_img);*/

        holder.gm_checkbox.setChecked(gmemberListItem.get(position).ischecked());

        holder.setItemClickListener(new GroupMembersItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.gmemberListItem.size();

    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterforGroupMembers(filterList, this);
        }

        return filter;
    }

    public class GroupMembersHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckBox gm_checkbox;
        public TextView gm_name_txt;
        public ImageView gm_img;
        GroupMembersItemClickListener groupMembersItemClickListener;

        public GroupMembersHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            gm_checkbox = (CheckBox) itemView.findViewById(R.id.gm_checkbox);
            gm_name_txt = (TextView) itemView.findViewById(R.id.gm_name_txt);
            gm_img = (ImageView) itemView.findViewById(R.id.gm_img);

            gm_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int pos = getAdapterPosition();
                    if (isChecked) {

                        gm_ids.add(gmemberListItem.get(pos).getUsername());
                        HashSet hs = new HashSet();
                        hs.addAll(gm_ids);
                        gm_ids.clear();
                        gm_ids.addAll(hs);
                        gm_names.add(gmemberListItem.get(pos).getName() + "" + gmemberListItem.get(pos).getSurname());
                        HashSet hs1 = new HashSet();
                        hs1.addAll(gm_names);
                        gm_names.clear();
                        gm_names.addAll(hs1);
                        context.setGmName(gm_ids, gm_names);
                        gmemberListItem.get(pos).setIschecked(true);

                    } else {
                        gm_ids.remove(gmemberListItem.get(pos).getUsername());
                        gm_names.remove(gmemberListItem.get(pos).getName());
                        context.setGmName(gm_ids, gm_names);
                        gmemberListItem.get(pos).setIschecked(false);
                    }
                }
            });
        }


        @Override
        public void onClick(View view) {

            this.groupMembersItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(GroupMembersItemClickListener ic) {
            this.groupMembersItemClickListener = ic;
        }
    }
}
