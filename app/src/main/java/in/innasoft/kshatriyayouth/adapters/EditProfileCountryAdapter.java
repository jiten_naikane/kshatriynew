package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.EditProfileActivity;
import in.innasoft.kshatriyayouth.filters.EditProfileCustomFilterForCountryList;
import in.innasoft.kshatriyayouth.holders.EditProfileCountryHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.EditProfileCountryItemClickListener;
import in.innasoft.kshatriyayouth.models.CountryModel;

public class EditProfileCountryAdapter extends RecyclerView.Adapter<EditProfileCountryHolder> implements Filterable {
    //  public ArrayList<CountryModel> countryModels,filterList;
    public ArrayList<CountryModel> countryModelArrayList, filterList;
    public EditProfileActivity context;
    EditProfileCustomFilterForCountryList filter;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public EditProfileCountryAdapter(ArrayList<CountryModel> countryModels, EditProfileActivity context, int resource) {
        this.countryModelArrayList = countryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = countryModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new EditProfileCustomFilterForCountryList(filterList, this);
        }

        return filter;
    }

    @Override
    public EditProfileCountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        EditProfileCountryHolder slh = new EditProfileCountryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(EditProfileCountryHolder holder, final int position) {

//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

//        holder.country_name_txt.setTypeface(typeface);
        holder.country_name_txt.setText(Html.fromHtml(countryModelArrayList.get(position).getName()));

        holder.setItemClickListener(new EditProfileCountryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setCountryName(countryModelArrayList.get(pos).getName(), countryModelArrayList.get(pos).getId());


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.countryModelArrayList.size();
    }
}
