package in.innasoft.kshatriyayouth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.activities.FilterActivity;
import in.innasoft.kshatriyayouth.filters.CustomFilterForAreaList;
import in.innasoft.kshatriyayouth.holders.AreaListHolder;
import in.innasoft.kshatriyayouth.itemClickListerners.AreaListItemClickListener;
import in.innasoft.kshatriyayouth.models.AreaModel;

public class AreaListAdapter extends RecyclerView.Adapter<AreaListHolder> implements Filterable {

    public ArrayList<AreaModel> areaArrayList, filterList;
    public FilterActivity context;
    CustomFilterForAreaList filter;
    LayoutInflater li;
    int resource;
//    Typeface typeface;

    public AreaListAdapter(ArrayList<AreaModel> areaArrayList, FilterActivity context, int resource) {
        this.areaArrayList = areaArrayList;
        this.filterList = areaArrayList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForAreaList(filterList, this);
        }

        return filter;
    }

    @Override
    public AreaListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        AreaListHolder slh = new AreaListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(AreaListHolder holder, int position) {


//        holder.row_area_txt.setTypeface(typeface);
        holder.row_area_txt.setText(Html.fromHtml(areaArrayList.get(position).getArea_name()));

        holder.setItemClickListener(new AreaListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setArea(areaArrayList.get(pos).getArea_name(), areaArrayList.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.areaArrayList.size();
    }


}
