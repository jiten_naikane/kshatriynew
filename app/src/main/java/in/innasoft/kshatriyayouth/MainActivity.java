package in.innasoft.kshatriyayouth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.activities.ChangePasswordPage;
import in.innasoft.kshatriyayouth.activities.EditProfileActivity;
import in.innasoft.kshatriyayouth.activities.FeedBackActivity;
import in.innasoft.kshatriyayouth.activities.FriendsBirthdayActivity;
import in.innasoft.kshatriyayouth.activities.LoginPage;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;
import in.innasoft.kshatriyayouth.activities.ProfileAnnouncementsActivity;
import in.innasoft.kshatriyayouth.activities.ProfileAssociationsActivity;
import in.innasoft.kshatriyayouth.activities.ProfileBlogsActivity;
import in.innasoft.kshatriyayouth.activities.ProfileBusinessDirectoryActivity;
import in.innasoft.kshatriyayouth.activities.ProfileCauseOrDonationActivity;
import in.innasoft.kshatriyayouth.activities.ProfileGroupsActivity;
import in.innasoft.kshatriyayouth.activities.ProfileInformationDirectoryActivity;
import in.innasoft.kshatriyayouth.activities.ProfileJobsActivity;
import in.innasoft.kshatriyayouth.activities.SearchingActivity;
import in.innasoft.kshatriyayouth.fragments.FriendsFragment;
import in.innasoft.kshatriyayouth.fragments.HomeFragment;
import in.innasoft.kshatriyayouth.fragments.NotificationsFragment;
import in.innasoft.kshatriyayouth.models.NotificationCountModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.BadgeView;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    private ViewPager viewPager;
    ImageView left_menu, left_menu1, profilePic, profilefullimage, home_tab_img, friend_tab_img, bell_tab_img, friend_dob_list;
    FloatingActionButton fab_birthday;
    DrawerLayout drawer;
    private TabLayout tabLayout;
    String notification_count = "0", notify;
    TextView count_notification_text, compeny_name;
    RelativeLayout searching_layout, profilelayout;
    private Boolean exit = false;
    int[] tabIcons = {
            R.drawable.ic_home_gray,
            R.drawable.ic_friends_add_gray,
            R.drawable.ic_bell_gray,
            R.drawable.ic_speaker_gray
    };
    BottomSheetDialog bottomSheetDialog;
    boolean doubleBackToExitPressedOnce = false;
    String profile_pic_url = "";
    String profile_name = "";
    String profile_membership = "";
    private ActionBarDrawerToggle mDrawerToggle;
    LinearLayout about, groups, associations, jobs, blogs, informationdirectory, businessdirectory, causeordonation, announcements, reset_password, feedback, share_lyout, logout;
    UserSessionManager sessionManager;
    TextView username, membership, about_text, groups_text, association_text, jobs_text, blogs_text, infor_directory_textView, busines_directory_text, cause_donation_text,
            announce_text, feedback_text, share_text, logout_text, birthday_count_txt;
    //    Typeface typeface;
    private BadgeView badge;
    String count = "";
    private boolean checkInternet;
    UserSessionManager session;
    String userid = "", user_profile_id = "";
    LinearLayout linearLayoutOne, linearLayout2;
    RelativeLayout linearLayout3;
    View headerView;
    String ids;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));
        sessionManager = new UserSessionManager(MainActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        profile_name = userDetails.get(UserSessionManager.USER_NAME);
        profile_membership = userDetails.get(UserSessionManager.USER_MEMBERSHIP);
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);

        Log.d("dsfgsdfsdf", profile_pic_url);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        left_menu = (ImageView) findViewById(R.id.left_menu);
        left_menu1 = (ImageView) findViewById(R.id.left_menu1);
        searching_layout = (RelativeLayout) findViewById(R.id.searching_layout);
        notificationData();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });
//...


        home_tab_img = (ImageView) findViewById(R.id.home_tab_img);
        friend_tab_img = (ImageView) findViewById(R.id.friend_tab_img);
        bell_tab_img = (ImageView) findViewById(R.id.bell_tab_img);

        friend_dob_list = (ImageView) findViewById(R.id.friend_dob_list);
        friend_dob_list.setOnClickListener(this);

        fab_birthday = (FloatingActionButton) findViewById(R.id.fab_birthday);
        fab_birthday.setOnClickListener(this);

        birthday_count_txt = (TextView) findViewById(R.id.birthday_count_txt);
        getDOBWishes();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        // setupTabIcons();
        //////////////////////////onClick
        searching_layout.setOnClickListener(this);

        headerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_tab_layout_home, null, false);

        linearLayoutOne = (LinearLayout) headerView.findViewById(R.id.linear_home);
        linearLayout2 = (LinearLayout) headerView.findViewById(R.id.linear_friends);
        linearLayout3 = (RelativeLayout) headerView.findViewById(R.id.linear_notification);

        home_tab_img = (ImageView) headerView.findViewById(R.id.home_tab_img);
        friend_tab_img = (ImageView) headerView.findViewById(R.id.friend_tab_img);
        bell_tab_img = (ImageView) headerView.findViewById(R.id.bell_tab_img);
        count_notification_text = (TextView) headerView.findViewById(R.id.count_notification_text);

        tabLayout.getTabAt(0).setCustomView(linearLayoutOne);
        tabLayout.getTabAt(1).setCustomView(linearLayout2);
        tabLayout.getTabAt(2).setCustomView(linearLayout3);

        home_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
        friend_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
        bell_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));


        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 0) {
                            home_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
                            friend_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
                            bell_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
                        } else if (tab.getPosition() == 1) {
                            home_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
                            friend_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
                            bell_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
                        } else if (tab.getPosition() == 2) {
                            home_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
                            friend_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.brown));
                            bell_tab_img.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
                            count_notification_text.setVisibility(View.GONE);

                            getNotificationRead();
                        }

                      /*  int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.white);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);*/
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                       /* int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.white);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);*/
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
        left_menu.setOnClickListener(this);
        left_menu1.setOnClickListener(this);
        ////////////////////////////////////headerview
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        profilelayout = (RelativeLayout) header.findViewById(R.id.profilelayout);
        profilePic = (ImageView) header.findViewById(R.id.profilePic);

        Picasso.with(MainActivity.this)
                .load(profile_pic_url)
                .into(profilePic);
        profilefullimage = (ImageView) header.findViewById(R.id.profilefullimage);

        Picasso.with(MainActivity.this)
                .load(profile_pic_url)
                .placeholder(R.drawable.user_profile)
                .into(profilefullimage);

        username = (TextView) header.findViewById(R.id.username);
        username.setText(profile_name);
//        username.setTypeface(typeface);

        membership = (TextView) header.findViewById(R.id.membership);
        membership.setText(profile_membership);


//        membership.setTypeface(typeface);
        /* ,,,,,,,
                ,,;*/

        about_text = (TextView) header.findViewById(R.id.about_text);
//        about_text.setTypeface(typeface);

        groups_text = (TextView) header.findViewById(R.id.groups_text);
//        groups_text.setTypeface(typeface);

        association_text = (TextView) header.findViewById(R.id.association_text);
//        association_text.setTypeface(typeface);

        jobs_text = (TextView) header.findViewById(R.id.jobs_text);
//        jobs_text.setTypeface(typeface);

        blogs_text = (TextView) header.findViewById(R.id.blogs_text);
//        blogs_text.setTypeface(typeface);

        infor_directory_textView = (TextView) header.findViewById(R.id.infor_directory_textView);
//        infor_directory_textView.setTypeface(typeface);

        busines_directory_text = (TextView) header.findViewById(R.id.busines_directory_text);
//        busines_directory_text.setTypeface(typeface);

        cause_donation_text = (TextView) header.findViewById(R.id.cause_donation_text);
//        cause_donation_text.setTypeface(typeface);

        announce_text = (TextView) header.findViewById(R.id.announce_text);
//        announce_text.setTypeface(typeface);

        logout_text = (TextView) header.findViewById(R.id.logout_text);
//        logout_text.setTypeface(typeface);

        feedback_text = (TextView) header.findViewById(R.id.feedback_text);
        share_text = (TextView) header.findViewById(R.id.share_text);
//        feedback_text.setTypeface(typeface);


        about = (LinearLayout) header.findViewById(R.id.about);
        groups = (LinearLayout) header.findViewById(R.id.groups);
        associations = (LinearLayout) header.findViewById(R.id.associations);
        jobs = (LinearLayout) header.findViewById(R.id.jobs);
        blogs = (LinearLayout) header.findViewById(R.id.blogs);
        informationdirectory = (LinearLayout) header.findViewById(R.id.informationdirectory);
        businessdirectory = (LinearLayout) header.findViewById(R.id.businessdirectory);
        causeordonation = (LinearLayout) header.findViewById(R.id.causeordonation);
        announcements = (LinearLayout) header.findViewById(R.id.announcements);
        logout = (LinearLayout) header.findViewById(R.id.logout);
        feedback = (LinearLayout) header.findViewById(R.id.feedback);
        share_lyout = (LinearLayout) header.findViewById(R.id.share_lyout);
        reset_password = (LinearLayout) header.findViewById(R.id.reset_password);
        compeny_name = (TextView) header.findViewById(R.id.compeny_name);
        compeny_name.setText(Html.fromHtml("Developed by " + "<b>Innasoft Technologies Pvt Ltd.</b>"));
        about.setOnClickListener(this);
        groups.setOnClickListener(this);
        associations.setOnClickListener(this);
        jobs.setOnClickListener(this);
        blogs.setOnClickListener(this);
        informationdirectory.setOnClickListener(this);
        businessdirectory.setOnClickListener(this);
        causeordonation.setOnClickListener(this);
        announcements.setOnClickListener(this);
        profilelayout.setOnClickListener(this);
        reset_password.setOnClickListener(this);
        logout.setOnClickListener(this);
        feedback.setOnClickListener(this);
        share_lyout.setOnClickListener(this);


        //getCountFriends();
    }

    private void getDOBWishes() {
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.DATE_OF_BIRTH_WHISHES;
            Log.d("DOB", url);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("DOBURLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    String count = jsonObject.getString("count");
                                    if (count.equals("0") || count.equals(0) || count.equals("null") || count.equals(null)) {
                                        birthday_count_txt.setVisibility(View.GONE);
                                    } else {
                                        birthday_count_txt.setVisibility(View.VISIBLE);
                                        birthday_count_txt.setText(count);
                                    }
                                }

                                if (responceCode.equals("10200")) {
                                    Toast.makeText(MainActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    Log.d("DOBPARAM::", params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


    private void setupTabIcons() {
       /* tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);*/

     /*   tabLayout.getTabAt(0).setCustomView(linearLayoutOne);
        tabLayout.getTabAt(1).setCustomView(linearLayout2);
        tabLayout.getTabAt(2).setCustomView(linearLayout3);*/


//        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), "");
        adapter.addFrag(new FriendsFragment(), "");
        adapter.addFrag(new NotificationsFragment(), "");
        viewPager.setAdapter(adapter);

    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawer(Gravity.LEFT);
        } else {
            drawer.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == searching_layout) {
            Intent intent = new Intent(MainActivity.this, SearchingActivity.class);
            startActivity(intent);
        }
        if (v == left_menu) {
            //  showBottomSheet();

            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);
            } else {
                drawer.openDrawer(Gravity.LEFT);
            }
          /*  Animation RightSwipe = AnimationUtils.loadAnimation(MainActivity.this, R.anim.righttoleft);
            drawer.startAnimation(RightSwipe);*/
           /* if(!drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.openDrawer(Gravity.RIGHT);

            }
            else {
                drawer.closeDrawer(Gravity.RIGHT);
            }*/


        }
        if (v == left_menu1) {

            left_menu.setVisibility(View.VISIBLE);
            left_menu1.setVisibility(View.GONE);
            drawer.closeDrawer(Gravity.LEFT);
           /* Animation RightSwipe = AnimationUtils.loadAnimation(MainActivity.this, R.anim.lefttoright);
            drawer.startAnimation(RightSwipe);*/
            /*if(drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.closeDrawer(Gravity.RIGHT);

            }
            else {
                drawer.openDrawer(Gravity.RIGHT);
            }
            */


        }
        if (v == about) {
            Intent intent = new Intent(this, EditProfileActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == profilelayout) {
            Intent intent = new Intent(this, MyProfileActivity.class);
            intent.putExtra("user_id", user_profile_id);
            startActivity(intent);
            closeDrawer();
        }
        if (v == groups) {
            // Intent intent = new Intent(this, ProfileGroupsActivity.class);
            Intent intent = new Intent(this, ProfileGroupsActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == associations) {
            Intent intent = new Intent(this, ProfileAssociationsActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == jobs) {
            Intent intent = new Intent(this, ProfileJobsActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == blogs) {
            Intent intent = new Intent(this, ProfileBlogsActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == informationdirectory) {
            Intent intent = new Intent(this, ProfileInformationDirectoryActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == businessdirectory) {
            Intent intent = new Intent(this, ProfileBusinessDirectoryActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == causeordonation) {
            Intent intent = new Intent(this, ProfileCauseOrDonationActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == announcements) {
            Intent intent = new Intent(this, ProfileAnnouncementsActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == reset_password) {
            Intent intent = new Intent(this, ChangePasswordPage.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == feedback) {
            Intent intent = new Intent(this, FeedBackActivity.class);
            startActivity(intent);
            closeDrawer();
        }
        if (v == share_lyout) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "KshatriyaS");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.innasoft.kshatriyayouth&hl=en");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
            closeDrawer();
        }
        if (v == logout) {
            sessionManager.logoutUser();
            Intent intent = new Intent(getApplicationContext(), LoginPage.class);
            startActivity(intent);
            finish();
            closeDrawer();
        }


        if (v == fab_birthday) {
            Intent intent = new Intent(this, FriendsBirthdayActivity.class);
            startActivity(intent);
        }
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.filter:

                if (!drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.openDrawer(Gravity.LEFT);
                    drawer.setVisibility(View.GONE);
                } else {
                    drawer.closeDrawer(Gravity.LEFT);

                }
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawer(Gravity.LEFT);
        } else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                //actionMenu.close(true);
                Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }

    }

    private void showBottomSheet() {
        bottomSheetDialog = new BottomSheetDialog(this);

        View view = MainActivity.this.getLayoutInflater().inflate(R.layout.fragment_shout_box, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();

    }

    private void getCountFriends() {

        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.NOTIFICATIONS_LIST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    //  context.feedItemFriendRequests();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("notification_count");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        NotificationCountModel notif = new NotificationCountModel();

                                        // notif.setId();
                                        //  notif.setCount();


                                        count = jsonObject2.getString("count");


                                    }
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    public void notificationData() {

        Log.d("sssssssssss", AppUrls.BASE_URL + AppUrls.NOTIFICATION_COUNT + "/" + user_profile_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.NOTIFICATION_COUNT + "/" + user_profile_id,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.d("RESSSSSCOUNTTT:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("10100")) {

                                JSONObject jobdata = jsonObject.getJSONObject("data");
                                JSONArray jarrnot = jobdata.getJSONArray("notification_count");
                                JSONObject obj = jarrnot.getJSONObject(0);
                                notification_count = obj.getString("count");
                                ids = obj.getString("ids");

                                /*for(int i = 0; i < jarrnot.length(); i++)
                                {


                                }*/

                                notify = notification_count;
                                if (!notification_count.equals("0")) {
                                    count_notification_text.setText(notify);
                                } else {
                                    count_notification_text.setVisibility(View.GONE
                                    );
                                }
                                //  tabLayout.getTabAt(2).setText(notify);
                                Log.d("COUNTTT:", notify);

                            }
                            /*if (status.equals("10200"))
                            {

                            }
                            if (status.equals("5000")){

                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                        } else if (error instanceof AuthFailureError) {
                            //TODO
                        } else if (error instanceof ServerError) {
                            //TODO
                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {
                            //TODO
                        }
                    }
                }
        ) {
/*
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();


                return params;
            }
*/
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    private void getNotificationRead() {


        Log.d("sssssssssss", AppUrls.BASE_URL + AppUrls.NOTIFICATION_SET_READ);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.NOTIFICATION_SET_READ,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.d("RESSSSSREAD:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("10100")) {

                               /* JSONObject jobdata=jsonObject.getJSONObject("data");
                                JSONArray jarrnot=jobdata.getJSONArray("notification_count");

                                for(int i = 0; i < jarrnot.length(); i++)
                                {
                                    JSONObject obj = jarrnot.getJSONObject(i);
                                    notification_count=obj.getString("count");
                                }

                                notify=notification_count;
                                count_notification_text.setText(notify);
                                //  tabLayout.getTabAt(2).setText(notify);
                                Log.d("COUNTTT:",notify);
*/
                            }
                            /*if (status.equals("10200"))
                            {

                            }
                            if (status.equals("5000")){

                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                        } else if (error instanceof AuthFailureError) {
                            //TODO
                        } else if (error instanceof ServerError) {
                            //TODO
                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {
                            //TODO
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_profile_id", user_profile_id);
                params.put("ids", ids);
                Log.d("COUNTTTsssss:", "" + params);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }

   /* private void getDobWishes()
    {
        Toast.makeText(MainActivity.this,"CALLING CALLING",Toast.LENGTH_LONG).show();
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if(checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.DATE_OF_BIRTH_WHISHES ;
            Log.d("DOB", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("DOBURLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100"))
                                {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jarrFreindDob=jsonObject1.getJSONArray("friendsBirthdayData");
                                    for (int i = 0; i < jarrFreindDob.length(); i++)
                                    {
                                        JSONObject jsonObject2 = jarrFreindDob.getJSONObject(i);

                                        DOBModel item = new DOBModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name")+" "+jsonObject2.getString("surname"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        Log.d("image_photo",image);
                                        item.setPhoto(image);
                                        item.setUsername(jsonObject2.getString("username"));


                                      //  feeditem.add(item);
                                    }


                                }
                                if (responceCode.equals("10800"))
                                {

                                }
                                if(responceCode.equals("10200"))
                                {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    Log.d("DOBPARAM::", params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        }
        else {
            Toast.makeText(MainActivity.this,"No Internet Connection...!",Toast.LENGTH_LONG).show();
        }



    }*/

}