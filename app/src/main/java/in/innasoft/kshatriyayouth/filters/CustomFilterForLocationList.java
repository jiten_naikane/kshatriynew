package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.LocationAdapter;
import in.innasoft.kshatriyayouth.models.LocationModel;

public class CustomFilterForLocationList extends Filter {

    LocationAdapter adapter;
    ArrayList<LocationModel> filterList;

    public CustomFilterForLocationList(ArrayList<LocationModel> filterList, LocationAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<LocationModel> filteredPlayers = new ArrayList<LocationModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getSurname().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.surnameArrayList = (ArrayList<LocationModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
