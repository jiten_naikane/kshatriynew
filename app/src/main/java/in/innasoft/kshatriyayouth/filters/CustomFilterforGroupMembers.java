package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.GroupMembersAdapter;
import in.innasoft.kshatriyayouth.models.GroupMembersModel;

public class CustomFilterforGroupMembers extends Filter {

    GroupMembersAdapter adapter;
    ArrayList<GroupMembersModel> filterList;

    public CustomFilterforGroupMembers(ArrayList<GroupMembersModel> filterList, GroupMembersAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<GroupMembersModel> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.gmemberListItem = (ArrayList<GroupMembersModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
