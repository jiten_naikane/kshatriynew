package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.PostCheckInCountryAdapter;
import in.innasoft.kshatriyayouth.models.CheckInCountriesModel;


/**
 * Created by purushotham on 27/1/17.
 */

public class PostCheckedInCustomFilterForCountries extends Filter {

    PostCheckInCountryAdapter adapter;
    ArrayList<CheckInCountriesModel> filterList;

    public PostCheckedInCustomFilterForCountries(ArrayList<CheckInCountriesModel> filterList, PostCheckInCountryAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<CheckInCountriesModel> filteredPlayers = new ArrayList<CheckInCountriesModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getCountry_name().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.checkInCountriesArrayList = (ArrayList<CheckInCountriesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
