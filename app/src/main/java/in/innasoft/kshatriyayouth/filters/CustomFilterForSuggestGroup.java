package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.SuggestGroupListAdapter;
import in.innasoft.kshatriyayouth.models.SuggestGroupModel;

public class CustomFilterForSuggestGroup extends Filter {
    SuggestGroupListAdapter adapter;
    ArrayList<SuggestGroupModel> filterList;

    public CustomFilterForSuggestGroup(ArrayList<SuggestGroupModel> filterList, SuggestGroupListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<SuggestGroupModel> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getGroup_name().toUpperCase().contains(constraint))// || filterList.get(i).getShow_lab_list_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.suggestgroupitem = (ArrayList<SuggestGroupModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
