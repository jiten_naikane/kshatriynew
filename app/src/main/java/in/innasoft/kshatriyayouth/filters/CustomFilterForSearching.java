package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.SearchingAdapter;
import in.innasoft.kshatriyayouth.models.SearchingModel;

public class CustomFilterForSearching extends Filter {
    SearchingAdapter adapter;
    ArrayList<SearchingModel> filterList;

    public CustomFilterForSearching(ArrayList<SearchingModel> filterList, SearchingAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<SearchingModel> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getName().toUpperCase().contains(constraint))// || filterList.get(i).getShow_lab_list_address().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.feeditem = (ArrayList<SearchingModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
