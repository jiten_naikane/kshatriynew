package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.EditProfileCityAdapter;
import in.innasoft.kshatriyayouth.models.CityModel;


public class EditProfileCustomFilterForCityList extends Filter {

    EditProfileCityAdapter adapter;
    ArrayList<CityModel> filterList;

    public EditProfileCustomFilterForCityList(ArrayList<CityModel> filterList, EditProfileCityAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<CityModel> filteredPlayers = new ArrayList<CityModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.cityModels = (ArrayList<CityModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
