package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.MultiCompletMemberGroupAdapter;
import in.innasoft.kshatriyayouth.models.AutoCompletGroupCrationModel;

/**
 * Created by Devolper on 29-Aug-17.
 */

public class MemberMultiFilterForSurnameList extends Filter {
    MultiCompletMemberGroupAdapter adapter;
    ArrayList<AutoCompletGroupCrationModel> filterList;

    public MemberMultiFilterForSurnameList(ArrayList<AutoCompletGroupCrationModel> filterList, MultiCompletMemberGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<AutoCompletGroupCrationModel> filteredPlayers = new ArrayList<AutoCompletGroupCrationModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getSurname().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        // adapter.feeditem = (ArrayList<AutoCompletGroupCrationModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
