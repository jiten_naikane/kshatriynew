package in.innasoft.kshatriyayouth.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.adapters.EditProfileCountryAdapter;
import in.innasoft.kshatriyayouth.models.CountryModel;


public class EditProfileCustomFilterForCountryList extends Filter {

    EditProfileCountryAdapter adapter;
    ArrayList<CountryModel> filterList;

    public EditProfileCustomFilterForCountryList(ArrayList<CountryModel> filterList, EditProfileCountryAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<CountryModel> filteredPlayers = new ArrayList<CountryModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getName().toUpperCase().contains(constraint)) //|| filterList.get(i).getCountry_iso_code().toUpperCase().contains(constraint) || filterList.get(i).getCountry_isd_code().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.countryModelArrayList = (ArrayList<CountryModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
