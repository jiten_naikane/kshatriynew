package in.innasoft.kshatriyayouth.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.GroupPhotoListAdapter;
import in.innasoft.kshatriyayouth.models.GroupPhotoListModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupPhotosFragment extends Fragment {
    View view;
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ArrayList<String> imgArray = new ArrayList<String>();
    GroupPhotoListAdapter gphotoListAdapter;
    ArrayList<GroupPhotoListModel> groupphotoListItem = new ArrayList<GroupPhotoListModel>();
    RecyclerView group_photo_recyclerview;
    ImageView nodata_photo_image;
    String group_id, userid;

    //http://kshatriyayouth.com/group_photos/1441472409392509
    public GroupPhotosFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_group_photos, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        group_id = getActivity().getIntent().getStringExtra("GROUP_ID");
        Log.d("groupid:", group_id);

        nodata_photo_image = (ImageView) view.findViewById(R.id.nodata_photo_image);

        group_photo_recyclerview = (RecyclerView) view.findViewById(R.id.group_photo_recyclerview);
        group_photo_recyclerview.setHasFixedSize(true);
        gphotoListAdapter = new GroupPhotoListAdapter(groupphotoListItem, GroupPhotosFragment.this, R.layout.row_group_photo_fragment);
        group_photo_recyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        group_photo_recyclerview.setNestedScrollingEnabled(false);
        group_photo_recyclerview.setSaveFromParentEnabled(true);

        getAllGroupPhoto();

        return view;
    }

    private void getAllGroupPhoto() {

        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            String groupid = "1441472409392509";
            groupphotoListItem.clear();
            //  String url = AppUrls.BASE_URL+AppUrls.GROUP_PHOTOS+"/"+group_id;
            String url = AppUrls.BASE_URL + AppUrls.GROUP_PHOTOS + "/" + group_id;
            Log.d("photolisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("photolistuRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_photo_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                    nodata_photo_image.setVisibility(View.GONE);

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupPhotos");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        GroupPhotoListModel item = new GroupPhotoListModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setPost_id(jsonObject2.getString("post_id"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        String image = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + jsonObject2.getString("photo_link");


                                        //   imgArray.add(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + jsonObject2.getString("photo_link"));
                                        // imgArray.add(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + jsonObject2.getString("photo_link"));//IMAGE ARRAY
                                        item.setPhoto_link(image);
                                        item.setDate(jsonObject2.getString("date"));
                                        item.setPhoto_identifier(jsonObject2.getString("photo_identifier"));

                                        groupphotoListItem.add(item);
                                    }


                                    group_photo_recyclerview.setAdapter(gphotoListAdapter);


                                }

                                if (responceCode.equals("10200")) {
                                    group_photo_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_photo_image.setVisibility(View.VISIBLE);


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_photo_image.setVisibility(View.VISIBLE);


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    nodata_photo_image.setVisibility(View.VISIBLE);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

}
