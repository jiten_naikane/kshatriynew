package in.innasoft.kshatriyayouth.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.AllAssociationsAdapter;
import in.innasoft.kshatriyayouth.models.AllAssociationsModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FixedSwipeRefreshLayout;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class AllAssociations extends Fragment {

    View rootview;
    private static final String TAG = MainActivity.class.getSimpleName();
    RecyclerView recyclerview;
    AllAssociationsAdapter adapter;
    ArrayList<AllAssociationsModel> feeditem = new ArrayList<AllAssociationsModel>();
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    int total_number_of_items = 0;
    private boolean userScrolled = true;
    private static int displayedposition = 0;
    RelativeLayout bottomLayout;
    int type_of_request = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;
    ProgressDialog progressDialog;
    ImageView nodata_image;
    FixedSwipeRefreshLayout swipeView;

    public AllAssociations() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragments_allassociations, container, false);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        swipeView = (FixedSwipeRefreshLayout) rootview.findViewById(R.id.swipe);
        nodata_image = (ImageView) rootview.findViewById(R.id.nodata_image);
        bottomLayout = (RelativeLayout) rootview.findViewById(R.id.loadItemsLayout_recyclerView);
        recyclerview = (RecyclerView) rootview.findViewById(R.id.recylerview);
        swipeView.setRecyclerView(recyclerview);
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(layoutManager);
        feeditem.clear();
        adapter = new AllAssociationsAdapter(feeditem, AllAssociations.this, R.layout.allassociations_row);
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                feedItem(defaultPageNo);

                            } else {
                                Toast.makeText(getContext(), "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

        });

        swipeView.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_light);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        feeditem.clear();
                        defaultPageNo = 1;
                        displayedposition = 0;
                        feedItem(defaultPageNo);
                    }
                }, 3000);
            }
        });
        feedItem(defaultPageNo);

        return rootview;

    }

    private void feedItem(int pagenumber) {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {
            if (type_of_request == 0) {
                progressDialog.show();
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
            }
            String url = AppUrls.BASE_URL + AppUrls.ASSOCIATIONS + "/10/" + pagenumber;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.d("URLRESPONCE", response);
                            try {
                                throw new IOException();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    swipeView.setRefreshing(false);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.GONE);

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    int total_numberof_records = Integer.valueOf(jsonObject1.getString("total_records"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + total_numberof_records);
                                    if (total_numberof_records > feeditem.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + total_numberof_records);
                                    } else {
                                        loading = false;
                                    }
                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        AllAssociationsModel item = new AllAssociationsModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name"));
                                        String image = AppUrls.BASE_URL + jsonObject2.getString("image");
                                        item.setImage(image);
                                        item.setUrl_name(jsonObject2.getString("url_name"));
                                        item.setDescription(jsonObject2.getString("description"));
                                        item.setArea_name(jsonObject2.getString("area_name"));
                                        item.setCity_name(jsonObject2.getString("city_name"));
                                        item.setCreate_date_time(jsonObject2.getString("create_date_time"));

                                        feeditem.add(item);
                                    }

                                    layoutManager.scrollToPositionWithOffset(displayedposition, feeditem.size());
                                    recyclerview.setAdapter(adapter);
                                    bottomLayout.setVisibility(View.GONE);
                                }

                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {
                                    swipeView.setRefreshing(false);
                                    recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.VISIBLE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_image.setVisibility(View.VISIBLE);

                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    nodata_image.setVisibility(View.VISIBLE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
