package in.innasoft.kshatriyayouth.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.LocationAdapter;
import in.innasoft.kshatriyayouth.models.LocationModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class Donation extends Fragment implements View.OnClickListener {
    private ArrayAdapter<String> adapter;
    View rootview;
    EditText donorname, amount, remarks;
    TextView location, purpusofdonation;
    String send_donation;
    Button submit;
    private static final String TAG = MainActivity.class.getSimpleName();
    String surname_txt = "";
    String item[] = {"Education", "Health", "Orphan Child", "Old Age Home"};

    ////////////////////////////////////
    LocationAdapter surnameListAdapter;
    ArrayList<LocationModel> surnameModels = new ArrayList<LocationModel>();
    ArrayList<String> surname_list = new ArrayList<String>();
    AlertDialog dialog;
    ProgressDialog progressDialog;
    private boolean checkInternet;

    public Donation() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragment_donation, container, false);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        donorname = (EditText) rootview.findViewById(R.id.donorname);
        amount = (EditText) rootview.findViewById(R.id.amount);
        remarks = (EditText) rootview.findViewById(R.id.remarks);
        purpusofdonation = (TextView) rootview.findViewById(R.id.purpusofdonation);
        purpusofdonation.setOnClickListener(this);
        submit = (Button) rootview.findViewById(R.id.submit);
        location = (TextView) rootview.findViewById(R.id.location);
        location.setOnClickListener(this);
        submit.setOnClickListener(this);

        return rootview;
    }

    @Override
    public void onClick(View v) {
        if (v == location) {
            getLocation();
        }
        if (v == submit) {
            createDonation();
        }

        if (v == purpusofdonation) {
            final String[] listValue;
            listValue = getResources().getStringArray(R.array.donation_type);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_donation_type_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            alertDialog.setTitle("Donation Type");
            final ListView lv_donation_type = (ListView) convertView.findViewById(R.id.lv_donation_type);
            final Button btn_donation_type_cancel = (Button) convertView.findViewById(R.id.btn_donation_type_cancel);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listValue);
            lv_donation_type.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_donation_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    purpusofdonation.setText(listValue[arg2]);
                    dialog.dismiss();
                }
            });
            btn_donation_type_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    }

    private void getLocation() {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {
            surname_list.clear();
            surnameModels.clear();
            progressDialog.show();
            String url = AppUrls.BASE_URL + "location_app";
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SURNAMERESPONSE:", response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("locationData");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        LocationModel surnameModel = new LocationModel();
                                        surnameModel.setId(jsonObject2.getString("id"));
                                        String surname_name = jsonObject2.getString("name");
                                        surnameModel.setSurname(surname_name);

                                        surname_list.add(surname_name);
                                        surnameModels.add(surnameModel);
                                    }
                                    getSurnameListDialog();
                                } else {
                                    progressDialog.dismiss();
                                }
                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getSurnameListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.surname_list_dialog, null);

        TextView surName_list_txt = (TextView) dialog_layout.findViewById(R.id.surName_list_txt);
        surName_list_txt.setText("Select Location");

        final SearchView surName_search = (SearchView) dialog_layout.findViewById(R.id.surName_search);
        EditText searchEditText = (EditText) surName_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView surName_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.surName_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        surName_recycler_view.setLayoutManager(layoutManager);

        surnameListAdapter = new LocationAdapter(surnameModels, Donation.this, R.layout.row_surname_list);

        surName_recycler_view.setAdapter(surnameListAdapter);

        surName_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                surName_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        surName_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                surnameListAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setSurName(String surname) {

        dialog.dismiss();
        surnameListAdapter.notifyDataSetChanged();
        location.setError(null);
        location.setText(surname);

    }

    private void createDonation() {

        if (validate()) {

            checkInternet = NetworkChecking.isConnected(getContext());
            if (checkInternet) {

                String url = AppUrls.BASE_URL + AppUrls.DONATION_ADD;
                Log.d("SENDURLRESPONCE", url);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("SENDURLRESPONCE", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Log.d("URLRESPONCE", response);

                                    String responceCode = jsonObject.getString("status");
                                    if (responceCode.equals("10100")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getContext(), "Successfully Donated..!", Toast.LENGTH_SHORT).show();
                                        purpusofdonation.setText("");
                                        donorname.setText("");
                                        remarks.setText("");
                                        location.setText("");
                                        amount.setText("");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ServerError) {
                        } else if (error instanceof NetworkError) {
                        } else if (error instanceof ParseError) {
                        }
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();

                        params.put("purpose_donation", purpusofdonation.getText().toString());
                        params.put("name", donorname.getText().toString());
                        params.put("remarks", remarks.getText().toString());
                        params.put("location", location.getText().toString());
                        params.put("amount", amount.getText().toString());
                        Log.d("SENDURLRESPONCE", "SENDDONATION " + params.toString());
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(stringRequest);
            } else {
                Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Please Provide All Details...!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validate() {

        boolean result = true;

        String d_name = donorname.getText().toString().trim();
        if ((d_name == null || d_name.equals("") || d_name.length() < 3) || d_name == "^[a-zA-Z\\\\s]+") {
            donorname.setError("Invalid Name");
            result = false;
        }
        String d_amount = amount.getText().toString().trim();
        if ((d_amount == null || d_amount.equals("") || d_amount.length() < 3) || d_amount == "^[a-zA-Z\\\\s]+") {
            amount.setError("Invalid Amount");
            result = false;
        }
        String d_reamrks = remarks.getText().toString().trim();
        if ((d_reamrks == null || d_reamrks.equals("") || d_reamrks.length() < 3) || d_reamrks == "^[a-zA-Z\\\\s]+") {
            remarks.setError("Invalid Remarks");
            result = false;
        }
        String d_location = location.getText().toString().trim();
        if ((d_location == null || d_location.equals(""))) {
            location.setError("Select Location");
            result = false;
        }
        String d_donrtype = purpusofdonation.getText().toString().trim();
        if ((d_donrtype == null || d_donrtype.equals(""))) {
            purpusofdonation.setError("Select Donation Type");
            result = false;
        }

        return result;
    }
}
