package in.innasoft.kshatriyayouth.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.AssociationEventAdapter;
import in.innasoft.kshatriyayouth.models.Association_EventModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class AssociationEventFragment extends Fragment {
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ImageView no_data;
    String userid, asociation_id;
    AssociationEventAdapter asociationeventAdapter;
    ArrayList<Association_EventModel> assoeventModels = new ArrayList<Association_EventModel>();
    LinearLayoutManager layoutManager;
    RecyclerView association_event_recyclerview;
    RelativeLayout bottomLayout;
    View view;
    private boolean userScrolled = true;
    int defaultPageNo = 1;
    int type_of_request = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    int total_number_of_items = 0;
    private static int displayedposition = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_association_event, container, false);

//        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        asociation_id = getActivity().getIntent().getStringExtra("ASSOCIATION_ID");
        Log.d("ASSSSSIDDDDDDD", asociation_id);

        no_data = (ImageView) view.findViewById(R.id.no_data);
        association_event_recyclerview = (RecyclerView) view.findViewById(R.id.association_event_recyclerview);
        bottomLayout = (RelativeLayout) view.findViewById(R.id.loadItemsLayout_recyclerView);


        association_event_recyclerview.setHasFixedSize(true);
        asociationeventAdapter = new AssociationEventAdapter(assoeventModels, this, R.layout.row_association_event);
        layoutManager = new LinearLayoutManager(getActivity());
        association_event_recyclerview.setLayoutManager(layoutManager);
        association_event_recyclerview.setItemAnimator(new DefaultItemAnimator());
        // association_post_recyclerview.setAdapter(asociationPostAdapter);
        association_event_recyclerview.setNestedScrollingEnabled(false);

        association_event_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("POSTTTTt", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("POSTTTTT", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                getAssociationEventData(defaultPageNo);
                                // feedItem("0",defaultPageNo);

                            } else {
                                Toast.makeText(getActivity(), "No More Results", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }


                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }
        });
        //  feedItem("0",defaultPageNo);

        getAssociationEventData(defaultPageNo);

        return view;
    }

    private void getAssociationEventData(int pagenumber) {


        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            if (type_of_request == 0) {
                progressDialog.show();
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
            }
            assoeventModels.clear();
            progressDialog.show();             //http://kshatriyayouth.com/associations_events/10/1/2/486
            final String url = AppUrls.BASE_URL + AppUrls.ASSOCIATIONS_EVENT + "/" + "10" + "/" + pagenumber + "/" + asociation_id + "/" + userid;
            Log.d("assPostlURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.ASSOCIATIONS_EVENT + "/" + "10" + "/" + pagenumber + "/" + asociation_id + "/" + userid,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASSOPOSTRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    int total_numberof_records = Integer.valueOf(jsonObject1.getString("total_recs"));
                                    if (total_numberof_records == 0) {
                                        no_data.setVisibility(View.VISIBLE);
                                    }
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + total_numberof_records);
                                    if (total_numberof_records > assoeventModels.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + total_numberof_records);
                                    } else {
                                        loading = false;
                                    }

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        Association_EventModel item = new Association_EventModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name"));
                                        item.setDescription(jsonObject2.getString("description"));
                                        item.setVenue(jsonObject2.getString("venue"));
                                        item.setStart_date(jsonObject2.getString("start_date"));
                                        item.setEnd_date(jsonObject2.getString("end_date"));

                                        String image = AppUrls.BASE_URL + jsonObject2.getString("image");
                                        item.setImage(image);

                                        assoeventModels.add(item);
                                    }

                                    association_event_recyclerview.setAdapter(asociationeventAdapter);
                                    bottomLayout.setVisibility(View.GONE);

                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_data.setVisibility(View.VISIBLE);
                                    bottomLayout.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            no_data.setVisibility(View.VISIBLE);
                            bottomLayout.setVisibility(View.GONE);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_SHORT).show();
        }
    }

}
