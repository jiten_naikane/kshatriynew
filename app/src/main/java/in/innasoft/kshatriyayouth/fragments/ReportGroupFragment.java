package in.innasoft.kshatriyayouth.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportGroupFragment extends Fragment implements View.OnClickListener {
    View view;
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String userid;
    EditText report_body_edt;
    TextView report_text_title, report_text, send_report_button;

    public ReportGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_report_group, container, false);

//        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();

        userid = userDetails.get(UserSessionManager.USER_ID);


        report_body_edt = (EditText) view.findViewById(R.id.report_body_edt);
        report_body_edt.setText("");
//        report_body_edt.setTypeface(typeface);

        report_text_title = (TextView) view.findViewById(R.id.report_text_title);
//        report_text_title.setTypeface(typeface);

        report_text = (TextView) view.findViewById(R.id.report_text);
//        report_text.setTypeface(typeface);

        send_report_button = (TextView) view.findViewById(R.id.send_report_button);
//        send_report_button.setTypeface(typeface);
        send_report_button.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == send_report_button) {
            checkInternet = NetworkChecking.isConnected(getActivity());
            if (checkInternet) {
                if (validate()) {
                    progressDialog.show();
                    final String messagebody = report_body_edt.getText().toString().trim();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GROUP_REPORT,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        Log.d("REPORTRESPONSE", response);
                                        String responceCode = jsonObject.getString("status");

                                        if (responceCode.equalsIgnoreCase("10100")) {

                                            Toast.makeText(getActivity(), "Report Sent Successfully ", Toast.LENGTH_LONG).show();
                                            report_body_edt.setText("");

                                        }

                                        if (responceCode.equals("10200")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getActivity(), "Failure, Please Try Again", Toast.LENGTH_LONG).show();
                                            report_body_edt.setText("");

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();

                            params.put("message", messagebody);
                            params.put("user_profile_id", userid);
                            Log.d("ReportGrpPARAMMY:", params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                    requestQueue.add(stringRequest);
                }
            } else {
                progressDialog.cancel();
                Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }

        }

    }


    private boolean validate() {

        boolean result = true;


        String dob = report_body_edt.getText().toString().trim();
        if (dob == null || dob.equals("")) {
            report_body_edt.setError("please fill report...!");
            result = false;
        }

        return result;
    }
}
