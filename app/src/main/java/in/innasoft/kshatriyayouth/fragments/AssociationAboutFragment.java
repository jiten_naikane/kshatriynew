package in.innasoft.kshatriyayouth.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class AssociationAboutFragment extends Fragment {

    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ImageView no_data;
    View view;
    Button follow;
    String userid, asociation_id;
    TextView about_me_text, association_area_name_location, association_city_name_location, association_district_name_location, association_state_name_location, association_country_name_location, members_text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_association_about, container, false);


//        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();

        asociation_id = getActivity().getIntent().getStringExtra("ASSOCIATION_ID");
        Log.d("hgskhgsdjjksd", asociation_id);

        userid = userDetails.get(UserSessionManager.USER_ID);


        about_me_text = (TextView) view.findViewById(R.id.about_me_text);
//        about_me_text.setTypeface(typeface);

        association_area_name_location = (TextView) view.findViewById(R.id.association_area_name_location);
//        association_area_name_location.setTypeface(typeface);

        association_city_name_location = (TextView) view.findViewById(R.id.association_city_name_location);
//        association_city_name_location.setTypeface(typeface);

        association_district_name_location = (TextView) view.findViewById(R.id.association_district_name_location);
//        association_district_name_location.setTypeface(typeface);

        association_state_name_location = (TextView) view.findViewById(R.id.association_state_name_location);
//        association_state_name_location.setTypeface(typeface);

        association_country_name_location = (TextView) view.findViewById(R.id.association_country_name_location);
//        association_country_name_location.setTypeface(typeface);

        follow = (Button) view.findViewById(R.id.follow);
//        follow.setTypeface(typeface);

        members_text = (TextView) view.findViewById(R.id.members_text);
//        members_text.setTypeface(typeface);
        //   members_text.setText("3 Members");

        no_data = (ImageView) view.findViewById(R.id.no_data);

        getAboutDetailData();

        return view;
    }

    private void getAboutDetailData() {

        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            progressDialog.show();
            final String url = AppUrls.BASE_URL + AppUrls.ASSOCIATIONS_DETAIL + "/" + asociation_id + "/" + userid;
            Log.d("assDetailURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.ASSOCIATIONS_DETAIL + "/" + asociation_id + "/" + userid,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASSODETAILRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");

                                    String description = jsonObject2.getString("description");
                                    Log.d("DESC:", description);
                                    about_me_text.setText(Html.fromHtml(description));

                                    String image = AppUrls.BASE_URL + jsonObject2.getString("image");


                                    String area_name = jsonObject2.getString("area_name");
                                    association_area_name_location.setText(Html.fromHtml(" : " + area_name));

                                    String city_name = jsonObject2.getString("city_name");
                                    association_city_name_location.setText(Html.fromHtml(" : " + city_name));

                                    String district_name = jsonObject2.getString("district_name");
                                    association_district_name_location.setText(Html.fromHtml(" : " + district_name));

                                    String state_name = jsonObject2.getString("state_name");
                                    association_state_name_location.setText(Html.fromHtml(" : " + state_name));

                                    String country_name = jsonObject2.getString("country_name");
                                    association_country_name_location.setText(Html.fromHtml(" : " + country_name));

                                   /* null -- Follow
                                    0 -- Request Pending
                                    1 -- Unfollow
                                    2 -- Request Rejected*/


                                    String association_status = jsonObject2.getString("association_status");
                                    if (association_status.equals("null")) {
                                        follow.setText("Follow");
                                        follow.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                followAssociation();

                                            }
                                        });
                                    }
                                    if (association_status.equals("0")) {
                                        follow.setText("Request Pending");
                                    }

                                    if (association_status.equals("1")) {
                                        follow.setText("Unfollow");
                                        follow.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                unfollowAssociation();
                                            }
                                        });
                                    }

                                    if (association_status.equals("2")) {
                                        follow.setText("Request Rejected");
                                    }


                                    String member = jsonObject1.getString("memberFollowDataCnt");
                                    members_text.setText(member);
                                    // String  member= jsonObject1.getJSONObject("memberFollowDataCnt");
                                    //        members_text.setText( member);
                                    // String member=jsonObject2.getString("member");
                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_data.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void followAssociation() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            progressDialog.show();
            String urllllll = AppUrls.BASE_URL + AppUrls.LOGIN;
            Log.d("AssocationFOLURRRRL:", urllllll);


            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ASSOCIATIONS_FOLLOW,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASSFOLLOWRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                    getAboutDetailData();
                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("associations_id", asociation_id);
                    params.put("user_id", userid);

                    Log.d("AssocationFOLLOwDATAIL:", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);


        } else {
            Snackbar snackbar = Snackbar.make(getView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }


    }

    private void unfollowAssociation() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            progressDialog.show();
            String urllllll = AppUrls.BASE_URL + AppUrls.LOGIN;
            Log.d("AssocatUNFOLURRRRL:", urllllll);


            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ASSOCIATIONS_UNFOLLOW,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASSUNFOLLOWRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                    getAboutDetailData();
                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("associations_id", asociation_id);
                    params.put("user_id", userid);

                    Log.d("AssocatUNFOLLOwDATAIL:", params.toString());
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
                   /* RequestQueue requestQueue = Volley.newRequestQueue(LoginPage.this);
                    requestQueue.add(stringRequest);*/

        } else {
            Snackbar snackbar = Snackbar.make(getView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }


}
