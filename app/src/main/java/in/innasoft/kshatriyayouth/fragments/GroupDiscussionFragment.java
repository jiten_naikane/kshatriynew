package in.innasoft.kshatriyayouth.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.PostStatusActivity;
import in.innasoft.kshatriyayouth.adapters.GroupFeedHomeAdapter;
import in.innasoft.kshatriyayouth.models.HomeFeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FixedSwipeRefreshLayout;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class GroupDiscussionFragment extends Fragment implements View.OnClickListener {
    RelativeLayout poststatus;
    TextView scrolling_text, status_text;
    RecyclerView after_reg_list_universies_recyclerview;
    GroupFeedHomeAdapter afterRegCPListofUniversitiesAdapter;
    ArrayList<HomeFeedItem> afterRegCPListofUniversitiesModels = new ArrayList<HomeFeedItem>();
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    String profile_pic_url = "";
    String profile_name = "";
    String profile_membership = "";
    String user_profile_id = "";
    String image1 = "";
    String image2 = "";
    String image3 = "";
    String video;
    String photo;
    ImageView image_view, nodata_image;
    //    Typeface typeface;
    UserSessionManager sessionManager;
    ProgressBar progress_bar;
    JSONArray photos_array;
    View rootview;
    String group_id;
    FixedSwipeRefreshLayout swipeView;
    RelativeLayout bottomLayout;
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    ProgressDialog progressDialog;

    public GroupDiscussionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_group_discussion, container, false);

//        typeface = Typeface.createFromAsset(getContext().getAssets(), getContext().getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        nodata_image = (ImageView) rootview.findViewById(R.id.nodata_image);
        swipeView = (FixedSwipeRefreshLayout) rootview.findViewById(R.id.swipe);
        bottomLayout = (RelativeLayout) rootview.findViewById(R.id.loadItemsLayout_recyclerView);
        group_id = getActivity().getIntent().getStringExtra("GROUP_ID");
        poststatus = (RelativeLayout) rootview.findViewById(R.id.poststatus);
        progress_bar = (ProgressBar) rootview.findViewById(R.id.progress_bar);
        scrolling_text = (TextView) rootview.findViewById(R.id.scrolling_text);
//        scrolling_text.setTypeface(typeface);
        status_text = (TextView) rootview.findViewById(R.id.status_text);
        image_view = (ImageView) rootview.findViewById(R.id.image_view);
        scrolling_text.setSelected(true);
        poststatus.setOnClickListener(this);
        sessionManager = new UserSessionManager(getContext());
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        profile_name = userDetails.get(UserSessionManager.USER_NAME);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);


        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            //  Toast.makeText(getContext(), "Good Morning", Toast.LENGTH_SHORT).show();
            status_text.setText(Html.fromHtml("Good Morning, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            // Toast.makeText(getContext(), "Good Afternoon", Toast.LENGTH_SHORT).show();
            // status_text.setText("Good Afternoon."+profile_name+"."+"Share something");
            status_text.setText(Html.fromHtml("Good Afternoon, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            //  Toast.makeText(getContext(), "Good Evening", Toast.LENGTH_SHORT).show();
            //status_text.setText("Good Evening."+profile_name+"."+"Share something");
            status_text.setText(Html.fromHtml("Good Evening, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            //Toast.makeText(getContext(), "Good Night", Toast.LENGTH_SHORT).show();
            // status_text.setText("Good Night."+profile_name+"."+"Share something");
            status_text.setText(Html.fromHtml("Good Evening, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        }

        Picasso.with(getContext())
                .load(profile_pic_url)
                .placeholder(R.drawable.user_profile)
                .into(image_view);
//////////////////////////////////////////////

        after_reg_list_universies_recyclerview = (RecyclerView) rootview.findViewById(R.id.feed_recylerview);
        swipeView.setRecyclerView(after_reg_list_universies_recyclerview);
        after_reg_list_universies_recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());

        after_reg_list_universies_recyclerview.setLayoutManager(layoutManager);
        afterRegCPListofUniversitiesModels.clear();
        afterRegCPListofUniversitiesAdapter = new GroupFeedHomeAdapter(afterRegCPListofUniversitiesModels, GroupDiscussionFragment.this, R.layout.feed_item);
        after_reg_list_universies_recyclerview.setNestedScrollingEnabled(false);
        //homeFeedItem();
        /////////////////////////////////////////////////

        after_reg_list_universies_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                homeFeedItem(defaultPageNo);

                            } else {
                                Toast.makeText(getContext(), "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        swipeView.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_light);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        afterRegCPListofUniversitiesModels.clear();
                        defaultPageNo = 1;
                        homeFeedItem(defaultPageNo);
                    }
                }, 3000);
            }
        });

        homeFeedItem(defaultPageNo);

        return rootview;

    }

    @Override
    public void onClick(View v) {
        if (v == poststatus) {
            Intent intent = new Intent(getContext(), PostStatusActivity.class);
            intent.putExtra("name", profile_name);
            intent.putExtra("page_owner", group_id);
            intent.putExtra("photo_identifier", "wallfeeds");
            intent.putExtra("feeds", "group");
            intent.putExtra("image", profile_pic_url);
            startActivity(intent);
        }
    }

    public void homeFeedItem(int defaultPageNo) {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {
            if (type_of_request == 0) {
                progress_bar.setVisibility(View.VISIBLE);
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
            }
            progress_bar.setVisibility(View.VISIBLE);
            String url = AppUrls.BASE_URL + AppUrls.GROUP_MAIN_FEED;
            Log.d("GroupDiscussionUrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {
                                    swipeView.setRefreshing(false);
                                    nodata_image.setVisibility(View.GONE);
                                    progress_bar.setVisibility(View.GONE);

                                    int total_numberof_records = Integer.valueOf(jsonObject.getString("count"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + total_numberof_records);
                                    if (total_numberof_records > afterRegCPListofUniversitiesModels.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + total_numberof_records);
                                    } else {
                                        loading = false;
                                    }

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);

                                        HomeFeedItem item = new HomeFeedItem();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setPage_id(jsonObject2.getString("page_id"));
                                        item.setType(jsonObject2.getString("type"));
                                        item.setPost(jsonObject2.getString("post"));
                                        item.setSecurity(jsonObject2.getString("security"));
                                        item.setHiddenitem(jsonObject2.getString("hiddenitem"));
                                        item.setDate(jsonObject2.getString("date"));
                                        item.setName(jsonObject2.getString("name") + " " + jsonObject2.getString("surname"));
                                        item.setPost_edit_count(jsonObject2.getString("post_edit_count"));
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        item.setUser_like_status(jsonObject2.getString("user_like_status"));
                                        item.setUser_like_status_type(jsonObject2.getString("user_like_status_type"));
                                        item.setShared_info_count(jsonObject2.getString("shared_info_count"));
                                        JSONObject shared_object = jsonObject2.getJSONObject("shared_info");
                                        item.setShared_info_name(shared_object.getString("name"));
                                        item.setShared_info_surname(shared_object.getString("surname"));
                                        item.setShared_info_username(shared_object.getString("username"));
                                        item.setShared_info_photo(AppUrls.BASE_IMAGE_URL + shared_object.getString("photo"));
                                        item.setShared_info_post(shared_object.getString("oldpost"));


                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");

                                        item.setPhoto(image);
                                        if (jsonObject2.getString("video_list").equals("[]")) {
                                            video = "";
                                        } else {

                                            JSONArray video_array = jsonObject2.getJSONArray("video_list");
                                            Log.d("video_list", "" + video_array);
                                            if (video_array.getJSONObject(0) != null) {
                                                JSONObject jobj1 = video_array.getJSONObject(0);
                                                video = jobj1.getString("video_id");

                                            }
                                        }
                                        item.setPhoto_list_count(jsonObject2.getString("photo_list_count"));
                                        Log.d("jkbdfbd", jsonObject2.getString("photo_list_count"));
                                        if (jsonObject2.getString("photo_list").equals("[]")) {
                                            image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                            image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                            image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                        } else {
                                            photos_array = jsonObject2.getJSONArray("photo_list");
                                            Log.d("sdhavgsbfsd", "" + photos_array);
                                            for (int j = 0; j < photos_array.length(); j++) {
                                                String photos = photos_array.getString(j);
                                                String pic = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos;
                                                photo = pic;
                                            }
                                            if (photos_array.getString(0) != null) {

                                                image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos_array.getString(0);

                                            } else if (photos_array.getString(1) != null) {

                                                image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos_array.getString(1);


                                            } else if (photos_array.getString(2) != null) {

                                                image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos_array.getString(2);

                                            } else {
                                                image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                                image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                                image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                            }
                                        }

                                        item.setPhoto_list_photo_link(photo);
                                        item.setImage_one(image1);
                                        item.setImage_two(image2);
                                        item.setImage_three(image3);

                                        item.setPost_likes_count(jsonObject2.getString("post_likes_count"));
                                        item.setPost_comments_count(jsonObject2.getString("post_comments_count"));
                                        item.setVideo_list_url(video);

                                        Log.d("cbgsd", "" + video);
                                        Log.d("cbxjvkb", image1 + "\n" + image2 + "\n" + image3);

                                        afterRegCPListofUniversitiesModels.add(item);
                                    }

                                    layoutManager.scrollToPositionWithOffset(displayedposition, afterRegCPListofUniversitiesModels.size());
                                    after_reg_list_universies_recyclerview.setAdapter(afterRegCPListofUniversitiesAdapter);
                                    bottomLayout.setVisibility(View.GONE);

                                }
                                if (responceCode.equals("10200")) {
                                    nodata_image.setVisibility(View.VISIBLE);
                                    progress_bar.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progress_bar.setVisibility(View.GONE);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    nodata_image.setVisibility(View.VISIBLE);
                    progress_bar.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    params.put("page_owner_id", group_id);
                    // params.put("page_owner_id", user_profile_id);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
    /*@Override
    public void onResume(){
        super.onResume();
        ( new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                afterRegCPListofUniversitiesModels.clear();
                homeFeedItem(defaultPageNo);
            }
        }, 2000);
    }*/

    public void notifyDatas() {
        after_reg_list_universies_recyclerview.setAdapter(afterRegCPListofUniversitiesAdapter);

    }

}

