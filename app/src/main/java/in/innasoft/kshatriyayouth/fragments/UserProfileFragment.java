package in.innasoft.kshatriyayouth.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.MyProfileActivity;

public class UserProfileFragment extends Fragment implements View.OnClickListener {

    View rootview;
    LinearLayout myprofile;
    TextView profile_text, reset_text, updatePasword_text, logout_text;
//    Typeface typeface;


    public UserProfileFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootview = inflater.inflate(R.layout.fragment_user_profile, container, false);
//        typeface = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.montserrat_light));
        myprofile = (LinearLayout) rootview.findViewById(R.id.myprofile);
        myprofile.setOnClickListener(this);

        //,,,
        profile_text = (TextView) rootview.findViewById(R.id.profile_text);
//        profile_text.setTypeface(typeface);

        reset_text = (TextView) rootview.findViewById(R.id.reset_text);
//        reset_text.setTypeface(typeface);

        updatePasword_text = (TextView) rootview.findViewById(R.id.updatePasword_text);
//        updatePasword_text.setTypeface(typeface);

        logout_text = (TextView) rootview.findViewById(R.id.logout_text);
//        logout_text.setTypeface(typeface);

        return rootview;

    }

    @Override
    public void onClick(View v) {
        if (v == myprofile) {
            Intent intent = new Intent(getContext(), MyProfileActivity.class);
            startActivity(intent);
        }

    }
}
