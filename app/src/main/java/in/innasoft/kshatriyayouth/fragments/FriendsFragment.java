package in.innasoft.kshatriyayouth.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.AllFriendsListAdapter;
import in.innasoft.kshatriyayouth.adapters.FriendsListAdapter;
import in.innasoft.kshatriyayouth.adapters.FriendsRequestAdapter;
import in.innasoft.kshatriyayouth.models.AllFriendsModel;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.NonScrollListView;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class FriendsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    View rootview;
    private static final String TAG = MainActivity.class.getSimpleName();
    private NonScrollListView listView;
    private FriendsListAdapter listAdapter;
    private List<FeedItem> feedItems;
    private String URL_FEED = "http://learningslot.in/srikanth/movielist";
    //////////////////////////////////////////////////
    RecyclerView recyclerview, recylerview_allfriends;
    AllFriendsListAdapter adapter;
    FriendsRequestAdapter adapter_friendreq;
    ArrayList<AllFriendsModel> feeditem = new ArrayList<AllFriendsModel>();
    ArrayList<AllFriendsModel> feeditem1 = new ArrayList<AllFriendsModel>();
    LinearLayoutManager layoutManager, layoutManager1;
    private boolean checkInternet;
    int total_number_of_items = 0;
    private boolean userScrolled = true;
    private static int displayedposition = 0;
    private static int displayedposition1 = 0;
    RelativeLayout bottomLayout;
    int type_of_request = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;
    ProgressDialog progressDialog;
    ImageView nodata_image;
    UserSessionManager session;
    String userid = "", user_profile_id = "", membership = "";
    ProgressBar progress_bar;
    SwipeRefreshLayout swipeView;

    TextView title, title_friendrequests, no_frds_txt;

    private int draggingView = -1;

    public FriendsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_friends, container, false);
        feedItems = new ArrayList<FeedItem>();

////////////////////////////////////////////////////////////
        session = new UserSessionManager(getContext());
        final HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        membership = userDetails.get(UserSessionManager.USER_MEMBERSHIP);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        swipeView = (SwipeRefreshLayout) rootview.findViewById(R.id.swipeView);
        swipeView.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_light);
        swipeView.setOnRefreshListener(this);

        title = (TextView) rootview.findViewById(R.id.title);
        title_friendrequests = (TextView) rootview.findViewById(R.id.title_friendrequests);
        no_frds_txt = (TextView) rootview.findViewById(R.id.no_frds_txt);

        feeditem.clear();

        nodata_image = (ImageView) rootview.findViewById(R.id.nodata_image);
        progress_bar = (ProgressBar) rootview.findViewById(R.id.progress_bar);
        bottomLayout = (RelativeLayout) rootview.findViewById(R.id.loadItemsLayout_recyclerView);
        recyclerview = (RecyclerView) rootview.findViewById(R.id.recylerview);
        //swipeView.setRecyclerView(recyclerview);
        recylerview_allfriends = (RecyclerView) rootview.findViewById(R.id.recylerview_allfriends);
        // swipeView.setRecyclerView(recylerview_allfriends);
        recyclerview.setHasFixedSize(true);
        recylerview_allfriends.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager1 = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(layoutManager);
        recylerview_allfriends.setLayoutManager(layoutManager1);
        adapter = new AllFriendsListAdapter(feeditem, FriendsFragment.this, R.layout.friends_list_item);
        adapter_friendreq = new FriendsRequestAdapter(feeditem1, FriendsFragment.this, R.layout.friends_list_item);
        recyclerview.setNestedScrollingEnabled(false);
        recylerview_allfriends.setNestedScrollingEnabled(false);
        layoutManager.setAutoMeasureEnabled(true);
        layoutManager1.setAutoMeasureEnabled(true);

        ///////////////////////////////////


        /*RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {
                super.onScrollStateChanged(recyclerView, newState);
                if (recyclerview == recyclerView && newState == RecyclerView.SCROLL_STATE_DRAGGING)
                {
                    draggingView = 1;
                } else if (recylerview_allfriends == recyclerView && newState == RecyclerView.SCROLL_STATE_DRAGGING)
                {
                    draggingView = 2;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                if (draggingView == 1 && recyclerView == recyclerview) {
                    recylerview_allfriends.scrollBy(dx, dy);
                } else if (draggingView == 2 && recyclerView == recylerview_allfriends) {
                    recyclerview.scrollBy(dx, dy);
                }
            }
        };*/


        ///////////////////////////////////

        /*recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener()
        {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);

                    if (loading)
                    {
                        if ( userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("FFFFFF", String.valueOf(defaultPageNo));
                            if(totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                feedItemFriendRequests(user_profile_id,defaultPageNo);

                            }else {
                                Toast.makeText(getContext(), "No More Results", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }
            }
        });


        recylerview_allfriends.addOnScrollListener(new RecyclerView.OnScrollListener()
        {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager1.getChildCount();
                    totalItemCount = layoutManager1.getItemCount();
                    pastVisiblesItems = layoutManager1.findFirstVisibleItemPosition();
                    displayedposition1 = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);

                    if (loading)
                    {
                        if ( userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("FFFFFF", String.valueOf(defaultPageNo));
                            if(totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                feedItem(defaultPageNo);

                            }else {
                                Toast.makeText(getContext(), "No More Results", Toast.LENGTH_SHORT).show();
                            }

                            //Do pagination.. i.e. fetch new data
                            //universitiesList
                        }
                    }
                }
            }
        });*/

     /*   swipeView.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_light);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                ( new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        feeditem1.clear();
                        feeditem.clear();
                        defaultPageNo = 1;
                        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
                        feedItemFriendRequests(user_profile_id,defaultPageNo);
                    }
                }, 3000);
            }
        });*/

        //feedItemFriendRequests(user_profile_id,defaultPageNo);
        //  feedItemFriendRequests();
        /////////////////////////////////////////////////////////////
        return rootview;

    }

    //public void feedItemFriendRequests(final String user_id, final int defaultPageNo) {
    public void feedItemFriendRequests() {
        feeditem1.clear();
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {
            /*if(type_of_request == 0) {
               // progressDialog.show();
                progress_bar.setVisibility(View.VISIBLE);
            }else {
                bottomLayout.setVisibility(View.VISIBLE);
            }*/

            progress_bar.setVisibility(View.VISIBLE);
            //String url = AppUrls.BASE_URL + AppUrls.FRIENDS_REQUEST_LIST + "/" + user_id + "/" + defaultPageNo ;
            String url = AppUrls.BASE_URL + AppUrls.FRIENDS_REQUEST_LIST + "/" + user_profile_id;
            Log.d("AllFrdReq", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("AllFrdReqResponce", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    //  progressDialog.dismiss();
                                    swipeView.setRefreshing(false);
                                    progress_bar.setVisibility(View.GONE);
                                    nodata_image.setVisibility(View.GONE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("request_count");
                                    JSONObject jsonObject3 = jsonArray1.getJSONObject(0);
                                    String request_count = jsonObject3.getString("count");

                                    /*int total_numberof_records = Integer.valueOf(request_count);
                                    total_number_of_items = total_numberof_records;

                                    Log.d("LOADSTATUS",  "OUTER "+loading+"  "+total_numberof_records);
                                    if(total_numberof_records > feeditem.size())
                                    {
                                        loading = true;
                                        Log.d("LOADSTATUS",  "INNER "+loading+"  "+total_numberof_records);
                                    }else {
                                        loading = false;
                                    }*/
                                    JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        AllFriendsModel item = new AllFriendsModel();
                                        item.setName(jsonObject2.getString("name"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        Log.d("image_photo", image);
                                        item.setPhoto(image);
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));

                                        feeditem1.add(item);
                                    }

                                    layoutManager.scrollToPositionWithOffset(displayedposition, feeditem1.size());
                                    recyclerview.setAdapter(adapter_friendreq);
                                    bottomLayout.setVisibility(View.GONE);
                                    //feedItem(defaultPageNo);
                                    feedItem();


                                }
                                if (responceCode.equals("10800")) {
                                    swipeView.setRefreshing(false);
                                    //no_frds_txt.setVisibility(View.VISIBLE);
                                    title.setVisibility(View.GONE);
                                    recyclerview.setVisibility(View.GONE);

                                }
                                if (responceCode.equals("10200")) {
                                    swipeView.setRefreshing(false);
                                    //feedItem(defaultPageNo);
                                    feedItem();
                                    //no_frds_txt.setVisibility(View.VISIBLE);
                                    title.setVisibility(View.GONE);
                                    recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_image.setVisibility(View.GONE);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    nodata_image.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_id);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    //private void feedItem(int defaultPageNo)
    private void feedItem() {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {
            /*if(type_of_request == 0) {
                //progressDialog.show();
                //progress_bar.setVisibility(View.VISIBLE);
            }else {
                bottomLayout.setVisibility(View.VISIBLE);
            }*/

            // String url = AppUrls.BASE_URL + AppUrls.FRIENDS_LIST + "/" + user_profile_id +"/"+ defaultPageNo;
            String url = AppUrls.BASE_URL + AppUrls.FRIENDS_LIST_NEW;
            Log.d("FrdRequest", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("FrdRequestResponce", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    //  progressDialog.dismiss();
                                    swipeView.setRefreshing(false);
                                    progress_bar.setVisibility(View.GONE);
                                    nodata_image.setVisibility(View.GONE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    /*int total_numberof_records = Integer.valueOf(jsonObject1.getString("friendsData_count"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS",  "OUTER "+loading+"  "+total_numberof_records);
                                    if(total_numberof_records > feeditem.size())
                                    {
                                        loading = true;
                                        Log.d("LOADSTATUS",  "INNER "+loading+"  "+total_numberof_records);
                                    }else {
                                        loading = false;
                                    }*/

                                    JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        AllFriendsModel item = new AllFriendsModel();
                                        item.setName(jsonObject2.getString("name"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        Log.d("image_photo", image);
                                        item.setPhoto(image);
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        item.setFriendstatus(jsonObject2.getString("friendstatus"));
                                        item.setRequeststatus(jsonObject2.getString("requeststatus"));
                                        item.setSentbystatus(jsonObject2.getString("sentbystatus"));

                                        feeditem.add(item);
                                    }

                                    layoutManager1.scrollToPositionWithOffset(displayedposition1, feeditem.size());
                                    recylerview_allfriends.setAdapter(adapter);
                                    bottomLayout.setVisibility(View.GONE);

                                }
                                if (responceCode.equals("10800")) {
                                    swipeView.setRefreshing(false);
                                    title_friendrequests.setVisibility(View.GONE);
                                    recylerview_allfriends.setVisibility(View.GONE);
                                }
                                if (responceCode.equals("10200")) {
                                    swipeView.setRefreshing(false);
                                    title_friendrequests.setVisibility(View.GONE);
                                    recylerview_allfriends.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_image.setVisibility(View.GONE);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    nodata_image.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRefresh() {
        swipeView.setRefreshing(true);
        Log.d("Swipe", "Refreshing Number");

        feeditem1.clear();
        feeditem.clear();
        feedItemFriendRequests();


    }

    @Override
    public void onResume() {
        super.onResume();
        feeditem1.clear();
        feeditem.clear();
        feedItemFriendRequests();
    }
    /*swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            swipeView.setRefreshing(true);
            Log.d("Swipe", "Refreshing Number");
            ( new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    feeditem1.clear();
                    feeditem.clear();
                    defaultPageNo = 1;
                    user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
                    feedItemFriendRequests();
                }
            }, 3000);
        }
    });*/



    /*@Override
    public void onResume(){
        super.onResume();
        feeditem.clear();
        feedItem(defaultPageNo);
    }*/
}
