package in.innasoft.kshatriyayouth.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SuggestGroupListAdapter;
import in.innasoft.kshatriyayouth.models.SuggestGroupModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FixedSwipeRefreshLayout;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class SuggestGroupFragment extends Fragment {

    View view;
    RecyclerView group_suggested_recyclerview;
    SuggestGroupListAdapter suggestGroupListAdapter;
    ArrayList<SuggestGroupModel> groupsuggestListItem = new ArrayList<SuggestGroupModel>();
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String userid, user_profile_id;
    ImageView nodata_suggestgrp_image;
    LinearLayoutManager layoutManager;
    SearchView mSearch_group_suggested_fragment;

    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    FixedSwipeRefreshLayout swipeView;

    RelativeLayout bottomLayout;

    public SuggestGroupFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_suggest_group, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);

        swipeView = (FixedSwipeRefreshLayout) view.findViewById(R.id.swipe);

        bottomLayout = (RelativeLayout) view.findViewById(R.id.loadItemsLayout_recyclerView);

        nodata_suggestgrp_image = (ImageView) view.findViewById(R.id.nodata_suggestgrp_image);

        mSearch_group_suggested_fragment = (SearchView) view.findViewById(R.id.mSearch_group_suggested_fragment);

        group_suggested_recyclerview = (RecyclerView) view.findViewById(R.id.group_suggested_recyclerview);
        swipeView.setRecyclerView(group_suggested_recyclerview);
        layoutManager = new LinearLayoutManager(getActivity());
        group_suggested_recyclerview.setHasFixedSize(true);
        group_suggested_recyclerview.setLayoutManager(layoutManager);
        groupsuggestListItem.clear();
        suggestGroupListAdapter = new SuggestGroupListAdapter(groupsuggestListItem, SuggestGroupFragment.this, R.layout.row_suggested_group);

        mSearch_group_suggested_fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearch_group_suggested_fragment.setIconified(false);

            }
        });

        mSearch_group_suggested_fragment.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                suggestGroupListAdapter.getFilter().filter(query);
                suggestGroupListAdapter.notifyDataSetChanged();
                return true;
            }
        });

        group_suggested_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                getAllSuggestedGroup(defaultPageNo);

                            } else {
                                Toast.makeText(getContext(), "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        swipeView.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_light);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        groupsuggestListItem.clear();
                        defaultPageNo = 1;
                        displayedposition = 0;
                        getAllSuggestedGroup(defaultPageNo);
                    }
                }, 3000);
            }
        });

        getAllSuggestedGroup(defaultPageNo);


        return view;
    }

    public void getAllSuggestedGroup(int defaultPageNo) {

        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            if (type_of_request == 0) {
                progressDialog.show();
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
            }

            String url = AppUrls.BASE_URL + AppUrls.SUGGEST_GROUP_LIST + "/" + user_profile_id + "/" + defaultPageNo;
            Log.d("suggestgrouplisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("suggestgroupRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    swipeView.setRefreshing(false);
                                    group_suggested_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                    nodata_suggestgrp_image.setVisibility(View.GONE);

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    Log.d("JOBJECT:", jsonObject1.toString());

                                    int total_numberof_records = Integer.valueOf(jsonObject1.getString("groupsList_cnt"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + total_numberof_records);
                                    if (total_numberof_records > groupsuggestListItem.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + total_numberof_records);
                                    } else {
                                        loading = false;
                                    }

                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupsList");
                                    Log.d("JARRRAY:", jsonArray.toString());


                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        SuggestGroupModel item = new SuggestGroupModel();

                                        item.setId(jsonObject2.getString("id"));
                                        item.setGroup_id(jsonObject2.getString("group_id"));
                                        String nammm = jsonObject2.getString("group_name");
                                        Log.d("sdafasdfasdfa", nammm);
                                        item.setGroup_manager(jsonObject2.getString("group_manager"));
                                        item.setGroup_name(jsonObject2.getString("group_name"));
                                        item.setGroup_description(jsonObject2.getString("group_description"));
                                        item.setGroup_type(jsonObject2.getString("group_type"));
                                        item.setGroup_count(jsonObject2.getString("group_count"));
                                        String image = AppUrls.BASE_IMAGE_URL_GROUP_LIST + jsonObject2.getString("group_picture");
                                        item.setGroup_picture(image);
                                        item.setDate(jsonObject2.getString("date"));
                                        item.setGroup_status(jsonObject2.getString("group_status"));

                                        groupsuggestListItem.add(item);
                                    }

                                    layoutManager.scrollToPositionWithOffset(displayedposition, groupsuggestListItem.size());
                                    group_suggested_recyclerview.setAdapter(suggestGroupListAdapter);
                                    bottomLayout.setVisibility(View.GONE);
                                }

                                if (responceCode.equals("10200")) {
                                    group_suggested_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_suggestgrp_image.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_suggestgrp_image.setVisibility(View.VISIBLE);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    nodata_suggestgrp_image.setVisibility(View.VISIBLE);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id",user_profile_id);

                    Log.d("suggestgroupDATAIL:",params.toString());
                    return params;
                }
            }*/;

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
