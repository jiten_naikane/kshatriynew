package in.innasoft.kshatriyayouth.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.GroupVideoListAdapter;
import in.innasoft.kshatriyayouth.models.GroupVideoListModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;


public class GroupVideosFragment extends Fragment {


    View view;
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;

    GroupVideoListAdapter gvideoListAdapter;
    ArrayList<GroupVideoListModel> groupvideoListItem = new ArrayList<GroupVideoListModel>();
    RecyclerView group_video_recyclerview;
    ImageView nodata_video_image;
    String group_id, userid;
    LinearLayoutManager layoutManager;

    public GroupVideosFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_group_videos, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        group_id = getActivity().getIntent().getStringExtra("GROUP_ID");
        Log.d("groupid:", group_id);

        nodata_video_image = (ImageView) view.findViewById(R.id.nodata_video_image);

        group_video_recyclerview = (RecyclerView) view.findViewById(R.id.group_video_recyclerview);
        group_video_recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        group_video_recyclerview.setLayoutManager(layoutManager);
        gvideoListAdapter = new GroupVideoListAdapter(groupvideoListItem, GroupVideosFragment.this, R.layout.row_group_video_fragment);
        group_video_recyclerview.setNestedScrollingEnabled(false);

        getAllGroupVideo();


        return view;
    }

    private void getAllGroupVideo() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            //http://kshatriyayouth.com/group_videos/
            String groupid = "138312511146940165";//
            groupvideoListItem.clear();
            //  String url = AppUrls.BASE_URL+AppUrls.GROUP_VIDEOS+"/"+group_id;
            String url = AppUrls.BASE_URL + AppUrls.GROUP_VIDEOS + "/" + group_id;
            Log.d("videolisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("videolistuRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_video_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                    nodata_video_image.setVisibility(View.GONE);

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupVideos");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        GroupVideoListModel item = new GroupVideoListModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setPost_id(jsonObject2.getString("post_id"));
                                        item.setVideo_id(jsonObject2.getString("video_id"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        item.setUrl(jsonObject2.getString("url"));
                                        item.setTitle(jsonObject2.getString("title"));
                                        item.setVideo_identifier(jsonObject2.getString("video_identifier"));

                                        groupvideoListItem.add(item);
                                        Log.d("LISTTTT:", groupvideoListItem.toString());
                                    }


                                    group_video_recyclerview.setAdapter(gvideoListAdapter);


                                }

                                if (responceCode.equals("10200")) {
                                    group_video_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_video_image.setVisibility(View.VISIBLE);


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_video_image.setVisibility(View.VISIBLE);


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    nodata_video_image.setVisibility(View.VISIBLE);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }


    }

}
