package in.innasoft.kshatriyayouth.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.GroupMemberListAdapter;
import in.innasoft.kshatriyayouth.models.GroupMembersListModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class GroupMembersFragment extends Fragment {
    View view;
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;

    GroupMemberListAdapter gmemberListAdapter;
    ArrayList<GroupMembersListModel> groupListItem = new ArrayList<GroupMembersListModel>();
    RecyclerView group_member_recyclerview;
    ImageView nodata_member_image;
    String group_id, userid;


    public GroupMembersFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group_members, container, false);

//        typeface = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);

        group_id = getActivity().getIntent().getStringExtra("GROUP_ID");
        Log.d("groupid:", group_id);

        nodata_member_image = (ImageView) view.findViewById(R.id.nodata_member_image);


        group_member_recyclerview = (RecyclerView) view.findViewById(R.id.group_member_recyclerview);
        group_member_recyclerview.setHasFixedSize(true);
        gmemberListAdapter = new GroupMemberListAdapter(groupListItem, GroupMembersFragment.this, R.layout.row_group_member_fragment);
        group_member_recyclerview.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        group_member_recyclerview.setNestedScrollingEnabled(false);
        group_member_recyclerview.setSaveFromParentEnabled(true);

        getMemberListData();

        return view;
    }

    private void getMemberListData() {
        //afterRegCPListofUniversitiesModels.clear();
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            groupListItem.clear();
            String url = AppUrls.BASE_URL + AppUrls.GROUP_WISE_MEMBER_LIST + "/" + group_id;
            Log.d("memberlisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("memberlistuRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_member_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                    nodata_member_image.setVisibility(View.GONE);

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        GroupMembersListModel item = new GroupMembersListModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        item.setPhoto(image);
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));

                                        groupListItem.add(item);
                                    }


                                    group_member_recyclerview.setAdapter(gmemberListAdapter);


                                }

                                if (responceCode.equals("10200")) {
                                    group_member_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_member_image.setVisibility(View.VISIBLE);


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_member_image.setVisibility(View.VISIBLE);


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    nodata_member_image.setVisibility(View.VISIBLE);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

}
/*
<ImageView
                    android:id="@+id/member_image_iv"
                            android:layout_width="@dimen/profile_row_image_width"
                            android:layout_height="@dimen/profile_row_image_height"
                            android:src="@drawable/normal_bg"
                            android:scaleType="fitXY"/>

<TextView
                android:id="@+id/member_name_text"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="Accounting"
                        android:layout_below="@+id/member_image_iv"
                        android:paddingLeft="24dp"
                        android:textSize="@dimen/profile_row_title_text_size"
                        android:textStyle="bold"
                        android:textColor="#555"/>*/
