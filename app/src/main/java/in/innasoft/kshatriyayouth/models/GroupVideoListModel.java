package in.innasoft.kshatriyayouth.models;


import java.io.Serializable;

public class GroupVideoListModel implements Serializable {
    public String id;
    public String post_id;
    public String video_id;
    public String username;
    public String url;
    public String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String video_identifier;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideo_identifier() {
        return video_identifier;
    }

    public void setVideo_identifier(String video_identifier) {
        this.video_identifier = video_identifier;
    }
}
