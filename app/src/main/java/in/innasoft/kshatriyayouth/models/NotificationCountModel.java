package in.innasoft.kshatriyayouth.models;

public class NotificationCountModel {
    public String id;
    public String count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
