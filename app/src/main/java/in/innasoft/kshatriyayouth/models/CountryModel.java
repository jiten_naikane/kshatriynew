package in.innasoft.kshatriyayouth.models;

public class CountryModel {
    public String id;
    public String name;
    public String country_iso_code;
    public String country_isd_code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry_iso_code() {
        return country_iso_code;
    }

    public void setCountry_iso_code(String country_iso_code) {
        this.country_iso_code = country_iso_code;
    }

    public String getCountry_isd_code() {
        return country_isd_code;
    }

    public void setCountry_isd_code(String country_isd_code) {
        this.country_isd_code = country_isd_code;
    }
}
