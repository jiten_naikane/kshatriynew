package in.innasoft.kshatriyayouth.models;

/**
 * Created by purushotham on 27/1/17.
 */

public class CheckInCountriesModel {
    public String id;
    public String country_name;
    public String country_iso_code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_iso_code() {
        return country_iso_code;
    }

    public void setCountry_iso_code(String country_iso_code) {
        this.country_iso_code = country_iso_code;
    }
}
