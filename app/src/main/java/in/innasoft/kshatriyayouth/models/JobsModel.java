package in.innasoft.kshatriyayouth.models;


public class JobsModel {
    public String id;
    public String name;
    public String url_name;
    public String image;
    public String skills;
    public String type;
    public String salary;
    public String description;
    public String experience;
    public String category_id;
    public String associations_id;
    public String associations_name;
    public String category_url_name;
    public String create_date_time;

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getAssociations_id() {
        return associations_id;
    }

    public void setAssociations_id(String associations_id) {
        this.associations_id = associations_id;
    }

    public String getAssociations_name() {
        return associations_name;
    }

    public void setAssociations_name(String associations_name) {
        this.associations_name = associations_name;
    }

    public String getCategory_url_name() {
        return category_url_name;
    }

    public void setCategory_url_name(String category_url_name) {
        this.category_url_name = category_url_name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl_name() {
        return url_name;
    }

    public void setUrl_name(String url_name) {
        this.url_name = url_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreate_date_time() {
        return create_date_time;
    }

    public void setCreate_date_time(String create_date_time) {
        this.create_date_time = create_date_time;
    }


}

