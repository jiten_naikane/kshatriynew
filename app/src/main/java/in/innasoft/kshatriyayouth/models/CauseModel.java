package in.innasoft.kshatriyayouth.models;


public class CauseModel {
    public String id;
    public String name;
    public String image;
    public String url_name;
    public String description;
    public String create_date_time;
    public String location;
    public String amount;
    public String associations_id;
    public String associations_name;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAssociations_id() {
        return associations_id;
    }

    public void setAssociations_id(String associations_id) {
        this.associations_id = associations_id;
    }

    public String getAssociations_name() {
        return associations_name;
    }

    public void setAssociations_name(String associations_name) {
        this.associations_name = associations_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl_name() {
        return url_name;
    }

    public void setUrl_name(String url_name) {
        this.url_name = url_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreate_date_time() {
        return create_date_time;
    }

    public void setCreate_date_time(String create_date_time) {
        this.create_date_time = create_date_time;
    }


}
