package in.innasoft.kshatriyayouth.models;

/**
 * Created by Devolper on 23-Aug-17.
 */

public class BlogsDetailModel {
    String blog_detail_id;
    String blog_detail_name;
    String blog_detail_image;
    String blog_detail_description;
    String getBlog_detail_datetime;

    public String getBlog_detail_id() {
        return blog_detail_id;
    }

    public void setBlog_detail_id(String blog_detail_id) {
        this.blog_detail_id = blog_detail_id;
    }

    public String getBlog_detail_name() {
        return blog_detail_name;
    }

    public void setBlog_detail_name(String blog_detail_name) {
        this.blog_detail_name = blog_detail_name;
    }

    public String getBlog_detail_image() {
        return blog_detail_image;
    }

    public void setBlog_detail_image(String blog_detail_image) {
        this.blog_detail_image = blog_detail_image;
    }

    public String getBlog_detail_description() {
        return blog_detail_description;
    }

    public void setBlog_detail_description(String blog_detail_description) {
        this.blog_detail_description = blog_detail_description;
    }

    public String getGetBlog_detail_datetime() {
        return getBlog_detail_datetime;
    }

    public void setGetBlog_detail_datetime(String getBlog_detail_datetime) {
        this.getBlog_detail_datetime = getBlog_detail_datetime;
    }
}
