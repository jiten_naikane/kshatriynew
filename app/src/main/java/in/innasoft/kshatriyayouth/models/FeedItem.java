package in.innasoft.kshatriyayouth.models;

public class FeedItem {
    private int id;
    private String name;


    private String id_comment;
    private String status;
    private String image;
    private String profilePic;
    private String timeStamp;
    private String url;
    private String time;
    private String username;


    private String user_like_status;
    private String comment_likes_count;


    private String user_name;


    private String comment_count;
    private String comment_edit_count;

    public FeedItem() {
    }

    public FeedItem(int id, String name, String image, String status,
                    String profilePic, String timeStamp, String url) {
        super();
        this.id = id;
        this.name = name;
        this.image = image;
        this.status = status;
        this.profilePic = profilePic;
        this.timeStamp = timeStamp;
        this.url = url;
    }

    public String getComment_edit_count() {
        return comment_edit_count;
    }

    public void setComment_edit_count(String comment_edit_count) {
        this.comment_edit_count = comment_edit_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImge() {
        return image;
    }

    public void setImge(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId_comment() {
        return id_comment;
    }

    public void setId_comment(String id_comment) {
        this.id_comment = id_comment;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_like_status() {
        return user_like_status;
    }

    public void setUser_like_status(String user_like_status) {
        this.user_like_status = user_like_status;
    }

    public String getComment_likes_count() {
        return comment_likes_count;
    }

    public void setComment_likes_count(String comment_likes_count) {
        this.comment_likes_count = comment_likes_count;
    }
}
