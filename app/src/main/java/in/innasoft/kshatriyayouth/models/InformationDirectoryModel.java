package in.innasoft.kshatriyayouth.models;

public class InformationDirectoryModel {
    public String info_id;
    public String occupation;
    public String name;
    public String info_image;

    public String getInfo_image() {
        return info_image;
    }

    public void setInfo_image(String info_image) {
        this.info_image = info_image;
    }

    public String getInfo_id() {
        return info_id;
    }

    public void setInfo_id(String info_id) {
        this.info_id = info_id;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
