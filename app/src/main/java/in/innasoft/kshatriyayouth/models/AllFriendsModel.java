package in.innasoft.kshatriyayouth.models;


public class AllFriendsModel {

    public String name;
    public String surname;
    public String username;
    public String photo;
    public String friendstatus;
    public String requeststatus;
    public String sentbystatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFriendstatus() {
        return friendstatus;
    }

    public void setFriendstatus(String friendstatus) {
        this.friendstatus = friendstatus;
    }

    public String getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(String requeststatus) {
        this.requeststatus = requeststatus;
    }

    public String getSentbystatus() {
        return sentbystatus;
    }

    public void setSentbystatus(String sentbystatus) {
        this.sentbystatus = sentbystatus;
    }

}
