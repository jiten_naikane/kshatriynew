package in.innasoft.kshatriyayouth.models;

/**
 * Created by purushotham on 30/5/17.
 */

public class SelectedImageModel2 {
    String image_uri;
    String orginal_url;

    public String getOrginal_url() {
        return orginal_url;
    }

    public void setOrginal_url(String orginal_url) {
        this.orginal_url = orginal_url;
    }

    public String getImage_uri() {
        return image_uri;
    }

    public void setImage_uri(String image_uri) {
        this.image_uri = image_uri;
    }
}
