package in.innasoft.kshatriyayouth.models;

public class SurnameListModel {
    public String id;
    public String surname;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
