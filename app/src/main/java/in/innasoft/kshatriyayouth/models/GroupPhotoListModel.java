package in.innasoft.kshatriyayouth.models;


import java.io.Serializable;

public class GroupPhotoListModel implements Serializable {
    public String id;
    public String post_id;
    public String username;
    public String photo_link;
    public String date;
    public String photo_identifier;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto_link() {
        return photo_link;
    }

    public void setPhoto_link(String photo_link) {
        this.photo_link = photo_link;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhoto_identifier() {
        return photo_identifier;
    }

    public void setPhoto_identifier(String photo_identifier) {
        this.photo_identifier = photo_identifier;
    }
}
