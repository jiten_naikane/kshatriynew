package in.innasoft.kshatriyayouth.models;

/**
 * Created by Devolper on 25-Sep-17.
 */

public class SuggestGroupModel {
    public String id;
    public String group_id;
    public String group_manager;
    public String group_name;
    public String group_description;
    public String group_type;
    public String group_picture;
    public String date;
    public String group_count;
    public String group_status;

    public String getGroup_status() {
        return group_status;
    }

    public void setGroup_status(String group_status) {
        this.group_status = group_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_manager() {
        return group_manager;
    }

    public void setGroup_manager(String group_manager) {
        this.group_manager = group_manager;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_description() {
        return group_description;
    }

    public void setGroup_description(String group_description) {
        this.group_description = group_description;
    }

    public String getGroup_type() {
        return group_type;
    }

    public void setGroup_type(String group_type) {
        this.group_type = group_type;
    }

    public String getGroup_picture() {
        return group_picture;
    }

    public void setGroup_picture(String group_picture) {
        this.group_picture = group_picture;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGroup_count() {
        return group_count;
    }

    public void setGroup_count(String group_count) {
        this.group_count = group_count;
    }
}
