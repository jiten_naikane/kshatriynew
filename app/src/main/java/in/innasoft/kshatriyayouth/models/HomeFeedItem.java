package in.innasoft.kshatriyayouth.models;

import java.util.ArrayList;

public class HomeFeedItem {


    private String id;
    private String page_id;
    private String type;
    private String post;
    private String security;
    private String date;
    private String name;
    private String surname;
    private String username;
    private String photo;
    private String photo_list_count;
    private String hiddenitem;
    ///////////////////////photo_list
    private String photo_list_id;
    private String photo_list_post_id;
    private String photo_list_username;
    private String photo_list_photo_link;
    private String photo_list_date;
    private String photo_list_photo_identifier;
    private String image_one;
    private String image_two;
    private String image_three;
    private String post_edit_count;
    private String user_like_status;
    private String video_list_count;
    /////////////////////////////video_list
    private String video_list_id;
    private String video_list_post_id;
    private String video_list_video_id;
    private String video_list_type;
    private String video_list_username;
    private String video_list_url;
    private String video_list_author;
    private String video_list_title;
    private String video_list_description;
    private String video_list_keywords;
    private String video_list_geo_position;
    private String video_list_ip;
    private String video_list_date;
    private String video_list_video_identifier;
    ///////////////////////////////location_list
    ////////////////////////////////post_likes_count
    private String post_likes_count;
    private String user_like_status_type;
    //////////////////////////////
    private String shared_info_count;
    private String shared_info_name;
    private String shared_info_surname;
    private String shared_info_username;
    private String shared_info_photo;
    private String shared_info_post;

    ///////////////////////////////post_comments_count
    private String post_comments_count;
    private ArrayList<HomeFeedItem> List;

    public ArrayList<HomeFeedItem> getList() {
        return List;
    }

    public void setList(ArrayList<HomeFeedItem> list) {
        List = list;
    }

    public String getVideo_list_count() {
        return video_list_count;
    }

    public void setVideo_list_count(String video_list_count) {
        this.video_list_count = video_list_count;
    }

    public String getUser_like_status_type() {
        return user_like_status_type;
    }

    public void setUser_like_status_type(String user_like_status_type) {
        this.user_like_status_type = user_like_status_type;
    }

    public String getShared_info_count() {
        return shared_info_count;
    }

    public void setShared_info_count(String shared_info_count) {
        this.shared_info_count = shared_info_count;
    }

    public String getShared_info_name() {
        return shared_info_name;
    }

    public void setShared_info_name(String shared_info_name) {
        this.shared_info_name = shared_info_name;
    }

    public String getShared_info_surname() {
        return shared_info_surname;
    }

    public void setShared_info_surname(String shared_info_surname) {
        this.shared_info_surname = shared_info_surname;
    }

    public String getShared_info_username() {
        return shared_info_username;
    }

    public void setShared_info_username(String shared_info_username) {
        this.shared_info_username = shared_info_username;
    }

    public String getShared_info_photo() {
        return shared_info_photo;
    }

    public void setShared_info_photo(String shared_info_photo) {
        this.shared_info_photo = shared_info_photo;
    }

    public String getShared_info_post() {
        return shared_info_post;
    }

    public void setShared_info_post(String shared_info_post) {
        this.shared_info_post = shared_info_post;
    }

    public String getHiddenitem() {
        return hiddenitem;
    }

    public void setHiddenitem(String hiddenitem) {
        this.hiddenitem = hiddenitem;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPage_id() {
        return page_id;
    }

    public void setPage_id(String page_id) {
        this.page_id = page_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto_list_count() {
        return photo_list_count;
    }

    public void setPhoto_list_count(String photo_list_count) {
        this.photo_list_count = photo_list_count;
    }

    public String getPhoto_list_id() {
        return photo_list_id;
    }

    public void setPhoto_list_id(String photo_list_id) {
        this.photo_list_id = photo_list_id;
    }

    public String getPhoto_list_post_id() {
        return photo_list_post_id;
    }

    public void setPhoto_list_post_id(String photo_list_post_id) {
        this.photo_list_post_id = photo_list_post_id;
    }

    public String getPhoto_list_username() {
        return photo_list_username;
    }

    public void setPhoto_list_username(String photo_list_username) {
        this.photo_list_username = photo_list_username;
    }

    public String getPhoto_list_photo_link() {
        return photo_list_photo_link;
    }

    public void setPhoto_list_photo_link(String photo_list_photo_link) {
        this.photo_list_photo_link = photo_list_photo_link;
    }

    public String getPhoto_list_date() {
        return photo_list_date;
    }

    public void setPhoto_list_date(String photo_list_date) {
        this.photo_list_date = photo_list_date;
    }

    public String getPhoto_list_photo_identifier() {
        return photo_list_photo_identifier;
    }

    public void setPhoto_list_photo_identifier(String photo_list_photo_identifier) {
        this.photo_list_photo_identifier = photo_list_photo_identifier;
    }

    public String getVideo_list_id() {
        return video_list_id;
    }

    public void setVideo_list_id(String video_list_id) {
        this.video_list_id = video_list_id;
    }

    public String getVideo_list_post_id() {
        return video_list_post_id;
    }

    public void setVideo_list_post_id(String video_list_post_id) {
        this.video_list_post_id = video_list_post_id;
    }

    public String getVideo_list_video_id() {
        return video_list_video_id;
    }

    public void setVideo_list_video_id(String video_list_video_id) {
        this.video_list_video_id = video_list_video_id;
    }

    public String getVideo_list_type() {
        return video_list_type;
    }

    public void setVideo_list_type(String video_list_type) {
        this.video_list_type = video_list_type;
    }

    public String getVideo_list_username() {
        return video_list_username;
    }

    public void setVideo_list_username(String video_list_username) {
        this.video_list_username = video_list_username;
    }

    public String getVideo_list_url() {
        return video_list_url;
    }

    public void setVideo_list_url(String video_list_url) {
        this.video_list_url = video_list_url;
    }

    public String getVideo_list_author() {
        return video_list_author;
    }

    public void setVideo_list_author(String video_list_author) {
        this.video_list_author = video_list_author;
    }

    public String getVideo_list_title() {
        return video_list_title;
    }

    public void setVideo_list_title(String video_list_title) {
        this.video_list_title = video_list_title;
    }

    public String getVideo_list_description() {
        return video_list_description;
    }

    public void setVideo_list_description(String video_list_description) {
        this.video_list_description = video_list_description;
    }

    public String getVideo_list_keywords() {
        return video_list_keywords;
    }

    public void setVideo_list_keywords(String video_list_keywords) {
        this.video_list_keywords = video_list_keywords;
    }

    public String getVideo_list_geo_position() {
        return video_list_geo_position;
    }

    public void setVideo_list_geo_position(String video_list_geo_position) {
        this.video_list_geo_position = video_list_geo_position;
    }

    public String getVideo_list_ip() {
        return video_list_ip;
    }

    public void setVideo_list_ip(String video_list_ip) {
        this.video_list_ip = video_list_ip;
    }

    public String getVideo_list_date() {
        return video_list_date;
    }

    public void setVideo_list_date(String video_list_date) {
        this.video_list_date = video_list_date;
    }

    public String getVideo_list_video_identifier() {
        return video_list_video_identifier;
    }

    public void setVideo_list_video_identifier(String video_list_video_identifier) {
        this.video_list_video_identifier = video_list_video_identifier;
    }

    public String getPost_likes_count() {
        return post_likes_count;
    }

    public void setPost_likes_count(String post_likes_count) {
        this.post_likes_count = post_likes_count;
    }

    public String getPost_comments_count() {
        return post_comments_count;
    }

    public void setPost_comments_count(String post_comments_count) {
        this.post_comments_count = post_comments_count;
    }


    public String getImage_one() {
        return image_one;
    }

    public void setImage_one(String image_one) {
        this.image_one = image_one;
    }

    public String getImage_two() {
        return image_two;
    }

    public void setImage_two(String image_two) {
        this.image_two = image_two;
    }

    public String getImage_three() {
        return image_three;
    }

    public void setImage_three(String image_three) {
        this.image_three = image_three;
    }

    public String getUser_like_status() {
        return user_like_status;
    }

    public void setUser_like_status(String user_like_status) {
        this.user_like_status = user_like_status;
    }

    public String getPost_edit_count() {
        return post_edit_count;
    }

    public void setPost_edit_count(String post_edit_count) {
        this.post_edit_count = post_edit_count;
    }


}
