package in.innasoft.kshatriyayouth.imageselector.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.activities.PostStatusActivity;
import in.innasoft.kshatriyayouth.holders.SelectedImageHolder2;
import in.innasoft.kshatriyayouth.itemClickListerners.SelectedImageItemClickListener2;
import in.innasoft.kshatriyayouth.models.SelectedImageModel2;


public class SelectedImageAdapter2 extends RecyclerView.Adapter<SelectedImageHolder2> {

    public ArrayList<SelectedImageModel2> selectedImageModels2;
    PostStatusActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public SelectedImageAdapter2(ArrayList<SelectedImageModel2> selectedImageModels2, PostStatusActivity context, int resource) {
        this.selectedImageModels2 = selectedImageModels2;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SelectedImageHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        SelectedImageHolder2 slh = new SelectedImageHolder2(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final SelectedImageHolder2 holder, final int position) {

               /* holder.selected_image_iv.setImageURI(Uri.parse(selectedImageModels2.get(position)
                        .getImage_uri()));*/

                /*Uri myUri = Uri.parse(selectedImageModels2.get(position).getImage_uri());

                final InputStream imageStream;
                try {
                        imageStream = context.getActivity().getContentResolver().openInputStream(myUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                        holder.selected_image_iv.setImageBitmap(selectedImage);
                } catch (FileNotFoundException e) {
                        e.printStackTrace();
                }*/


        try {
            final Uri imageUri = Uri.parse(selectedImageModels2.get(position).getImage_uri());
            final InputStream imageStream = context.getContentResolver().openInputStream(imageUri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            Bitmap bt = Bitmap.createScaledBitmap(selectedImage, 150, 150, false);
            holder.selected_image_iv.setImageBitmap(bt);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
        }


        Picasso.with(context).load(selectedImageModels2
                .get(position)
                .getImage_uri())
                .resize(100, 100)
                .placeholder(R.drawable.user_profile)
                .into(holder.selected_image_iv);

        holder.setItemClickListener(new SelectedImageItemClickListener2() {
            @Override
            public void onItemClick(View v, int pos) {

                //Toast.makeText(context.getActivity(), ""+selectedImageModels2.get(pos).getImage_uri(), Toast.LENGTH_SHORT).show();

            }
        });

        holder.selected_image_delete_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedImageModels2.remove(position);
                context.reloadeSelectedImage2();
            }
        });

    }

    @Override
    public int getItemCount() {


        return this.selectedImageModels2.size();

    }
}
