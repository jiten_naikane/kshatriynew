package in.innasoft.kshatriyayouth.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SlidingImage_Adapter;
import in.innasoft.kshatriyayouth.models.GroupPhotoListModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class FeedAllImagesActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView delete_group_photo_image;

    //    Typeface typeface;
    String image_path, title_caps, u_id, id, modelsize;
    ViewPager viewPager;
    File imagePath = null;
    int images[] = {R.drawable.app_logo_new, R.drawable.app_logo_new};
    // MyZoomImageCustomPagerAdapter myCustomPagerAdapter;
    UserSessionManager session;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    ImageView iv1, iv2;
    private ArrayList<String> ImagesArray = new ArrayList<>();
    private String[] IMAGES;
    private boolean checkInternet;
    ArrayList<String> imgArray = new ArrayList<String>();

    GroupPhotoListModel groupPhotoListModel = new GroupPhotoListModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_images);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");

        showImages();


        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        u_id = userDetails.get(UserSessionManager.USER_ID);


        delete_group_photo_image = (ImageView) findViewById(R.id.delete_group_photo_image);
        delete_group_photo_image.setOnClickListener(this);


        viewPager = (ViewPager) findViewById(R.id.viewPager);

       /* myCustomPagerAdapter = new MyZoomImageCustomPagerAdapter(ZoomImageDescActivity.this, myList);
        viewPager.setAdapter(myCustomPagerAdapter);
        myCustomPagerAdapter.notifyDataSetChanged();*/

        // init();


        iv1 = (ImageView) findViewById(R.id.iv1);
        iv2 = (ImageView) findViewById(R.id.iv2);


        NUM_PAGES = imgArray.size();
        if (NUM_PAGES != 1) {
            iv2.setVisibility(View.GONE);
            iv1.setVisibility(View.GONE);
        } else {
            iv2.setVisibility(View.GONE);
            iv1.setVisibility(View.GONE);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == NUM_PAGES - 1) {
                    iv1.setVisibility(View.GONE);
                    iv2.setVisibility(View.GONE);
                } else if (position == 0) {
                    iv2.setVisibility(View.GONE);
                    iv1.setVisibility(View.GONE);
                } else if (position > 0 || position < NUM_PAGES - 1) {
                    iv2.setVisibility(View.GONE);
                    iv1.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);


            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab--;
                viewPager.setCurrentItem(tab);
                if (tab == NUM_PAGES - 1) {
                    iv1.setVisibility(View.GONE);
                    iv2.setVisibility(View.GONE);
                } else if (tab == 0) {
                    iv2.setVisibility(View.GONE);
                    iv1.setVisibility(View.GONE);
                } else if (tab > 0 || tab < NUM_PAGES - 1) {
                    iv2.setVisibility(View.GONE);
                    iv1.setVisibility(View.GONE);
                }
            }
        });


    }

   /* private void init()
    {
        for(int i=0;i<IMAGES.length;i++)
            try {
                ImagesArray.add(IMAGES[i]);
            }catch (final NumberFormatException e) {

            }


    }*/

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap) {
        imagePath = new File(Environment.getExternalStorageDirectory() + "/screenshot.png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == delete_group_photo_image) {
            Toast.makeText(FeedAllImagesActivity.this, "DELETE", Toast.LENGTH_LONG).show();
        }

    }

    private void showImages() {

        checkInternet = NetworkChecking.isConnected(FeedAllImagesActivity.this);
        if (checkInternet) {
            String groupid = "1441472409392509";


            String url = AppUrls.BASE_URL + AppUrls.GROUP_POST_PHOTOS + "/" + id;
            Log.d("photolisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("photolistuRESP", response);


                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {

                                    JSONObject data = jsonObject.getJSONObject("data");
                                    JSONArray photo_list = data.getJSONArray("photo_list");
                                    for (int i = 0; i < photo_list.length(); i++) {
                                        JSONObject json_photos = photo_list.getJSONObject(i);
                                        String images = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + json_photos.getString("photo_link");
                                        imgArray.add(images);

                                    }
                                    viewPager.setAdapter(new SlidingImage_Adapter(FeedAllImagesActivity.this, imgArray));
                                    Log.d("xcnvbxckbvjx", "" + imgArray);


                                }

                                if (responceCode.equals("10200")) {


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(FeedAllImagesActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(FeedAllImagesActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
}
