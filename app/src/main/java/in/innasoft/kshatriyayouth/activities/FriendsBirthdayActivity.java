package in.innasoft.kshatriyayouth.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.DOBFriendsVeiwPagerAdapter;
import in.innasoft.kshatriyayouth.models.DOBModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class FriendsBirthdayActivity extends AppCompatActivity {
    ViewPager viewPager_birthday;
    // ImageView  iv1_birthday,iv2_birthday;
    DOBFriendsVeiwPagerAdapter dobFriendsVeiwPagerAdapter;
    ArrayList<DOBModel> db_friend_list = new ArrayList<DOBModel>();
    UserSessionManager sessionManager;
    String userid = "", user_profile_id = "";
    private boolean checkInternet;
    TextView birthdays_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_birthday);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager_birthday = (ViewPager) findViewById(R.id.viewPager_birthday);
        birthdays_text = (TextView) findViewById(R.id.birthdays_text);
        //   iv1_birthday=(ImageView)findViewById(R.id.iv1_birthday);
        //    iv2_birthday=(ImageView)findViewById(R.id.iv2_birthday);

        sessionManager = new UserSessionManager(FriendsBirthdayActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();

        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);

        getDobWishes();

    }

    private void getDobWishes() {
        checkInternet = NetworkChecking.isConnected(FriendsBirthdayActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.DATE_OF_BIRTH_WHISHES;
            Log.d("DOB", url);
            db_friend_list.clear();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("DOBURLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jarrFreindDob = jsonObject1.getJSONArray("friendsBirthdayData");
                                    String freind_array = jarrFreindDob.toString();
                                    if (freind_array.equals("[]")) {
                                        birthdays_text.setText("No Birthdays");
                                        // Toast.makeText(FriendsBirthdayActivity.this, "No Birthdays", Toast.LENGTH_SHORT).show();
                                    } else {
                                        birthdays_text.setVisibility(View.GONE);
                                        for (int i = 0; i < jarrFreindDob.length(); i++) {
                                            JSONObject jsonObject2 = jarrFreindDob.getJSONObject(i);

                                            DOBModel item = new DOBModel();
                                            item.setId(jsonObject2.getString("id"));
                                            item.setName(jsonObject2.getString("name") + " " + jsonObject2.getString("surname"));
                                            String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                            Log.d("image_photo", image);
                                            item.setPhoto(image);
                                            item.setUsername(jsonObject2.getString("username"));

                                            db_friend_list.add(item);
                                            Log.d("LLLLLLLL", db_friend_list.toString());
                                        }
                                        dobFriendsVeiwPagerAdapter = new DOBFriendsVeiwPagerAdapter(db_friend_list, FriendsBirthdayActivity.this);

                                        viewPager_birthday.setAdapter(dobFriendsVeiwPagerAdapter);
                                    }

                                }
                                if (responceCode.equals("10800")) {


                                }
                                if (responceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    Log.d("DOBPARAM::", params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(FriendsBirthdayActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(FriendsBirthdayActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
