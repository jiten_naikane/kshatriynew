package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SubNotificationsListAdapter;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.FeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FilePath;
import in.innasoft.kshatriyayouth.utilities.ImagePermissions;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

import static in.innasoft.kshatriyayouth.app.AppController.TAG;

public class SubCommentsActivity extends AppCompatActivity {
    //    Typeface typeface;
    ImageView image_view;
    UserSessionManager sessionManager;
    Drawable d;
    EditText commenttext;
    ImageView send_image, select_image;
    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";


    private boolean checkInternet;
    private List<FeedItem> feedItems;
    String profile_pic_url = "";
    String user_name = "";
    //////////////////////////////
    private ListView listView;
    private SubNotificationsListAdapter listAdapter;
    String id = "";
    String friends = "";
    private String URL_FEED = AppUrls.BASE_URL + AppUrls.GROUP_POST_COMMENT_LIKECOMMENT;
    Uri selectedFileUri;
    private String selectedFilePath = "null";
    HttpEntity resEntity;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_comments);
        id = getIntent().getExtras().getString("id");
        friends = getIntent().getExtras().getString("friends");
        sessionManager = new UserSessionManager(SubCommentsActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        user_name = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        //  Log.d("COMING",friends);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        listView = (ListView) findViewById(R.id.list);
        image_view = (ImageView) findViewById(R.id.image_view1);
        commenttext = (EditText) findViewById(R.id.commenttext);
        send_image = (ImageView) findViewById(R.id.send_image);
        select_image = (ImageView) findViewById(R.id.select_image);
        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(SubCommentsActivity.this);
                if (checkInternet) {
                    final boolean result = ImagePermissions.checkPermission(SubCommentsActivity.this);
                    final Dialog mBottomSheetDialog = new Dialog(SubCommentsActivity.this, R.style.MaterialDialogSheet);
               /* TextView dialog_title = (TextView) mBottomSheetDialog.findViewById(R.id.dialog_title);
//                dialog_title.setTypeface(typeface);*/
                    mBottomSheetDialog.setContentView(R.layout.custom_dialog_image_selection); // your custom view.
                    mBottomSheetDialog.setCancelable(true);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                    mBottomSheetDialog.show();

                    TextView take_photo_text = (TextView) mBottomSheetDialog.findViewById(R.id.take_photo_text);
//                take_photo_text.setTypeface(typeface);
                    take_photo_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(SubCommentsActivity.this, "take photo", Toast.LENGTH_LONG).show();
                            if (result) {
                                if (ContextCompat.checkSelfPermission(SubCommentsActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    if (getFromPref(SubCommentsActivity.this, ALLOW_KEY)) {
                                        showSettingsAlert();
                                    } else if (ContextCompat.checkSelfPermission(SubCommentsActivity.this,
                                            android.Manifest.permission.CAMERA)

                                            != PackageManager.PERMISSION_GRANTED) {

                                        // Should we show an explanation?
                                        if (ActivityCompat.shouldShowRequestPermissionRationale(SubCommentsActivity.this,
                                                android.Manifest.permission.CAMERA)) {
                                            showAlert();
                                        } else {
                                            // No explanation needed, we can request the permission.
                                            ActivityCompat.requestPermissions(SubCommentsActivity.this,
                                                    new String[]{android.Manifest.permission.CAMERA},
                                                    CAMERA_REQUEST_IMAGE);
                                        }
                                    }
                                } else {
                                    //  cameraIntent();
                                    mBottomSheetDialog.dismiss();
                                }

                            }


                        }
                    });

                    TextView select_from_gallery = (TextView) mBottomSheetDialog.findViewById(R.id.select_from_gallery);
//                select_from_gallery.setTypeface(typeface);
                    select_from_gallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Toast.makeText(GroupCreateActivityDialog.this, "gallry", Toast.LENGTH_LONG).show();
                        /*if (result) {

                        }*/
                            galleryIntent();
                            mBottomSheetDialog.dismiss();


                        }
                    });

                    mBottomSheetDialog.show();
                } else {
                    Toast.makeText(SubCommentsActivity.this, "No Internet Connection ...!", Toast.LENGTH_SHORT).show();

                }
            }
        });

        send_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!commenttext.getText().toString().equals("")) {
                    postComment(id);
                } else {
                    Toast.makeText(SubCommentsActivity.this, "Please write some text", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Picasso.with(SubCommentsActivity.this)
                .load(profile_pic_url)
                .error(R.drawable.normal_bg)
                .into(image_view);
        feedItems = new ArrayList<FeedItem>();

        listAdapter = new SubNotificationsListAdapter(SubCommentsActivity.this, feedItems);
        listView.setAdapter(listAdapter);
        subComments(id, user_name);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, final long id) {
                final String comment_id = feedItems.get(position).getId_comment();
                final String name = feedItems.get(position).getTimeStamp();
                if (user_name.equals(feedItems.get(position).getUsername())) {
                    CharSequence[] items = {"Edit Reply", "Delete"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(SubCommentsActivity.this);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            if (item == 0) {

                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        SubCommentsActivity.this);
                                LayoutInflater inflater = (LayoutInflater) SubCommentsActivity.this
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = inflater.inflate(R.layout.dialog_demo, null);
                                alertDialogBuilder.setView(view);
                                alertDialogBuilder.setCancelable(false);
                                final AlertDialog dialog1 = alertDialogBuilder.create();
                                dialog1.show();
                                final EditText report_edit_text = (EditText) view.findViewById(R.id.report_edit_text);
                                report_edit_text.setText(name);
                                final TextView text = (TextView) view.findViewById(R.id.text);
                                text.setText("EDIT REPLY");
                                Button dialog_report = (Button) view.findViewById(R.id.dialog_report);
                                dialog_report.setText("Save");
                                Button dialog_cancel = (Button) view.findViewById(R.id.dialog_cancel);
                                final String edt_text = report_edit_text.getText().toString();
                                dialog_report.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!report_edit_text.getText().toString().equals(name)) {
                                            commentEdit(comment_id, report_edit_text.getText().toString(), name);
                                            dialog1.cancel();
                                        }
                                    }
                                });

                                dialog_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog1.cancel();
                                    }
                                });
                            } else if (item == 1) {

                                commentDelete(comment_id);
                            }
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
                    wmlp.gravity = Gravity.NO_GRAVITY | Gravity.NO_GRAVITY;
                    wmlp.x = 100;   //x position
                    wmlp.y = 100;   //y position

                    dialog.show();

                }


                return false;
            }
        });
        // We first check for cached request
    }

    public void subComments(String id, String username) {
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL_FEED + "/" + id + "/" + username, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    private void parseJsonFeed(JSONObject response) {
        feedItems.clear();
        try {
            JSONObject feedObject = response.getJSONObject("data");
            JSONArray feedArray = feedObject.getJSONArray("comments_list");
            //  JSONArray feedArray_likes = feedObject.getJSONArray("post_likes");
            Log.d("sdjvkbbkvsdfbvsjk", "" + feedArray);

            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setId(feedObj.getInt("id"));
                item.setId_comment(feedObj.getString("id"));
                item.setName(feedObj.getString("name") + " " + feedObj.getString("surname"));
                item.setTimeStamp(feedObj.getString("reply"));
                item.setUsername(feedObj.getString("username"));
                item.setUser_like_status(feedObj.getString("user_like_status"));
                // item.setComment_count(feedObj.getString("comment_count"));
                item.setComment_edit_count(feedObj.getString("reply_edit_count"));
                item.setTime(feedObj.getString("date"));
                String profile_pic = AppUrls.BASE_IMAGE_URL + feedObj.getString("photo");
                item.setProfilePic(profile_pic);
                String image = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + feedObj.getString("images");
                item.setImge(image);

               /* // Image might be null sometimes
                String image = feedObj.isNull("jukebox_images") ? null : feedObj
                        .getString("jukebox_images");
                item.setImge(image);
                item.setStatus(feedObj.getString("movietype"));



                // url might be null sometimes
                String feedUrl = feedObj.isNull("still") ? null : feedObj
                        .getString("still");
                item.setUrl(feedUrl);*/
                Log.d("image", image);
                feedItems.add(item);
            }
           /* for (int i = 0; i < feedArray_likes.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray_likes.get(i);
                FeedItem item = new FeedItem();

                item.setUser_name(feedObj.getString("username"));


                feedItems.add(item);
            }*/

            // notify data changes to list adapater
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postComment(final String post_id) {

        if (selectedFileUri == null) {

            SubCommentsActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (progressDialog != null)
                        progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String urlString = "";
            String response_str = null;
              /*  String response_str = null;
                File file1 = null;
                FileBody bin1 = null;
                String empty = "";
                Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                file1 = new File(path.replaceAll("file://", "").replace("%20", " "));
                // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
                String urlString = "";*/


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                urlString = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            } else {
                urlString = AppUrls.BASE_IMAGE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            }
            Log.d("REQUESTURL", urlString);
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(urlString);

                MultipartEntity reqEntity = new MultipartEntity();
                Log.d("USERIDETAILIFF:", post_id + "///" + friends + "///" + user_name + "///" + commenttext.getText().toString());

                reqEntity.addPart("comment_id", new StringBody(post_id));
                reqEntity.addPart("friend", new StringBody(friends));
                reqEntity.addPart("username", new StringBody(user_name));
                reqEntity.addPart("reply", new StringBody(commenttext.getText().toString()));
                // bin1 = new FileBody(file1);
                reqEntity.addPart("userfile", new StringBody(""));
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                response_str = EntityUtils.toString(resEntity);
                Log.d("APPLYRESP", response_str);
                try {
                    JSONObject jsonObject = new JSONObject(response_str);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equalsIgnoreCase("10100")) {
                        listView.setAdapter(listAdapter);
                        selectedFileUri = null;
                        commenttext.setText("");
                        Cache cache = AppController.getInstance().getRequestQueue().getCache();
                        Cache.Entry entry = cache.get(URL_FEED);
                        if (entry != null) {
                            // fetch the data from cache
                            try {
                                String data = new String(entry.data, "UTF-8");
                                try {
                                    parseJsonFeed(new JSONObject(data));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        } else {
                            // making fresh volley request and getting json
                            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                    URL_FEED + "/" + post_id + "/" + user_name, null, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    VolleyLog.d(TAG, "Response: " + response.toString());
                                    if (response != null) {
                                        parseJsonFeed(response);
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                                }
                            });

                            // Adding request to volley request queue
                            AppController.getInstance().addToRequestQueue(jsonReq);
                        }

                    }
                    if (responceCode.equals("10200")) {

                        Toast.makeText(SubCommentsActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }

            // Toast.makeText(this, "Please move your .pdf file to internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            String path = FilePath.getPath(this, selectedFileUri);
            SubCommentsActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (progressDialog != null)
                        progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            String response_str = null;
            File file1 = null;
            FileBody bin1 = null;
            String empty = "";
            Log.d("SDFAFGSGFSG", "sfafafsaf 1");
            file1 = new File(path.replaceAll("file://", "").replace("%20", " "));
            // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
            String urlString = "";


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                urlString = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            } else {
                urlString = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            }
            Log.d("REQUESTURL", urlString);
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(urlString);

                MultipartEntity reqEntity = new MultipartEntity();
                Log.d("USERIDETAILIFF:", post_id + "///" + friends + "///" + user_name + "///" + commenttext.getText().toString() + "//" + file1);

                reqEntity.addPart("comment_id", new StringBody(post_id));
                reqEntity.addPart("username", new StringBody(user_name));
                reqEntity.addPart("friend", new StringBody(friends));
                reqEntity.addPart("reply", new StringBody(commenttext.getText().toString()));
                bin1 = new FileBody(file1);
                reqEntity.addPart("userfile", bin1);

                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                response_str = EntityUtils.toString(resEntity);
                Log.d("APPLYRESP", response_str);
                try {
                    JSONObject jsonObject = new JSONObject(response_str);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equalsIgnoreCase("10100")) {
                        listView.setAdapter(listAdapter);
                        commenttext.setText("");
                        selectedFileUri = null;
                        //     selectedFileUri = null;
                        Cache cache = AppController.getInstance().getRequestQueue().getCache();
                        Cache.Entry entry = cache.get(URL_FEED);
                        if (entry != null) {
                            // fetch the data from cache
                            try {
                                String data = new String(entry.data, "UTF-8");
                                try {
                                    parseJsonFeed(new JSONObject(data));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        } else {
                            // making fresh volley request and getting json
                            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                    URL_FEED + "/" + post_id + "/" + user_name, null, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    VolleyLog.d(TAG, "Response: " + response.toString());
                                    if (response != null) {
                                        parseJsonFeed(response);
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                                }
                            });

                            // Adding request to volley request queue
                            AppController.getInstance().addToRequestQueue(jsonReq);
                        }

                    }
                    if (responceCode.equals("10200")) {

                        Toast.makeText(SubCommentsActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
        }
/*

        if (selectedFileUri == null)
          {

            // Toast.makeText(SubCommentsActivity.this,"null",Toast.LENGTH_LONG).show();
            SubCommentsActivity.this.runOnUiThread(new Runnable()
            {
                public void run()
                {
                    if (progressDialog != null)
                        progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String urlString = "";
            String response_str = null;
              *//*  String response_str = null;
                File file1 = null;
                FileBody bin1 = null;
                String empty = "";
                Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                file1 = new File(path.replaceAll("file://", "").replace("%20", " "));
                // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
                String urlString = "";*//*


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                urlString = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            } else
                {
                urlString = AppUrls.BASE_IMAGE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            }
            Log.d("REQUESTURLIf", urlString);
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(urlString);

                MultipartEntity reqEntity = new MultipartEntity();
               Log.d("USERIDETAILIFFF:",post_id+"///"+user_name+"///"+user_name+"///"+friends+"//"+commenttext.getText().toString());

                reqEntity.addPart("comment_id", new StringBody(post_id));
                reqEntity.addPart("friend", new StringBody(friends));
                reqEntity.addPart("username", new StringBody(user_name));
                reqEntity.addPart("reply", new StringBody(commenttext.getText().toString()));
                // bin1 = new FileBody(file1);
                reqEntity.addPart("userfile", new StringBody(""));
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                response_str = EntityUtils.toString(resEntity);
                Log.d("APPLYRESPIFreeere", response_str);
                try {
                    JSONObject jsonObject = new JSONObject(response_str);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equalsIgnoreCase("10100"))
                    {
                        listView.setAdapter(listAdapter);
                        commenttext.setText("");
                        selectedFileUri = null;
                        Cache cache = AppController.getInstance().getRequestQueue().getCache();
                        Cache.Entry entry = cache.get(URL_FEED);
                        if (entry != null) {
                            // fetch the data from cache
                            try {
                                String data = new String(entry.data, "UTF-8");
                                try {
                                    parseJsonFeed(new JSONObject(data));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        }
                        else {
                            // making fresh volley request and getting json
                            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                    URL_FEED+"/"+post_id+"/"+user_name, null, new Response.Listener<JSONObject>()
                            {

                                @Override
                                public void onResponse(JSONObject response) {
                                    VolleyLog.d(TAG, "Response: " + response.toString());
                                    if (response != null) {
                                        parseJsonFeed(response);
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                                }
                            });

                            // Adding request to volley request queue
                            AppController.getInstance().addToRequestQueue(jsonReq);
                        }

                    }
                    if (responceCode.equals("10200")) {

                        Toast.makeText(SubCommentsActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }

            // Toast.makeText(this, "Please move your .pdf file to internal storage and retry", Toast.LENGTH_LONG).show();
        }
        else
        {
            String path = FilePath.getPath(this, selectedFileUri);
            Log.d("PPPPPPP:",path);
            //String path =selectedFilePath;
            SubCommentsActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (progressDialog != null)
                        progressDialog.show();
                }
            });

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            String response_str = null;
            File file1 = null;
            FileBody bin1 = null;
            String empty = "";
            Log.d("SDFAFGSGFSGELSE", "sfafafsaf 1");
            file1 = new File(path.replaceAll("file://", "").replace("%20", " "));
            // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
            String urlString = "";


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                urlString = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            } else {
                urlString = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS;
            }
            Log.d("REQUESTURLELSE", urlString);
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(urlString);

                MultipartEntity reqEntity = new MultipartEntity();
                Log.d("USERIDETAILELSE:",post_id+"///"+user_name+"///"+user_name+"///"+friends+"//"+commenttext.getText().toString()+"///"+file1);

                reqEntity.addPart("comment_id", new StringBody(post_id));
                reqEntity.addPart("username", new StringBody(user_name));
                reqEntity.addPart("friend", new StringBody(friends));
                reqEntity.addPart("reply", new StringBody(commenttext.getText().toString()));
                bin1 = new FileBody(file1);
                reqEntity.addPart("userfile", bin1);

                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                response_str = EntityUtils.toString(resEntity);
                Log.d("APPLYRESPELSE", response_str);
                try {
                    JSONObject jsonObject = new JSONObject(response_str);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equalsIgnoreCase("10100"))
                    {
                        listView.setAdapter(listAdapter);
                        commenttext.setText("");
                        selectedFileUri = null;
                        Cache cache = AppController.getInstance().getRequestQueue().getCache();
                        Cache.Entry entry = cache.get(URL_FEED);
                        if (entry != null) {
                            // fetch the data from cache
                            try {
                                String data = new String(entry.data, "UTF-8");
                                try {
                                    parseJsonFeed(new JSONObject(data));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        }
                        else {
                            // making fresh volley request and getting json
                            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                    URL_FEED+"/"+post_id+"/"+user_name, null, new Response.Listener<JSONObject>()
                            {

                                @Override
                                public void onResponse(JSONObject response) {
                                    VolleyLog.d(TAG, "Response: " + response.toString());
                                    if (response != null) {
                                        parseJsonFeed(response);
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                                }
                            });

                            // Adding request to volley request queue
                            AppController.getInstance().addToRequestQueue(jsonReq);
                        }

                    }
                    if (responceCode.equals("10200")) {

                        Toast.makeText(SubCommentsActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
        }

       *//* //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(SubCommentsActivity.this);
        if(checkInternet)
        {

            String url = AppUrls.BASE_URL + AppUrls.USER_POST_COMMENT_COMMENTS ;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    listView.setAdapter(listAdapter);
                                    commenttext.setText("");
                                    Cache cache = AppController.getInstance().getRequestQueue().getCache();
                                    Cache.Entry entry = cache.get(URL_FEED);
                                    if (entry != null) {
                                        // fetch the data from cache
                                        try {
                                            String data = new String(entry.data, "UTF-8");
                                            try {
                                                parseJsonFeed(new JSONObject(data));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        // making fresh volley request and getting json
                                        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                                                URL_FEED+"/"+post_id+"/"+user_name, null, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                VolleyLog.d(TAG, "Response: " + response.toString());
                                                if (response != null) {
                                                    parseJsonFeed(response);
                                                }
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                            }
                                        });

                                        // Adding request to volley request queue
                                        AppController.getInstance().addToRequestQueue(jsonReq);
                                    }

*//**//*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*//**//*





                                }
                                if(responceCode.equals("10800"))
                                {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", post_id);
                    params.put("username", user_name);
                    params.put("reply", commenttext.getText().toString());
                    params.put("userfile", selectedFilePath);
                    params.put("friend", friends);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                *//**//*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*//**//*
            };
            *//**//*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*//**//*
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(SubCommentsActivity.this);
            requestQueue.add(stringRequest);
        }
        else
        {
            Toast.makeText(SubCommentsActivity.this,"No Internet Connection...!",Toast.LENGTH_LONG).show();
        }*/

    }

    @Override
    public void onResume() {
        super.onResume();
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL_FEED);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    parseJsonFeed(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json
            JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                    URL_FEED + "/" + id, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        parseJsonFeed(response);
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(jsonReq);
        }

    }

    private void commentDelete(final String post_id) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(SubCommentsActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_COMMENT_REPLY_DELETE;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    subComments(id, user_name);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("reply_id", post_id);
                    params.put("type", "comment");
                   /* params.put("type", "Like");
                    params.put("friend", "10823861019183948");*/
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(SubCommentsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(SubCommentsActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void commentEdit(final String post_id, final String edit_text_text, final String name) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(SubCommentsActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.USER_COMMENT_REPLY_EDIT;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    subComments(id, user_name);

/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("comment_id", id);
                    params.put("username", user_name);
                    params.put("reply_new", edit_text_text);
                    params.put("reply_old", name);
                    params.put("reply_id", post_id);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(SubCommentsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(SubCommentsActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(SubCommentsActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(SubCommentsActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                CAMERA_REQUEST_IMAGE);
                    }
                });
        alertDialog.show();
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(SubCommentsActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(SubCommentsActivity.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

          /*  else if (requestCode == CAMERA_REQUEST_IMAGE)
            {
                try {

                   onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    private void onSelectFromGalleryResult(Intent data) throws IOException {

        selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        Log.d("GETTING::::", selectedFilePath);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                // select_image.setVisibility(View.VISIBLE);
                //  select_image.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //final  String sendingimagepath = destination.getPath();


            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }
    }

}
