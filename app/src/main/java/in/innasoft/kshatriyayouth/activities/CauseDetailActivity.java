package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bluejamesbond.text.DocumentView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class CauseDetailActivity extends AppCompatActivity implements View.OnClickListener {

    String cause_id, cause_image_url, cause_name;
    ImageView cause_detail_img;
    Toolbar toolbar_cause;
    CollapsingToolbarLayout collapse_toolbar_cause;

    TextView about_name, about_amount, about_location, about_date_time, posted_by, contact_detail, button_donate;
    DocumentView text_overview;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    BottomSheetDialog bottomSheetDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cause_detail);


        toolbar_cause = (Toolbar) findViewById(R.id.toolbar_cause);
        toolbar_cause.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar_cause);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapse_toolbar_cause = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar_cause);
        collapse_toolbar_cause.setCollapsedTitleTextColor(Color.WHITE);
        collapse_toolbar_cause.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_toolbar_cause.setExpandedTitleTextAppearance(R.style.expandAppBar);
//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));
        cause_detail_img = (ImageView) findViewById(R.id.cause_detail_img);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        cause_id = bundle.getString("CAUSE_ID");
        cause_name = bundle.getString("CAUSE_NAME");
        cause_image_url = bundle.getString("CAUSE_IMAGE");
        Log.d("ASSociatDETAILID", cause_id + ":::" + cause_name + ":::" + cause_image_url);

        about_name = (TextView) findViewById(R.id.about_name);
//        about_name.setTypeface(typeface);

        about_amount = (TextView) findViewById(R.id.about_amount);
//        about_amount.setTypeface(typeface);

        about_location = (TextView) findViewById(R.id.about_location);
//        about_location.setTypeface(typeface);

        about_date_time = (TextView) findViewById(R.id.about_date_time);
//        about_date_time.setTypeface(typeface);

        //  posted_by=(TextView) findViewById(R.id.posted_by);
        //   posted_by.setTypeface(typeface);

        contact_detail = (TextView) findViewById(R.id.contact_detail);
//        contact_detail.setTypeface(typeface);

        text_overview = (DocumentView) findViewById(R.id.text_overview);


        button_donate = (TextView) findViewById(R.id.button_donate);
//        button_donate.setTypeface(typeface);
        button_donate.setOnClickListener(this);

        Picasso.with(CauseDetailActivity.this)
                .load(cause_image_url)
                .placeholder(R.drawable.app_logo_new)
                .into(cause_detail_img);

        getCauseDetailData();

    }

    private void getCauseDetailData() {


        checkInternet = NetworkChecking.isConnected(CauseDetailActivity.this);
        if (checkInternet) {


            progressDialog.show();
            final String url = AppUrls.BASE_URL + AppUrls.CAUSES_DETAILS + "/" + cause_id;
            Log.d("asscauseURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CAUSES_DETAILS + "/" + cause_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("CAUSERESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");


                                    String name = jsonObject2.getString("name");
                                    about_name.setText(Html.fromHtml(name));
                                    collapse_toolbar_cause.setTitle(Html.fromHtml(name));

                                    String amount = jsonObject2.getString("amount");
                                    about_amount.setText(Html.fromHtml("<b> Amount : </b>" + amount));

                                    String location = jsonObject2.getString("location");
                                    about_location.setText(Html.fromHtml("<b> Location : </b>" + location));

                                    String date_time = jsonObject2.getString("create_date_time");
                                    about_date_time.setText(Html.fromHtml("<b> Date and Time : </b>" + date_time));


                                    // String detail=jsonObject2.getString("description");
                                    //  contact_detail.setText(Html.fromHtml(detail));


                                    String overview = jsonObject2.getString("description");
                                    text_overview.setText(Html.fromHtml(overview));

                                    // String image=AppUrls.BASE_URL+jsonObject2.getString("image");


                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    //   no_data.setVisibility(View.VISIBLE);
                                    Toast.makeText(CauseDetailActivity.this, "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            //  no_data.setVisibility(View.VISIBLE);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(CauseDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(CauseDetailActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View v) {
        if (v == button_donate) {
            bottomSheetDialog = new BottomSheetDialog(CauseDetailActivity.this);

            View view = getLayoutInflater().inflate(R.layout.donation_pop, null);
            bottomSheetDialog.setContentView(view);
            bottomSheetDialog.show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
