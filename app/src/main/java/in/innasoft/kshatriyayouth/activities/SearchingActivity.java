package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SearchingAdapter;
import in.innasoft.kshatriyayouth.models.SearchingModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class SearchingActivity extends AppCompatActivity {
    RecyclerView recyclerview;
    SearchingAdapter adapter;
    ArrayList<SearchingModel> feeditem = new ArrayList<SearchingModel>();
    ArrayList<String> surname_list = new ArrayList<String>();
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    int total_number_of_items = 0;
    private boolean userScrolled = true;
    private static int displayedposition = 0;
    RelativeLayout bottomLayout;
    int type_of_request = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;
    ProgressDialog progressDialog;
    ImageView nodata_image;
    String user_id = "", user_profile_id;
    SearchView friends_search;
    UserSessionManager sessionManager;
    ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searching);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sessionManager = new UserSessionManager(SearchingActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        progressDialog = new ProgressDialog(SearchingActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        nodata_image = (ImageView) findViewById(R.id.nodata_image);

        friends_search = (SearchView) findViewById(R.id.friends_search);

        bottomLayout = (RelativeLayout) findViewById(R.id.loadItemsLayout_recyclerView);

        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        // EditText searchEditText = (EditText) friends_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        recyclerview = (RecyclerView) findViewById(R.id.recylerview);
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(SearchingActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new SearchingAdapter(feeditem, SearchingActivity.this, R.layout.searching_row);
        recyclerview.setNestedScrollingEnabled(false);

        friends_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                friends_search.setIconified(false);
            }
        });
        friends_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                adapter.getFilter().filter(query);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                feedItem();

                            } else {
                                Toast.makeText(SearchingActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }

                            //Do pagination.. i.e. fetch new data
                            //universitiesList
                        }
                    }


                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }
        });
        feedItem();
    }

    public void feedItem() {
        checkInternet = NetworkChecking.isConnected(SearchingActivity.this);
        if (checkInternet) {
          /*  if(type_of_request == 0) {
               // progressDialog.show();
                progress_bar.setVisibility(View.VISIBLE);
            }else {
                bottomLayout.setVisibility(View.VISIBLE);
            }*/
            feeditem.clear();
            progress_bar.setVisibility(View.VISIBLE);
            String url = AppUrls.BASE_URL + AppUrls.FRIENDS_LIST + "/" + user_profile_id + "/1";
            Log.d("SERARCHFRIENDLIST", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLSEARCHRESPONCE", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    //  progressDialog.dismiss();
                                    progress_bar.setVisibility(View.GONE);
                                    nodata_image.setVisibility(View.GONE);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                  /*  int total_numberof_records = Integer.valueOf(jsonObject1.getString("total_recs"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS",  "OUTER "+loading+"  "+total_numberof_records);
                                    if(total_numberof_records > feeditem.size())
                                    {
                                        loading = true;
                                        Log.d("LOADSTATUS",  "INNER "+loading+"  "+total_numberof_records);
                                    }else {
                                        loading = false;
                                    }*/
                                    JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        SearchingModel item = new SearchingModel();
                                        item.setName(jsonObject2.getString("name") + " " + jsonObject2.getString("surname"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        Log.d("image_photo", image);
                                        item.setPhoto(image);
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        item.setFriendstatus(jsonObject2.getString("friendstatus"));
                                        item.setRequeststatus(jsonObject2.getString("requeststatus"));
                                        item.setSentbystatus(jsonObject2.getString("sentbystatus"));

                                        feeditem.add(item);
                                    }


                                    recyclerview.setAdapter(adapter);
                                    bottomLayout.setVisibility(View.GONE);


                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {
                                    recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_image.setVisibility(View.GONE);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    nodata_image.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

            };

            RequestQueue requestQueue = Volley.newRequestQueue(SearchingActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(SearchingActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}