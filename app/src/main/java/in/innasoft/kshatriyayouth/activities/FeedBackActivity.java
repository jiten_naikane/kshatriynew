package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FilePath;
import in.innasoft.kshatriyayouth.utilities.ImagePermissions;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class FeedBackActivity extends AppCompatActivity implements View.OnClickListener {
    EditText name_edt, email_edt, feedback_desc_edt;
    TextView button_submit;
    Button choose_file_button;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    UserSessionManager session;
    String user_id, selectedSubject, selected_file_path;
    String user_name;
    String user_mail;
    Spinner spinnerFeedback;
    ImageView selectedImage;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    HttpEntity resEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback Form");

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_mail = userDetails.get(UserSessionManager.USER_EMAIL);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        name_edt = (EditText) findViewById(R.id.name_edt);
        name_edt.setText(user_name);
//        name_edt.setTypeface(typeface);

        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setText(user_mail);
//        email_edt.setTypeface(typeface);

        feedback_desc_edt = (EditText) findViewById(R.id.feedback_desc_edt);
//        feedback_desc_edt.setTypeface(typeface);

        selectedImage = (ImageView) findViewById(R.id.selectedImage);

        button_submit = (TextView) findViewById(R.id.button_submit);
//        button_submit.setTypeface(typeface);
        button_submit.setOnClickListener(this);

        choose_file_button = (Button) findViewById(R.id.choose_file_button);
//        choose_file_button.setTypeface(typeface);
        choose_file_button.setOnClickListener(this);

        spinnerFeedback = (Spinner) findViewById(R.id.spinnerFeedback);


        String[] listSubjects = new String[]{"Feedback For Mobile Development", "Content Request to Admin",};

        ArrayList<String> listallSubjects = new ArrayList<String>(Arrays.asList(listSubjects));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_row, listallSubjects);
        spinnerFeedback.setAdapter(spinnerArrayAdapter);

        spinnerFeedback.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSubject = (String) parent.getItemAtPosition(position);

                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#FFFFFF"));

                ((TextView) spinnerFeedback.getSelectedView()).setTextColor(getResources().getColor(R.color.white));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        if (view == choose_file_button) {
            checkInternet = NetworkChecking.isConnected(FeedBackActivity.this);
            if (checkInternet) {
                final boolean result = ImagePermissions.checkPermission(FeedBackActivity.this);
                final Dialog mBottomSheetDialog = new Dialog(FeedBackActivity.this, R.style.MaterialDialogSheet);
               /* TextView dialog_title = (TextView) mBottomSheetDialog.findViewById(R.id.dialog_title);
//                dialog_title.setTypeface(typeface);*/
                mBottomSheetDialog.setContentView(R.layout.custom_dialog_image_selection); // your custom view.
                mBottomSheetDialog.setCancelable(true);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                mBottomSheetDialog.show();

                TextView take_photo_text = (TextView) mBottomSheetDialog.findViewById(R.id.take_photo_text);
//                take_photo_text.setTypeface(typeface);
                take_photo_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(FeedBackActivity.this, "take photo", Toast.LENGTH_LONG).show();
                        if (result) {
                            if (ContextCompat.checkSelfPermission(FeedBackActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (getFromPref(FeedBackActivity.this, ALLOW_KEY)) {
                                    showSettingsAlert();
                                } else if (ContextCompat.checkSelfPermission(FeedBackActivity.this,
                                        android.Manifest.permission.CAMERA)

                                        != PackageManager.PERMISSION_GRANTED) {

                                    // Should we show an explanation?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(FeedBackActivity.this,
                                            android.Manifest.permission.CAMERA)) {
                                        showAlert();
                                    } else {
                                        // No explanation needed, we can request the permission.
                                        ActivityCompat.requestPermissions(FeedBackActivity.this,
                                                new String[]{android.Manifest.permission.CAMERA},
                                                CAMERA_REQUEST_IMAGE);
                                    }
                                }
                            } else {
                                //  cameraIntent();
                                mBottomSheetDialog.dismiss();
                            }

                        }


                    }
                });

                TextView select_from_gallery = (TextView) mBottomSheetDialog.findViewById(R.id.select_from_gallery);
//                select_from_gallery.setTypeface(typeface);
                select_from_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Toast.makeText(GroupCreateActivityDialog.this, "gallry", Toast.LENGTH_LONG).show();
                        /*if (result) {

                        }*/
                        galleryIntent();
                        mBottomSheetDialog.dismiss();


                    }
                });

                mBottomSheetDialog.show();
            } else {

                Toast.makeText(FeedBackActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();


            }


        }

        if (view == button_submit) {
            checkInternet = NetworkChecking.isConnected(FeedBackActivity.this);
            if (checkInternet) {
                if (validate()) {
                    if (selected_file_path == null) {
                        final String name = name_edt.getText().toString().trim();
                        final String emaill = email_edt.getText().toString().trim();
                        final String messagebody = feedback_desc_edt.getText().toString().trim();
                        FeedBackActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                if (pprogressDialog != null)
                                    pprogressDialog.show();
                            }
                        });
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        String response_str = null;
                        File file1 = null;
                        FileBody bin1 = null;
                        String empty = "";
                        Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                        //   file1 = new File(selected_file_path.replaceAll("file://", "").replace("%20", " "));
                        // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
                        String urlString = "";


                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            urlString = AppUrls.BASE_URL + AppUrls.FEEDBACK;
                        } else {
                            urlString = AppUrls.BASE_IMAGE_URL + AppUrls.FEEDBACK;
                        }
                        Log.d("REQUESTURLNO", urlString);
                        try {
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(urlString);

                            MultipartEntity reqEntity = new MultipartEntity();
                            Log.d("USERIDETAILNO:", user_id + "///" + selectedSubject + "///" + name + "///" + emaill + "//" + messagebody + "///" + file1);

                            reqEntity.addPart("user_id", new StringBody(user_id));
                            reqEntity.addPart("category", new StringBody(selectedSubject));
                            reqEntity.addPart("name", new StringBody(name));
                            reqEntity.addPart("email", new StringBody(emaill));
                            reqEntity.addPart("description", new StringBody(messagebody));

                            //  bin1 = new FileBody(file1);
                            reqEntity.addPart("userfile", new StringBody(""));
                            post.setEntity(reqEntity);
                            HttpResponse response = client.execute(post);
                            resEntity = response.getEntity();
                            response_str = EntityUtils.toString(resEntity);
                            Log.d("MERESPONCENO", response_str);
                            try {
                                JSONObject jsonObject = new JSONObject(response_str);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equalsIgnoreCase("10100")) {
                                    Toast.makeText(FeedBackActivity.this, "Feedbck sent Successfully..!", Toast.LENGTH_SHORT).show();
                                    finish();

                                }
                                if (responceCode.equals("10200")) {

                                    Toast.makeText(FeedBackActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            pprogressDialog.dismiss();
                        } catch (Exception ex) {
                            Log.e("Debug", "error: " + ex.getMessage(), ex);
                        }
                    } else {
                        Log.d("FILEPATHHHHHYES:", selected_file_path);

                        final String name = name_edt.getText().toString().trim();
                        final String emaill = email_edt.getText().toString().trim();
                        final String messagebody = feedback_desc_edt.getText().toString().trim();

                        FeedBackActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                if (pprogressDialog != null)
                                    pprogressDialog.show();
                            }
                        });
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        String response_str = null;
                        File file1 = null;
                        FileBody bin1 = null;
                        String empty = "";
                        Log.d("SDFAFGSGFSGYESS", "sfafafsaf 1");
                        file1 = new File(selected_file_path.replaceAll("file://", "").replace("%20", " "));
                        // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
                        String urlString = "";


                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            urlString = AppUrls.BASE_URL + AppUrls.FEEDBACK;
                        } else {
                            urlString = AppUrls.BASE_IMAGE_URL + AppUrls.FEEDBACK;
                        }
                        Log.d("REQUESTURLYSESS", urlString);
                        try {
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(urlString);

                            MultipartEntity reqEntity = new MultipartEntity();
                            Log.d("USERIDETAILYESS:", user_id + "///" + selectedSubject + "///" + name + "///" + emaill + "//" + messagebody + "///" + file1);

                            reqEntity.addPart("user_id", new StringBody(user_id));
                            reqEntity.addPart("category", new StringBody(selectedSubject));
                            reqEntity.addPart("name", new StringBody(name));
                            reqEntity.addPart("email", new StringBody(emaill));
                            reqEntity.addPart("description", new StringBody(messagebody));

                            bin1 = new FileBody(file1);
                            reqEntity.addPart("userfile", bin1);
                            post.setEntity(reqEntity);
                            HttpResponse response = client.execute(post);
                            resEntity = response.getEntity();
                            response_str = EntityUtils.toString(resEntity);
                            Log.d("MERESPONCEYESS", response_str);
                            try {
                                JSONObject jsonObject = new JSONObject(response_str);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equalsIgnoreCase("10100")) {
                                    Toast.makeText(FeedBackActivity.this, "Feedback sent Succesfully..!", Toast.LENGTH_SHORT).show();
                                    finish();

                                }
                                if (responceCode.equals("10200")) {

                                    Toast.makeText(FeedBackActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            pprogressDialog.dismiss();
                        } catch (Exception ex) {
                            Log.e("Debug", "error: " + ex.getMessage(), ex);
                        }
                    }

                  /*  Log.d("FILEPATHHHHH:", selected_file_path);

                    final String name = name_edt.getText().toString().trim();
                    final String emaill = email_edt.getText().toString().trim();
                    final String messagebody = feedback_desc_edt.getText().toString().trim();

                    FeedBackActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if(pprogressDialog != null)
                                pprogressDialog.show();
                        }
                    });
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                            .permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    String response_str = null;
                    File file1 = null;
                    FileBody bin1 = null;
                    String empty = "";
                    Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                    file1 = new File(selected_file_path.replaceAll("file://", "").replace("%20", " "));
                    // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
                    String urlString = "";


                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        urlString = AppUrls.BASE_URL+AppUrls.FEEDBACK;
                    } else{
                        urlString = AppUrls.BASE_IMAGE_URL+AppUrls.FEEDBACK;
                    }
                    Log.d("REQUESTURL", urlString);
                    try
                    {
                        HttpClient client = new DefaultHttpClient();
                        HttpPost post = new HttpPost(urlString);

                        MultipartEntity reqEntity = new MultipartEntity();
                        Log.d("USERIDETAIL:",user_id+"///"+selectedSubject+"///"+name+"///"+emaill+"//"+messagebody+"///"+file1);

                        reqEntity.addPart("user_id", new StringBody(user_id));
                        reqEntity.addPart("category", new StringBody(selectedSubject));
                        reqEntity.addPart("name", new StringBody(name));
                        reqEntity.addPart("email", new StringBody(emaill));
                        reqEntity.addPart("description", new StringBody(messagebody));

                        bin1 = new FileBody(file1);
                        reqEntity.addPart("userfile", bin1);
                        post.setEntity(reqEntity);
                        HttpResponse response = client.execute(post);
                        resEntity = response.getEntity();
                        response_str = EntityUtils.toString(resEntity);
                        Log.d("MERESPONCE", response_str);
                       try
                       {
                           JSONObject jsonObject = new JSONObject(response_str);
                           String responceCode = jsonObject.getString("status");
                           if (responceCode.equalsIgnoreCase("10100"))
                           {
                               Toast.makeText(FeedBackActivity.this, "Feedbck sent Succesfully..!", Toast.LENGTH_SHORT).show();
                              finish();

                           }
                           if (responceCode.equals("10200"))
                           {

                               Toast.makeText(FeedBackActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                           }
                       }
                       catch (JSONException e) {
                           e.printStackTrace();
                       }

                        pprogressDialog.dismiss();
                    }
                    catch (Exception ex){
                        Log.e("Debug", "error: " + ex.getMessage(), ex);
                    }*/
                }
            } else {
                pprogressDialog.cancel();
                Toast.makeText(FeedBackActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {

                onSelectFromGalleryResult(data);
            }

          /*  else if (requestCode == CAMERA_REQUEST_IMAGE)
            {
                try {

                   onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedFileUri = data.getData();
        selected_file_path = FilePath.getPath(this, selectedFileUri);
        if (selected_file_path != null && !selected_file_path.equals("")) {

            try {
                File f = new File(selected_file_path);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                selectedImage.setVisibility(View.VISIBLE);
                selectedImage.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();


            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    public boolean validate() {
        boolean result = true;

        String msg = feedback_desc_edt.getText().toString().trim();
        if (msg.isEmpty()) {
            feedback_desc_edt.setError("Please Enter Message");
            result = false;
        } else {
            feedback_desc_edt.setError(null);
        }

        /*String name = name_edt.getText().toString().trim();
        if (msg.isEmpty())
        {
            name_edt.setError("Please Enter Name");
            result = false;
        }else
        {
            name_edt.setError(null);
        }
*/
      /*  String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_edt.setError("Invalid Email");
            result = false;
        }*/

        return result;
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(FeedBackActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(FeedBackActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                CAMERA_REQUEST_IMAGE);
                    }
                });
        alertDialog.show();
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(FeedBackActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(FeedBackActivity.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
}
