package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bluejamesbond.text.DocumentView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.FamilyMembersListAdapter;
import in.innasoft.kshatriyayouth.models.FamilyMembersModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class InformationDirectoryDetailActivity extends AppCompatActivity {

    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String userid, info_detail_id;
    ImageView info_detail_img;
    Toolbar info_toolbar;
    CollapsingToolbarLayout info_collapse_toolbar;
    TextView name_title, name_text, gender_title, gender_text, age_title, age_text, education_title, education_text, occupation_title,
            occupation_text, contact_no_title, contact_no_text, location_title, location_text, family_title;
    DocumentView text_description;
    LinearLayout education_ll;

    RecyclerView family_recyclerview;
    FamilyMembersListAdapter familyMembersListAdapter;
    ArrayList<FamilyMembersModel> familyMembersModels = new ArrayList<FamilyMembersModel>();
    LinearLayoutManager layoutManager;
    AppBarLayout app_bar_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_directory_detail);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        info_toolbar = (Toolbar) findViewById(R.id.info_toolbar);
        info_toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(info_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        info_detail_img = (ImageView) findViewById(R.id.info_detail_img);

        info_collapse_toolbar = (CollapsingToolbarLayout) findViewById(R.id.info_collapse_toolbar);
        info_collapse_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        info_collapse_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        info_collapse_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);

        education_ll = (LinearLayout) findViewById(R.id.education_ll);

        family_recyclerview = (RecyclerView) findViewById(R.id.family_recyclerview);
        family_recyclerview.setHasFixedSize(true);
        familyMembersListAdapter = new FamilyMembersListAdapter(familyMembersModels, this, R.layout.row_family_members);
        layoutManager = new LinearLayoutManager(this);
        family_recyclerview.setLayoutManager(layoutManager);
        family_recyclerview.setItemAnimator(new DefaultItemAnimator());
        family_recyclerview.setAdapter(familyMembersListAdapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);


        Bundle bundle = getIntent().getExtras();
        info_detail_id = bundle.getString("INFO_DIRECTROY_ID");
        Log.d("INFODETAILID", info_detail_id);


        name_title = (TextView) findViewById(R.id.name_title);
//         name_title.setTypeface(typeface);
        name_text = (TextView) findViewById(R.id.name_text);
//         name_text.setTypeface(typeface);

        family_title = (TextView) findViewById(R.id.family_title);
//        family_title.setTypeface(typeface);

        gender_title = (TextView) findViewById(R.id.gender_title);
//        gender_title.setTypeface(typeface);
        gender_text = (TextView) findViewById(R.id.gender_text);
//        gender_text.setTypeface(typeface);

        age_title = (TextView) findViewById(R.id.age_title);
//        age_title.setTypeface(typeface);
        age_text = (TextView) findViewById(R.id.age_text);
//        age_text.setTypeface(typeface);

        education_title = (TextView) findViewById(R.id.education_title);
//        education_title.setTypeface(typeface);
        education_text = (TextView) findViewById(R.id.education_text);
//        education_text.setTypeface(typeface);

        occupation_title = (TextView) findViewById(R.id.occupation_title);
//        occupation_title.setTypeface(typeface);
        occupation_text = (TextView) findViewById(R.id.occupation_text);
//        occupation_text.setTypeface(typeface);

        contact_no_title = (TextView) findViewById(R.id.contact_no_title);
//        contact_no_title.setTypeface(typeface);
        contact_no_text = (TextView) findViewById(R.id.contact_no_text);
//        contact_no_text.setTypeface(typeface);

        location_title = (TextView) findViewById(R.id.location_title);
//        location_title.setTypeface(typeface);
        location_text = (TextView) findViewById(R.id.location_text);
//        location_text.setTypeface(typeface);

        text_description = (DocumentView) findViewById(R.id.text_description);


        getInfoDirectoryDetail();
    }

    private void getInfoDirectoryDetail() {
        checkInternet = NetworkChecking.isConnected(this);

        if (checkInternet) {
            progressDialog.show();
            String url = AppUrls.BASE_URL + AppUrls.INFORMATION_DETAILS + "/" + info_detail_id;
            Log.d("DETAILURLLLL?:", url);

            StringRequest strinfoDetail = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.INFORMATION_DETAILS + "/" + info_detail_id, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("INFODETAILRESP", response);
                        String SuccessResponceCode = jsonObject.getString("status");
                        if (SuccessResponceCode.equalsIgnoreCase("10100")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");

                            String name = jsonObject2.getString("name");
                            info_collapse_toolbar.setTitle(Html.fromHtml(name));
                            name_text.setText(Html.fromHtml(" : " + name));

                            String gender = jsonObject2.getString("gender");
                            gender_text.setText(Html.fromHtml(" : " + gender));

                            String age = jsonObject2.getString("age");
                            age_text.setText(Html.fromHtml(" : " + age + " years"));

                            String eduction = jsonObject2.getString("education");
                            if (eduction.equals("") || eduction.equals("null") || eduction.equals(null)) {
                                education_ll.setVisibility(View.GONE);
                            } else {
                                education_text.setText(Html.fromHtml(" : " + eduction));
                            }

                            String occupation = jsonObject2.getString("occupation");
                            occupation_text.setText(Html.fromHtml(" : " + occupation));

                            String image = jsonObject2.getString("image");
                            if (image.equals("") || image.equals(null) || image.equals("null")) {
                                app_bar_layout.setExpanded(false, true);
                            } else {
                                Picasso.with(InformationDirectoryDetailActivity.this)
                                        .load(AppUrls.BASE_URL + jsonObject2.getString("image"))
                                        .placeholder(R.drawable.app_logo_new)
                                        .into(info_detail_img);
                            }

                            String contact_no = jsonObject2.getString("contact_no");
                            contact_no_text.setText(Html.fromHtml(" : " + contact_no));

                            String location = jsonObject2.getString("location");
                            location_text.setText(Html.fromHtml(" : " + location));

                            String decription = jsonObject2.getString("description");
                            text_description.setText(Html.fromHtml(decription));


                            JSONArray jsonArray = jsonObject1.getJSONArray("familyMembers");
                            Log.d("SDFSDFDSFSF", jsonArray.toString());

                            if (jsonArray.equals("")) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                                    FamilyMembersModel fm = new FamilyMembersModel();
                                    fm.setId(jsonObject3.getString("id"));
                                    fm.setInfo_id(jsonObject3.getString("info_id"));
                                    fm.setCategory_id(jsonObject3.getString("category_id"));
                                    fm.setType(jsonObject3.getString("type"));
                                    fm.setName(jsonObject3.getString("name"));
                                    fm.setUrl_name(jsonObject3.getString("url_name"));
                                    fm.setGender(jsonObject3.getString("gender"));
                                    fm.setAge(jsonObject3.getString("age"));
                                    fm.setEducation(jsonObject3.getString("education"));
                                    fm.setContact_no(jsonObject3.getString("contact_no"));
                                    fm.setOccupation(jsonObject3.getString("occupation"));
                                    fm.setStatus(jsonObject3.getString("status"));
                                    fm.setCreate_date_time(jsonObject3.getString("create_date_time"));
                                    fm.setUpdate_date_time(jsonObject3.getString("update_date_time"));
                                    familyMembersModels.add(fm);
                                }
                                family_recyclerview.setAdapter(familyMembersListAdapter);
                            } else {
                                family_title.setVisibility(View.GONE);
                            }
                        }
                        if (SuccessResponceCode.equals("10200")) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();

                    } catch (JSONException ed) {
                        ed.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }

                }
            });

            strinfoDetail.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(InformationDirectoryDetailActivity.this);
            requestQueue.add(strinfoDetail);

        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
