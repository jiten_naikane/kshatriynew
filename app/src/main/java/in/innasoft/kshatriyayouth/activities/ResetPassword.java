package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener {
    TextView reset_txt;
    EditText otp_edt, password_edt, confrm_password_edt;
    Button submit_password_btn;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String userid, otp, mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        userSessionManager = new UserSessionManager(ResetPassword.this);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        mobile = bundle.getString("mobile");
        otp = bundle.getString("otp");
        Log.d("DATATTAT:", otp + "" + mobile);

        otp_edt = (EditText) findViewById(R.id.otp_edt);
        //otp_edt.setText(otp);
        password_edt = (EditText) findViewById(R.id.password_edt);
        confrm_password_edt = (EditText) findViewById(R.id.confrm_password_edt);
        submit_password_btn = (Button) findViewById(R.id.submit_password_btn);
        submit_password_btn.setOnClickListener(this);
        reset_txt = (TextView) findViewById(R.id.reset_txt);


    }

    @Override
    public void onClick(View v) {
        if (v == submit_password_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    progressDialog.show();
                    String yurrll = AppUrls.BASE_URL + AppUrls.RESET_PASSWORD;
                    final String pasword = password_edt.getText().toString().trim();
                    Log.d("RESETURL", yurrll);

                    StringRequest strforgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESET_PASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("RESETResp", response);

                                    try {
                                        progressDialog.dismiss();
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCodeForget = jsonObject.getString("status");
                                        if (successResponceCodeForget.equals("10100")) {
                                            Toast.makeText(getApplicationContext(), " Password Changed Successfully", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                        if (successResponceCodeForget.equals("10200")) {
                                            Toast.makeText(getApplicationContext(), "Otp not matched..!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", userid);
                            params.put("otp", otp);
                            params.put("password", pasword);
                            params.put("mobile", mobile);
                            Log.d("RESETPARAMS", params.toString());
                            return params;
                        }
                    };
                    strforgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(ResetPassword.this);
                    requestQueue.add(strforgetReq);
                } else {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    private boolean validate() {
        boolean result = true;
        int flag = 0;

        String new_password = password_edt.getText().toString().trim();
        if (new_password.length() < 6) {
            password_edt.setError("Invalid Password or max 6 characters");
            result = false;
        } else {
            password_edt.setError(null);
        }

        String strConformPassword = confrm_password_edt.getText().toString().trim();
        if (strConformPassword.length() < 6) {
            confrm_password_edt.setError("Password must have 6 characters");
            result = false;
        } else {
            confrm_password_edt.setError(null);
        }

        if (new_password != "" && strConformPassword != "" && !strConformPassword.equals(new_password) && flag == 0) {
            //password_edt.setError("Password and Confirm not Match");
            confrm_password_edt.setError("Password and Confirm not Match");
            result = false;
        }

        return result;
    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                //Do whatever you want with the code here
                Log.d("AUTOREAD", message);
                /*String sms = message.replaceAll("[^0-9]", "");
                Log.d("SMS",sms);
                otp_edt.setText(sms);*/
                try {
                    Pattern p = Pattern.compile("(|^)\\d{6}");


                    if (message != null) {
                        Matcher m = p.matcher(message);
                        if (m.find()) {
                            otp_edt.setText(m.group(0));
                        } else {
                            //something went wrong
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

}
