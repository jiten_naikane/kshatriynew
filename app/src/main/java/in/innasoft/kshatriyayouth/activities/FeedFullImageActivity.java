package in.innasoft.kshatriyayouth.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.android.volley.toolbox.ImageLoader;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.utilities.FeedImageView;

public class FeedFullImageActivity extends AppCompatActivity {
    FeedImageView feedImage1;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_full_image);
        feedImage1 = (FeedImageView) findViewById(R.id.feedImage1);
        if (getIntent().getExtras().getString("image") != null) {
            feedImage1.setImageUrl(getIntent().getExtras().getString("image"), imageLoader);
            feedImage1.setVisibility(View.VISIBLE);
            feedImage1
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
        } else {
            feedImage1.setVisibility(View.GONE);
        }
    }
}
