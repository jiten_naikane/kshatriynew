package in.innasoft.kshatriyayouth.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.IncomingSms;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class ForgotPasswordPage extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView forgot_txt;
    EditText mobile_edt;
    Button request_btn;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String mobile;
    UserSessionManager userSessionManager;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static int TIME_OUT = 1000;
    private static final int PERMISSION_REQUEST_ID = 100;
    private static final String TAG = ResetPassword.class.getSimpleName();
    IncomingSms mSmsBroadcastReceiver;
    private final String BROADCAST_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private IntentFilter intentFilter;
    //TextView otp;
    String messageText;

    //8341843826
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_page);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        userSessionManager = new UserSessionManager(ForgotPasswordPage.this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        forgot_txt = (TextView) findViewById(R.id.forgot_txt);

        mobile_edt = (EditText) findViewById(R.id.mobile_edt);

        request_btn = (Button) findViewById(R.id.request_btn);
        request_btn.setOnClickListener(this);

        requestRuntimePermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS);
    }

    private void requestRuntimePermissions(String... permissions) {
        for (String perm : permissions) {

            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{perm}, PERMISSION_REQUEST_ID);

            }
        }
    }

    @Override
    public void onClick(View v) {

        if (v == request_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    progressDialog.show();

                    StringRequest strforgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FORGOT_PASSWORD, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.d("forget-url", AppUrls.BASE_URL + AppUrls.FORGOT_PASSWORD);
                            Log.d("forget-Resp", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCodeForget = jsonObject.getString("status");
                                if (successResponceCodeForget.equals("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String msg = jsonObject1.getString("msg");
                                    String mobile = jsonObject1.getString("mobile");
                                    String otp = jsonObject1.getString("otp");

                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "OTP sent Successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), ResetPassword.class);
                                    intent.putExtra("otp", otp);
                                    intent.putExtra("mobile", mobile);
                                    startActivity(intent);
                                    finish();
                                }

                                if (successResponceCodeForget.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ForgotPasswordPage.this, "Admin Approval Required..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("mobile", mobile_edt.getText().toString().trim());
                            Log.d("FORGET-PARAMS", params.toString());
                            return params;
                        }
                    };
                    strforgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(ForgotPasswordPage.this);
                    requestQueue.add(strforgetReq);


                } else {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    private boolean validate() {

        boolean result = true;

        String psw = mobile_edt.getText().toString().trim();
        if (psw == null || psw.equals("")) {
            mobile_edt.setError("Invalid Mobile");
            result = false;
        }

        return result;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_ID) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(ForgotPasswordPage.this, "permission granted", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(ForgotPasswordPage.this, "permission not granted", Toast.LENGTH_LONG).show();


            }
        }
    }
}
