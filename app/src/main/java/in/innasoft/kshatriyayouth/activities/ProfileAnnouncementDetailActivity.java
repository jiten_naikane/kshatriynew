package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class ProfileAnnouncementDetailActivity extends AppCompatActivity {
    TextView announce_name, announce_url_title, announce_url_nme, description_title, announc_description, seo_title_text, seo_description_text, seo_keyword_text;
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ImageView close, announc_detail_img;
    String userid, detail_id;
    Toolbar toolbar1;
    CollapsingToolbarLayout collapse_toolbar;
    AppBarLayout app_bar_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_announcement_detail);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        toolbar1 = (Toolbar) findViewById(R.id.toolbar1);
        toolbar1.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapse_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapse_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapse_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);

        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();
        detail_id = bundle.getString("DETAIL_ID");
        Log.d("DETAILID", detail_id);


        announc_detail_img = (ImageView) findViewById(R.id.announc_detail_img);

        announce_name = (TextView) findViewById(R.id.announce_name);
//        announce_name.setTypeface(typeface);

        announce_url_title = (TextView) findViewById(R.id.announce_url_title);
//       // announce_url_title.setTypeface(typeface);

        announce_url_nme = (TextView) findViewById(R.id.announce_url_nme);
//        announce_url_nme.setTypeface(typeface);

        description_title = (TextView) findViewById(R.id.description_title);
//     //   description_title.setTypeface(typeface);

        announc_description = (TextView) findViewById(R.id.announc_description);
//        announc_description.setTypeface(typeface);

        seo_title_text = (TextView) findViewById(R.id.seo_title_text);
//        seo_title_text.setTypeface(typeface);

        seo_description_text = (TextView) findViewById(R.id.seo_description_text);
//        seo_description_text.setTypeface(typeface);

        seo_keyword_text = (TextView) findViewById(R.id.seo_keyword_text);
//        seo_keyword_text.setTypeface(typeface);

        getAnnounceDetail();
    }

    private void getAnnounceDetail() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();


            final String url = AppUrls.BASE_URL + AppUrls.ANNOUNCEMENT_DETAIL + "/" + detail_id;
            Log.d("announcedetailURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.ANNOUNCEMENT_DETAIL + "/" + detail_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ANNDETAILRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");

                                    String name = jsonObject2.getString("name");
                                    collapse_toolbar.setTitle(Html.fromHtml(name));
                                    //  announce_name.setText(name);

                                    String image = jsonObject2.getString("image");
                                    if (image.equals("") || image.equals(null) || image.equals("null")) {
                                        app_bar_layout.setExpanded(false, true);
                                    } else {
                                        Picasso.with(ProfileAnnouncementDetailActivity.this)
                                                .load(AppUrls.BASE_URL + jsonObject2.getString("image"))
                                                .placeholder(R.drawable.app_logo_new)
                                                .into(announc_detail_img);
                                    }

                                    String url_name = jsonObject2.getString("url_name");
                                    String announcement = url_name.substring(0, 1).toUpperCase() + url_name.substring(1);
                                    announce_url_nme.setText(Html.fromHtml(announcement));

                                    String description = jsonObject2.getString("description");
                                    announc_description.setText(Html.fromHtml(description));

                                    String seo_title = jsonObject2.getString("seo_title");
                                    seo_title_text.setText(Html.fromHtml(seo_title));

                                    String seo_description = jsonObject2.getString("seo_description");
                                    seo_description_text.setText(Html.fromHtml(seo_description));

                                    String seo_keywords = jsonObject2.getString("seo_keywords");
                                    seo_keyword_text.setText(Html.fromHtml(seo_keywords));


                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProfileAnnouncementDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
