package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bluejamesbond.text.DocumentView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class BusinessDirectoryDetailActivity extends AppCompatActivity {
    TextView about_title, about_text, contact_title, contact_text, description_title, descreption_simple_text, location_text, occupation_text;
    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ImageView close, business_detail_img;
    String userid, business_detail_id;
    Toolbar business_toolbar1;
    CollapsingToolbarLayout busines_collapse_toolbar;
    DocumentView descreption_text;
    AppBarLayout app_bar_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_directory_detail);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        business_toolbar1 = (Toolbar) findViewById(R.id.business_toolbar1);
        business_toolbar1.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(business_toolbar1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        busines_collapse_toolbar = (CollapsingToolbarLayout) findViewById(R.id.busines_collapse_toolbar);
        busines_collapse_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        busines_collapse_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        busines_collapse_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);

        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();
        business_detail_id = bundle.getString("BUSINESSDETAIL_ID");
        Log.d("BUSINESDETAILID", business_detail_id);


        business_detail_img = (ImageView) findViewById(R.id.business_detail_img);
        descreption_text = (DocumentView) findViewById(R.id.descreption_text);

        about_title = (TextView) findViewById(R.id.about_title);
//        about_title.setTypeface(typeface);

        about_text = (TextView) findViewById(R.id.about_text);
//        about_text.setTypeface(typeface);

        contact_title = (TextView) findViewById(R.id.contact_title);
//        contact_title.setTypeface(typeface);

        contact_text = (TextView) findViewById(R.id.contact_text);
//        contact_text.setTypeface(typeface);

        description_title = (TextView) findViewById(R.id.description_title);
//        description_title.setTypeface(typeface);

        descreption_simple_text = (TextView) findViewById(R.id.descreption_simple_text);
//        descreption_simple_text.setTypeface(typeface);

        location_text = (TextView) findViewById(R.id.location_text);
//        location_text.setTypeface(typeface);

        occupation_text = (TextView) findViewById(R.id.occupation_text);
//        occupation_text.setTypeface(typeface);


        getBusinesDirectoryData();


    }

    private void getBusinesDirectoryData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();


            final String url = AppUrls.BASE_URL + AppUrls.BUSINESS_DETAILS + "/" + business_detail_id;
            Log.d("announcedetailURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.BUSINESS_DETAILS + "/" + business_detail_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ANNDETAILRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");

                                    String name = jsonObject2.getString("name");
                                    busines_collapse_toolbar.setTitle(Html.fromHtml(name));
                                    about_text.setText(Html.fromHtml(name));

                                    String image = jsonObject2.getString("image");
                                    if (image.equals("") || image.equals(null) || image.equals("null")) {
                                        app_bar_layout.setExpanded(false, true);
                                    } else {
                                        Picasso.with(BusinessDirectoryDetailActivity.this)
                                                .load(AppUrls.BASE_URL + jsonObject2.getString("image"))
                                                .placeholder(R.drawable.app_logo_new)
                                                .into(business_detail_img);
                                    }

                                    /*Picasso.with(BusinessDirectoryDetailActivity.this)
                                            .load(AppUrls.BASE_IMAGE_URL+jsonObject2.getString("image"))
                                            .placeholder(R.drawable.no_images_found)
                                            .into(business_detail_img);*/

                                    String occupation_name = jsonObject2.getString("occupation");
                                    String occupation = occupation_name.substring(0, 1).toUpperCase() + occupation_name.substring(1);
                                    occupation_text.setText(Html.fromHtml(occupation));

                                    String description = jsonObject2.getString("description");
                                    descreption_text.setText(Html.fromHtml(description));

                                    String location = jsonObject2.getString("location");
                                    location_text.setText(Html.fromHtml(location));


                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(BusinessDirectoryDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
