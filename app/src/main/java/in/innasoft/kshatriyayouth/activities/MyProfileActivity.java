package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.MyProfileGroupListAdapter;
import in.innasoft.kshatriyayouth.adapters.MyProfileMemberListAdapter;
import in.innasoft.kshatriyayouth.adapters.MyProfilePhotoListAdapter;
import in.innasoft.kshatriyayouth.adapters.MyProfileVideoListAdapter;
import in.innasoft.kshatriyayouth.adapters.UserFeedHomeAdapter;
import in.innasoft.kshatriyayouth.models.GroupListModel;
import in.innasoft.kshatriyayouth.models.GroupMembersListModel;
import in.innasoft.kshatriyayouth.models.GroupPhotoListModel;
import in.innasoft.kshatriyayouth.models.GroupVideoListModel;
import in.innasoft.kshatriyayouth.models.HomeFeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class MyProfileActivity extends AppCompatActivity {

    private boolean checkInternet;
    UserSessionManager sessionManager;
    String profile_pic_url = "";
    String profile_name = "";
    String user_profile_id = "";
    ImageView profile_image, image_view;
    Button edit_details;
    TextView full_name, address, membership, email, mobile, dob, blood_group, adhaar, interests, about_me, gender, relation_status, college_name, lives_in_city_name,
            language, religion, professional_skill, job_position, company, country, status_text, more_txt, friend_status;
    ////////////////////////////Photos

    //    Typeface typeface;
    ProgressDialog progressDialog;
    ArrayList<String> imgArray = new ArrayList<String>();
    MyProfilePhotoListAdapter gphotoListAdapter;
    ArrayList<GroupPhotoListModel> groupphotoListItem = new ArrayList<GroupPhotoListModel>();
    RecyclerView group_photo_recyclerview;
    ///////////////////////////////////////////Videos
    MyProfileVideoListAdapter gvideoListAdapter;
    ArrayList<GroupVideoListModel> groupvideoListItem = new ArrayList<GroupVideoListModel>();
    RecyclerView group_video_recyclerview;
    ImageView nodata_video_image;
    ///////////////////////////////////////////////////Groups
    RecyclerView group_profile_recyclerview;
    MyProfileGroupListAdapter groupListAdapter;
    ArrayList<GroupListModel> groupList = new ArrayList<GroupListModel>();
    //////////////////////////////////////////////////Friends
    MyProfileMemberListAdapter gmemberListAdapter;
    ArrayList<GroupMembersListModel> groupListItem = new ArrayList<GroupMembersListModel>();
    RecyclerView group_member_recyclerview;
    ////////////////////////////////////////////////Feed
    RecyclerView after_reg_list_universies_recyclerview;
    UserFeedHomeAdapter afterRegCPListofUniversitiesAdapter;
    ArrayList<HomeFeedItem> afterRegCPListofUniversitiesModels = new ArrayList<HomeFeedItem>();

    String image1 = "";
    String image2 = "";
    String image3 = "";
    String video;
    String photo;

    ProgressBar progress_bar;
    JSONArray photos_array;
    LinearLayoutManager layoutManager;

    String group_id, userid;
    RelativeLayout poststatus;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bundle = getIntent().getExtras();
        userid = bundle.getString("user_id");
        progressDialog = new ProgressDialog(MyProfileActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        sessionManager = new UserSessionManager(MyProfileActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);
        profile_name = userDetails.get(UserSessionManager.USER_NAME);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        image_view = (ImageView) findViewById(R.id.image_view);
        full_name = (TextView) findViewById(R.id.full_name);
        address = (TextView) findViewById(R.id.address);
        membership = (TextView) findViewById(R.id.membership);
        email = (TextView) findViewById(R.id.email);
        mobile = (TextView) findViewById(R.id.mobile);
        dob = (TextView) findViewById(R.id.dob);
        blood_group = (TextView) findViewById(R.id.blood_group);
        adhaar = (TextView) findViewById(R.id.adhaar);
        interests = (TextView) findViewById(R.id.interests);
        about_me = (TextView) findViewById(R.id.about_me);
        gender = (TextView) findViewById(R.id.gender);
        relation_status = (TextView) findViewById(R.id.relation_status);
        college_name = (TextView) findViewById(R.id.college_name);
        lives_in_city_name = (TextView) findViewById(R.id.lives_in_city_name);
        language = (TextView) findViewById(R.id.language);
        religion = (TextView) findViewById(R.id.religion);
        professional_skill = (TextView) findViewById(R.id.professional_skill);
        job_position = (TextView) findViewById(R.id.job_position);
        company = (TextView) findViewById(R.id.company);
        country = (TextView) findViewById(R.id.country);
        status_text = (TextView) findViewById(R.id.status_text);
        friend_status = (TextView) findViewById(R.id.friend_status);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            //  Toast.makeText(getContext(), "Good Morning", Toast.LENGTH_SHORT).show();
            status_text.setText(Html.fromHtml("Good Morning, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            // Toast.makeText(getContext(), "Good Afternoon", Toast.LENGTH_SHORT).show();
            // status_text.setText("Good Afternoon."+profile_name+"."+"Share something");
            status_text.setText(Html.fromHtml("Good Afternoon, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            //  Toast.makeText(getContext(), "Good Evening", Toast.LENGTH_SHORT).show();
            //status_text.setText("Good Evening."+profile_name+"."+"Share something");
            status_text.setText(Html.fromHtml("Good Evening, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            //Toast.makeText(getContext(), "Good Night", Toast.LENGTH_SHORT).show();
            // status_text.setText("Good Night."+profile_name+"."+"Share something");
            status_text.setText(Html.fromHtml("Good Evening, " + "<b>" + profile_name + "</b>" + ". " + "\nShare something?"));
//            status_text.setTypeface(typeface);
        }
        Picasso.with(MyProfileActivity.this)
                .load(profile_pic_url)
                .placeholder(R.drawable.user_profile)
                .into(image_view);
        edit_details = (Button) findViewById(R.id.edit_details);
        poststatus = (RelativeLayout) findViewById(R.id.poststatus);
        poststatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, PostStatusActivity.class);
                intent.putExtra("name", profile_name);
                intent.putExtra("page_owner", userid);
                intent.putExtra("photo_identifier", "wallfeeds");
                intent.putExtra("feeds", "wall");
                intent.putExtra("image", profile_pic_url);
                startActivity(intent);
            }
        });
        if (user_profile_id.equals(userid)) {
            friend_status.setVisibility(View.GONE);
            edit_details.setVisibility(View.VISIBLE);
            edit_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            friend_status.setVisibility(View.VISIBLE);
            edit_details.setVisibility(View.GONE);
        }
        ////////////////////////////////////////////
        group_photo_recyclerview = (RecyclerView) findViewById(R.id.group_photo_recyclerview);
        group_photo_recyclerview.getRecycledViewPool().clear();
        group_photo_recyclerview.setHasFixedSize(true);
        gphotoListAdapter = new MyProfilePhotoListAdapter(groupphotoListItem, MyProfileActivity.this, R.layout.my_profile_photo_fragment);
        group_photo_recyclerview.setLayoutManager(new GridLayoutManager(MyProfileActivity.this, 2, GridLayoutManager.HORIZONTAL, false));
        group_photo_recyclerview.setNestedScrollingEnabled(false);
        // group_photo_recyclerview.setSaveFromParentEnabled(true);
        ///////////////////////////////////////////////
        group_video_recyclerview = (RecyclerView) findViewById(R.id.group_video_recyclerview);
        //  group_video_recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MyProfileActivity.this);
        group_video_recyclerview.setLayoutManager(new GridLayoutManager(MyProfileActivity.this, 2, GridLayoutManager.HORIZONTAL, false));
        gvideoListAdapter = new MyProfileVideoListAdapter(groupvideoListItem, MyProfileActivity.this, R.layout.row_my_profile_video);
        group_video_recyclerview.setNestedScrollingEnabled(false);
        ///////////////////////////////////////////////
        group_profile_recyclerview = (RecyclerView) findViewById(R.id.group_profile_recyclerview);
        //group_profile_recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MyProfileActivity.this);
        group_profile_recyclerview.setLayoutManager(new GridLayoutManager(MyProfileActivity.this, 2, GridLayoutManager.HORIZONTAL, false));
        groupListAdapter = new MyProfileGroupListAdapter(groupList, MyProfileActivity.this, R.layout.row_my_profile);
        //groupListAdapter.notifyDataSetChanged();
        ////////////////////////////////////////////////////
        group_member_recyclerview = (RecyclerView) findViewById(R.id.group_member_recyclerview);
        //group_member_recyclerview.setHasFixedSize(true);
        gmemberListAdapter = new MyProfileMemberListAdapter(groupListItem, MyProfileActivity.this, R.layout.my_profile_member_fragment);
        group_member_recyclerview.setLayoutManager(new GridLayoutManager(MyProfileActivity.this, 2, GridLayoutManager.HORIZONTAL, false));
        group_member_recyclerview.setNestedScrollingEnabled(false);
        //  group_member_recyclerview.setSaveFromParentEnabled(true);
        ///////////////////////////////////////////////////////
        after_reg_list_universies_recyclerview = (RecyclerView) findViewById(R.id.feed_recylerview);
        //  after_reg_list_universies_recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MyProfileActivity.this);

        after_reg_list_universies_recyclerview.setLayoutManager(layoutManager);
        afterRegCPListofUniversitiesAdapter = new UserFeedHomeAdapter(afterRegCPListofUniversitiesModels, MyProfileActivity.this, R.layout.feed_item);
        after_reg_list_universies_recyclerview.setNestedScrollingEnabled(false);

        userAbout();
    }

    public void userAbout() {

        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.USER_PROFILE_INFO_LIST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject json_profile = jsonObject1.getJSONObject("getProfile");
                                    String name = json_profile.getString("name") + " " + json_profile.getString("surname");
                                    String address_user = json_profile.getString("address");
                                    String membership_user = json_profile.getString("membership");
                                    String image_user = AppUrls.BASE_IMAGE_URL + json_profile.getString("photo");
                                    String email_user = json_profile.getString("email");
                                    String mobile_user = json_profile.getString("mobile");
                                    String dob_user = json_profile.getString("dob");
                                    String blood_group_user = json_profile.getString("blood_group");
                                    String adhaar_group_user = json_profile.getString("adhaar");
                                    String interests_group_user = json_profile.getString("interests");
                                    String about_me_group_user = json_profile.getString("about_me");
                                    String gender_user = json_profile.getString("gender");
                                    String relation_status_user = json_profile.getString("relation_status");
                                    String college_name_user = json_profile.getString("college_name");
                                    String lives_in_city_name_user = json_profile.getString("lives_in_city_name");
                                    String language_user = json_profile.getString("language");
                                    String religion_user = json_profile.getString("religion");
                                    String professional_skill_user = json_profile.getString("professional_skill");
                                    String job_position_user = json_profile.getString("job_position");
                                    String company_user = json_profile.getString("company");
                                    String country_user = json_profile.getString("country_name");
                                    String country_id = json_profile.getString("country");
                                    String friendstatus = json_profile.getString("friendstatus");
                                    String sentbystatus = json_profile.getString("sentbystatus");
                                    String requeststatus = json_profile.getString("requeststatus");
                                    String mobile_privacy = json_profile.getString("mobile_privacy");

                                    //////////////////////////////////

                                    if (requeststatus.equals("null") && friendstatus.equals("null") && sentbystatus.equals("null")) {
                                        friend_status.setText("Add Friend");
                                        //delete.setVisibility(View.INVISIBLE);
                                        friend_status.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                sendFriendRequest();
                                            }
                                        });
                                    } else if (!requeststatus.equals("null") && friendstatus.equals("null") && sentbystatus.equals("null")) {
                                        friend_status.setText("Confirm");
                                        friend_status.setBackgroundResource(R.drawable.background_for_friendslist_thick);
                                        //delete.setText("Reject");
                                        friend_status.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                accseptFriendRequest();
                                            }
                                        });
                                       /* delete.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                rejectFriend();
                                            }
                                        });*/

                                    } else if (requeststatus.equals("null") && friendstatus.equals("null") && !sentbystatus.equals("null")) {
                                        friend_status.setText("Request Sent");
                                        //delete.setVisibility(View.INVISIBLE);
                                        friend_status.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //  accseptFriendRequest();
                                            }
                                        });

                                    } else if (requeststatus.equals("null") && !friendstatus.equals("null") && sentbystatus.equals("null")) {
                                        friend_status.setText("Unfriend");
                                        friend_status.setTextColor(getResources().getColor(R.color.black));
                                        friend_status.setBackgroundResource(R.drawable.background_for_friendslist_thick);
                                        //delete.setVisibility(View.INVISIBLE);
                                        friend_status.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                unFriendRequest();
                                            }
                                        });

                                    }
                                    /////////////////////////////////////////////

                                    Picasso.with(MyProfileActivity.this).load(image_user).placeholder(R.drawable.user_profile).fit().into(profile_image);

                                    if (name == null || name.equals("null")) {
                                        full_name.setVisibility(View.GONE);
                                    } else {
                                        full_name.setText(name);
                                    }

                                    if (address_user == null || address_user.equals("null")) {
                                        address.setVisibility(View.GONE);
                                    } else {
                                        address.setText(address_user);
                                    }

                                    if (membership_user == null || membership_user.equals("null")) {
                                        membership.setVisibility(View.GONE);
                                    } else {
                                        membership.setText(membership_user);
                                    }


                                    if (email_user == null || email_user.equals("null")) {
                                        email.setVisibility(View.GONE);
                                    } else {
                                        email.setText(Html.fromHtml("Email " + "<b>" + email_user + "</b>"));
                                    }

                                    if (mobile_privacy.equals("0")) {
                                        mobile.setVisibility(View.GONE);
                                    }
                                    if (mobile_user == null || mobile_user.equals("null")) {
                                        mobile.setVisibility(View.GONE);
                                    } else {
                                        mobile.setText(Html.fromHtml("Contact " + "<b>" + mobile_user + "</b>"));
                                    }

                                    if (dob_user == null || dob_user.equals("null")) {
                                        dob.setVisibility(View.GONE);
                                    } else {
                                        dob.setText(Html.fromHtml("Date of Birth " + "<b>" + dob_user + "</b>"));
                                    }

                                    if (blood_group_user == null || blood_group_user.equals("null")) {
                                        blood_group.setVisibility(View.GONE);
                                    } else {
                                        blood_group.setText(Html.fromHtml("Blood Group " + "<b>" + blood_group_user + "</b>"));
                                    }
                                    if (user_profile_id.equals(userid)) {
                                        if (adhaar_group_user == null || adhaar_group_user.equals("null")) {
                                            adhaar.setVisibility(View.GONE);
                                        } else {
                                            adhaar.setText(Html.fromHtml("Adhaar Number " + "<b>" + adhaar_group_user + "</b>"));
                                        }
                                    } else {
                                        adhaar.setVisibility(View.GONE);
                                    }


                                    if (interests_group_user == null || interests_group_user.equals("null") || interests_group_user.equals("")) {
                                        interests.setVisibility(View.GONE);
                                    } else {


                                        if (interests_group_user.equals("1")) {
                                            interests.setText(Html.fromHtml("Interests " + "<b>" + "Jobs" + "</b>"));
                                        } else if (interests_group_user.equals("2")) {
                                            interests.setText(Html.fromHtml("Interests " + "<b>" + "Blogs" + "</b>"));
                                        } else if (interests_group_user.equals("3")) {
                                            interests.setText(Html.fromHtml("Interests " + "<b>" + "Cause/Donation" + "</b>"));
                                        } else if (interests_group_user.equals("4")) {
                                            interests.setText(Html.fromHtml("Interests " + "<b>" + "Information Directory" + "</b>"));
                                        } else if (interests_group_user.equals("5")) {
                                            interests.setText(Html.fromHtml("Interests " + "<b>" + "Business Directory" + "</b>"));
                                        }

                                    }

                                    if (about_me_group_user == null || about_me_group_user.equals("null")) {
                                        about_me.setVisibility(View.GONE);
                                    } else {
                                        about_me.setText(Html.fromHtml("I'm a " + "<b>" + about_me_group_user + "</b>"));
                                    }

                                    if (gender_user == null || gender_user.equals("null")) {
                                        gender.setVisibility(View.GONE);
                                    } else {
                                        gender.setText(Html.fromHtml("I'm " + "<b>" + gender_user + "</b>"));
                                    }

                                    if (relation_status_user == null || relation_status_user.equals("null")) {
                                        relation_status.setVisibility(View.GONE);
                                    } else {
                                        relation_status.setText(Html.fromHtml("Relation Status " + "<b>" + relation_status_user + "</b>"));
                                    }

                                    if (college_name_user == null || college_name_user.equals("null")) {
                                        college_name.setVisibility(View.GONE);
                                    } else {
                                        college_name.setText(Html.fromHtml("College " + "<b>" + college_name_user + "</b>"));
                                    }


                                    if (lives_in_city_name_user == null || lives_in_city_name_user.equals("null")) {
                                        lives_in_city_name.setVisibility(View.GONE);
                                    } else {
                                        lives_in_city_name.setText(Html.fromHtml("Lives in " + "<b>" + lives_in_city_name_user + "</b>"));
                                    }

                                    if (language_user == null || language_user.equals("null")) {
                                        language.setVisibility(View.GONE);
                                    } else {
                                        language.setText(Html.fromHtml("Language " + "<b>" + language_user + "</b>"));
                                    }

                                    if (religion_user == null || religion_user.equals("null")) {
                                        religion.setVisibility(View.GONE);
                                    } else {
                                        religion.setText(Html.fromHtml("Religion " + "<b>" + religion_user + "</b>"));
                                    }

                                    if (professional_skill_user == null || professional_skill_user.equals("null")) {
                                        professional_skill.setVisibility(View.GONE);
                                    } else {
                                        professional_skill.setText(Html.fromHtml("Professional in " + "<b>" + professional_skill_user + "</b>"));
                                    }

                                    if (job_position_user == null || job_position_user.equals("null")) {
                                        job_position.setVisibility(View.GONE);
                                    } else {
                                        job_position.setText(Html.fromHtml("Job Role " + "<b>" + job_position_user + "</b>"));
                                    }


                                    if (company_user == null || company_user.equals("null")) {
                                        company.setVisibility(View.GONE);
                                    } else {
                                        company.setText(Html.fromHtml("Working at " + "<b>" + company_user + "</b>"));
                                    }

                                    if (country_user == null || country_user.equals("null") || country_user.equals("0") || country_user.equals("")) {
                                        country.setVisibility(View.GONE);
                                    } else {
                                        country.setText(Html.fromHtml("Country " + "<b>" + country_user + "</b>"));
                                    }

                                    getAllGroupPhoto();

                                }

                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", userid);
                    params.put("friend_profile_id", user_profile_id);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void sendFriendRequest() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ADD_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    userAbout();
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", userid);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void accseptFriendRequest() {

        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.ACCEPT_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    userAbout();
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", userid);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void unFriendRequest() {

        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.UNFRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    userAbout();
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", userid);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void rejectFriend() {

        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.REJECT_FRIEND_REQUEST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    userAbout();
                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", user_profile_id);
                    params.put("friend", userid);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getAllGroupPhoto() {

        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            groupphotoListItem.clear();
            //  String url = AppUrls.BASE_URL+AppUrls.GROUP_PHOTOS+"/"+group_id;
            String url = AppUrls.BASE_URL + AppUrls.USER_GROUP_PHOTOS;
            Log.d("photolisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("photolistuRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_photo_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();


                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupPhotos");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        GroupPhotoListModel item = new GroupPhotoListModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setPost_id(jsonObject2.getString("post_id"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        String image = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + jsonObject2.getString("photo_link");


                                        //   imgArray.add(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + jsonObject2.getString("photo_link"));
                                        // imgArray.add(AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + jsonObject2.getString("photo_link"));//IMAGE ARRAY
                                        item.setPhoto_link(image);
                                        item.setDate(jsonObject2.getString("date"));
                                        item.setPhoto_identifier(jsonObject2.getString("photo_identifier"));

                                        groupphotoListItem.add(item);
                                    }


                                    group_photo_recyclerview.setAdapter(gphotoListAdapter);

                                    getAllGroupVideo();
                                }

                                if (responceCode.equals("10200")) {
                                    getAllGroupVideo();
                                    group_photo_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", userid);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            }
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getAllGroupVideo() {
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            //http://kshatriyayouth.com/group_videos/
            String groupid = "138312511146940165";//
            groupvideoListItem.clear();
            //  String url = AppUrls.BASE_URL+AppUrls.GROUP_VIDEOS+"/"+group_id;
            String url = AppUrls.BASE_URL + AppUrls.USER_GROUP_VIDEOS;
            Log.d("videolisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("videolistuRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_video_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();


                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupVideos");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        GroupVideoListModel item = new GroupVideoListModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setPost_id(jsonObject2.getString("post_id"));
                                        item.setVideo_id(jsonObject2.getString("video_id"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        item.setUrl(jsonObject2.getString("url"));
                                        item.setTitle(jsonObject2.getString("title"));
                                        item.setVideo_identifier(jsonObject2.getString("video_identifier"));

                                        groupvideoListItem.add(item);
                                        Log.d("LISTTTT:", groupvideoListItem.toString());
                                    }


                                    group_video_recyclerview.setAdapter(gvideoListAdapter);
                                    getGroupList();

                                }

                                if (responceCode.equals("10200")) {
                                    getGroupList();
                                    group_video_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", userid);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            }
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }


    }

    private void getGroupList() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            groupList.clear();
            String url = AppUrls.BASE_URL + AppUrls.USER_GROUP_LIST;
            ;
            Log.d("GROUPLISTURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("GROUPLISTRESP", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_profile_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupsList");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        GroupListModel item = new GroupListModel();

                                        item.setId(jsonObject2.getString("id"));
                                        item.setGroup_id(jsonObject2.getString("group_id"));
                                        String image = AppUrls.BASE_IMAGE_URL_GROUP_LIST + jsonObject2.getString("group_picture");
                                        item.setGroup_picture(image);
                                        item.setGroup_manager(jsonObject2.getString("group_manager"));
                                        item.setGroup_name(jsonObject2.getString("group_name"));
                                        item.setGroup_description(jsonObject2.getString("group_description"));
                                        item.setGroup_type(jsonObject2.getString("group_type"));
                                        item.setGroup_count(jsonObject2.getString("group_count"));
                                        item.setDate(jsonObject2.getString("date"));


                                        groupList.add(item);
                                        Log.d("LISTITEM:", groupList.toString());
                                    }
                                    group_profile_recyclerview.setAdapter(groupListAdapter);
                                    getUserFriends();
                                }

                                if (responceCode.equals("10200")) {
                                    getUserFriends();
                                    group_profile_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", userid);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getUserFriends() {
        //afterRegCPListofUniversitiesModels.clear();
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {

            groupListItem.clear();
            String url = AppUrls.BASE_URL + AppUrls.USER_FRIEND_LIST;
            Log.d("memberlisturrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("memberlistuRESP", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    group_member_recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();


                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("friendsData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        GroupMembersListModel item = new GroupMembersListModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name"));
                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        item.setPhoto(image);
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));

                                        groupListItem.add(item);
                                    }


                                    group_member_recyclerview.setAdapter(gmemberListAdapter);
                                    homeFeedItem();

                                }

                                if (responceCode.equals("10200")) {
                                    group_member_recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    homeFeedItem();


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", userid);

                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
            };


            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    public void homeFeedItem() {
        //afterRegCPListofUniversitiesModels.clear();
        afterRegCPListofUniversitiesModels.clear();
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            progress_bar.setVisibility(View.VISIBLE);
            String url = AppUrls.BASE_URL + AppUrls.GROUP_MAIN_FEED;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE_group", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    progress_bar.setVisibility(View.GONE);
                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");


                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);

                                        HomeFeedItem item = new HomeFeedItem();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setPage_id(jsonObject2.getString("page_id"));
                                        item.setType(jsonObject2.getString("type"));
                                        item.setPost(jsonObject2.getString("post"));
                                        item.setSecurity(jsonObject2.getString("security"));
                                        item.setHiddenitem(jsonObject2.getString("hiddenitem"));
                                        item.setDate(jsonObject2.getString("date"));
                                        item.setName(jsonObject2.getString("name") + " " + jsonObject2.getString("surname"));
                                        item.setPost_edit_count(jsonObject2.getString("post_edit_count"));
                                        item.setSurname(jsonObject2.getString("surname"));
                                        item.setUsername(jsonObject2.getString("username"));
                                        item.setUser_like_status(jsonObject2.getString("user_like_status"));
                                        item.setUser_like_status_type(jsonObject2.getString("user_like_status_type"));
                                        item.setShared_info_count(jsonObject2.getString("shared_info_count"));
                                        JSONObject shared_object = jsonObject2.getJSONObject("shared_info");
                                        item.setShared_info_name(shared_object.getString("name"));
                                        item.setShared_info_surname(shared_object.getString("surname"));
                                        item.setShared_info_username(shared_object.getString("username"));
                                        item.setShared_info_photo(AppUrls.BASE_IMAGE_URL + shared_object.getString("photo"));
                                        item.setShared_info_post(shared_object.getString("oldpost"));


                                        String image = AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo");
                                        // Log.d("image_photo",image);
                                        item.setPhoto(image);
                                        if (jsonObject2.getString("video_list").equals("[]")) {
                                            video = "";
                                        } else {

                                            JSONArray video_array = jsonObject2.getJSONArray("video_list");
                                            Log.d("video_list", "" + video_array);
                                            if (video_array.getJSONObject(0) != null) {
                                                JSONObject jobj1 = video_array.getJSONObject(0);
                                                video = jobj1.getString("video_id");


                                            }
                                        }
                                        item.setPhoto_list_count(jsonObject2.getString("photo_list_count"));
                                        Log.d("jkbdfbd", jsonObject2.getString("photo_list_count"));
                                        if (jsonObject2.getString("photo_list").equals("[]")) {
                                            image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                            image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                            image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                        } else {
                                            photos_array = jsonObject2.getJSONArray("photo_list");
                                            Log.d("sdhavgsbfsd", "" + photos_array);
                                            for (int j = 0; j < photos_array.length(); j++) {
                                                String photos = photos_array.getString(j);
                                                String pic = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos;
                                                photo = pic;
                                            }
                                            if (photos_array.getString(0) != null) {
                                                //  JSONObject jobj1 = photos_array.getJSONObject(0);

                                                image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos_array.getString(0);

                                            } else if (photos_array.getString(1) != null) {
                                                // JSONObject jobj2 = photos_array.getJSONObject(1);

                                                image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos_array.getString(1);


                                            } else if (photos_array.getString(2) != null) {
                                                // JSONObject jobj3 = photos_array.getJSONObject(2);

                                                image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + photos_array.getString(2);

                                            } else {
                                                image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                                image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                                image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS;
                                            }
                                        }
                                       /* else {
                                            image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS ;
                                            image2 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS ;
                                            image3 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS ;
                                        }*/
                                        // Log.d("sdbvfsdkav",photo);
                                        item.setPhoto_list_photo_link(photo);
                                        item.setImage_one(image1);
                                        item.setImage_two(image2);
                                        item.setImage_three(image3);

                                        item.setPost_likes_count(jsonObject2.getString("post_likes_count"));
                                        item.setPost_comments_count(jsonObject2.getString("post_comments_count"));
                                        item.setVideo_list_url(video);
                                        Log.d("cbgsd", "" + video);


                                        Log.d("cbxjvkb", image1 + "\n" + image2 + "\n" + image3);

                                        afterRegCPListofUniversitiesModels.add(item);
                                    }


                                    after_reg_list_universies_recyclerview.setAdapter(afterRegCPListofUniversitiesAdapter);


                                }
                                if (responceCode.equals("10800")) {
                                    progress_bar.setVisibility(View.GONE);
                                }
                                if (responceCode.equals("10200")) {
                                    progress_bar.setVisibility(View.GONE);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                progress_bar.setVisibility(View.GONE);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progress_bar.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    //params.put("page_owner_id", "10823861019183948");
                    params.put("page_owner_id", userid);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                afterRegCPListofUniversitiesModels.clear();
                homeFeedItem();
            }
        }, 2000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
