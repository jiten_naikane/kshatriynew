package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.InformationDirectoryLocationAdapter;
import in.innasoft.kshatriyayouth.adapters.ProfileInformationDirectoryAdapter;
import in.innasoft.kshatriyayouth.adapters.ProfileInformationDirectoryCatogeriesAdapter;
import in.innasoft.kshatriyayouth.models.CatogeriesModel;
import in.innasoft.kshatriyayouth.models.InformationDirectoryModel;
import in.innasoft.kshatriyayouth.models.LocationModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FixedSwipeRefreshLayout;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class ProfileInformationDirectoryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    String[] country = {"All", "Accounting", "Admin and Clerical", "Automotive", "Banking", "BioTech", "Retail", "Sales and Marketing", "Transportation",};
    RecyclerView recyclerview, recylerview_catogiries;
    ProfileInformationDirectoryAdapter adapter;

    ProfileInformationDirectoryCatogeriesAdapter cat_adapter;

    ArrayList<InformationDirectoryModel> info_directory_list = new ArrayList<InformationDirectoryModel>();
    ArrayList<CatogeriesModel> cat_feeditem = new ArrayList<CatogeriesModel>();
    LinearLayoutManager layoutManager;

    private boolean checkInternet;
    int total_number_of_items = 0;
    private boolean userScrolled = true;
    private static int displayedposition = 0;
    RelativeLayout bottomLayout;
    int type_of_request = 0;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 1;
    ProgressDialog progressDialog;
    ImageView nodata_image;
    TextView location, sublocation;
    String locality = "0";
    String sublocality = "0";
    String id_catogery;

    ////////////////////////////////////
    InformationDirectoryLocationAdapter surnameListAdapter;
    ArrayList<LocationModel> surnameModels = new ArrayList<LocationModel>();
    ArrayList<String> surname_list = new ArrayList<String>();
    AlertDialog dialog;
    Spinner spin;

    FixedSwipeRefreshLayout swipeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_information_directory);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(ProfileInformationDirectoryActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        bottomLayout = (RelativeLayout) findViewById(R.id.loadItemsLayout_recyclerView);
        spin = (Spinner) findViewById(R.id.spinner1);
        location = (TextView) findViewById(R.id.location);
        sublocation = (TextView) findViewById(R.id.sublocation);
        spin.setOnItemSelectedListener(this);
        sublocation.setOnClickListener(this);
        location.setOnClickListener(this);
        nodata_image = (ImageView) findViewById(R.id.nodata_image);
        //Creating the ArrayAdapter instance having the country list

        swipeView = (FixedSwipeRefreshLayout) findViewById(R.id.swipe);

        recylerview_catogiries = (RecyclerView) findViewById(R.id.recylerview_catogiries);
        recylerview_catogiries.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ProfileInformationDirectoryActivity.this);
        //recylerview_catogiries.setLayoutManager(layoutManager);
        recylerview_catogiries.setLayoutManager(new LinearLayoutManager(ProfileInformationDirectoryActivity.this, LinearLayoutManager.HORIZONTAL, false));
        cat_adapter = new ProfileInformationDirectoryCatogeriesAdapter(cat_feeditem, ProfileInformationDirectoryActivity.this, R.layout.catogiries_row);

        recylerview_catogiries.setNestedScrollingEnabled(false);

        cat_feedItem();

        recyclerview = (RecyclerView) findViewById(R.id.recylerview);
        swipeView.setRecyclerView(recyclerview);
        recyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ProfileInformationDirectoryActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        info_directory_list.clear();
        adapter = new ProfileInformationDirectoryAdapter(info_directory_list, ProfileInformationDirectoryActivity.this, R.layout.row_information_directory);
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                feedItem("0", defaultPageNo, locality, sublocality);

                            } else {
                                Toast.makeText(ProfileInformationDirectoryActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

        });

        swipeView.setColorScheme(android.R.color.holo_blue_dark,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_green_light);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        info_directory_list.clear();
                        defaultPageNo = 1;
                        displayedposition = 0;
                        feedItem("0", defaultPageNo, locality, sublocality);
                    }
                }, 3000);
            }
        });

        // getLocation();
        feedItem("0", defaultPageNo, locality, sublocality);
    }

    public void feedItem(String ids, int pagenumber, String locality, String sublocality) {
        //afterRegCPListofUniversitiesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            if (type_of_request == 0) {
                progressDialog.show();
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
            }
            // info_directory_list.clear();
            id_catogery = ids;
            String url = AppUrls.BASE_URL + AppUrls.INFORMATION_DIRECTORY + "/10/" + pagenumber + "/" + ids + "/" + locality + "/" + sublocality;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    swipeView.setRefreshing(false);
                                    recyclerview.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.GONE);

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    int total_numberof_records = Integer.valueOf(jsonObject1.getString("total_recs"));
                                    total_number_of_items = total_numberof_records;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + total_numberof_records);
                                    if (total_numberof_records > info_directory_list.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + total_numberof_records);
                                    } else {
                                        loading = false;
                                    }

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        InformationDirectoryModel item = new InformationDirectoryModel();
                                        item.setInfo_id(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name"));
                                        String image = AppUrls.BASE_URL + jsonObject2.getString("image");
                                        item.setInfo_image(image);
                                        item.setOccupation(jsonObject2.getString("occupation"));

                                        info_directory_list.add(item);
                                    }

                                    layoutManager.scrollToPositionWithOffset(displayedposition, info_directory_list.size());
                                    recyclerview.setAdapter(adapter);
                                    bottomLayout.setVisibility(View.GONE);

                                }
                                if (responceCode.equals("10800")) {

                                }
                                if (responceCode.equals("10200")) {
                                    recyclerview.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.VISIBLE);
                                    bottomLayout.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                nodata_image.setVisibility(View.VISIBLE);
                                bottomLayout.setVisibility(View.GONE);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    nodata_image.setVisibility(View.VISIBLE);
                    bottomLayout.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(ProfileInformationDirectoryActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void cat_feedItem() {
        //afterRegCPListofUniversitiesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.CATEGORY_LIST;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {


                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");


                                    JSONArray jsonArray = jsonObject1.getJSONArray("categoryData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                                        CatogeriesModel item = new CatogeriesModel();
                                        item.setId(jsonObject2.getString("id"));
                                        item.setName(jsonObject2.getString("name"));

                                        item.setUrl_name(jsonObject2.getString("url_name"));

                                        cat_feeditem.add(item);
                                    }


                                    recylerview_catogiries.setAdapter(cat_adapter);

                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            })
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/;

            RequestQueue requestQueue = Volley.newRequestQueue(ProfileInformationDirectoryActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            case R.id.filter:
                Intent intent = new Intent(ProfileInformationDirectoryActivity.this, FilterActivity.class);
                startActivityForResult(intent, 1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getApplicationContext(), String.valueOf(id), Toast.LENGTH_LONG).show();
        String locational = String.valueOf(id);
        feedItem("0", defaultPageNo, locational, sublocality);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == location) {
            // getLocation();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                String result1 = data.getStringExtra("result1");
                locality = result;
                sublocality = result1;
                feedItem(id_catogery, defaultPageNo, result, result1);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onA
}