package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.GroupViewPagerAdapter;
import in.innasoft.kshatriyayouth.fragments.GroupDiscussionFragment;
import in.innasoft.kshatriyayouth.fragments.GroupMembersFragment;
import in.innasoft.kshatriyayouth.fragments.GroupPhotosFragment;
import in.innasoft.kshatriyayouth.fragments.GroupVideosFragment;
import in.innasoft.kshatriyayouth.fragments.ReportGroupFragment;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class GroupDetailActivity extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String userid, grp_id, grp_img, grp_type, grp_name, user_profile_id, grp_manager_id;
    Toolbar grp_toolbar;
    ImageView grp_detail_img, close, delete_group, edit_group;
    CollapsingToolbarLayout collapse_grp_toolbar;
    ViewPager view_pager;
    TabLayout tabLayout_grp;
    boolean hideIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        grp_toolbar = (Toolbar) findViewById(R.id.grp_toolbar);
        //  grp_toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        //  setSupportActionBar(grp_toolbar);
        //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapse_grp_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_grp_toolbar);
        collapse_grp_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapse_grp_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_grp_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);
//        collapse_grp_toolbar.setCollapsedTitleTypeface(typeface);
//        collapse_grp_toolbar.setExpandedTitleTypeface(typeface);


//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);


        Bundle bundle = getIntent().getExtras();
        grp_id = bundle.getString("GROUP_ID");
        grp_manager_id = bundle.getString("GROUP_MANAGER");
        grp_name = bundle.getString("GROUP_NAME");
        grp_type = bundle.getString("GROUP_TYPE");
        grp_img = bundle.getString("GROUP_IMAGE");
        Log.d("DETRAILS", grp_id + "///" + grp_name + "///" + grp_type + "////" + grp_img + "/////" + grp_manager_id + "/////" + user_profile_id);

        view_pager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(view_pager);
        tabLayout_grp = (TabLayout) findViewById(R.id.tabLayout_grp);
        tabLayout_grp.setupWithViewPager(view_pager);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        delete_group = (ImageView) findViewById(R.id.delete_group);
        delete_group.setOnClickListener(this);
        edit_group = (ImageView) findViewById(R.id.edit_group);
        edit_group.setOnClickListener(this);

        grp_detail_img = (ImageView) findViewById(R.id.grp_detail_img);
        collapse_grp_toolbar.setTitle(grp_name);

        Log.d("asdfdsfdsf", grp_manager_id + "gggg" + user_profile_id);

        if (!(grp_manager_id.equals(user_profile_id))) {
            delete_group.setVisibility(View.GONE);
            edit_group.setVisibility(View.GONE);

        }


        Picasso.with(GroupDetailActivity.this)
                .load(grp_img)
                .placeholder(R.drawable.no_images_found)
                .into(grp_detail_img);


    }

    private void setupViewPager(ViewPager view_pager) {
        if (grp_manager_id.equals(user_profile_id)) {
            GroupViewPagerAdapter adapter = new GroupViewPagerAdapter(getSupportFragmentManager());
            adapter.addFrag(new GroupDiscussionFragment(), "Discussion");
            adapter.addFrag(new GroupMembersFragment(), "Members");
            adapter.addFrag(new GroupPhotosFragment(), "Photos");
            adapter.addFrag(new GroupVideosFragment(), "Videos");

            //  adapter.addFrag(new ReportGroupFragment(), "Report Group");

            view_pager.setAdapter(adapter);

        } else {
            GroupViewPagerAdapter adapter = new GroupViewPagerAdapter(getSupportFragmentManager());
            adapter.addFrag(new GroupDiscussionFragment(), "Discussion");
            adapter.addFrag(new GroupMembersFragment(), "Members");
            adapter.addFrag(new GroupPhotosFragment(), "Photos");
            adapter.addFrag(new GroupVideosFragment(), "Videos");

            adapter.addFrag(new ReportGroupFragment(), "Report Group");

            view_pager.setAdapter(adapter);
        }


    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.group_detail_menu_bar, menu);


        return true;
    }*/
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
       *//*  if(user_profile_id != grp_manager_id)
         {
             item.setVisible(false);
         }*//*
        switch (item.getItemId())
        {

            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;



            case R.id.edit_group :

                Toast.makeText(GroupDetailActivity.this,"edit",Toast.LENGTH_LONG).show();
                //Intent craetefroup=new Intent(GroupDetailActivity.this,GroupCreateActivityDialog.class);
               // startActivity(craetefroup);
                return true;

            case R.id.delete_group :



                AlertDialog.Builder builder = new AlertDialog.Builder(GroupDetailActivity.this);
                builder.setTitle("Are you Sure want to Delete Group ? ");
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                checkInternet = NetworkChecking.isConnected(GroupDetailActivity.this);
                                if (checkInternet)
                                {
                                    String  urldelete=AppUrls.BASE_URL + AppUrls.GROUP_DELETE+"/"+grp_id;
                                    Log.d("URLDELETE:",urldelete);
                                    StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GROUP_DELETE+"/"+grp_id,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    progressDialog.dismiss();

                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        Log.d("DeleteResoponce",response);
                                                        String responceCode = jsonObject.getString("status");

                                                        if (responceCode.equalsIgnoreCase("10100"))
                                                        {
                                                            Toast.makeText(GroupDetailActivity.this, "Successfully Deleted..!", Toast.LENGTH_SHORT).show();
                                                            progressDialog.dismiss();
                                                            Intent i =new Intent(GroupDetailActivity.this,ProfileGroupsActivity.class);
                                                            startActivity(i);

                                                            finish();

                                                        }

                                                        if (responceCode.equals("10200")) {
                                                            progressDialog.dismiss();
                                                            Toast.makeText(GroupDetailActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                                        }

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressDialog.dismiss();

                                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                    } else if (error instanceof AuthFailureError) {

                                                    } else if (error instanceof ServerError) {

                                                    } else if (error instanceof NetworkError) {

                                                    } else if (error instanceof ParseError) {

                                                    }
                                                }
                                            }) {

                                    };
                                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
                                    requestQueue.add(stringRequest);

                                } else {
                                    progressDialog.cancel();
                                    Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();

                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == edit_group) {

            Intent craetefroup = new Intent(GroupDetailActivity.this, GroupCreateActivityDialog.class);
            craetefroup.putExtra("Profile", "editprofile");
            Log.d("GRRRRRR:", grp_id);
            craetefroup.putExtra("GroupId", grp_id);

            startActivity(craetefroup);
        }
        if (view == delete_group) {


            AlertDialog.Builder builder = new AlertDialog.Builder(GroupDetailActivity.this);
            builder.setTitle("Are you Sure want to Delete Group ? ");
            builder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            checkInternet = NetworkChecking.isConnected(GroupDetailActivity.this);
                            if (checkInternet) {
                                String urldelete = AppUrls.BASE_URL + AppUrls.GROUP_DELETE + "/" + grp_id;
                                Log.d("URLDELETE:", urldelete);
                                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GROUP_DELETE + "/" + grp_id,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                progressDialog.dismiss();

                                                try {
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    Log.d("DeleteResoponce", response);
                                                    String responceCode = jsonObject.getString("status");

                                                    if (responceCode.equalsIgnoreCase("10100")) {
                                                        Toast.makeText(GroupDetailActivity.this, "Successfully Deleted..!", Toast.LENGTH_SHORT).show();
                                                        progressDialog.dismiss();
                                                        Intent i = new Intent(GroupDetailActivity.this, ProfileGroupsActivity.class);
                                                        startActivity(i);

                                                        finish();

                                                    }

                                                    if (responceCode.equals("10200")) {
                                                        progressDialog.dismiss();
                                                        Toast.makeText(GroupDetailActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }) {

                                };
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
                                requestQueue.add(stringRequest);

                            } else {
                                progressDialog.cancel();
                                Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }
}
