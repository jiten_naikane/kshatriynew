package in.innasoft.kshatriyayouth.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SharePostAdapter;
import in.innasoft.kshatriyayouth.app.AppController;
import in.innasoft.kshatriyayouth.models.EditPostFeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FeedImageView;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class SharePostActivity extends AppCompatActivity {
    Bundle bundle;
    String post_id, user_id;
    private boolean checkInternet;
    private List<EditPostFeedItem> feedItems;
    private ListView listView;
    private SharePostAdapter listAdapter;
    NetworkImageView profilePic;
    ImageLoader imageLoader;
    TextView name, txtStatusMsg;
    FeedImageView feedImage1;
    String items[] = {"share on My wall", "share on My Friend's wall"};
    String items1[] = {"public", "private", "friends"};
    Spinner spinner, spinner1;
    Button share_button;
    EditText edittext;
    String spinner_text, spinner_text1;
    String user_profile_id = "";
    UserSessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post);
        sessionManager = new UserSessionManager(SharePostActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();

        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        bundle = getIntent().getExtras();
        post_id = bundle.getString("post_id");
        user_id = bundle.getString("user_id");
        profilePic = (NetworkImageView) findViewById(R.id.profilePic);
        feedImage1 = (FeedImageView) findViewById(R.id.feedImage1);
        name = (TextView) findViewById(R.id.name);
        txtStatusMsg = (TextView) findViewById(R.id.txtStatusMsg);
        edittext = (EditText) findViewById(R.id.edittext);
        share_button = (Button) findViewById(R.id.share_button);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items1);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner1.setAdapter(spinnerArrayAdapter1);
        imageLoader = AppController.getInstance().getImageLoader();
        postDisplay(post_id, user_id);

        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if (!edittext.getText().toString().equals("")) {
                    sharePost(post_id, user_id, edittext.getText().toString());
                }else{
                    Toast.makeText(SharePostActivity.this, "please post text", Toast.LENGTH_SHORT).show();
                }*/
                sharePost(post_id, user_id, edittext.getText().toString());
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // TODO Auto-generated method stub
                spinner_text = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

                // TODO Auto-generated method stub
            }
        });
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // TODO Auto-generated method stub
                spinner_text1 = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

                // TODO Auto-generated method stub
            }
        });
    }

    private void postDisplay(final String post_id, String user_id) {
        checkInternet = NetworkChecking.isConnected(SharePostActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.POST_EDIT_INFO_LIST + "/" + user_id + "/" + post_id;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject response_boj = new JSONObject(response);
                                Log.d("Post_response", response);
                                String responceCode = response_boj.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject feedObject = response_boj.getJSONObject("data");
                                    JSONArray feedArray = feedObject.getJSONArray("post_data");
                                    JSONArray imagesarray = feedObject.getJSONArray("photo_list");
                                    String images = "" + imagesarray;
                                    if (images.equals("[]")) {

                                    } else {
                                        String image1 = AppUrls.BASE_IMAGE_URL_GROUP_ALL_PHOTOS + imagesarray.getString(0);
                                        Log.d("imagesssssssssssssss", image1);
                                        feedImage1.setImageUrl(image1, imageLoader);
                                        feedImage1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                    }
                                                });

                                    }
                                    JSONObject share_object = feedArray.getJSONObject(0);
                                    Log.d("sdjvkbbkvsdfbvsjk", "" + feedArray);
                                    String profile_pic = AppUrls.BASE_IMAGE_URL + share_object.getString("photo");
                                    profilePic.setImageUrl(profile_pic, imageLoader);
                                    name.setText(share_object.getString("name") + " " + share_object.getString("surname"));
                                    txtStatusMsg.setText(share_object.getString("post"));
                                    /*for (int i = 0; i < feedArray.length(); i++) {
                                        JSONObject feedObj = (JSONObject) feedArray.get(i);

                                        EditPostFeedItem item = new EditPostFeedItem();
                                        item.setPost(feedObj.getString("post"));
                                        item.setDate(feedObj.getString("date"));
                                        item.setName(feedObj.getString("name")+" "+feedObj.getString("surname"));
                                        String profile_pic = AppUrls.BASE_IMAGE_URL+feedObj.getString("photo");
                                        item.setPhoto(profile_pic);
                                        feedItems.add(item);
                                    }*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

            };
            RequestQueue requestQueue = Volley.newRequestQueue(SharePostActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(SharePostActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void sharePost(final String post_id, String user_id, final String post) {
        //afterRegCPListofUniversitiesModels.clear();

        checkInternet = NetworkChecking.isConnected(SharePostActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.UPDATE_SHARE;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);


                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    finish();
/*

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");



                                    //  JSONArray jsonArray = jsonObject1.getJSONArray("records1");

                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject jsonObject2 = jsonObject1.getJSONObject(i);





                                    }
*/


                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("post_id", post_id);
                    params.put("post", post);
                    params.put("share_type", spinner_text);
                    params.put("share_profile_ids", user_profile_id);
                    params.put("post_profile_id", user_profile_id);
                    params.put("privacy_type", spinner_text1);
                    Log.d("GRPCREATIONPARAM::", params.toString());
                    return params;
                }
                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("user_profile_id", user_profile_id);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };
            /*{
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            }*/
            ;

            RequestQueue requestQueue = Volley.newRequestQueue(SharePostActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(SharePostActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
}
