package in.innasoft.kshatriyayouth.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FilePath;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class JobsDetailActivity extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager session;
    ImageView no_data, job_detail_img;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    Toolbar job_toolbar;
    String user_id, job_detail_id, job_name;
    Toolbar toolbar1;
    CollapsingToolbarLayout collapse_job_toolbar;

    TextView job_detailTitle, job_posted_by, job_experience_title, job_experience_text, job_skill_title, job_skill_text, job_posteddate_title,
            job_posteddate_text, job_salary_title, job_salary_text, job_employ_type_title, job_employ_type_text, button_apply_now, selected_file_path;
    BottomSheetDialog bottomSheetDialog;
    String name, file_description;

    private int PICK_PDF_REQUEST = 1;
    private Uri filePath;
    private static final int STORAGE_PERMISSION_CODE = 123;
    HttpEntity resEntity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_detail);


//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        job_toolbar = (Toolbar) findViewById(R.id.job_toolbar);
        job_toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(job_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapse_job_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_job_toolbar);
        collapse_job_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapse_job_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_job_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();
        job_detail_id = bundle.getString("JOB_ID");
        job_name = bundle.getString("JOBNAME");
        Log.d("JOBIDDD:", job_detail_id + "//" + job_name);

        job_detail_img = (ImageView) findViewById(R.id.job_detail_img);

        button_apply_now = (TextView) findViewById(R.id.button_apply_now);
//        button_apply_now.setTypeface(typeface);
        button_apply_now.setOnClickListener(this);


        job_detailTitle = (TextView) findViewById(R.id.job_detailTitle);
//        job_detailTitle.setTypeface(typeface);

        job_posted_by = (TextView) findViewById(R.id.job_posted_by);
//        job_posted_by.setTypeface(typeface);

        job_experience_title = (TextView) findViewById(R.id.job_experience_title);
//        job_experience_title.setTypeface(typeface);
        job_experience_text = (TextView) findViewById(R.id.job_experience_text);
//        job_experience_text.setTypeface(typeface);

        job_skill_title = (TextView) findViewById(R.id.job_skill_title);
//        job_skill_title.setTypeface(typeface);
        job_skill_text = (TextView) findViewById(R.id.job_skill_text);
//        job_skill_text.setTypeface(typeface);

        job_posteddate_title = (TextView) findViewById(R.id.job_posteddate_title);
//        job_posteddate_title.setTypeface(typeface);
        job_posteddate_text = (TextView) findViewById(R.id.job_posteddate_text);
//        job_posteddate_text.setTypeface(typeface);

        job_salary_title = (TextView) findViewById(R.id.job_salary_title);
//        job_salary_title.setTypeface(typeface);
        job_salary_text = (TextView) findViewById(R.id.job_salary_text);
//        job_salary_text.setTypeface(typeface);

        job_employ_type_title = (TextView) findViewById(R.id.job_employ_type_title);
//        job_employ_type_title.setTypeface(typeface);
        job_employ_type_text = (TextView) findViewById(R.id.job_employ_type_text);


//        job_employ_type_text.setTypeface(typeface);


        getJobDetail();

    }

    private void getJobDetail()

    {

        checkInternet = NetworkChecking.isConnected(JobsDetailActivity.this);
        if (checkInternet) {


            progressDialog.show();
            final String url = AppUrls.BASE_URL + AppUrls.JOBS_DETAILS + "/" + job_detail_id;
            Log.d("assjoblURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.JOBS_DETAILS + "/" + job_detail_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASSOJOBRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");


                                    name = jsonObject2.getString("name");
                                    String tittle = name.substring(0, 1).toUpperCase() + name.substring(1);
                                    job_detailTitle.setText(Html.fromHtml(tittle));

                                    collapse_job_toolbar.setTitle(Html.fromHtml(name));

                                    String posted_by = jsonObject2.getString("associations_name");

                                    if (posted_by.equals("null") || posted_by.equals("")) {
                                        job_posted_by.setVisibility(View.GONE);
                                    }

                                    job_posted_by.setText(Html.fromHtml(posted_by));

                                    String posted_date_time = jsonObject2.getString("create_date_time");
                                    job_posteddate_text.setText(Html.fromHtml(posted_date_time));

                                    String emp_type = jsonObject2.getString("type");
                                    job_employ_type_text.setText(Html.fromHtml(emp_type));

                                    String salary = jsonObject2.getString("salary");
                                    job_salary_text.setText(Html.fromHtml(salary));

                                    String skills = jsonObject2.getString("skills");
                                    job_skill_text.setText(Html.fromHtml(skills));

                                    String experience = jsonObject2.getString("experience");
                                    job_experience_text.setText(Html.fromHtml(experience));

                                    Picasso.with(JobsDetailActivity.this)
                                            .load(AppUrls.BASE_URL + jsonObject2.getString("image"))
                                            .placeholder(R.drawable.no_images_found)
                                            .into(job_detail_img);

                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_data.setVisibility(View.VISIBLE);
                                    Toast.makeText(JobsDetailActivity.this, "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            no_data.setVisibility(View.VISIBLE);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(JobsDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(JobsDetailActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == button_apply_now) {
            bottomSheetDialog = new BottomSheetDialog(JobsDetailActivity.this);

            View view = getLayoutInflater().inflate(R.layout.custom_apply_job, null);

            Button choose_file_button;
            TextView apply_header_txt, button_custom_appy;
            final EditText file_descreption_text;
            bottomSheetDialog.setContentView(view);

            apply_header_txt = (TextView) view.findViewById(R.id.apply_header_txt);
//            apply_header_txt.setTypeface(typeface);

            selected_file_path = (TextView) view.findViewById(R.id.selected_file_path);
//            selected_file_path.setTypeface(typeface);

            file_descreption_text = (EditText) view.findViewById(R.id.file_descreption_text);
//            file_descreption_text.setTypeface(typeface);
            file_description = file_descreption_text.getText().toString().trim();
            Log.d("DEWSCRIPTIONNNNNN:", file_description);

            choose_file_button = (Button) view.findViewById(R.id.choose_file_button);
//            choose_file_button.setTypeface(typeface);

            button_custom_appy = (TextView) view.findViewById(R.id.button_custom_appy);
//            button_custom_appy.setTypeface(typeface);

            apply_header_txt.setText("Job Application For " + name);


            choose_file_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {/*
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                   intent.setType("file*//*");
                   String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/docx"};
                  intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                    startActivityForResult(intent, PICK_PDF_REQUEST);*/
                    // Intent intent = new Intent();
                    //  intent.setType("application");
                    //  intent.setAction(Intent.ACTION_GET_CONTENT);
                    //  startActivityForResult(Intent.createChooser(intent, "select File"), PICK_PDF_REQUEST);
                    ActivityCompat.requestPermissions(JobsDetailActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_PDF_REQUEST);
                    Intent intent = new Intent();
                    // intent.setType("application/msword");
                    intent.setType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "select File"), PICK_PDF_REQUEST);
                }
            });


            button_custom_appy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (filePath == null || file_descreption_text.equals("")) {
                        Toast.makeText(JobsDetailActivity.this, "Please fill Details", Toast.LENGTH_LONG).show();
                    } else {
                        uploadfile();
                    }


                }
            });


            bottomSheetDialog.show();
            file_descreption_text.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    file_description = s.toString();

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });

        }
    }

    private void uploadfile() {
        String path = FilePath.getPath(this, filePath);


        if (path == null) {

            Toast.makeText(this, "Please Attach File ", Toast.LENGTH_LONG).show();
        } else {
            JobsDetailActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (progressDialog != null)
                        progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            String response_str = null;
            File file1 = null;
            FileBody bin1 = null;
            String empty = "";
            Log.d("SDFAFGSGFSG", "sfafafsaf 1");
            file1 = new File(path.replaceAll("file://", "").replace("%20", " "));
            // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
            String urlString = "";


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                urlString = AppUrls.BASE_URL + AppUrls.UPLOAD_RESUME;
            } else {
                urlString = AppUrls.BASE_URL + AppUrls.UPLOAD_RESUME;
            }
            Log.d("REQUESTURL", urlString);
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(urlString);

                MultipartEntity reqEntity = new MultipartEntity();
                //  Log.d("USERIDETAIL:",user_id+"///"+selectedSubject+"///"+name+"///"+emaill+"//"+descrption+"///"+file1);


                reqEntity.addPart("user_id", new StringBody(user_id));
                reqEntity.addPart("description", new StringBody(file_description));
                reqEntity.addPart("job_id", new StringBody(job_detail_id));
                bin1 = new FileBody(file1);
                reqEntity.addPart("userfile", bin1);
                Log.d("APPLYDETAIL:", user_id + "///" + file_description + "///" + job_detail_id + "//" + bin1);
                post.setEntity(reqEntity);
                HttpResponse response = client.execute(post);
                resEntity = response.getEntity();
                response_str = EntityUtils.toString(resEntity);

                Log.d("APPLYRESP", response_str);
                try {

                    JSONObject jsonObject = new JSONObject(response_str);
                    String responceCode = jsonObject.getString("status");
                    if (responceCode.equalsIgnoreCase("10100")) {

                              /* Intent i =new Intent(JobsDetailActivity.this,ProfileJobsActivity.class);
                                startActivity(i);*/
                                /*JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                if(jsonObject1.equals("Success"))
                                {*/
                        Toast.makeText(getApplicationContext(), "Applied Job Successfully", Toast.LENGTH_SHORT).show();
                               /* }*/
                        finish();
                    }
                    if (responceCode.equals("10200")) {

                        Toast.makeText(JobsDetailActivity.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            } catch (Exception ex) {
                Log.e("Debug", "error: " + ex.getMessage(), ex);
            }
            //Uploading code
               /* try {
                    String uploadId = UUID.randomUUID().toString();

                    Log.d("FILEPATHONACTIVUT:", path.toString()+"////"+user_id+"///"+file_description+"//"+job_detail_id);


                    //Creating a multi part request
                    new MultipartUploadRequest(this, uploadId, url_upload)
                            .addFileToUpload(path.toString(),"userfile" ) //Adding file
                            .addParameter("user_id", user_id) //Adding text parameter to the request
                            .addParameter("description", file_description) //Adding text parameter to the request
                            .addParameter("job_id", job_detail_id)
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .setMethod("POST")

                            .startUpload();
                    Toast.makeText(this, "Applied Successfully.." , Toast.LENGTH_SHORT).show();
                    bottomSheetDialog.dismiss();

                    //Starting the upload

                } catch (Exception exc) {
                    Toast.makeText(this, "DEEBBUUGUGU:" + exc.getMessage(), Toast.LENGTH_SHORT).show();

                }*/
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            String uriString = filePath.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            Log.d("erwerew:", filePath.toString());
            String displayName = null;

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = getApplicationContext().getContentResolver().query(filePath, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        Log.d("SHORTTTTTTT11:", displayName);
                        selected_file_path.setText(displayName);


                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
                Log.d("SHORTTTTTTT22:", displayName);
                selected_file_path.setText(displayName);
            }


            /*Log.d("FILEPATHONACTIVUT:",filePath.toString());

            String file_path=filePath.toString();
            String original=file_path.substring(file_path.lastIndexOf("/")+1);
            selected_file_path.setText(original);
            Log.d("SHORTTTTTTT:",original);*/

        }
    }

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}
