package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.AreaListAdapter;
import in.innasoft.kshatriyayouth.adapters.InformationDirectoryLocationAdapter;
import in.innasoft.kshatriyayouth.models.AreaModel;
import in.innasoft.kshatriyayouth.models.LocationModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {
    TextView location, sublocation;
    InformationDirectoryLocationAdapter surnameListAdapter;
    AreaListAdapter areaListAdapter;

    ArrayList<LocationModel> surnameModels = new ArrayList<LocationModel>();
    ArrayList<String> surname_list = new ArrayList<String>();

    ArrayList<AreaModel> areaModels = new ArrayList<AreaModel>();
    ArrayList<String> area_list = new ArrayList<String>();

    ProgressDialog progressDialog;
    private boolean checkInternet;
    AlertDialog dialog;
    Button apply;
    String location_id = "", sub_location_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        progressDialog = new ProgressDialog(FilterActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        location = (TextView) findViewById(R.id.location);
        sublocation = (TextView) findViewById(R.id.sublocation);
        apply = (Button) findViewById(R.id.apply);
        sublocation.setOnClickListener(this);
        location.setOnClickListener(this);
        apply.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == location) {
            getLocation();
        }
        if (v == apply) {
            if (!location.getText().toString().equals("") && !sublocation.getText().toString().equals("")) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", location_id);
                returnIntent.putExtra("result1", sub_location_id);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == sublocation) {
            if (sublocation.equals("")) {
                Toast.makeText(FilterActivity.this, "Please Select Location", Toast.LENGTH_LONG).show();
            } else {
                getAreaDetail(location_id);
            }

        }
    }


    private void getLocation() {
        checkInternet = NetworkChecking.isConnected(FilterActivity.this);
        if (checkInternet) {
            surname_list.clear();
            surnameModels.clear();
            progressDialog.show();
            String url = AppUrls.BASE_URL + "location_app";
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SURNAMERESPONSE:", response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("locationData");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        LocationModel surnameModel = new LocationModel();
                                        surnameModel.setId(jsonObject2.getString("id"));
                                        String surname_name = jsonObject2.getString("name");
                                        surnameModel.setSurname(surname_name);

                                        surname_list.add(surname_name);
                                        surnameModels.add(surnameModel);
                                    }
                                    getSurnameListDialog();


                                } else {
                                    progressDialog.dismiss();
                                }

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(FilterActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(FilterActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getSurnameListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(FilterActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.surname_list_dialog, null);

        TextView surName_list_txt = (TextView) dialog_layout.findViewById(R.id.surName_list_txt);
        surName_list_txt.setText("Select Location");


        final SearchView surName_search = (SearchView) dialog_layout.findViewById(R.id.surName_search);
        EditText searchEditText = (EditText) surName_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);


        RecyclerView surName_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.surName_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FilterActivity.this);
        surName_recycler_view.setLayoutManager(layoutManager);

        /*for (int i = 0; i < surname_list.size(); i++){
            LocationModel surnameListModel = new LocationModel();
            surnameListModel.setSurname(surname_list.get(i));
            surnameModels.add(surnameListModel);
        }*/

        surnameListAdapter = new InformationDirectoryLocationAdapter(surnameModels, FilterActivity.this, R.layout.row_surname_list);

        surName_recycler_view.setAdapter(surnameListAdapter);

        surName_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                surName_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        surName_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                surnameListAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setSurName(String surname, String id) {

        dialog.dismiss();
        surnameListAdapter.notifyDataSetChanged();
        location.setError(null);
        location.setText(surname);
        location_id = id;

    }

    private void getAreaDetail(String location_id) {
        checkInternet = NetworkChecking.isConnected(FilterActivity.this);
        if (checkInternet) {
            area_list.clear();
            areaModels.clear();
            progressDialog.show();
            String url = AppUrls.BASE_URL + AppUrls.AREA_DETAIL + "/" + location_id;
            Log.d("LISTAREAURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("AREAREAPONSE:", response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("area");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        AreaModel surnameModel = new AreaModel();
                                        surnameModel.setId(jsonObject2.getString("id"));
                                        String area_name = jsonObject2.getString("area_name");
                                        surnameModel.setArea_name(area_name);

                                        area_list.add(area_name);
                                        areaModels.add(surnameModel);
                                    }
                                    getAreaListDialog();


                                } else {
                                    progressDialog.dismiss();
                                }

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(FilterActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(FilterActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }


    }

    private void getAreaListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FilterActivity.this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.area_list_dialog, null);

        TextView area_list_txt = (TextView) dialog_layout.findViewById(R.id.area_list_txt);


        final SearchView area_search = (SearchView) dialog_layout.findViewById(R.id.area_search);
        EditText areaEditText = (EditText) area_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);


        RecyclerView area_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.area_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FilterActivity.this);
        area_recycler_view.setLayoutManager(layoutManager);


        areaListAdapter = new AreaListAdapter(areaModels, FilterActivity.this, R.layout.row_area_list);

        area_recycler_view.setAdapter(areaListAdapter);

        area_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                area_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        area_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                areaListAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setArea(String area, String id) {

        dialog.dismiss();
        areaListAdapter.notifyDataSetChanged();
        sublocation.setError(null);
        sublocation.setText(area);
        sub_location_id = id;
        Log.d("DETAIIIIL", area + "" + id);

    }
}
