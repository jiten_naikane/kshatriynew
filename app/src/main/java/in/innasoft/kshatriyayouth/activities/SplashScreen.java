package in.innasoft.kshatriyayouth.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;
import pl.droidsonroids.gif.GifImageView;


public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2200;
    GifImageView gifview;
    UserSessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        session = new UserSessionManager(getApplicationContext());
        gifview = (GifImageView) findViewById(R.id.gifview);

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        Animation fadeout = new AlphaAnimation(1.f, 1.f);
                        fadeout.setDuration(2200);
                        final View viewToAnimate = gifview;

                        fadeout.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                gifview.setBackgroundResource(R.drawable.loading_spalsh);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        gifview.startAnimation(fadeout);


                        if (session.checkLogin() != false) {

                            Intent i = new Intent(SplashScreen.this, LoginPage.class);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(SplashScreen.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }

                        //Intent i = new Intent(SplashScreen.this, LoginPage.class);
                        //startActivity(i);


                        //   finish();
                    }
                }, SPLASH_TIME_OUT);
    }


   /* public void startSplash()
    {
        Animation fadeout = new AlphaAnimation(1.f, 1.f);
        fadeout.setDuration(1500);
        final View viewToAnimate = gifview;

        fadeout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation)
            {
                gifview.setBackgroundResource(R.raw.loading_spalsh);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        gifview.startAnimation(fadeout);
    }*/


}




