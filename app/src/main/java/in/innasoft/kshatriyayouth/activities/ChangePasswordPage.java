package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class ChangePasswordPage extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    Toolbar toolbar;
    TextView change_password_txt;
    EditText current_password_edt, new_password_edt;
    CheckBox current_password_view, new_password_view;
    Button change_password_btn;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_page);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        change_password_txt = (TextView) findViewById(R.id.change_password_txt);
//        change_password_txt.setTypeface(typeface);

        current_password_edt = (EditText) findViewById(R.id.current_password_edt);
//        current_password_edt.setTypeface(typeface);

        new_password_edt = (EditText) findViewById(R.id.new_password_edt);
//        new_password_edt.setTypeface(typeface);

        current_password_view = (CheckBox) findViewById(R.id.current_password_view);
//        current_password_view.setTypeface(typeface);
        current_password_view.setOnCheckedChangeListener(this);

        new_password_view = (CheckBox) findViewById(R.id.new_password_view);
//        new_password_view.setTypeface(typeface);
        new_password_view.setOnCheckedChangeListener(this);

        change_password_btn = (Button) findViewById(R.id.change_password_btn);
//        change_password_btn.setTypeface(typeface);
        change_password_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == change_password_btn) {

            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    progressDialog.show();

                    StringRequest strChangPass = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CHANGE_PASSWORD, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ChangePassRESP", response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Your Password Successfully Updated...!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }

                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Old password wrong", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10400")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All Fields Required...!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException ed) {
                                ed.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("old_pass", current_password_edt.getText().toString().trim());
                            params.put("new_pass", new_password_edt.getText().toString().trim());
                            Log.d("ChangePas-PARAMS", params.toString());
                            return params;
                        }
                    };
                    strChangPass.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(ChangePasswordPage.this);
                    requestQueue.add(strChangPass);


                } else {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        if (current_password_view == compoundButton) {
            if (isChecked) {
                current_password_edt.setInputType(InputType.TYPE_CLASS_TEXT);
                current_password_edt.setSelection(current_password_edt.getText().length());
            } else {
                current_password_edt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                current_password_edt.setSelection(current_password_edt.getText().length());
            }
        }

        if (new_password_view == compoundButton) {
            if (isChecked) {
                new_password_edt.setInputType(InputType.TYPE_CLASS_TEXT);
                new_password_edt.setSelection(new_password_edt.getText().length());
            } else {
                new_password_edt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                new_password_edt.setSelection(new_password_edt.getText().length());
            }
        }
    }

    private boolean validate() {

        boolean result = true;
        String currentPassword = current_password_edt.getText().toString().trim();
        if (currentPassword == null || currentPassword.equals("") || currentPassword.length() < 6) {
            current_password_edt.setError("password must have 6 characters");
            result = false;
        }

        String newPassword = new_password_edt.getText().toString().trim();
        if (newPassword == null || newPassword.equals("") || newPassword.length() < 6) {
            new_password_edt.setError("Invalid Password");
            result = false;
        }

        return result;
    }

}
