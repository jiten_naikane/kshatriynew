package in.innasoft.kshatriyayouth.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.PostCheckInCountryAdapter;
import in.innasoft.kshatriyayouth.imageselector.adapters.SelectedImageAdapter2;
import in.innasoft.kshatriyayouth.models.CheckInCountriesModel;
import in.innasoft.kshatriyayouth.models.SearchingModel;
import in.innasoft.kshatriyayouth.models.SelectedImageModel2;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.LocationTrack;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class PostStatusActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout photo_layout, video_layout, checkin_layout, feelings_layout, tag_friends_post;
    private static final int SELECT_PICTURE = 1;
    ImageView selected_image, image;
    VideoView videoView;
    private static final int PICK_FROM_GALLERY = 1;
    private String selectedImagePath;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String userid = "", user_profile_id = "", membership = "";
    EditText whatsonyoyrmind;
    RecyclerView selected_image_recyclerview;
    ArrayList<SelectedImageModel2> selectedImageModels2 = new ArrayList<SelectedImageModel2>();
    ArrayList<String> postimagesarray = new ArrayList<String>();
    private static final int READ_STORAGE_PERMISSION = 4000;
    private static final int LIMIT = 10;
    SelectedImageAdapter2 selectedImageAdapter2;
    String fileName = "";
    TextView name, statustypee, memberships, feeling;
    String selectCheckInCountry, newlocset;
    Bitmap bitmap = null;
    Uri fileUri;
    ArrayList<SearchingModel> lisssstFriend = new ArrayList<SearchingModel>();
    ArrayList<String> valueTagList = new ArrayList<String>();

    /////
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    ArrayList<String> checkincountryArrayList = new ArrayList<String>();
    ArrayList<CheckInCountriesModel> checkinCountyModelList = new ArrayList<CheckInCountriesModel>();
    PostCheckInCountryAdapter postCheckInCountryAdapter;
    SpannableStringBuilder builder = new SpannableStringBuilder();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTrack locationTrack;
    Geocoder geocoder;
    List<Address> addresses;
    String emoji = "";
    String address = "";
    String privacystatus;
    Bitmap bm1 = null, bm2 = null, bm3 = null, bm4 = null, bm5 = null;
    byte[] data1, data2, data3, data4, data5;
    ByteArrayBody bab, bab1, bab2, bab3, bab4;
    HttpEntity resEntity;
    BottomSheetBehavior behavior;
    int unicode;
    Drawable d;
    String friends_list = "";
    ////////////////////////////////////////////////
    private TextView messageText, noImage;
    private Button uploadButton, btnselectpic;
    private EditText etxtUpload;
    private ProgressDialog progressdialog;
    private JSONObject jsonObject;
    ArrayList<Uri> imagesUriList;
    ArrayList<String> encodedImageList;
    String imageURI;
    String imageSelectedPath;
    GridView PhoneImageGrid;
    EditText edt;
    String edittexturl, namewithloction;
    String video_id;
    String smile_id;
    AlertDialog dialog1;
    ArrayList<String> selectedPath1 = new ArrayList<String>();
    String selectedPath2 = "NONE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_status);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }


        geocoder = new Geocoder(this, Locale.getDefault());
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        membership = userDetails.get(UserSessionManager.USER_MEMBERSHIP);

       /* progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);*/

        photo_layout = (LinearLayout) findViewById(R.id.photo_layout);
        video_layout = (LinearLayout) findViewById(R.id.video_layout);
        checkin_layout = (LinearLayout) findViewById(R.id.checkin_layout);
        feelings_layout = (LinearLayout) findViewById(R.id.feelings_layout);
        tag_friends_post = (LinearLayout) findViewById(R.id.tag_friends_post);
        whatsonyoyrmind = (EditText) findViewById(R.id.whatsonyoyrmind);
        if (getIntent().getExtras().getString("feeds").equals("group")) {
            tag_friends_post.setVisibility(View.GONE);
        }
        name = (TextView) findViewById(R.id.name);
        statustypee = (TextView) findViewById(R.id.statustypee);
        memberships = (TextView) findViewById(R.id.membership);
        feeling = (TextView) findViewById(R.id.feeling);
        memberships.setText(membership);
        //statustypee.setText("public");
        statustypee.setText("friends");
        selected_image = (ImageView) findViewById(R.id.selected_image);
        image = (ImageView) findViewById(R.id.image);
        videoView = (VideoView) findViewById(R.id.videoView);
        View bottomSheet = findViewById(R.id.design_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });
        ////////////////////////////////////////////////////
      /*  dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
*/
        jsonObject = new JSONObject();
        encodedImageList = new ArrayList<>();
        ///////////////////////////////////////////////////
        namewithloction = getIntent().getExtras().getString("name");
        name.setText(getIntent().getExtras().getString("name"));

        Picasso.with(PostStatusActivity.this)
                .load(getIntent().getExtras().getString("image"))
                .into(image);

        selected_image_recyclerview = (RecyclerView) findViewById(R.id.selected_image_recyclerview);
        selected_image_recyclerview.setHasFixedSize(true);

        selected_image_recyclerview.setLayoutManager(new GridLayoutManager(PostStatusActivity.this, 5, GridLayoutManager.VERTICAL, false));
        selected_image_recyclerview.setNestedScrollingEnabled(false);
        selected_image_recyclerview.setSaveFromParentEnabled(true);
        MediaController mediaController = new
                MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        photo_layout.setOnClickListener(this);
        video_layout.setOnClickListener(this);
        checkin_layout.setOnClickListener(this);
        feelings_layout.setOnClickListener(this);
        tag_friends_post.setOnClickListener(this);
        statustypee.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            case R.id.post:
                // API 5+ solution
                //postStatus();
                if (!whatsonyoyrmind.getText().toString().equals("")) {
                    uploadPost();
                } else {
                    Toast.makeText(PostStatusActivity.this, "Please write post Context", Toast.LENGTH_SHORT).show();
                }
                //doFileUpload();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == photo_layout) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Choose application"), AppUrls.REQCODE);


        }
        if (v == video_layout) {
            showChangeLangDialog();
           /* // Creating alert Dialog with one Button
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PostStatusActivity.this);

            //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

            // Setting Dialog Title
            alertDialog.setTitle("Paste URL");

            EditText input = new EditText(this);
            input.setHint("Enter URL");
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            alertDialog.setView(input);

            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                           dialog.dismiss();
                        }
                    });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });

            // closed

            // Showing Alert Message
            alertDialog.show();*/

           /* behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            Intent intent = new Intent();

            intent.setType("video");
            intent.setAction(Intent.ACTION_GET_CONTENT);

            startActivityForResult(Intent.createChooser(intent, "Complete action using"),PICK_FROM_GALLERY);
*/
        }
        if (v == checkin_layout) {
            Toast.makeText(PostStatusActivity.this, "coming soon ...!", Toast.LENGTH_SHORT).show();
            /*checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                checkincountryArrayList.clear();
                Log.d("CHECDINRESP",AppUrls.BASE_URL+AppUrls.COUNTRY_LIST);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL+AppUrls.COUNTRY_LIST, new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObjectdata = jsonObject.getJSONObject("data");
                            JSONArray jsonArray = jsonObjectdata.getJSONArray("countryData");

                            checkinCountyModelList.clear();
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObjectnew = jsonArray.getJSONObject(i);
                                CheckInCountriesModel citiesModel = new CheckInCountriesModel();

                                String country_id = jsonObjectnew.getString("id");
                                String country_name = jsonObjectnew.getString("country_name");
                                String country_iso_code = jsonObjectnew.getString("country_iso_code");

                                citiesModel.setId(country_id);
                                citiesModel.setCountry_name(country_name);
                                citiesModel.setCountry_iso_code(country_iso_code);

                                checkincountryArrayList.add(country_name);
                                checkinCountyModelList.add(citiesModel);
                            }

                            checkInCountryDialog(checkinCountyModelList);

                        } catch (JSONException e)
                        {

                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError)
                        {
                        } else if (error instanceof AuthFailureError)
                        {
                        } else if (error instanceof ServerError)
                        {
                        } else if (error instanceof NetworkError)
                        {
                        } else if (error instanceof ParseError)
                        {
                        }
                    }
                });

                RequestQueue requestQueue = Volley.newRequestQueue(PostStatusActivity.this);
                requestQueue.add(stringRequest);
            }
            else
            {

                Toast.makeText(PostStatusActivity.this, "No Internet Connection....", Toast.LENGTH_SHORT).show();
            }*/

           /* behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            locationTrack = new LocationTrack(PostStatusActivity.this);


            if (locationTrack.canGetLocation())
            {


                double longitude = locationTrack.getLongitude();
                double latitude = locationTrack.getLatitude();
                try {
                    getAddress(latitude,longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }
               *//* try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    address = addresses.get(0).getAddressLine(0)+","+addresses.get(0).getAddressLine(1);

                    whatsonyoyrmind.append(Html.fromHtml("at "+"<b>"+address+"</b>"));
                    name.append(Html.fromHtml(" at "+"<b>"+address+"</b>"));
                } catch (IOException e) {
                    e.printStackTrace();
                }*//*
                Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();
            } else {

                locationTrack.showSettingsAlert();
            }*/
        }
        if (v == feelings_layout) {
           /* behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            whatsonyoyrmind.setText("");
            feeling.setText("");
            Intent intent = new Intent(PostStatusActivity .this,EmojisActivity.class);
            startActivityForResult(intent, 1);*/
            Toast.makeText(PostStatusActivity.this, "coming soon ...!", Toast.LENGTH_SHORT).show();
        }
        if (v == statustypee) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Choose Privacy");
            builder.setItems(new CharSequence[]
                            {"Public", "Friends", "Private"},
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            switch (which) {
                                case 0:
                                    statustypee.setText("public");
                                    break;
                                case 1:
                                    statustypee.setText("friends");
                                    break;
                                case 2:
                                    statustypee.setText("private");
                                    break;

                            }
                        }
                    });
            builder.create().show();


        }

        if (v == tag_friends_post) {
            Toast.makeText(PostStatusActivity.this, "coming soon ...!", Toast.LENGTH_SHORT).show();
             /*final Dialog tagDialog = new Dialog(PostStatusActivity.this, R.style.MaterialDialogSheet);
             tagDialog.setContentView(R.layout.tag_layout_custom_dialog); // your custom view.
             tagDialog.setCancelable(true);
             tagDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
             tagDialog.getWindow().setGravity(Gravity.CENTER);
             tagDialog.show();

             Button btn_tag_add = (Button) tagDialog.findViewById(R.id.btn_tag_add);
             Button btn_tag_cancel = (Button) tagDialog.findViewById(R.id.btn_tag_cancel);
            final   MultiAutoCompleteTextView autoComplete_tag_friends = (MultiAutoCompleteTextView) tagDialog.findViewById(R.id.autoComplete_tag_friends);
             MultiCompletTagFriendAdapter adapter = new MultiCompletTagFriendAdapter(PostStatusActivity.this,R.layout.row_multiselect_members,lisssstFriend);
             autoComplete_tag_friends.setThreshold(0);
             autoComplete_tag_friends.setAdapter(adapter);
             autoComplete_tag_friends.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

             btn_tag_add.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v)
                 {
                     String val_tag=autoComplete_tag_friends.getText().toString();
                     valueTagList.add(val_tag);
                     String va=valueTagList.toString();

                     String strWithoutSpace = va.replaceAll(" ","");

                     int mem=strWithoutSpace.lastIndexOf(",");
                     if(mem>0)
                         strWithoutSpace=new StringBuilder(strWithoutSpace).replace(mem,mem+1,"").toString();

                     whatsonyoyrmind.setText("with "+removeCharacter(strWithoutSpace));
                     friends_list = removeCharacter(strWithoutSpace);
                     tagDialog.dismiss();

                 }
             });

             btn_tag_cancel.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v)
                 {


                     tagDialog.dismiss();
                  }
             });

             tagDialog.show();
*/


        }

    }

    private void galleryIntent() {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                if (requestCode == AppUrls.REQCODE && null != data) {


                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    imagesUriList = new ArrayList<Uri>();


                    encodedImageList.clear();
                    if (data.getData() != null) {

                        Uri uri = data.getData();
                        imagesUriList.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageURI = cursor.getString(columnIndex);

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                        encodedImageList.add(encodedImage);
                        cursor.close();
                        SelectedImageModel2 sim = new SelectedImageModel2();

                        fileName = uri.toString();
                        sim.setImage_uri(fileName);
                        selectedImageModels2.add(sim);
                        Log.d("IMAGESINGLE:", selectedImageModels2.toString());
                        Log.d("FILENAMESINGLE:", fileName);


                        selectedImageAdapter2 = new SelectedImageAdapter2(selectedImageModels2, PostStatusActivity.this, R.layout.row_selected_image2);
                        selected_image_recyclerview.setAdapter(selectedImageAdapter2);

                   /* Uri mImageUri=data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri, filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageURI  = cursor.getString(columnIndex);



                    cursor.close();*/

                    } else {
                        if (data.getClipData() != null) {
                            video_layout.setVisibility(View.GONE);
                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);
                                // Get the cursor
                                Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                                // Move to first row
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                imageURI = cursor.getString(columnIndex);

                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                                encodedImageList.add(encodedImage);
                                cursor.close();
                                SelectedImageModel2 sim = new SelectedImageModel2();

                                fileName = uri.toString();
                                sim.setImage_uri(fileName);
                                selectedImageModels2.add(sim);
                                Log.d("IMAGEMULTIPLE:", selectedImageModels2.toString());
                                Log.d("FILENAMULTIPLE:", fileName);


                                selectedImageAdapter2 = new SelectedImageAdapter2(selectedImageModels2, PostStatusActivity.this, R.layout.row_selected_image2);
                                selected_image_recyclerview.setAdapter(selectedImageAdapter2);
                                // noImage.setText("Selected Images: " + mArrayUri.size());
                            }

                        }
                    }

                } else {
                    Toast.makeText(this, "",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == PICK_FROM_GALLERY) {
            Uri mVideoURI = data.getData();
            videoView.setVideoURI(mVideoURI);
            videoView.start();

        }
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                String result1 = data.getStringExtra("result1");
                smile_id = result;
                whatsonyoyrmind.append(smile_id);
                Log.d("sdmn", result1);
                try {
                    unicode = Integer.parseInt(result1);
                } catch (NumberFormatException e) {
                    // do something about it
                }

                SpannableString ss = new SpannableString("    feeling ");
                if (result.equals(":blushing-angel:")) {


                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.angel);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "angel" + "</b> "));


                } else if (result.equals("o.O")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.confused);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "confused" + "</b> "));

                } else if (result.equals("B|")) {


                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.cool);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "cool" + "</b> "));

                } else if (result.equals(":cry:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.cry);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "cry" + "</b> "));

                } else if (result.equals(":tea-cup:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.cup_of_tea);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "cup of tea" + "</b> "));

                } else if (result.equals(":laughing-devil:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.devil);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "devil" + "</b> "));

                } else if (result.equals(":(")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.frown);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "cry" + "</b> "));

                } else if (result.equals("B)")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.glasses);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "glasses" + "</b> "));
                } else if (result.equals(":lve:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.heart);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "love" + "</b> "));

                } else if (result.equals("^_^")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.kiki);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "hehe" + "</b> "));

                } else if (result.equals(":kiss:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.kiss);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "kiss" + "</b> "));

                } else if (result.equals(":v")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.pacman);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "packman" + "</b> "));

                } else if (result.equals(":(")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.sadj);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "sad" + "</b> "));

                } else if (result.equals(":)")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.shock);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "shock" + "</b> "));

                } else if (result.equals(":unsure:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.unsure);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "unsure" + "</b> "));

                } else if (result.equals(":grumpy:")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.grumpy);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "grumpy" + "</b> "));

                } else if (result.equals("(wink)")) {

                    Drawable img = PostStatusActivity.this.getResources().getDrawable(R.drawable.wink);
                    img.setBounds(0, 0, 60, 60);
                    feeling.setCompoundDrawables(img, null, null, null);
                    feeling.append(Html.fromHtml(" feeling " + "<b>" + "wink" + "</b> "));

                }

               /*     if (result.equals(":blushing-angel:")) {
                        d = getResources().getDrawable(R.drawable.angel);
                    }
                    else if (result.equals("o.O")){
                        d = getResources().getDrawable(R.drawable.confused);
                    } else if (result.equals("B|")){
                        d = getResources().getDrawable(R.drawable.cool);
                    } else if (result.equals(":cry:")){
                        d = getResources().getDrawable(R.drawable.cry);
                    } else if (result.equals(":tea-cup:")){
                        d = getResources().getDrawable(R.drawable.cup_of_tea);
                    } else if (result.equals(":laughing-devil:")){
                        d = getResources().getDrawable(R.drawable.devil);
                    } else if (result.equals(":(")){
                        d = getResources().getDrawable(R.drawable.frown);
                    } else if (result.equals("B)")){
                        d = getResources().getDrawable(R.drawable.glasses);
                    } else if (result.equals(":lve:")){
                        d = getResources().getDrawable(R.drawable.heart);
                    }else if (result.equals("^_^")){
                        d = getResources().getDrawable(R.drawable.kiki);
                    }else if (result.equals(":kiss:")){
                        d = getResources().getDrawable(R.drawable.kiss);
                    }else if (result.equals(":v")){
                        d = getResources().getDrawable(R.drawable.pacman);
                    }else if (result.equals(":(")){
                        d = getResources().getDrawable(R.drawable.sadj);
                    }else if (result.equals(":)")){
                        d = getResources().getDrawable(R.drawable.shock);
                    }else if (result.equals(":unsure:")){
                        d = getResources().getDrawable(R.drawable.unsure);
                    }else if (result.equals(":grumpy:")){
                        d = getResources().getDrawable(R.drawable.grumpy);
                    }else if (result.equals("(wink)")){
                        d = getResources().getDrawable(R.drawable.wink);
                    }
                    if (!result.equals("")&&!result1.equals("")) {
                        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                        ss.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    *//*String emoji1 = getEmojiByUnicode(unicode);
                    emoji = result;*//*
                        // whatsonyoyrmind.append(ss);
                        name.append(" "+ss+" ");*/
                    /*}*/
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            }
        }

    }

    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        // this is our fallback here
        return uri.getPath();
    }

    private void postStatus() {


        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.POST_USER_FEED;
            Log.d("SENDURLRESPONCE", url);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("SENDURLRESPONCE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("URLRESPONCE", response);

                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    // progressDialog.dismiss();
                                    //  Toast.makeText(PostStatusActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                //  progressDialog.dismiss();


                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //  progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                   /* @Override
                    public Map<String, String> get() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();

                        params.put("purpose_donation", purpusofdonation.getText().toString());
                        params.put("name", donorname.getText().toString());
                        params.put("remarks", remarks.getText().toString());
                        params.put("location", location.getText().toString());
                        params.put("amount", amount.getText().toString());
                        Log.d("SENDURLRESPONCE", "SENDDONATION " + params.toString());
                        return params;
                    }*/


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("username", user_profile_id);
                    params.put("page_owner", userid);
                    params.put("page_id", getIntent().getExtras().getString("feeds"));
                    params.put("post", whatsonyoyrmind.getText().toString());
                    params.put("security", statustypee.getText().toString());
                    params.put("photo_link1", String.valueOf(bab));
                    params.put("photo_link2", String.valueOf(bab1));
                    params.put("photo_link3", String.valueOf(bab2));
                    params.put("photo_link4", String.valueOf(bab3));
                    params.put("photo_link5", String.valueOf(bab4));
                    params.put("photo_identifier", getIntent().getExtras().getString("photo_identifier"));
                    params.put("video_id", "");
                    params.put("url", "");
                    params.put("type", "");
                    params.put("title", "");
                    params.put("description", "");
                    params.put("keywords", "");
                    params.put("video_identifier", getIntent().getExtras().getString("photo_identifier"));
                    params.put("location", address);
                    params.put("friends", friends_list);
                    Log.d("SENDURLRESPONCE", "SENDDONATION " + params.toString());
                    return null;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(PostStatusActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(PostStatusActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }


    }

    public void reloadeSelectedImage2() {

        selectedImageAdapter2.notifyDataSetChanged();
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(Object permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(String.valueOf(permission)) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(String.valueOf(permissionsRejected.get(0)))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(PostStatusActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener();
    }*/

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }


    public void uploadPost() {
        checkInternet = NetworkChecking.isConnected(this);

        if (checkInternet) {
//        dialog.show();


            JSONArray jsonArray = new JSONArray();


            for (String encoded : encodedImageList) {
                jsonArray.put(encoded);
            }
            Log.d("SENDDDDDDDEDDDD", jsonArray.toString());

            try {
                jsonObject.put(AppUrls.imageName, "uploadimages");
                jsonObject.put("username", user_profile_id);
                jsonObject.put("page_owner", getIntent().getExtras().getString("page_owner"));
                jsonObject.put("page_id", getIntent().getExtras().getString("feeds"));
                jsonObject.put("post", whatsonyoyrmind.getText().toString());
                jsonObject.put("security", statustypee.getText().toString());
                jsonObject.put("photo_identifier", getIntent().getExtras().getString("photo_identifier"));
                jsonObject.put(AppUrls.imageList, jsonArray);
                jsonObject.put("video_id", video_id);
                jsonObject.put("url", edittexturl);
                jsonObject.put("type", "youtube");
                jsonObject.put("title", "");
                jsonObject.put("description", "");
                jsonObject.put("keywords", "");
                jsonObject.put("video_identifier", getIntent().getExtras().getString("photo_identifier"));
                jsonObject.put("location", address);
                jsonObject.put("friends", "");
                Log.d("SENDURLRESPONCE", "SENDDONATION " + jsonObject.toString());
            } catch (JSONException e) {
                Log.e("JSONObject Here", e.toString());

            }

            String url = AppUrls.BASE_URL + AppUrls.POST_USER_FEED;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.d("MessagefromserverRESP", jsonObject.toString());
                            //  dialog1.dismiss();
                            //  messageText.setText(""+jsonObject);

                            //   Toast.makeText(getApplication(), ""+jsonObject, Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d("MessagefromserverERROR", volleyError.toString());
                    Toast.makeText(getApplication(), "Error Occurred", Toast.LENGTH_SHORT).show();
                    // dialog1.dismiss();
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(200 * 30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(jsonObjectRequest);
        } else {
            Toast.makeText(PostStatusActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    public void showChangeLangDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        edt = (EditText) dialogView.findViewById(R.id.edit1);
        final Button fetch = (Button) dialogView.findViewById(R.id.fetch);
        final Button close = (Button) dialogView.findViewById(R.id.cancel);
        final WebView browser = (WebView) dialogView.findViewById(R.id.browser);


        edittexturl = edt.getText().toString();


        // dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter Video URL");
       /* dialogBuilder.setPositiveButton("Fetch", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });*/
        final AlertDialog b = dialogBuilder.create();


        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edt.getText().toString().equals("")) {
                    whatsonyoyrmind.append(edt.getText().toString());


                    WebSettings webSettings = browser.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webSettings.setAllowFileAccess(true);
                    if (Build.VERSION.SDK_INT > 7) {
                        webSettings.setPluginState(WebSettings.PluginState.ON);
                    } else {
                        //   webSettings.setPluginsEnabled(true);
                    }
                    String frameVideo = "<html><body>Youtube video .. <br> <iframe width=\"320\" height=\"315\" src=\"" + edt.getText().toString().trim() + "\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

                    browser.loadData(frameVideo, "text/html", "utf-8");

                    browser.loadUrl(edt.getText().toString().trim());


                    browser.setWebViewClient(new WebViewClient());
                    String[] bits = edt.getText().toString().trim().split("/");
                    String lastOne = bits[bits.length - 1];
                    Log.d("sdjbvgjk", lastOne);
                    video_id = lastOne;
                    fetch.setText("Set");
                    if (fetch.getText().toString().equals("Set")) {
                        fetch.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                edittexturl = edt.getText().toString();
                                b.dismiss();
                                fetch.setText("Fetch");
                            }
                        });
                    }

                    getTitleQuietly(edt.getText().toString());
                    b.show();
                    photo_layout.setVisibility(View.GONE);
                } else {
                    b.show();
                    Toast.makeText(PostStatusActivity.this, "Please Submit URL", Toast.LENGTH_SHORT).show();
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                edt.setText("");
                photo_layout.setVisibility(View.VISIBLE);
                video_id = "";
                edittexturl = "";
            }
        });
        b.show();
    }

    public void getAddress(double lat, double lng) throws IOException {
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            address = addresses.get(0).getAddressLine(0) + "," + addresses.get(0).getAddressLine(1);

            //      whatsonyoyrmind.append(Html.fromHtml("at "+"<b>"+address+"</b>"));
            name.append(Html.fromHtml(" at " + "<b>" + address + "</b>"));
        } catch (IOException e) {
            e.printStackTrace();
        }
       /* try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.v("IGA", "Address" + add);
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();

            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }*/
    }

    public static String getTitleQuietly(String youtubeUrl) {
        String json;
        String titile = null;

        HttpClient Client = new DefaultHttpClient();

        try {

            HttpGet httpget = new HttpGet("http://gdata.youtube.com/feeds/api/videos/" + youtubeUrl + "?v=2&alt=jsonc");
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            json = Client.execute(httpget, responseHandler);

            JSONObject object = new JSONObject(json);

            JSONObject obj = object.getJSONObject("data");
            Log.d("sjdfhasduhfasdjg", "" + obj);
            titile = obj.getString("title");


        } catch (ClientProtocolException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return titile;
    }

    private void checkInCountryDialog(ArrayList<CheckInCountriesModel> checkinCountyModelList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.custom_dialog_layout_checkin_country, null);

        RecyclerView checkin_country_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.checkin_country_recycler_view);

        final SearchView checkin_country_search = (SearchView) dialog_layout.findViewById(R.id.checkin_country_search);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(PostStatusActivity.this);
        checkin_country_recycler_view.setLayoutManager(layoutManager);

        postCheckInCountryAdapter = new PostCheckInCountryAdapter(checkinCountyModelList, PostStatusActivity.this, R.layout.row_checkin_country);

        checkin_country_recycler_view.setAdapter(postCheckInCountryAdapter);

        checkin_country_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkin_country_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //  progressDialog.dismiss();
                // dialogInterface.dismiss();
            }
        });
        dialog1 = builder.create();

        checkin_country_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                postCheckInCountryAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog1.show();
    }

    public void refreshMyList(String selectedCountry, String selectedCountry_id) {

        dialog1.dismiss();
        selectCheckInCountry = selectedCountry;
        whatsonyoyrmind.setText(selectedCountry);
        newlocset = "   at " + selectCheckInCountry;


        SpannableString spannable = new SpannableString(namewithloction + newlocset);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ContextCompat.getColor(getApplicationContext(), R.color.locat_color));
            }

            @Override
            public void onClick(View widget) {
                //Do your click action
            }
        };
        spannable.setSpan(clickableSpan, namewithloction.length(), namewithloction.length() + newlocset.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        name.setText(spannable);
        //  name.setMovementMethod(LinkMovementMethod.getInstance());
        name.setHighlightColor(ContextCompat.getColor(getApplicationContext(), R.color.locat_color));

        //  name.setText(namewithloction +newlocset);


        postCheckInCountryAdapter.notifyDataSetChanged();
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private String removeCharacter(String word) {
        String[] specialCharacters = {"-", "[", "]", "}", ".", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        StringBuilder sb = new StringBuilder(word);
        for (int i = 0; i < sb.toString().length() - 1; i++) {
            for (String specialChar : specialCharacters) {
                if (sb.toString().contains(specialChar)) {
                    int index = sb.indexOf(specialChar);
                    sb.deleteCharAt(index);
                }
            }
        }
        return sb.toString();
    }
}
