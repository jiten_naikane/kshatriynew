package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.kshatriyayouth.R;

public class EmojisActivity extends AppCompatActivity {

    String[] iconTitles = new String[]{
            "Blushing Angel",
            "Confused",
            "Cool",
            "Cry",
            "Cup Of Tea",
            "Laughing Devil",
            "Frown",
            "Glasses",
            "Love",
            "Kekeke Happy",
            "kiss",
            "Pacman",
            "Sad",
            "Shock & Surprised",
            "Smile",
            "Unsure",
            "Upset & Angry",
            "Wink"
    };

    String[] toastText = new String[]{
            ":blushing-angel:",
            "o.O",
            "B|",
            ":cry:",
            ":tea-cup:",
            ":laughing-devil:",
            ":(",
            "B)",
            ":lve:",
            "^_^",
            ":kiss:",
            ":v",
            ":(",
            ":O",
            ":)",
            ":unsure:",
            ":grumpy:",
            "(wink)"
    };

    String[] emoji_identifier = new String[]{
            "0x1F47C",
            "0x1F615",
            "0x1F646",
            "0x1F622",
            "0x1F375",
            "0x1F638",
            "0x1F64D",
            "0x1F60E",
            "0x1F493",
            "0x1F64B",
            "0x1F618",
            "0x1F645",
            "0x1F61E",
            "0x1F633",
            "0x1F604",
            "0x1F60F",
            "0x1F620",
            "0x1F609"
    };
    int[] icons = new int[]{
            R.drawable.angel,
            R.drawable.confused,
            R.drawable.cool,
            R.drawable.cry,
            R.drawable.cup_of_tea,
            R.drawable.devil,
            R.drawable.frown,
            R.drawable.glasses,
            R.drawable.heart,
            R.drawable.kiki,
            R.drawable.kiss,
            R.drawable.pacman,
            R.drawable.sadj,
            R.drawable.shock,
            R.drawable.smilea,
            R.drawable.unsure,
            R.drawable.upset,
            R.drawable.wink

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emojis);
        final List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < iconTitles.length; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txts", iconTitles[i]);
            hm.put("imgs", Integer.toString(icons[i]));
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = {"imgs", "txts"};
        // Ids of views in listview_layout
        int[] to = {R.id.imgs, R.id.txts};
        // R.layout.listview_layout defines the layout of each item
        final GridView listView = (GridView) findViewById(R.id.listview);
        // Instantiating an adapter to store each items
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.emojis_row, from, to);
        // Setting the adapter to the listView
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String tT = toastText[position];
                String tT_icon = emoji_identifier[position];
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", tT);
                returnIntent.putExtra("result1", tT_icon);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                Toast.makeText(EmojisActivity.this, "" + tT_icon, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "");
        returnIntent.putExtra("result1", "");
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}