package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SuggestedGroupViewPagerAdapter;
import in.innasoft.kshatriyayouth.fragments.MyGroupsFragment;
import in.innasoft.kshatriyayouth.fragments.SuggestGroupFragment;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class ProfileGroupsActivity extends AppCompatActivity {

    UserSessionManager session;
    //Typeface typeface;
    ImageView close, create_group_plus;
    ProgressDialog progressDialog;
    String userid, user_profile_id;
    ViewPager view_pager_suggest;
    TabLayout tabLayout_suggestgrp;
    SuggestedGroupViewPagerAdapter adapter;
    Toolbar toolbar_tab_lyout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_groups);

        toolbar_tab_lyout = (Toolbar) findViewById(R.id.toolbar_tab_lyout);
        toolbar_tab_lyout.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        toolbar_tab_lyout.setTitleTextColor(Color.parseColor("#FFFFFF"));
        toolbar_tab_lyout.setTitle("Groups");
        setSupportActionBar(toolbar_tab_lyout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);


        view_pager_suggest = (ViewPager) findViewById(R.id.view_pager_suggest);
        setupViewPager(view_pager_suggest);

        tabLayout_suggestgrp = (TabLayout) findViewById(R.id.tabLayout_suggestgrp);
        tabLayout_suggestgrp.setupWithViewPager(view_pager_suggest);

    }

    private void setupViewPager(ViewPager view_pager_suggest) {
        adapter = new SuggestedGroupViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new MyGroupsFragment(), "My Groups");
        adapter.addFrag(new SuggestGroupFragment(), "Suggested Groups");
        view_pager_suggest.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            case R.id.create_group_plus:

                Intent craetefroup = new Intent(ProfileGroupsActivity.this, GroupCreateActivityDialog.class);
                craetefroup.putExtra("Profile", "ditprofile");

                startActivity(craetefroup);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
  /*  @Override
    public void onResume()
    {
        super.onResume();
        getGroupList();
    }*/


}
