package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.EditProfileCityAdapter;
import in.innasoft.kshatriyayouth.adapters.EditProfileCountryAdapter;
import in.innasoft.kshatriyayouth.adapters.EditProfileStateAdapter;
import in.innasoft.kshatriyayouth.adapters.EditProfileSurnameListAdapter;
import in.innasoft.kshatriyayouth.models.CityModel;
import in.innasoft.kshatriyayouth.models.CountryModel;
import in.innasoft.kshatriyayouth.models.StateModel;
import in.innasoft.kshatriyayouth.models.SurnameListModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FilePath;
import in.innasoft.kshatriyayouth.utilities.ImagePermissions;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextView toolbar_title, save_text, username_header, surname_txt, surname_header, mobile_header, email_header, dob_header, dob_txt, blood_header, membership_header, adhaar_header, gender_header,
            relationship_status_header, relationship_status_txt, who_r_u_header, country_header, country_txt, state_header, state_txt, city_header, city_txt, current_city_header,
            address_header, intrested_header, fav_quote_header, current_work_header, current_position_header, prof_skill_header, highschool_header,
            highschool_strt_date, highschool_end_date, field_study_header, college_naem_header, college_strt_date, college_end_date, language_header, language_txt, relegious_header,
            political_header, intrested_in_text, blood_group_txt, nodata_text, mobile_privcy_txt, mobile_privcy_header;

    EditText username_edt_txt, mobile_edt_txt, email_edt_txt, adhaar_edt_txt, who_r_u_edt_text, current_city_edt_text, address_edt_text,
            fav_quote_edt_text, current_work_edt_text, current_position_edt_text, prof_skill_edt_text, highschool_edt_text, field_study_edt_text,
            colleg_name_edt_text, relegious_edt_text, political_edt_text;
    ImageView close, editpen_pic_click, user_profile_img;
    RadioGroup gender_group, membership_group;
    RadioButton business_radio, association_radio, normal_radio, male_radio, female_radio, genderStatus, memberstatus;
    int gunder_status = 0;
    int member_init_status = 0;
    UserSessionManager session;
    ////    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager userSessionManager;
    String userid, user_profile_id, profile_pic_url;
    private String selectedFilePath;
    private int mYear, mMonth, mDay;
    private int birth_Year, birth_Day, birth_Month;
    String send_biirt_month, send_birth_yr, send_birth_day, sendgender, sendmember, send_intrested_index, mobile_privacy;
    int send_mobilePrivacy_vlue;
    LinearLayout mobile_ll;

    AlertDialog dialog;
    EditProfileSurnameListAdapter surnameEditProfileListAdapter;
    ArrayList<SurnameListModel> surnameModels = new ArrayList<SurnameListModel>();
    ArrayList<String> surname_list = new ArrayList<String>();

    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    private Uri fileUri;
    HttpEntity resEntity;

    String send_country_id = "", send_state_id = "", send_city_id = "";
    int state_check = 0, city_check = 0;

    /*Countrys List*/
    EditProfileCountryAdapter countryAdapter;
    ArrayList<CountryModel> countryModelArrayList = new ArrayList<CountryModel>();
    ArrayList<String> countryList = new ArrayList<String>();

    /*State List*/
    EditProfileStateAdapter stateAdapter;
    ArrayList<StateModel> stateModels = new ArrayList<StateModel>();
    ArrayList<String> stateList = new ArrayList<String>();

    /*City List*/
    EditProfileCityAdapter cityAdapter;
    ArrayList<CityModel> cityModels = new ArrayList<CityModel>();
    ArrayList<String> cityList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

////        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
////        toolbar_title.setTypeface(typeface);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        profile_pic_url = userDetails.get(UserSessionManager.PROFILE_PIC_URL);


        Log.d("IMAGEURL", profile_pic_url);
        user_profile_img = (ImageView) findViewById(R.id.user_profile_img);
        if (!profile_pic_url.equals("") || !profile_pic_url.equals(null)) {
            Picasso.with(EditProfileActivity.this).load(profile_pic_url).placeholder(R.drawable.user_profile).fit().into(user_profile_img);
        } else {
            // Toast.makeText(this, "No Profile Image Found..!", Toast.LENGTH_SHORT).show();
        }

        mobile_ll = (LinearLayout) findViewById(R.id.mobile_ll);

        // userSessionManager = new UserSessionManager(EditProfileActivity.this);

        username_header = (TextView) findViewById(R.id.username_header);
////        username_header.setTypeface(typeface);

        mobile_header = (TextView) findViewById(R.id.mobile_header);
////        mobile_header.setTypeface(typeface);

        blood_header = (TextView) findViewById(R.id.blood_header);
////        blood_header.setTypeface(typeface);


        email_header = (TextView) findViewById(R.id.email_header);
////        email_header.setTypeface(typeface);

        surname_txt = (TextView) findViewById(R.id.surname_txt);
////        surname_txt.setTypeface(typeface);
        //surname_txt.setOnClickListener(this);

        surname_header = (TextView) findViewById(R.id.surname_header);
////        surname_header.setTypeface(typeface);


        dob_header = (TextView) findViewById(R.id.dob_header);
////        dob_header.setTypeface(typeface);

        membership_header = (TextView) findViewById(R.id.membership_header);
////        membership_header.setTypeface(typeface);

        gender_header = (TextView) findViewById(R.id.gender_header);
////        gender_header.setTypeface(typeface);

        adhaar_header = (TextView) findViewById(R.id.adhaar_header);
////        adhaar_header.setTypeface(typeface);

        dob_txt = (TextView) findViewById(R.id.dob_txt);
////        dob_txt.setTypeface(typeface);
        dob_txt.setOnClickListener(this);

        gender_group = (RadioGroup) findViewById(R.id.gender_group);
        membership_group = (RadioGroup) findViewById(R.id.membership_group);

        male_radio = (RadioButton) findViewById(R.id.male_radio);
////        male_radio.setTypeface(typeface);

        female_radio = (RadioButton) findViewById(R.id.female_radio);
////        female_radio.setTypeface(typeface);

        business_radio = (RadioButton) findViewById(R.id.business_radio);
////        business_radio.setTypeface(typeface);

        association_radio = (RadioButton) findViewById(R.id.association_radio);
////        association_radio.setTypeface(typeface);

        normal_radio = (RadioButton) findViewById(R.id.normal_radio);
////        normal_radio.setTypeface(typeface);


        username_edt_txt = (EditText) findViewById(R.id.username_edt_txt);
        username_edt_txt.setSelection(username_edt_txt.getText().length());
////       username_edt_txt.setTypeface(typeface);

        mobile_edt_txt = (EditText) findViewById(R.id.mobile_edt_txt);
////        mobile_edt_txt.setTypeface(typeface);

        email_edt_txt = (EditText) findViewById(R.id.email_edt_txt);
////        email_edt_txt.setTypeface(typeface);

        adhaar_edt_txt = (EditText) findViewById(R.id.adhaar_edt_txt);
////        adhaar_edt_txt.setTypeface(typeface);

        blood_group_txt = (TextView) findViewById(R.id.blood_group_txt);
////        blood_group_txt.setTypeface(typeface);
        blood_group_txt.setOnClickListener(this);


        mobile_privcy_txt = (TextView) findViewById(R.id.mobile_privcy_txt);
        mobile_privcy_txt.setOnClickListener(this);

        mobile_privcy_header = (TextView) findViewById(R.id.mobile_privcy_header);


        male_radio = (RadioButton) findViewById(R.id.male_radio);
////        male_radio.setTypeface(typeface);

        female_radio = (RadioButton) findViewById(R.id.female_radio);
////        female_radio.setTypeface(typeface);

        business_radio = (RadioButton) findViewById(R.id.business_radio);
////        business_radio.setTypeface(typeface);

        association_radio = (RadioButton) findViewById(R.id.association_radio);
////        association_radio.setTypeface(typeface);

        normal_radio = (RadioButton) findViewById(R.id.normal_radio);
////        normal_radio.setTypeface(typeface);

        relationship_status_header = (TextView) findViewById(R.id.relationship_status_header);
////        relationship_status_header.setTypeface(typeface);

        relationship_status_txt = (TextView) findViewById(R.id.relationship_status_txt);
////        relationship_status_txt.setTypeface(typeface);
        relationship_status_txt.setOnClickListener(this);

        who_r_u_header = (TextView) findViewById(R.id.who_r_u_header);
////        who_r_u_header.setTypeface(typeface);

        country_header = (TextView) findViewById(R.id.country_header);
////        country_header.setTypeface(typeface);

        country_txt = (TextView) findViewById(R.id.country_txt);
////        country_txt.setTypeface(typeface);
        country_txt.setOnClickListener(this);

        state_header = (TextView) findViewById(R.id.state_header);
//        state_header.setTypeface(typeface);

        state_txt = (TextView) findViewById(R.id.state_txt);
//        state_txt.setTypeface(typeface);
        state_txt.setOnClickListener(this);

        country_header = (TextView) findViewById(R.id.country_header);
//        country_header.setTypeface(typeface);

        city_header = (TextView) findViewById(R.id.city_header);
//        city_header.setTypeface(typeface);

        city_txt = (TextView) findViewById(R.id.city_txt);
//        city_txt.setTypeface(typeface);
        city_txt.setOnClickListener(this);

        current_city_header = (TextView) findViewById(R.id.current_city_header);
//        current_city_header.setTypeface(typeface);

        // nodata_text=(TextView)findViewById(R.id.nodata_text);
//       // nodata_text.setTypeface(typeface);

        address_header = (TextView) findViewById(R.id.address_header);
//        address_header.setTypeface(typeface);

        intrested_header = (TextView) findViewById(R.id.intrested_header);
//        intrested_header.setTypeface(typeface);

        fav_quote_header = (TextView) findViewById(R.id.fav_quote_header);
//        fav_quote_header.setTypeface(typeface);

        current_work_header = (TextView) findViewById(R.id.current_work_header);
//        current_work_header.setTypeface(typeface);

        current_position_header = (TextView) findViewById(R.id.current_position_header);
//        current_position_header.setTypeface(typeface);

        prof_skill_header = (TextView) findViewById(R.id.prof_skill_header);
//        prof_skill_header.setTypeface(typeface);

        highschool_header = (TextView) findViewById(R.id.highschool_header);
//        highschool_header.setTypeface(typeface);

        highschool_strt_date = (TextView) findViewById(R.id.highschool_strt_date);
//        highschool_strt_date.setTypeface(typeface);
        highschool_strt_date.setOnClickListener(this);


        highschool_end_date = (TextView) findViewById(R.id.highschool_end_date);
//        highschool_end_date.setTypeface(typeface);
        highschool_end_date.setOnClickListener(this);

        field_study_header = (TextView) findViewById(R.id.field_study_header);
//        field_study_header.setTypeface(typeface);

        college_naem_header = (TextView) findViewById(R.id.college_naem_header);
//        college_naem_header.setTypeface(typeface);

        college_strt_date = (TextView) findViewById(R.id.college_strt_date);
//        college_strt_date.setTypeface(typeface);
        college_strt_date.setOnClickListener(this);

        college_end_date = (TextView) findViewById(R.id.college_end_date);
//        college_end_date.setTypeface(typeface);
        college_end_date.setOnClickListener(this);

        language_header = (TextView) findViewById(R.id.language_header);
//        language_header.setTypeface(typeface);


        language_txt = (TextView) findViewById(R.id.language_txt);
//        language_txt.setTypeface(typeface);
        language_txt.setOnClickListener(this);

        relegious_header = (TextView) findViewById(R.id.relegious_header);
//        relegious_header.setTypeface(typeface);

        political_header = (TextView) findViewById(R.id.political_header);
//        political_header.setTypeface(typeface);

        who_r_u_edt_text = (EditText) findViewById(R.id.who_r_u_edt_text);
//        who_r_u_edt_text.setTypeface(typeface);

        current_city_edt_text = (EditText) findViewById(R.id.current_city_edt_text);
//        current_city_edt_text.setTypeface(typeface);

        address_edt_text = (EditText) findViewById(R.id.address_edt_text);
//        address_edt_text.setTypeface(typeface);

        intrested_in_text = (TextView) findViewById(R.id.intrested_in_text);
//        intrested_in_text.setTypeface(typeface);
        intrested_in_text.setOnClickListener(this);

        fav_quote_edt_text = (EditText) findViewById(R.id.fav_quote_edt_text);
//        fav_quote_edt_text.setTypeface(typeface);

        current_work_edt_text = (EditText) findViewById(R.id.current_work_edt_text);
//        current_work_edt_text.setTypeface(typeface);

        current_position_edt_text = (EditText) findViewById(R.id.current_position_edt_text);
//        current_position_edt_text.setTypeface(typeface);

        prof_skill_edt_text = (EditText) findViewById(R.id.prof_skill_edt_text);
//        prof_skill_edt_text.setTypeface(typeface);

        highschool_edt_text = (EditText) findViewById(R.id.highschool_edt_text);
//        highschool_edt_text.setTypeface(typeface);

        field_study_edt_text = (EditText) findViewById(R.id.field_study_edt_text);
//        field_study_edt_text.setTypeface(typeface);

        colleg_name_edt_text = (EditText) findViewById(R.id.colleg_name_edt_text);
//        colleg_name_edt_text.setTypeface(typeface);

        relegious_edt_text = (EditText) findViewById(R.id.relegious_edt_text);
//        relegious_edt_text.setTypeface(typeface);

        political_edt_text = (EditText) findViewById(R.id.political_edt_text);
//        political_edt_text.setTypeface(typeface);


        save_text = (TextView) findViewById(R.id.save_text);
//        save_text.setTypeface(typeface);
        save_text.setOnClickListener(this);

        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        editpen_pic_click = (ImageView) findViewById(R.id.editpen_pic_click);
        editpen_pic_click.setOnClickListener(this);

        user_profile_img = (ImageView) findViewById(R.id.user_profile_img);

        getProfileData();

    }

    private void getProfileData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            Log.d("URLqqqqqq:", AppUrls.BASE_URL + AppUrls.ABOUT_USER + "/" + user_profile_id);
            StringRequest strEditProfile = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.ABOUT_USER + "/" + user_profile_id, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    progressDialog.dismiss();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("USERDetail", response);
                        String editSuccessResponceCode = jsonObject.getString("status");

                        if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                            JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                            JSONObject jsonObjectProfile1 = jsonObjectData.getJSONObject("getProfile1");

                            Log.d("PROFILE1:", jsonObjectProfile1.toString());

                            String user_name = jsonObjectProfile1.getString("name");
                            username_edt_txt.setText(user_name);

                            String surname = jsonObjectProfile1.getString("surname");
                            surname_txt.setText(surname);

                            String mobile = jsonObjectProfile1.getString("mobile");
                            mobile_edt_txt.setText(mobile);

                            String email = jsonObjectProfile1.getString("email");
                            email_edt_txt.setText(email);

                            String doh = jsonObjectProfile1.getString("dob");
                            dob_txt.setText(doh);

                            String gender = jsonObjectProfile1.getString("gender");

                            if (gender.equals("Male") || gender.equals("male")) {
                                male_radio.setChecked(true);
                            }
                            if (gender.equals("Female") || gender.equals("female")) {
                                female_radio.setChecked(true);
                            }

                            String blood_group = jsonObjectProfile1.getString("blood_group");
                            blood_group_txt.setText(blood_group);

                            String membership = jsonObjectProfile1.getString("membership");
                            if (membership.equals("Normal")) {
                                normal_radio.setChecked(true);
                            }
                            if (membership.equals("Business")) {
                                business_radio.setChecked(true);
                            }
                            if (membership.equals("Association")) {
                                association_radio.setChecked(true);
                            }

                            String adhaar = jsonObjectProfile1.getString("adhaar");
                            adhaar_edt_txt.setText(adhaar);

                            mobile_privacy = jsonObjectProfile1.getString("mobile_privacy");
                            if (mobile_privacy.equals("0")) {
                                mobile_privcy_txt.setText("Hide");
                                mobile_ll.setVisibility(View.GONE);
                                mobile_header.setVisibility(View.GONE);
                                mobile_edt_txt.setVisibility(View.GONE);
                            }
                            if (mobile_privacy.equals("1")) {
                                mobile_privcy_txt.setText("Show");
                                mobile_ll.setVisibility(View.VISIBLE);
                                mobile_header.setVisibility(View.VISIBLE);
                                mobile_edt_txt.setVisibility(View.VISIBLE);
                            }
                            String who_r_u_edt_tex = jsonObjectProfile1.getString("about_me");
                            who_r_u_edt_text.setText(who_r_u_edt_tex);

                            String relation_status = jsonObjectProfile1.getString("relation_status");
                            relationship_status_txt.setText(relation_status);

                            String country = jsonObjectProfile1.getString("country_name");
                            String country_id = jsonObjectProfile1.getString("country");
                            String state_id = jsonObjectProfile1.getString("state");
                            String city_id = jsonObjectProfile1.getString("city");
                            if (country.equals("null") || country.equals(null) | country.equals("")) {
                                country_txt.setText("Choose Your Country");
                            } else {
                                country_txt.setText(country);

                            }

                            String state = jsonObjectProfile1.getString("state_name");
                            if (state.equals("null") || state.equals(null) | state.equals("")) {
                                state_txt.setText("Choose Your State");
                            } else {
                                state_txt.setText(state);

                            }

                            String city = jsonObjectProfile1.getString("city_name");
                            if (city.equals("null") || city.equals(null) | city.equals("")) {
                                city_txt.setText("Choose Your City");
                            } else {
                                city_txt.setText(city);

                            }

                            if (country_id.equals("null") || country_id.equals(null) | country_id.equals("")) {
                                country_txt.setText("Choose Your Country");
                            } else {
                                send_country_id = country_id;
                            }

                            if (state_id.equals("null") || state_id.equals(null) | state_id.equals("")) {
                                state_txt.setText("Choose Your State");
                            } else {

                                send_state_id = state_id;
                            }

                            if (city_id.equals("null") || city_id.equals(null) | city_id.equals("")) {
                                city_txt.setText("Choose Your City");
                            } else {

                                send_city_id = city_id;
                            }

////////////////////////////////start  second profile jsonobject.

                            JSONObject jsonObjectProfile2 = jsonObjectData.getJSONObject("getProfile2");

                            Log.d("PROFILE2:", jsonObjectProfile2.toString());

                            String b_date, b_month, b_year;
                            b_date = jsonObjectProfile2.getString("birth_day");
                            b_month = jsonObjectProfile2.getString("birth_month");
                            b_year = jsonObjectProfile2.getString("birth_year");
                            dob_txt.setText(b_date + "-" + b_month + "-" + b_year);
                            Log.d("DOB", b_date + "-" + b_month + "-" + b_year);
                            if (!b_date.equals("") && !b_month.equals("") && !b_year.equals("")) {
                                send_biirt_month = b_month;
                                send_birth_day = b_date;
                                send_birth_yr = b_year;
                            }
                            String fav_quote = jsonObjectProfile2.getString("favorite_quotes");
                            fav_quote_edt_text.setText(fav_quote);

                            String interseted_inn = jsonObjectProfile2.getString("interested_in");
                            intrested_in_text.setText(interseted_inn);

                            String company_current = jsonObjectProfile2.getString("company");
                            current_work_edt_text.setText(company_current);

                            String job_position = jsonObjectProfile2.getString("job_position");
                            current_position_edt_text.setText(job_position);

                            String profess_skill = jsonObjectProfile2.getString("professional_skill");
                            prof_skill_edt_text.setText(profess_skill);

                            String address = jsonObjectProfile2.getString("address");
                            address_edt_text.setText(address);


                            String high_school_name = jsonObjectProfile2.getString("high_school_name");
                            highschool_edt_text.setText(high_school_name);

                            String start_school_date = jsonObjectProfile2.getString("started_high_school_from_date");
                            highschool_strt_date.setText(start_school_date);

                            String end_school_date = jsonObjectProfile2.getString("ended_high_school_at_date");
                            highschool_end_date.setText(end_school_date);

                            String colg_field_study = jsonObjectProfile2.getString("college_field_of_study");
                            field_study_edt_text.setText(colg_field_study);

                            String colegg_name = jsonObjectProfile2.getString("college_name");
                            colleg_name_edt_text.setText(colegg_name);

                            String start_colg_date = jsonObjectProfile2.getString("started_college_from_date");
                            college_strt_date.setText(start_colg_date);

                            String end_colleg_date = jsonObjectProfile2.getString("ended_college_at_date");
                            college_end_date.setText(end_colleg_date);

                            String current_city = jsonObjectProfile2.getString("lives_in_city_name");
                            current_city_edt_text.setText(current_city);

                            String language_known = jsonObjectProfile2.getString("language");
                            language_txt.setText(language_known);

                            String relegion = jsonObjectProfile2.getString("religion");
                            relegious_edt_text.setText(relegion);

                            String political_view = jsonObjectProfile2.getString("politicl_view");
                            political_edt_text.setText(political_view);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            });


            strEditProfile.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(EditProfileActivity.this);
            requestQueue.add(strEditProfile);

        } else {
            Toast.makeText(EditProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }

        if (v == editpen_pic_click) {
            checkInternet = NetworkChecking.isConnected(EditProfileActivity.this);
            if (checkInternet) {
                final boolean result = ImagePermissions.checkPermission(EditProfileActivity.this);
                final Dialog mBottomSheetDialog = new Dialog(EditProfileActivity.this, R.style.MaterialDialogSheet);
               /* TextView dialog_title = (TextView) mBottomSheetDialog.findViewById(R.id.dialog_title);
//                dialog_title.setTypeface(typeface);*/
                mBottomSheetDialog.setContentView(R.layout.custom_dialog_image_selection); // your custom view.
                mBottomSheetDialog.setCancelable(true);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                mBottomSheetDialog.show();

                TextView take_photo_text = (TextView) mBottomSheetDialog.findViewById(R.id.take_photo_text);
//                take_photo_text.setTypeface(typeface);
                take_photo_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(EditProfileActivity.this, "take photo", Toast.LENGTH_LONG).show();
                        if (result) {
                            if (ContextCompat.checkSelfPermission(EditProfileActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (getFromPref(EditProfileActivity.this, ALLOW_KEY)) {
                                    showSettingsAlert();
                                } else if (ContextCompat.checkSelfPermission(EditProfileActivity.this,
                                        android.Manifest.permission.CAMERA)

                                        != PackageManager.PERMISSION_GRANTED) {

                                    // Should we show an explanation?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                                            android.Manifest.permission.CAMERA)) {
                                        showAlert();
                                    } else {
                                        // No explanation needed, we can request the permission.
                                        ActivityCompat.requestPermissions(EditProfileActivity.this,
                                                new String[]{android.Manifest.permission.CAMERA},
                                                CAMERA_REQUEST_IMAGE);
                                    }
                                }
                            } else {
                                cameraIntent();
                                mBottomSheetDialog.dismiss();
                            }
                        }
                    }
                });

                TextView select_from_gallery = (TextView) mBottomSheetDialog.findViewById(R.id.select_from_gallery);
//                select_from_gallery.setTypeface(typeface);
                select_from_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Toast.makeText(EditProfileActivity.this,"gallry",Toast.LENGTH_LONG).show();
                        /*if (result) {

                        }*/
                        galleryIntent();
                        mBottomSheetDialog.dismiss();
                    }
                });

                mBottomSheetDialog.show();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }

        if (v == surname_txt) {
            surnameModels.clear();
            getSurnameList();
        }

        if (v == blood_group_txt) {
            final String[] listValue;
            listValue = getResources().getStringArray(R.array.blood_group_list);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_blood_grp_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            //alertDialog.setTitle("Blood Group");
            final TextView blood_txt = (TextView) convertView.findViewById(R.id.blood_txt);
            final ListView lv_blood_grp = (ListView) convertView.findViewById(R.id.lv_blood_grp);
            final Button btn_blood_cancel = (Button) convertView.findViewById(R.id.btn_blood_cancel);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listValue);
            lv_blood_grp.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_blood_grp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    blood_group_txt.setText(listValue[arg2]);
                    dialog.dismiss();

                }
            });

            btn_blood_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }

        if (v == language_txt) {
            final String[] listValue;
            listValue = getResources().getStringArray(R.array.language_array);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_ianguage_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            //alertDialog.setTitle("Language");
            final TextView lng_txt = (TextView) convertView.findViewById(R.id.language_txt);
            final ListView lv_language = (ListView) convertView.findViewById(R.id.lv_language);
            final Button btn_language_cancel = (Button) convertView.findViewById(R.id.btn_language_cancel);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listValue);
            lv_language.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_language.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    language_txt.setText(listValue[arg2]);
                    dialog.dismiss();

                }
            });

            btn_language_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

        }
        if (v == mobile_privcy_txt) {
            final String[] listValue;
            listValue = getResources().getStringArray(R.array.mobile_privacy);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_mobile_privacy_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            //  alertDialog.setTitle("Mobile Privacy");
            final ListView lv_mob_privacy = (ListView) convertView.findViewById(R.id.lv_mob_privacy);
            final TextView mobiletitle_text = (TextView) convertView.findViewById(R.id.mobiletitle_text);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listValue);
            lv_mob_privacy.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_mob_privacy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    mobile_privcy_txt.setText(listValue[arg2]);
                    send_mobilePrivacy_vlue = arg2;

                    dialog.dismiss();

                }
            });
        }

        if (v == intrested_in_text) {
            final String[] listValue;
            listValue = getResources().getStringArray(R.array.intrested_in_array);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_intrested_in_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            //alertDialog.setTitle("Intrested In");
            final TextView interested_txt = (TextView) convertView.findViewById(R.id.interested_txt);
            final ListView lv_intrested = (ListView) convertView.findViewById(R.id.lv_intrested);
            final Button btn_intrested_cancel = (Button) convertView.findViewById(R.id.btn_intrested_cancel);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listValue);
            lv_intrested.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_intrested.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    send_intrested_index = String.valueOf(arg2 + 1);
                    intrested_in_text.setText(listValue[arg2]);
                    dialog.dismiss();
                }
            });

            btn_intrested_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }

        if (v == relationship_status_txt) {
            final String[] listValue;
            listValue = getResources().getStringArray(R.array.relation_status_array);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_reltion_status_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            //alertDialog.setTitle("Relationship Status");
            final TextView relation_txt = (TextView) convertView.findViewById(R.id.relation_txt);
            final Button btn_religion_cancel = (Button) convertView.findViewById(R.id.btn_religion_cancel);
            final ListView lv_relation_status = (ListView) convertView.findViewById(R.id.lv_relation_status);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listValue);
            lv_relation_status.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_relation_status.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    relationship_status_txt.setText(listValue[arg2]);
                    dialog.dismiss();

                }
            });
            btn_religion_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

        }
        if (v == country_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                countryList();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
            /*final String[] listValue;
            listValue = getResources().getStringArray(R.array.country_array);
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditProfileActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_country_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            alertDialog.setTitle("Country List");
            final ListView lv_country = (ListView) convertView.findViewById(R.id.lv_country);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listValue);
            lv_country.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    country_txt.setText(listValue[arg2]);
                    dialog.dismiss();

                }
            });
*/
        }

       /* if(v==state_txt)
        {

        }*/
        if (v == city_txt) {

            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                cityDialog();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == dob_txt) {
            // birth_Year,birth_Day,birth_Month;
            final Calendar c = Calendar.getInstance();
            birth_Year = c.get(Calendar.YEAR);
            birth_Month = c.get(Calendar.MONTH);
            birth_Day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }//birth_Year,birth_Day,birth_Month;///// send_biirt_month,send_birth_yr,send_birth_day

                    send_biirt_month = strMonth;
                    send_birth_day = strDay;
                    send_birth_yr = String.valueOf(year);
                    Log.d("BIRTHDATE:", send_birth_day + "/" + send_biirt_month + "/" + send_birth_yr);

                    dob_txt.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                    dob_txt.setError(null);
                }
            }, birth_Year, birth_Month, birth_Day);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        }
        if (v == highschool_strt_date) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    highschool_strt_date.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                    highschool_strt_date.setError(null);
                }
            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        }
        if (v == highschool_end_date) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    highschool_end_date.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                    highschool_end_date.setError(null);
                }
            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        }
        if (v == college_strt_date) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    college_strt_date.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                    college_strt_date.setError(null);
                }
            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        }

        if (v == college_end_date) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    college_end_date.setText(String.valueOf(strDay + "-" + strMonth + "-" + year));
                    college_end_date.setError(null);
                }
            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        }

        if (v == save_text) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                if (validate()) {

                    if (male_radio.isChecked() || female_radio.isChecked()) {
                        genderStatus = (RadioButton) findViewById(gender_group.getCheckedRadioButtonId());

                        if (genderStatus.getId() == male_radio.getId()) {
                            gunder_status = 1;
                        }
                        if (genderStatus.getId() == female_radio.getId()) {
                            gunder_status = 1;
                        }
                    }
                    if (business_radio.isChecked() || association_radio.isChecked() || normal_radio.isChecked()) {
                        memberstatus = (RadioButton) findViewById(membership_group.getCheckedRadioButtonId());

                        if (memberstatus.getId() == business_radio.getId()) {
                            member_init_status = 1;
                        }
                        if (memberstatus.getId() == association_radio.getId()) {
                            member_init_status = 1;
                        }
                        if (memberstatus.getId() == normal_radio.getId()) {
                            member_init_status = 1;
                        }
                    }
                    if (male_radio.isChecked() || female_radio.isChecked()) {
                        genderStatus = (RadioButton) findViewById(gender_group.getCheckedRadioButtonId());

                        if (genderStatus.getId() == male_radio.getId()) {
                            sendgender = "Male";

                        }
                        if (genderStatus.getId() == female_radio.getId()) {
                            sendgender = "Female";

                        }
                    }

                    if (business_radio.isChecked() || association_radio.isChecked() || normal_radio.isChecked()) {
                        memberstatus = (RadioButton) findViewById(membership_group.getCheckedRadioButtonId());

                        if (memberstatus.getId() == business_radio.getId()) {
                            sendmember = "Business";

                        }
                        if (memberstatus.getId() == association_radio.getId()) {
                            sendmember = "Association";

                        }
                        if (memberstatus.getId() == normal_radio.getId()) {
                            sendmember = "Normal";

                        }
                    }

                    progressDialog.show();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SAVE_ABOUT_USER,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        Log.d("EDITPROFILE", AppUrls.BASE_URL + AppUrls.SAVE_ABOUT_USER);
                                        Log.d("ProfileRESP", response);
                                        String editSuccessResponceCode = jsonObject.getString("status");

                                        if (editSuccessResponceCode.equals("10100")) {
                                            // JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            Toast.makeText(EditProfileActivity.this, "Profile Saved Successfully ", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }

                                        if (editSuccessResponceCode.equals("10200")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Try Again..!", Toast.LENGTH_SHORT).show();
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_profile_id", user_profile_id);
                            params.put("name", username_edt_txt.getText().toString());
                            params.put("surname", surname_txt.getText().toString());
                            params.put("mobile", mobile_edt_txt.getText().toString());
                            params.put("mobile_privacy", String.valueOf(send_mobilePrivacy_vlue));
                            params.put("email", email_edt_txt.getText().toString());
                            params.put("adhaar", adhaar_edt_txt.getText().toString());
                            params.put("birth_day", send_birth_day);//birth_Year, birth_Month, birth_Day
                            params.put("birth_month", send_biirt_month);
                            params.put("birth_year", send_birth_yr);
                            params.put("blood_group", blood_group_txt.getText().toString());
                            params.put("relation_status", relationship_status_txt.getText().toString());
                            params.put("country", send_country_id);
                            params.put("state", send_state_id);
                            params.put("city", send_city_id);
                            params.put("about", who_r_u_edt_text.getText().toString());
                            params.put("lives_in_city_name", current_city_edt_text.getText().toString());
                            params.put("address", address_edt_text.getText().toString());
                            params.put("interested_in", intrested_in_text.getText().toString());
                            params.put("favorite_quotes", fav_quote_edt_text.getText().toString());
                            params.put("company", current_work_edt_text.getText().toString());
                            params.put("job_position", current_position_edt_text.getText().toString());
                            params.put("professional_skill", prof_skill_edt_text.getText().toString());
                            params.put("high_school_name", highschool_edt_text.getText().toString());
                            params.put("started_high_school_from_date", highschool_strt_date.getText().toString());
                            params.put("ended_high_school_at_date", highschool_end_date.getText().toString());
                            params.put("college_field_of_study", field_study_edt_text.getText().toString());
                            params.put("college_name", colleg_name_edt_text.getText().toString());
                            params.put("started_college_from_date", college_strt_date.getText().toString());
                            params.put("ended_college_at_date", college_end_date.getText().toString());
                            params.put("language", language_txt.getText().toString());
                            params.put("religion", relegious_edt_text.getText().toString());
                            params.put("politicl_view", political_edt_text.getText().toString());
                            params.put("membership", sendmember);
                            params.put("gender", sendgender);
                            Log.d("SAVEUSERDATA :", params.toString());
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(EditProfileActivity.this);
                    requestQueue.add(stringRequest);

                } else {
                    Snackbar snackbar = Snackbar.make(v, "Please Provide All Details...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST_IMAGE);
    }

    private void cameraIntent() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Toast.makeText(ProfileActivity.this, "GRANTED", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        ContentValues values = new ContentValues(1);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
        fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(intent, CAMERA_REQUEST_IMAGE);
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(EditProfileActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(EditProfileActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                CAMERA_REQUEST_IMAGE);
                    }
                });
        alertDialog.show();
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(EditProfileActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(EditProfileActivity.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {
                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_REQUEST_IMAGE) {
                try {
                    onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void onCaptureImageResult(Intent data) throws IOException {


        selectedFilePath = FilePath.getPath(this, fileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                correctBmp = getResizedBitmap(correctBmp, 200);
                user_profile_img.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();

                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        String responce_value = null;
                        try {
                            Log.d("DOCameraFILEUPLOAD", sendingimagepath);
                            responce_value = doFileUpload(sendingimagepath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            final JSONObject jsonObject = new JSONObject(responce_value);
                            String responceCode = jsonObject.getString("status");
                            if (responceCode.equals("10200")) {

                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String img_path = jsonObject1.getString("image_path");
                                Log.d("SERVERIMAGEPATH", img_path);
                                String img_pt = AppUrls.BASE_IMAGE_URL + img_path;
                                Log.d("SDAFTHSH", img_pt);
                                session.updateProfilePicInfo(img_pt);
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();


                                                    Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                                }
                                            });


                                            /*Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                                            startActivity(intent);*/

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("RASPONCEDATAVALUE", responce_value);
                    }
                });
                thread.start();
            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }


    }

    private void onSelectFromGalleryResult(Intent data) throws IOException {
        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {
            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                correctBmp = getResizedBitmap(correctBmp, 200);
                user_profile_img.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                final String sendingimagepath = destination.getPath();

                Thread thread = new Thread(new Runnable() {
                    public void run() {


                        String responce_value = null;
                        try {


                            responce_value = doFileUpload(sendingimagepath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(responce_value);
                            String responceCode = jsonObject.getString("status");
                            if (responceCode.equals("10200"))//50100
                            {
                                JSONObject jsonObject2 = jsonObject.getJSONObject("data");
                                String img_path = jsonObject2.getString("image_path");
                                Log.d("SERVERIMAGEPATH", img_path);
                                String img_pt = AppUrls.BASE_IMAGE_URL + img_path;
                                Log.d("SDAFTHSH", img_pt);
                                session.updateProfilePicInfo(img_pt);

                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                    Toast.makeText(getApplicationContext(), "Successfully Uploaded...!", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                           /* Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                                            startActivity(intent);*/


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    if (progressDialog.isShowing())
                                                        progressDialog.dismiss();
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }
    }

    private String doFileUpload(String selectedFilePath) throws IOException {
        EditProfileActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                if (progressDialog != null)
                    progressDialog.show();
            }
        });

        Log.d("IMAGESDFDFAF", selectedFilePath.toString());
        String response_str = null;
        File file1 = null;
        FileBody bin1 = null;
        String empty = "";
        Log.d("SDFAFGSGFSG", "sfafafsaf 1");
        file1 = new File(selectedFilePath.replaceAll("file://", "").replace("%20", " "));
        // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
        String urlString = "";


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            urlString = AppUrls.BASE_URL + AppUrls.UPDATE_USER_PIC;
        } else {
            urlString = AppUrls.BASE_URL + AppUrls.UPDATE_USER_PIC;
        }
        Log.d("REQUESTURL", urlString);
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            MultipartEntity reqEntity = new MultipartEntity();
            Log.e("USERID:ffff", userid);
            reqEntity.addPart("user_id", new StringBody(userid));
            bin1 = new FileBody(file1);
            reqEntity.addPart("userfile", bin1);
            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            response_str = EntityUtils.toString(resEntity);
            Log.d("MERESPONCE", response_str);
        } catch (Exception ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
        return response_str;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private boolean validate() {
        boolean result = true;

        String name = username_edt_txt.getText().toString().trim();
        if (name == null || name.equals("") || name.length() < 3) {
            username_edt_txt.setError("Invalid Name");
            result = false;
        }

        String surName = surname_txt.getText().toString().trim();
        if (surName == null || surName.equals("")) {
            surname_txt.setError("Invalid Surname");
            result = false;
        }

        return result;
    }

    private void getSurnameList() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            surname_list.clear();
            surnameModels.clear();
            progressDialog.show();
            String url = AppUrls.BASE_URL + AppUrls.SURNAME;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SURNAMERESPONSE:", response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("surnameData");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        SurnameListModel surnameModel = new SurnameListModel();
                                        surnameModel.setId(jsonObject2.getString("id"));
                                        String surname_name = jsonObject2.getString("name");
                                        surnameModel.setSurname(surname_name);

                                        surname_list.add(surname_name);
                                        surnameModels.add(surnameModel);
                                    }
                                    getSurnameListDialog();
                                } else {
                                    progressDialog.dismiss();
                                }

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(EditProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getSurnameListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.surname_list_dialog, null);

        TextView surName_list_txt = (TextView) dialog_layout.findViewById(R.id.surName_list_txt);
//        surName_list_txt.setTypeface(typeface);

        final SearchView surName_search = (SearchView) dialog_layout.findViewById(R.id.surName_search);
        EditText searchEditText = (EditText) surName_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setTypeface(typeface);

        RecyclerView surName_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.surName_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        surName_recycler_view.setLayoutManager(layoutManager);

        for (int i = 0; i < surname_list.size(); i++) {
            SurnameListModel surnameListModel = new SurnameListModel();
            surnameListModel.setSurname(surname_list.get(i));
            surnameModels.add(surnameListModel);
        }

        surnameEditProfileListAdapter = new EditProfileSurnameListAdapter(surnameModels, EditProfileActivity.this, R.layout.row_surname_list);

        surName_recycler_view.setAdapter(surnameEditProfileListAdapter);

        surName_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                surName_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        surName_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                surnameEditProfileListAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setSurName(String surname) {

        dialog.dismiss();
        surnameEditProfileListAdapter.notifyDataSetChanged();
        surname_txt.setError(null);
        surname_txt.setText(surname);

    }

    private void countryList() {
        countryModelArrayList.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.COUNTRY_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("RESPONSE:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("countryData");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        CountryModel cm = new CountryModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        String country_name = jsonObject2.getString("country_name");
                                        cm.setName(country_name);
                                        cm.setCountry_iso_code(jsonObject2.getString("country_iso_code"));

                                        countryList.add(country_name);
                                        countryModelArrayList.add(cm);
                                    }
                                    countryDialog();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.cancel();
                                    Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Data Found...!", Snackbar.LENGTH_LONG);

                                    snack.show();
                                }


                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
               /* @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void countryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.country_list, null);

        TextView country_txt = (TextView) dialog_layout.findViewById(R.id.country_txt);
//        country_txt.setTypeface(typeface);

        final SearchView country_search = (SearchView) dialog_layout.findViewById(R.id.country_search);
        EditText searchEditText = (EditText) country_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        // searchEditText.setTypeface(typeface);

        RecyclerView country_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.country_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        country_recyclerview.setLayoutManager(layoutManager);

        countryAdapter = new EditProfileCountryAdapter(countryModelArrayList, EditProfileActivity.this, R.layout.row_country);

        country_recyclerview.setAdapter(countryAdapter);

        country_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        country_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                countryAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setCountryName(String countryName, String countryId) {

        dialog.dismiss();
        countryAdapter.notifyDataSetChanged();

        String sendCountryName = countryName;
        country_txt.setText(sendCountryName);
        send_country_id = countryId;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        state_check = 1;
        state_txt.setText("Select State");
        stateList(send_country_id);
    }

    private void stateList(String send_country_id) {
        stateModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.STATE_LIST + send_country_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("stateData");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        StateModel sm = new StateModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        sm.setId(jsonObject2.getString("id"));
                                        String state_name = jsonObject2.getString("state_name");
                                        sm.setName(state_name);
                                        sm.setName(jsonObject2.getString("state_name"));

                                        stateList.add(state_name);
                                        stateModels.add(sm);
                                    }
                                    stateDialog();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.cancel();
                                    state_txt.setText("No States Found");

                                    Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Data Found...!", Snackbar.LENGTH_LONG);

                                    snack.show();
                                }


                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    private void stateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.state_list, null);

        TextView state_txt = (TextView) dialog_layout.findViewById(R.id.state_txt);
//        state_txt.setTypeface(typeface);

        final SearchView state_search = (SearchView) dialog_layout.findViewById(R.id.state_search);
        EditText searchEditText = (EditText) state_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setTypeface(typeface);

        RecyclerView state_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.state_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        state_recyclerview.setLayoutManager(layoutManager);

        stateAdapter = new EditProfileStateAdapter(stateModels, EditProfileActivity.this, R.layout.row_state);

        state_recyclerview.setAdapter(stateAdapter);
        state_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        state_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                stateAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setStateName(String stateName, String stateId) {

        dialog.dismiss();
        stateAdapter.notifyDataSetChanged();

        String sendStateName = stateName;
        send_state_id = stateId;
        state_txt.setText(sendStateName);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        city_check = 1;
        city_txt.setText("Select City");
        cityList(send_state_id);

        //cityDialog();

    }

    private void cityList(String send_state_id) {
        cityModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CITY_LIST + send_state_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("CCCCCCCCCCCCCC", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("cityData");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        CityModel cm = new CityModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        String city_name = jsonObject2.getString("city_name");
                                        cm.setName(city_name);
                                        cm.setName(jsonObject2.getString("city_name"));

                                        cityList.add(city_name);
                                        cityModels.add(cm);
                                    }
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.cancel();
                                    city_txt.setText("No City Found");
                                    Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Data Found...!", Snackbar.LENGTH_LONG);

                                    snack.show();
                                }


                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }


    private void cityDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.city_list, null);

        TextView city_txt = (TextView) dialog_layout.findViewById(R.id.city_txt);
//        city_txt.setTypeface(typeface);

        final SearchView city_search = (SearchView) dialog_layout.findViewById(R.id.city_search);
        EditText searchEditText = (EditText) city_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setTypeface(typeface);

        RecyclerView city_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.city_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        city_recyclerview.setLayoutManager(layoutManager);

        cityAdapter = new EditProfileCityAdapter(cityModels, EditProfileActivity.this, R.layout.row_city);

        city_recyclerview.setAdapter(cityAdapter);
        city_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        city_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                cityAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setCityName(String cityName, String city_id) {

        dialog.dismiss();
        cityAdapter.notifyDataSetChanged();
        String sendCityName = cityName;
        send_city_id = city_id;
        city_txt.setText(sendCityName);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
