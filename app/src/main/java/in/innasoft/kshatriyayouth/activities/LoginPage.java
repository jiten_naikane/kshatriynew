package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.MainActivity;
import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class LoginPage extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    Toolbar toolbar;
    TextView login_txt, forgot_txt, login_dntaccount_txt, register_here_txt;
    EditText email_edt, password_edt;
    CheckBox login_view;
    Button login_btn;
    //    Typeface typeface;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    private String device_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_activity_login_page);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager = new UserSessionManager(LoginPage.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        login_txt = (TextView) findViewById(R.id.login_txt);
//        login_txt.setTypeface(typeface);
        forgot_txt = (TextView) findViewById(R.id.forgot_txt);
//        forgot_txt.setTypeface(typeface);
        forgot_txt.setOnClickListener(this);
        login_dntaccount_txt = (TextView) findViewById(R.id.login_dntaccount_txt);
//        login_dntaccount_txt.setTypeface(typeface);
        register_here_txt = (TextView) findViewById(R.id.register_here_txt);
//        register_here_txt.setTypeface(typeface);

        register_here_txt.setOnClickListener(this);

        email_edt = (EditText) findViewById(R.id.email_edt);
//        email_edt.setTypeface(typeface);

        password_edt = (EditText) findViewById(R.id.password_edt);
//        password_edt.setTypeface(typeface);

        login_view = (CheckBox) findViewById(R.id.login_view);
//        login_view.setTypeface(typeface);
        login_view.setOnCheckedChangeListener(this);

        login_btn = (Button) findViewById(R.id.login_btn);
//        login_btn.setTypeface(typeface);
        login_btn.setOnClickListener(this);

        device_id = Settings.Secure.getString(LoginPage.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID", device_id);

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        if (login_view == compoundButton) {
            if (isChecked) {
                password_edt.setInputType(InputType.TYPE_CLASS_TEXT);
                password_edt.setSelection(password_edt.getText().length());
            } else {
                password_edt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                password_edt.setSelection(password_edt.getText().length());
            }
        }
    }

    private boolean validate() {

        boolean result = true;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String email = email_edt.getText().toString();
        if (!email.matches(EMAIL_REGEX)) {
            email_edt.setError("Invalid Email");
            result = false;
        }

        String psw = password_edt.getText().toString().trim();
        if (psw == null || psw.equals("") || psw.length() < 6) {
            password_edt.setError("Invalid Password");
            result = false;
        }

        return result;
    }

    @Override
    public void onClick(View v) {
        if (v == forgot_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                Intent intent = new Intent(getApplicationContext(), ForgotPasswordPage.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == register_here_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                Intent intent = new Intent(getApplicationContext(), RegisterPage.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == login_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    final String email = email_edt.getText().toString().trim();
                    final String password = password_edt.getText().toString().trim();
                    progressDialog.show();
                    String urllllll = AppUrls.BASE_URL + AppUrls.LOGIN;
                    Log.d("LOGINURRRRLLLLLL:", urllllll);

                    //   Toast.makeText(LoginPage.this,"Login Click",Toast.LENGTH_LONG).show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        Log.d("LOGIN", response);
                                        String editSuccessResponceCode = jsonObject.getString("status");

                                        if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            // String message = jsonObject1.getString("msg");
                                            String user_id = jsonObject1.getString("id");
                                            String user_name = jsonObject1.getString("name");
                                            String mobile = jsonObject1.getString("mobile");
                                            String email = jsonObject1.getString("email");
                                            String surname = jsonObject1.getString("surname");
                                            String user_profile_id = jsonObject1.getString("username");
                                            String membership = jsonObject1.getString("membership");
                                            String photo = AppUrls.BASE_IMAGE_URL + jsonObject1.getString("photo");
                                            Log.d("PHOTOURLLL", photo);
                                            Log.d("USER_PROFILE_ID", user_profile_id);//puru==USER_PROFILE_ID: 15033728995916034
                                            Log.d("MEMBERSHIP:", membership);//puru==USER_PROFILE_ID: 15033728995916034
                                            Log.d("USER_ID:", user_id);//puru==USER_PROFILE_ID: 15033728995916034


                                            userSessionManager.createUserLoginSession(user_id, user_name, mobile, email, surname, user_profile_id, photo, membership, device_id);
                                            Toast.makeText(getApplicationContext(), "You Logged in Successfully...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(LoginPage.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            progressDialog.dismiss();
                                            startActivity(intent);
                                            finish();
                                        } else if (editSuccessResponceCode.equals("10200")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Password Wrong", Toast.LENGTH_SHORT).show();
                                        } else if (editSuccessResponceCode.equals("10300")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Sorry..! User Does Not Exist", Toast.LENGTH_SHORT).show();
                                        } else if (editSuccessResponceCode.equalsIgnoreCase("10160")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(LoginPage.this, "Admin approval required..!", Toast.LENGTH_SHORT).show();

                                        } else if (editSuccessResponceCode.equals("10500")) {
                                            progressDialog.dismiss();
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            String userId = jsonObject1.getString("user_id");
                                            String mobile = jsonObject1.getString("mobile");
                                            String otp = jsonObject1.getString("otp");
                                            Toast.makeText(getApplicationContext(), "Your account not verified....!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), AccountVerificationPage.class);
                                            intent.putExtra("userId", userId);
                                            intent.putExtra("otp", otp);
                                            intent.putExtra("mobile", mobile);
                                            startActivity(intent);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", password);
                            params.put("token", device_id);
                            Log.d("LOGINDATAIL:", params.toString());
                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(LoginPage.this);
                    requestQueue.add(stringRequest);
                   /* RequestQueue requestQueue = Volley.newRequestQueue(LoginPage.this);
                    requestQueue.add(stringRequest);*/

                } else {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }


        }
    }

}
