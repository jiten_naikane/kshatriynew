package in.innasoft.kshatriyayouth.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.EditPostAdapter;
import in.innasoft.kshatriyayouth.models.EditPostFeedItem;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class EditPostActivity extends AppCompatActivity {
    private boolean checkInternet;
    Bundle bundle;
    String id, sub_id, type;

    private List<EditPostFeedItem> feedItems;
    private ListView listView;
    private EditPostAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);
        feedItems = new ArrayList<EditPostFeedItem>();
        bundle = getIntent().getExtras();
        id = bundle.getString("id");
        sub_id = bundle.getString("sub_id");
        type = bundle.getString("type");
        listView = (ListView) findViewById(R.id.list);


        postDisplay(id);
    }

    private void postDisplay(final String post_id) {
        checkInternet = NetworkChecking.isConnected(EditPostActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.POST_EDIT_INFO + "/" + post_id + "/" + sub_id + "/" + type;
            Log.d("SDFASFAFAFAF", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject response_boj = new JSONObject(response);
                                Log.d("Post_response", response);
                                String responceCode = response_boj.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject feedObject = response_boj.getJSONObject("data");
                                    JSONArray feedArray = feedObject.getJSONArray("edit_info");
                                    Log.d("sdjvkbbkvsdfbvsjk", "" + feedArray);

                                    for (int i = 0; i < feedArray.length(); i++) {
                                        JSONObject feedObj = (JSONObject) feedArray.get(i);

                                        EditPostFeedItem item = new EditPostFeedItem();
                                        item.setPost(feedObj.getString("edit_info"));
                                        item.setDate(feedObj.getString("date"));
                                        item.setName(feedObj.getString("name") + " " + feedObj.getString("surname"));
                                        String profile_pic = AppUrls.BASE_IMAGE_URL + feedObj.getString("photo");
                                        item.setPhoto(profile_pic);
                                        feedItems.add(item);
                                    }
                                    listAdapter = new EditPostAdapter(EditPostActivity.this, feedItems);
                                    listView.setAdapter(listAdapter);

                                }
                                if (responceCode.equals("10800")) {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

            };
            RequestQueue requestQueue = Volley.newRequestQueue(EditPostActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(EditPostActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

}
