package in.innasoft.kshatriyayouth.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.GroupViewPagerAdapter;
import in.innasoft.kshatriyayouth.fragments.AssociationAboutFragment;
import in.innasoft.kshatriyayouth.fragments.AssociationEventFragment;
import in.innasoft.kshatriyayouth.fragments.AssociationPeopleFragment;
import in.innasoft.kshatriyayouth.fragments.PostAssociationFragment;

public class AssociationDetailActivity extends AppCompatActivity {
    String asociation_id, asociation_image_url, asociation_name;
    ImageView association_detail_img;
    Toolbar toolbar1;
    CollapsingToolbarLayout collapse_toolbar;
    TabLayout tabLayout;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_association_detail);


        toolbar1 = (Toolbar) findViewById(R.id.toolbar1);
        toolbar1.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapse_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapse_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapse_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);


        Bundle bundle = getIntent().getExtras();
        asociation_id = bundle.getString("ASSOCIATION_ID");
        asociation_name = bundle.getString("ASSOCIATION_NAME");
        asociation_image_url = bundle.getString("ASSOCIATION_IMAGE");
        Log.d("ASSociatDETAILID", asociation_id + ":::" + asociation_image_url + ":::" + asociation_name);


        association_detail_img = (ImageView) findViewById(R.id.association_detail_img);

        pager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(pager);


        Picasso.with(AssociationDetailActivity.this)
                .load(asociation_image_url)
                .placeholder(R.drawable.no_images_found)
                .into(association_detail_img);

        collapse_toolbar.setTitle(asociation_name);
    }

    private void setupViewPager(ViewPager view_pager) {

        GroupViewPagerAdapter adapter = new GroupViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AssociationAboutFragment(), "About");
        adapter.addFrag(new PostAssociationFragment(), "Posts");
        adapter.addFrag(new AssociationEventFragment(), "Events");
        adapter.addFrag(new AssociationPeopleFragment(), "People");

        view_pager.setAdapter(adapter);

    }

      /*  //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("About"));
        tabLayout.addTab(tabLayout.newTab().setText("Posts"));
        tabLayout.addTab(tabLayout.newTab().setText("Events"));
        tabLayout.addTab(tabLayout.newTab().setText("People"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);
                        int tabIconColor = ContextCompat.getColor(AssociationDetailActivity.this, R.color.colorPrimary);
                       // tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        int tabIconColor = ContextCompat.getColor(AssociationDetailActivity.this, R.color.tab_unselected_color);
                       // tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
    }

    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
