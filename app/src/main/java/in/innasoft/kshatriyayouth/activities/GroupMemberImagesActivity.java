package in.innasoft.kshatriyayouth.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SlidingImage_Adapter;
import in.innasoft.kshatriyayouth.models.GroupPhotoListModel;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class GroupMemberImagesActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView delete_group_photo_image;

    //    Typeface typeface;
    String image_path, title_caps, u_id, position, modelsize;
    ViewPager viewPager;
    File imagePath = null;
    int images[] = {R.drawable.app_logo_new, R.drawable.app_logo_new};
    // MyZoomImageCustomPagerAdapter myCustomPagerAdapter;
    UserSessionManager session;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    ImageView iv1, iv2;
    private ArrayList<String> ImagesArray = new ArrayList<>();
    private String[] IMAGES;

    ArrayList<String> imgArray = new ArrayList<String>();

    GroupPhotoListModel groupPhotoListModel = new GroupPhotoListModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_images);

        Bundle bundle = getIntent().getExtras();
        groupPhotoListModel = (GroupPhotoListModel) bundle.getSerializable("gphoto_list");
        position = bundle.getString("position");
        modelsize = bundle.getString("modelsize");
        imgArray = bundle.getStringArrayList("image_arraylist");
        String path = groupPhotoListModel.getPhoto_link();
        Log.d("PATHSHDHHDHD", imgArray.toString());


        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        u_id = userDetails.get(UserSessionManager.USER_ID);


        delete_group_photo_image = (ImageView) findViewById(R.id.delete_group_photo_image);
        delete_group_photo_image.setOnClickListener(this);


        viewPager = (ViewPager) findViewById(R.id.viewPager);

       /* myCustomPagerAdapter = new MyZoomImageCustomPagerAdapter(ZoomImageDescActivity.this, myList);
        viewPager.setAdapter(myCustomPagerAdapter);
        myCustomPagerAdapter.notifyDataSetChanged();*/

        // init();


        iv1 = (ImageView) findViewById(R.id.iv1);
        iv2 = (ImageView) findViewById(R.id.iv2);


        viewPager.setAdapter(new SlidingImage_Adapter(GroupMemberImagesActivity.this, imgArray));
        NUM_PAGES = imgArray.size();
        if (NUM_PAGES == 1) {
            iv2.setVisibility(View.GONE);
            iv1.setVisibility(View.GONE);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == NUM_PAGES - 1) {
                    iv1.setVisibility(View.GONE);
                    iv2.setVisibility(View.VISIBLE);
                } else if (position == 0) {
                    iv2.setVisibility(View.GONE);
                    iv1.setVisibility(View.VISIBLE);
                } else if (position > 0 || position < NUM_PAGES - 1) {
                    iv2.setVisibility(View.VISIBLE);
                    iv1.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);


            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab--;
                viewPager.setCurrentItem(tab);
                if (tab == NUM_PAGES - 1) {
                    iv1.setVisibility(View.GONE);
                    iv2.setVisibility(View.VISIBLE);
                } else if (tab == 0) {
                    iv2.setVisibility(View.GONE);
                    iv1.setVisibility(View.VISIBLE);
                } else if (tab > 0 || tab < NUM_PAGES - 1) {
                    iv2.setVisibility(View.VISIBLE);
                    iv1.setVisibility(View.VISIBLE);
                }
            }
        });


    }

   /* private void init()
    {
        for(int i=0;i<IMAGES.length;i++)
            try {
                ImagesArray.add(IMAGES[i]);
            }catch (final NumberFormatException e) {

            }


    }*/

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap) {
        imagePath = new File(Environment.getExternalStorageDirectory() + "/screenshot.png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == delete_group_photo_image) {
            Toast.makeText(GroupMemberImagesActivity.this, "DELETE", Toast.LENGTH_LONG).show();
        }

    }
}
