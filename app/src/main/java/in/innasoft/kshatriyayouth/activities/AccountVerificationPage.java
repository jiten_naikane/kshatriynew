package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class AccountVerificationPage extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView acc_verify_txt, resend_txt;
    EditText otp_edt;
    Button submit_btn;
    private boolean checkInternet;
    String mobile, otp;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification_page);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        acc_verify_txt = (TextView) findViewById(R.id.acc_verify_txt);
        resend_txt = (TextView) findViewById(R.id.resend_txt);
        resend_txt.setOnClickListener(this);

        otp_edt = (EditText) findViewById(R.id.otp_edt);

        submit_btn = (Button) findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);

        Intent intent = getIntent();
        mobile = intent.getStringExtra("mobile");
        otp = intent.getStringExtra("otp");

    }

    @Override
    public void onClick(View v) {

        if (v == resend_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            pprogressDialog.show();
            if (checkInternet) {
                pprogressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RE_SEND_OTP,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("RECIVINGDATA", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("status");
                                    if (successResponceCode.equals("10100")) {
                                        pprogressDialog.dismiss();
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                        String otp1 = jsonObject1.getString("otp");

                                        Toast.makeText(getApplicationContext(), "New OTP Sent Successfully", Toast.LENGTH_SHORT).show();

                                    }
                                    if (successResponceCode.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "All Fields Required...!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pprogressDialog.cancel();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("mobile", mobile);
                        Log.d("RESENDOTPPARAM:", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationPage.this);
                requestQueue.add(stringRequest);

            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }


        if (v == submit_btn) {

            if (validate()) {
                if (validate()) {
                    checkInternet = NetworkChecking.isConnected(this);
                    if (checkInternet) {
                        final String otp = otp_edt.getText().toString().trim();
                        pprogressDialog.show();
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCOUNT_VERIFICATION,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("ACCOUNTVERIFICATION", response);

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String successResponceCode = jsonObject.getString("status");
                                            if (successResponceCode.equals("10200")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Your Account has been Successfully verified...!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(AccountVerificationPage.this, LoginPage.class);
                                                startActivity(intent);

                                            }
                                            if (successResponceCode.equals("10100")) {
                                                pprogressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Incorrect OTP...!", Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                        } else if (error instanceof AuthFailureError) {

                                        } else if (error instanceof ServerError) {

                                        } else if (error instanceof NetworkError) {

                                        } else if (error instanceof ParseError) {

                                        }
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("otp", otp);
                                params.put("mobile", mobile);
                                Log.d("OTPPARAM:", params.toString());
                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationPage.this);
                        requestQueue.add(stringRequest);

                    } else {
                        Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }
            }
        }
    }

    private boolean validate() {

        boolean result = true;
        String otp = otp_edt.getText().toString().trim();
        if (otp == null || otp.equals("") || otp.length() != 6) {
            otp_edt.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Log.d("AUTOREAD", message);
                try {
                    Pattern p = Pattern.compile("(|^)\\d{6}");


                    if (message != null) {
                        Matcher m = p.matcher(message);
                        if (m.find()) {
                            otp_edt.setText(m.group(0));
                        } else {
                            //something went wrong
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
}
