package in.innasoft.kshatriyayouth.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bluejamesbond.text.DocumentView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class BlogsDetailActivity extends AppCompatActivity {

    UserSessionManager session;
    ImageView no_data, blog_detail_img;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    Toolbar toolbar;
    String user_id, blog_detil_id;
    Toolbar toolbar1;
    CollapsingToolbarLayout collapse_blog_toolbar;
    TextView blog_detailTitle, blog_detail_date_time;//blog_detail_descriptyion;
    DocumentView blog_detail_descriptyion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogs_detail);


//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.mark_simonson_proxima_nova_lt_regular));

        toolbar1 = (Toolbar) findViewById(R.id.toolbar1);
        toolbar1.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapse_blog_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_blog_toolbar);
        collapse_blog_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapse_blog_toolbar.setCollapsedTitleTextAppearance(R.style.collapsedAppBar);
        collapse_blog_toolbar.setExpandedTitleTextAppearance(R.style.expandAppBar);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);

        Bundle bundle = getIntent().getExtras();
        blog_detil_id = bundle.getString("BLOGDETAIL_ID");
        Log.d("BLOGGGGG:", blog_detil_id);

        no_data = (ImageView) findViewById(R.id.no_data);
        blog_detail_img = (ImageView) findViewById(R.id.blog_detail_img);

        blog_detailTitle = (TextView) findViewById(R.id.blog_detailTitle);
//        blog_detailTitle.setTypeface(typeface);

        blog_detail_date_time = (TextView) findViewById(R.id.blog_detail_date_time);
//        blog_detail_date_time.setTypeface(typeface);

        // blog_detail_descriptyion=(TextView)findViewById(R.id.blog_detail_descriptyion);
        //  blog_detail_descriptyion.setTypeface(typeface);

        blog_detail_descriptyion = (DocumentView) findViewById(R.id.blog_detail_descriptyion);


        getblogDetailData();
    }


    private void getblogDetailData() {

        checkInternet = NetworkChecking.isConnected(BlogsDetailActivity.this);
        if (checkInternet) {


            progressDialog.show();
            final String url = AppUrls.BASE_URL + AppUrls.BLOG_DETAILS + "/" + blog_detil_id;
            Log.d("assbloglURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.BLOG_DETAILS + "/" + blog_detil_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ASSOBLOGRESP", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("rowData");


                                    String name = jsonObject2.getString("name");
                                    blog_detailTitle.setText(Html.fromHtml(name));
                                    collapse_blog_toolbar.setTitle(Html.fromHtml(name));

                                    String date_time = jsonObject2.getString("create_date_time");
                                    blog_detail_date_time.setText(Html.fromHtml(date_time));

                                    String description = jsonObject2.getString("description");
                                    blog_detail_descriptyion.setText(Html.fromHtml(description));

                                    String image = AppUrls.BASE_URL + jsonObject2.getString("image");

                                    Picasso.with(BlogsDetailActivity.this)
                                            .load(AppUrls.BASE_URL + jsonObject2.getString("image"))
                                            .placeholder(R.drawable.app_logo_new)
                                            .into(blog_detail_img);


                                }
                                if (editSuccessResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    no_data.setVisibility(View.VISIBLE);
                                    Toast.makeText(BlogsDetailActivity.this, "No Data", Toast.LENGTH_SHORT).show();
                                }
                                progressDialog.dismiss();

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            no_data.setVisibility(View.VISIBLE);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(BlogsDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(BlogsDetailActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
