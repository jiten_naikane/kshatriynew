package in.innasoft.kshatriyayouth.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.GroupMembersAdapter;
import in.innasoft.kshatriyayouth.adapters.MultiCompletMemberGroupAdapter;
import in.innasoft.kshatriyayouth.models.AutoCompletGroupCrationModel;
import in.innasoft.kshatriyayouth.models.AutoCompletGroupUpdateCrationModel;
import in.innasoft.kshatriyayouth.models.GroupMembersModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.FilePath;
import in.innasoft.kshatriyayouth.utilities.ImagePermissions;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;
import in.innasoft.kshatriyayouth.utilities.UserSessionManager;

public class
GroupCreateActivityDialog extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager session;
    //    Typeface typeface;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ImageView select_click_image, select_image;
    String userid, user_profile_id, user_surname, user_name, send_privacy;
    private String upLoadServerUri = null;
    private String imagepath = null;
    String update_key, update_group_id;
    TextView group_title, group_desc_title, group_member_title, group_photo_title, group_privacy_title, group_privacy_header, group_privacy_text,
            button_cancel_now, button_Create_now, button_update_group, group_dummy_text, group_member_txt;
    RadioGroup privacy_group;
    RadioButton public_radio, secret_radio, privcyStatus;
    public String data;
    ArrayList<AutoCompletGroupCrationModel> memberslist = new ArrayList<AutoCompletGroupCrationModel>();
    ArrayList<AutoCompletGroupUpdateCrationModel> updatememberslist = new ArrayList<AutoCompletGroupUpdateCrationModel>();
    ArrayList<String> updatemembersliststring = new ArrayList<String>();
    ArrayList<String> update_gm_ids = new ArrayList<String>();
    EditText group_name_edt, group_desc_edt;
    String group_picture;

    MultiAutoCompleteTextView autoComplete_members;
    ArrayList<String> valueList = new ArrayList<String>();
    ArrayList<String> sendList = new ArrayList<String>();
    private static final int CAMERA_REQUEST_IMAGE = 1888;
    private static final int GALLERY_REQUEST_IMAGE = 1999;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    private Uri fileUri;
    HashSet<String> hashset = new HashSet<String>();
    ArrayList<AutoCompletGroupCrationModel> lisssst = new ArrayList<AutoCompletGroupCrationModel>();
    private String selectedFilePath = "";
    String fileName = "";
    private String imageURI;
    HttpEntity resEntity;

    /*GroupMembers List*/
    GroupMembersAdapter groupMembersAdapter;
    ArrayList<GroupMembersModel> groupMembersModels = new ArrayList<GroupMembersModel>();
    ArrayList<String> gmList = new ArrayList<String>();

    AlertDialog dialog;
    String selected_gmNames, selected_gmIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_create_dialog);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Create New Group");

        update_key = getIntent().getStringExtra("Profile");
        update_group_id = getIntent().getStringExtra("GroupId");


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        user_surname = userDetails.get(UserSessionManager.USER_SURNAME);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_profile_id = userDetails.get(UserSessionManager.USER_PROFILE_ID);
        final AutoCompletGroupCrationModel multiCompletGroupCrationModel = new AutoCompletGroupCrationModel();

        Log.d("group_creat_ID:", update_group_id + "::" + user_profile_id + "::" + user_surname + "::" + user_name + "::" + update_key);


        select_click_image = (ImageView) findViewById(R.id.select_click_image);
        select_click_image.setOnClickListener(this);

        select_image = (ImageView) findViewById(R.id.select_image);

        autoComplete_members = (MultiAutoCompleteTextView) findViewById(R.id.autoComplete_members);
//        autoComplete_members.setTypeface(typeface);


        privacy_group = (RadioGroup) findViewById(R.id.privacy_group);

        public_radio = (RadioButton) findViewById(R.id.public_radio);
//        public_radio.setTypeface(typeface);

        secret_radio = (RadioButton) findViewById(R.id.secret_radio);
//        secret_radio.setTypeface(typeface);


        group_name_edt = (EditText) findViewById(R.id.group_name_edt);
//        group_name_edt.setTypeface(typeface);

        group_desc_edt = (EditText) findViewById(R.id.group_desc_edt);
//        group_desc_edt.setTypeface(typeface);

        group_member_txt = (TextView) findViewById(R.id.group_member_txt);
        group_member_txt.setOnClickListener(this);

        group_title = (TextView) findViewById(R.id.group_title);
//        group_title.setTypeface(typeface);

        group_desc_title = (TextView) findViewById(R.id.group_desc_title);
//        group_desc_title.setTypeface(typeface);

        group_member_title = (TextView) findViewById(R.id.group_member_title);
//        group_member_title.setTypeface(typeface);

        group_photo_title = (TextView) findViewById(R.id.group_photo_title);

//        group_photo_title.setTypeface(typeface);

        group_privacy_title = (TextView) findViewById(R.id.group_privacy_title);
//        group_privacy_title.setTypeface(typeface);


        button_cancel_now = (TextView) findViewById(R.id.button_cancel_now);
//        button_cancel_now.setTypeface(typeface);
        button_cancel_now.setOnClickListener(this);

        group_dummy_text = (TextView) findViewById(R.id.group_dummy_text);

        button_Create_now = (TextView) findViewById(R.id.button_Create_now);
//        button_Create_now.setTypeface(typeface);
        button_Create_now.setOnClickListener(this);

        button_update_group = (TextView) findViewById(R.id.button_update_group);
//        button_update_group.setTypeface(typeface);
        button_update_group.setOnClickListener(this);
        group_dummy_text.setText("");

        if (update_key.equals("editprofile")) {
            button_update_group.setVisibility(View.VISIBLE);
            button_Create_now.setVisibility(View.GONE);

            getDataForUpdate();


        }
        if (update_key.equals("ditprofile")) {
            button_update_group.setVisibility(View.GONE);
            button_Create_now.setVisibility(View.VISIBLE);
            getDataForUpdate();
        }


//MultiAutoCompleteTextView

        autoComplete_members.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        MultiCompletMemberGroupAdapter adapter = new MultiCompletMemberGroupAdapter(this, R.layout.row_multiselect_members, lisssst);

        autoComplete_members.setThreshold(1);
        autoComplete_members.setAdapter(adapter);
        //adapter.notifyDataSetChanged();


        //when autocomplete is clicked
       /* autoComplete_members.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String countryName = adapter.getItem(position).getName();

                Log.d("dfdfdsfasdfdsfdsf",countryName);
                autoComplete_members.setText(countryName);
            }
        });*/

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == select_click_image) {
            checkInternet = NetworkChecking.isConnected(GroupCreateActivityDialog.this);
            if (checkInternet) {
                final boolean result = ImagePermissions.checkPermission(GroupCreateActivityDialog.this);
                final Dialog mBottomSheetDialog = new Dialog(GroupCreateActivityDialog.this, R.style.MaterialDialogSheet);
               /* TextView dialog_title = (TextView) mBottomSheetDialog.findViewById(R.id.dialog_title);
//                dialog_title.setTypeface(typeface);*/
                mBottomSheetDialog.setContentView(R.layout.custom_dialog_image_selection); // your custom view.
                mBottomSheetDialog.setCancelable(true);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                mBottomSheetDialog.show();

                TextView take_photo_text = (TextView) mBottomSheetDialog.findViewById(R.id.take_photo_text);
//                take_photo_text.setTypeface(typeface);
                take_photo_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(GroupCreateActivityDialog.this, "take photo", Toast.LENGTH_LONG).show();
                        if (result) {
                            if (ContextCompat.checkSelfPermission(GroupCreateActivityDialog.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (getFromPref(GroupCreateActivityDialog.this, ALLOW_KEY)) {
                                    showSettingsAlert();
                                } else if (ContextCompat.checkSelfPermission(GroupCreateActivityDialog.this,
                                        android.Manifest.permission.CAMERA)

                                        != PackageManager.PERMISSION_GRANTED) {

                                    // Should we show an explanation?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(GroupCreateActivityDialog.this,
                                            android.Manifest.permission.CAMERA)) {
                                        showAlert();
                                    } else {
                                        // No explanation needed, we can request the permission.
                                        ActivityCompat.requestPermissions(GroupCreateActivityDialog.this,
                                                new String[]{android.Manifest.permission.CAMERA},
                                                CAMERA_REQUEST_IMAGE);
                                    }
                                }
                            } else {
                                //  cameraIntent();
                                mBottomSheetDialog.dismiss();
                            }

                        }


                    }
                });

                TextView select_from_gallery = (TextView) mBottomSheetDialog.findViewById(R.id.select_from_gallery);
//                select_from_gallery.setTypeface(typeface);
                select_from_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Toast.makeText(GroupCreateActivityDialog.this, "gallry", Toast.LENGTH_LONG).show();
                        /*if (result) {

                        }*/
                        galleryIntent();
                        mBottomSheetDialog.dismiss();


                    }
                });

                mBottomSheetDialog.show();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }


        }
        if (v == button_cancel_now) {
            finish();
        }
        if (v == button_Create_now) {

            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                if (validate()) {

                    if (public_radio.isChecked() || secret_radio.isChecked()) {
                        privcyStatus = (RadioButton) findViewById(privacy_group.getCheckedRadioButtonId());

                        if (privcyStatus.getId() == public_radio.getId()) {
                            send_privacy = "Public";

                        }
                        if (privcyStatus.getId() == secret_radio.getId()) {
                            send_privacy = "Secret";

                        }

                        final String name = group_name_edt.getText().toString().trim();
                        final String descrption = group_desc_edt.getText().toString().trim();

                        String urllllll = AppUrls.BASE_URL + AppUrls.GROUP_CREATION;
                        Log.d("GROUPCREATIONURL:", urllllll);

                        GroupCreateActivityDialog.this.runOnUiThread(new Runnable() {
                            public void run() {
                                //  if (progressDialog != null)
                                //   progressDialog.show();
                            }
                        });
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        String response_str = null;
                        File file1 = null;
                        FileBody bin1 = null;
                        String empty = "";
                        Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                        file1 = new File(selectedFilePath.replaceAll("file://", "").replace("%20", " "));
                        Log.d("FILE URLKLL111", file1.toString());

                        // file1 = new File(fileName.replaceAll("file://", "").replace("%20", " "));
                        String urlString = "";


                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            urlString = AppUrls.BASE_URL + AppUrls.GROUP_CREATION;
                        } else {
                            urlString = AppUrls.BASE_IMAGE_URL + AppUrls.GROUP_CREATION;
                        }
                        Log.d("REQUESTURL", urlString);
                        try {
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(urlString);

                            MultipartEntity reqEntity = new MultipartEntity();
                            //  Log.d("USERIDETAIL:",user_id+"///"+selectedSubject+"///"+name+"///"+emaill+"//"+descrption+"///"+file1);


                            reqEntity.addPart("user_profile_id", new StringBody(user_profile_id));
                            reqEntity.addPart("name", new StringBody(user_name));
                            reqEntity.addPart("surname", new StringBody(user_surname));
                            reqEntity.addPart("group_name", new StringBody(name));
                            reqEntity.addPart("group_description", new StringBody(descrption));
                            reqEntity.addPart("group_type", new StringBody(send_privacy));

                            //String val = autoComplete_members.getText().toString();
                            String val = selected_gmIds;
                            valueList.add(val);
                            String va = valueList.toString();

                            String strWithoutSpace = va.replaceAll(" ", "");
                            int mem = strWithoutSpace.lastIndexOf(",");
                            if (mem > 0)
                                strWithoutSpace = new StringBuilder(strWithoutSpace).replace(mem, mem + 1, "").toString();

                            Log.d("VLUELIST::", valueList.toString());
                            Log.d("TRIMMMMMMM::", strWithoutSpace);
                            //reqEntity.addPart("group_memers_list", new StringBody(removeCharacter(strWithoutSpace)));
                            reqEntity.addPart("group_memers_list", new StringBody(removeCharacter(valueList.toString())));

                            Log.d("USERIDETAIL1:", user_profile_id + "///" + user_name + "///" + user_surname + "//" + name + "///" + descrption + "///" + send_privacy + "//" + bin1);

                            bin1 = new FileBody(file1);
                            Log.d("BINNNN:", bin1.toString());
                            reqEntity.addPart("userfile", bin1);
                            post.setEntity(reqEntity);
                            HttpResponse response = client.execute(post);
                            resEntity = response.getEntity();
                            response_str = EntityUtils.toString(resEntity);
                            Log.d("MERESPONCE", response_str);
                            try {
                                JSONObject jsonObject = new JSONObject(response_str);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equalsIgnoreCase("10100")) {
                                    finish();
                                    Intent i = new Intent(GroupCreateActivityDialog.this, ProfileGroupsActivity.class);
                                    startActivity(i);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    if (jsonObject1.equals("Success")) {
                                        Toast.makeText(getApplicationContext(), "Group Created Successfully" + jsonObject1.toString(), Toast.LENGTH_SHORT).show();
                                    }

                                }
                                if (responceCode.equals("10200")) {

                                    Toast.makeText(GroupCreateActivityDialog.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            progressDialog.dismiss();
                        } catch (Exception ex) {
                            Log.e("Debug", "error: " + ex.getMessage(), ex);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please Select Privacy Type", Toast.LENGTH_SHORT).show();
                    }
                }

                    /*StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.GROUP_CREATION,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response)
                                {
                                    progressDialog.dismiss();
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        Log.d("GROUPCREATIORESP:", response);
                                        String editSuccessResponceCode = jsonObject.getString("status");

                                        if (editSuccessResponceCode.equalsIgnoreCase("10100"))
                                        {
                                            finish();
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            if(jsonObject1.equals("Success"))
                                            {
                                                Toast.makeText(getApplicationContext(), "Group Created Successfully" + jsonObject1.toString(), Toast.LENGTH_SHORT).show();
                                            }


                                            //Log.d("USER_PROFILE_ID",user_profile_id);//puru==USER_PROFILE_ID: 15033728995916034
                                        }

                                        if (editSuccessResponceCode.equals("10200")) {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Failed to Create Group", Toast.LENGTH_SHORT).show();
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_profile_id", user_profile_id);
                            params.put("name", user_name);
                            params.put("surname", user_surname);
                            params.put("group_name", name);
                            params.put("group_description", descrption);
                            params.put("group_type", send_privacy);
                            params.put("userfile", selectedFilePath);

                            String val=autoComplete_members.getText().toString();
                               valueList.add(val);
                            String va=valueList.toString();

                            String strWithoutSpace = va.replaceAll(" ","");
                            int mem=strWithoutSpace.lastIndexOf(",");
                            if(mem>0)
                                strWithoutSpace=new StringBuilder(strWithoutSpace).replace(mem,mem+1,"").toString();

                            Log.d("VLUELIST::", valueList.toString());
                            Log.d("TRIMMMMMMM::",strWithoutSpace);
                            params.put("group_memers_list",removeCharacter(strWithoutSpace));

                             Log.d("GRPCREATIONPARAM::", params.toString());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(GroupCreateActivityDialog.this);
                    requestQueue.add(stringRequest);*/

            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }


/////////////////////////////////UPDATE BELOW//////////////////////////////////////////////////////////////////////////////////////
        if (v == button_update_group) {

            if (validate()) {
                if (selectedFilePath == null || selectedFilePath.equals("")) {

                    // Log.d("COMMING11", "COMMING");
                    // Log.d("PATHHHHH:",selectedFilePath);
                    checkInternet = NetworkChecking.isConnected(this);
                    if (checkInternet) {

                        if (public_radio.isChecked() || secret_radio.isChecked()) {
                            privcyStatus = (RadioButton) findViewById(privacy_group.getCheckedRadioButtonId());

                            if (privcyStatus.getId() == public_radio.getId()) {
                                send_privacy = "Public";

                            }
                            if (privcyStatus.getId() == secret_radio.getId()) {
                                send_privacy = "Secret";

                            }
                        }
                        final String name = group_name_edt.getText().toString().trim();
                        final String descrption = group_desc_edt.getText().toString().trim();


                        // String urllllll = AppUrls.BASE_URL + AppUrls.UPDATE_GROUP;
                        // Log.d("UPDATEGROUPURLwithout:", urllllll);

                        GroupCreateActivityDialog.this.runOnUiThread(new Runnable() {
                            public void run() {
                                if (progressDialog != null)
                                    progressDialog.show();
                            }
                        });
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        String response_str = null;
                        File file1 = null;
                        FileBody bin1 = null;
                        String empty = "";
                        Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                        //  file1 = new File(selectedFilePath.replaceAll("file://", "").replace("%20", " "));
                        //    Log.d("FILE URLKLL", file1.toString());
                        String urlString = "";

                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            urlString = AppUrls.BASE_URL + AppUrls.UPDATE_GROUP;
                        } else {
                            urlString = AppUrls.BASE_IMAGE_URL + AppUrls.UPDATE_GROUP;
                        }
                        Log.d("REQUESTURLwithout:", urlString);
                        Log.d("COMMING22", "COMMING");
                        try {
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(urlString);

                            MultipartEntity reqEntity = new MultipartEntity();
                            //  Log.d("USERIDETAIL:",user_id+"///"+selectedSubject+"///"+name+"///"+emaill+"//"+descrption+"///"+file1);

                            reqEntity.addPart("group_id", new StringBody(update_group_id));
                            reqEntity.addPart("user_profile_id", new StringBody(user_profile_id));
                            reqEntity.addPart("name", new StringBody(user_name));
                            reqEntity.addPart("surname", new StringBody(user_surname));
                            reqEntity.addPart("group_name", new StringBody(name));
                            reqEntity.addPart("group_description", new StringBody(descrption));
                            reqEntity.addPart("group_type", new StringBody(send_privacy));

                            //String val1 = autoComplete_members.getText().toString();
                            String val1 = selected_gmIds;
                            Log.d("UPDATEAUTOCOMPLETE:", val1);
                            String va = val1.toString();
                            String strWithoutSpace1 = va.replaceAll(" ", "");
                            int mem = strWithoutSpace1.lastIndexOf(",");
                            if (mem > 0)
                                strWithoutSpace1 = new StringBuilder(strWithoutSpace1).replace(mem, mem + 1, "").toString();

                            Log.d("UPDATEVLUELIST::", val1.toString());
                            Log.d("UPDATETRIMMMMMMM::", strWithoutSpace1);
                            //reqEntity.addPart("group_memers_list", new StringBody(removeCharacter(strWithoutSpace1)));
                            reqEntity.addPart("group_memers_list", new StringBody(removeCharacter(val1.toString())));
                            Log.d("COMMING44 ", "COMMING");
                            Log.d("USERIDETAIL244", update_group_id + "///" + user_profile_id + "///" + user_name + "///" + user_surname + "//" + name + "///" + descrption + "///" + send_privacy);

                            //   bin1 = new FileBody(file1);
                            reqEntity.addPart("userfile", new StringBody(""));
                            post.setEntity(reqEntity);
                            HttpResponse response = client.execute(post);
                            resEntity = response.getEntity();
                            response_str = EntityUtils.toString(resEntity);
                            Log.d("COMMING33", "COMMING");
                            Log.d("MERESPONCEwithout:", response_str);
                            try {
                                JSONObject jsonObject = new JSONObject(response_str);
                                Log.d("RESPPIFF", jsonObject.toString());
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equalsIgnoreCase("10100")) {

                                    finish();
                                    Intent i = new Intent(GroupCreateActivityDialog.this, ProfileGroupsActivity.class);
                                    startActivity(i);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    if (jsonObject1.equals("Success")) {
                                        Toast.makeText(getApplicationContext(), "Group Updated Successfully" + jsonObject1.toString(), Toast.LENGTH_SHORT).show();
                                    }

                                }
                                if (responceCode.equals("10200")) {

                                    Toast.makeText(GroupCreateActivityDialog.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            progressDialog.dismiss();
                        } catch (Exception ex) {
                            Log.e("Debug", "error: " + ex.getMessage(), ex);
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                } else {

                    checkInternet = NetworkChecking.isConnected(this);
                    if (checkInternet) {

                        if (public_radio.isChecked() || secret_radio.isChecked()) {
                            privcyStatus = (RadioButton) findViewById(privacy_group.getCheckedRadioButtonId());

                            if (privcyStatus.getId() == public_radio.getId()) {
                                send_privacy = "Public";

                            }
                            if (privcyStatus.getId() == secret_radio.getId()) {
                                send_privacy = "Secret";

                            }
                        }
                        final String name = group_name_edt.getText().toString().trim();
                        final String descrption = group_desc_edt.getText().toString().trim();


                        String urllllll = AppUrls.BASE_URL + AppUrls.UPDATE_GROUP;
                        Log.d("UPDATEGROUPURLWITH:", urllllll);

                        GroupCreateActivityDialog.this.runOnUiThread(new Runnable() {
                            public void run() {
                                if (progressDialog != null)
                                    progressDialog.show();
                            }
                        });
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                        String response_str = null;
                        File file1 = null;
                        FileBody bin1 = null;
                        String empty = "";
                        Log.d("SDFAFGSGFSG", "sfafafsaf 1");
                        file1 = new File(selectedFilePath.replaceAll("file://", "").replace("%20", " "));
                        Log.d("FILEURLKLLelse", file1.toString());
                        String urlString = "";


                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            urlString = AppUrls.BASE_URL + AppUrls.UPDATE_GROUP;
                        } else {
                            urlString = AppUrls.BASE_IMAGE_URL + AppUrls.UPDATE_GROUP;
                        }
                        Log.d("REQUESTURLwith", urlString);
                        try {
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(urlString);

                            MultipartEntity reqEntity = new MultipartEntity();
                            //  Log.d("USERIDETAIL:",user_id+"///"+selectedSubject+"///"+name+"///"+emaill+"//"+descrption+"///"+file1);

                            reqEntity.addPart("group_id", new StringBody(update_group_id));
                            reqEntity.addPart("user_profile_id", new StringBody(user_profile_id));
                            reqEntity.addPart("name", new StringBody(user_name));
                            reqEntity.addPart("surname", new StringBody(user_surname));
                            reqEntity.addPart("group_name", new StringBody(name));
                            reqEntity.addPart("group_description", new StringBody(descrption));
                            reqEntity.addPart("group_type", new StringBody(send_privacy));

                            //String val1 = autoComplete_members.getText().toString();
                            String val1 = selected_gmIds;
                            Log.d("UPDATEAUTOCOMPLETE::", val1.toString());
                            String va = val1.toString();
                            String strWithoutSpace1 = va.replaceAll(" ", "");
                            int mem = strWithoutSpace1.lastIndexOf(",");
                            if (mem > 0)
                                strWithoutSpace1 = new StringBuilder(strWithoutSpace1).replace(mem, mem + 1, "").toString();

                            Log.d("UPDATEVLUELIST::", val1.toString());
                            Log.d("UPDATETRIMMMMMMM::", strWithoutSpace1);
                            //reqEntity.addPart("group_memers_list", new StringBody(removeCharacter(strWithoutSpace1)));
                            reqEntity.addPart("group_memers_list", new StringBody(removeCharacter(val1.toString())));

                            Log.d("USERIDETAIL222:", update_group_id + "///" + user_profile_id + "///" + user_name + "///" + user_surname + "//" + name + "///" + descrption + "///" + send_privacy + "//" + file1);

                            bin1 = new FileBody(file1);
                            reqEntity.addPart("userfile", bin1);
                            post.setEntity(reqEntity);
                            HttpResponse response = client.execute(post);
                            resEntity = response.getEntity();
                            response_str = EntityUtils.toString(resEntity);
                            Log.d("MERESPONCEwith", response_str);
                            try {
                                JSONObject jsonObject = new JSONObject(response_str);
                                Log.d("RESPPIFF", jsonObject.toString());
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equalsIgnoreCase("10100")) {
                                    finish();
                                    finish();
                                    Intent i = new Intent(GroupCreateActivityDialog.this, ProfileGroupsActivity.class);
                                    startActivity(i);
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    if (jsonObject1.equals("Success")) {
                                        Toast.makeText(getApplicationContext(), "Group Updated Successfully" + jsonObject1.toString(), Toast.LENGTH_SHORT).show();
                                    }

                                }
                                if (responceCode.equals("10200")) {

                                    Toast.makeText(GroupCreateActivityDialog.this, "Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            progressDialog.dismiss();
                        } catch (Exception ex) {
                            Log.e("Debug", "error: " + ex.getMessage(), ex);
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }

            }
        }

        if (v == group_member_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                groupMembersList();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void groupMembersList() {
        groupMembersModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GROUP_MEMBERS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("RESPONSE:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("groupData");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        GroupMembersModel cm = new GroupMembersModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        String name = jsonObject2.getString("name");
                                        cm.setName(name);
                                        String surName = jsonObject2.getString("surname");
                                        cm.setSurname(surName);
                                        String gm_name = name + surName;
                                        cm.setUsername(jsonObject2.getString("username"));
                                        cm.setPhoto(AppUrls.BASE_IMAGE_URL + jsonObject2.getString("photo"));

                                        gmList.add(gm_name);
                                        groupMembersModels.add(cm);
                                    }
                                    gmDialog();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.cancel();
                                    Snackbar snack = Snackbar.make(getWindow().getDecorView().getRootView(), "No Data Found...!", Snackbar.LENGTH_LONG);

                                    snack.show();
                                }


                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_profile_id", user_profile_id);
                    Log.d("GM_PARAMS", params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void gmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.gm_list, null);

        TextView gm_txt = (TextView) dialog_layout.findViewById(R.id.gm_txt);

        final SearchView gm_search = (SearchView) dialog_layout.findViewById(R.id.gm_search);
        EditText searchEditText = (EditText) gm_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView gm_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.gm_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        gm_recyclerview.setLayoutManager(layoutManager);

        groupMembersAdapter = new GroupMembersAdapter(groupMembersModels, GroupCreateActivityDialog.this, R.layout.row_group_members);

        gm_recyclerview.setAdapter(groupMembersAdapter);

        gm_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gm_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        /*builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });*/

        dialog = builder.create();
        //   dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#"));

        gm_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                groupMembersAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setGmName(ArrayList<String> gm_ids, ArrayList<String> gm_names) {

        //selected_gmIds = gm_ids.toString().replaceAll("\\[", "").replaceAll("\\]","").replaceAll(" ", "");
        selected_gmIds = gm_ids.toString();
        Log.d("GMIDS", selected_gmIds);

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < gm_names.size(); i++) {
            stringBuilder.append(gm_names.get(i));
            //stringBuilder.append(",");
            stringBuilder.append("  ");

        }
        selected_gmNames = stringBuilder.toString();
        group_member_txt.setText(stringBuilder.toString());
    }


    private String removeCharacter(String word) {
        String[] specialCharacters = {"-", " ", "(", ")", "[", "]", "}", ".", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        StringBuilder sb = new StringBuilder(word);
        for (int i = 0; i < sb.toString().length() - 1; i++) {
            for (String specialChar : specialCharacters) {
                if (sb.toString().contains(specialChar)) {
                    int index = sb.indexOf(specialChar);
                    sb.deleteCharAt(index);
                }
            }
        }
        return sb.toString();
    }


    private boolean validate() {

        boolean result = true;

        String name = group_name_edt.getText().toString().trim();
        if (name == null || name.equals("")) {
            group_name_edt.setError("Please Enter Group Name");
            result = false;
        }

        String desc = group_desc_edt.getText().toString().trim();
        if (desc == null || desc.equals("")) {
            group_desc_edt.setError("Please Enter Description");
            result = false;
        }

        if (selectedFilePath.equals("")) {
            Toast.makeText(GroupCreateActivityDialog.this, "Select Group Profile Picture", Toast.LENGTH_LONG).show();
        }


        return result;
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(GroupCreateActivityDialog.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(GroupCreateActivityDialog.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                CAMERA_REQUEST_IMAGE);
                    }
                });
        alertDialog.show();
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(GroupCreateActivityDialog.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(GroupCreateActivityDialog.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_IMAGE) {

                try {
                    onSelectFromGalleryResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

          /*  else if (requestCode == CAMERA_REQUEST_IMAGE)
            {
                try {

                   onCaptureImageResult(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    private void onSelectFromGalleryResult(Intent data) throws IOException {

        Uri selectedFileUri = data.getData();
        selectedFilePath = FilePath.getPath(this, selectedFileUri);
        group_dummy_text.setText(selectedFilePath);
        if (selectedFilePath != null && !selectedFilePath.equals("")) {

            try {
                File f = new File(selectedFilePath);
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, null);
                Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
                select_image.setVisibility(View.VISIBLE);
                select_image.setImageBitmap(correctBmp);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                correctBmp.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //final  String sendingimagepath = destination.getPath();


            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public void getDataForUpdate() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();

            String urlupdate = AppUrls.BASE_URL + AppUrls.GROUP_EDIT + "/" + update_group_id;
            Log.d("GROUPDATAURL:", urlupdate);
            StringRequest strupdate = new StringRequest(Request.Method.GET, urlupdate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("GROUPDATAORESP:", response);
                        String editSuccessResponceCode = jsonObject.getString("status");

                        if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                            String group_ids = jsonObject.getString("groupmembers");
                            selected_gmIds = group_ids;
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            JSONObject jsonObjGrpInfo = jsonObject1.getJSONObject("groupInfo");

                            String group_name = jsonObjGrpInfo.getString("group_name");
                            group_name_edt.setText(group_name);

                            String group_description = jsonObjGrpInfo.getString("group_description");
                            group_desc_edt.setText(group_description);


                            String group_type = jsonObjGrpInfo.getString("group_type");
                            if (group_type.equals("Public")) {
                                public_radio.setChecked(true);
                            } else {
                                secret_radio.setChecked(true);
                            }

                            //  group_path=jsonObjGrpInfo.getString("group_picture");
                            group_picture = AppUrls.BASE_IMAGE_URL_GROUP_LIST + jsonObjGrpInfo.getString("group_picture");

                            select_image.setVisibility(View.VISIBLE);
                            Picasso.with(GroupCreateActivityDialog.this)
                                    .load(group_picture)
                                    .placeholder(R.drawable.no_images_found)
                                    .into(select_image);


                            JSONArray jarrayMembers = jsonObject1.getJSONArray("groupMembers");
                            for (int i = 0; i < jarrayMembers.length(); i++) {
                                JSONObject jobjMember = jarrayMembers.getJSONObject(i);

                                AutoCompletGroupUpdateCrationModel sutoupdatemodel = new AutoCompletGroupUpdateCrationModel();

                                sutoupdatemodel.setId(jobjMember.getString("id"));
                                sutoupdatemodel.setName(jobjMember.getString("name"));
                                sutoupdatemodel.setSurname(jobjMember.getString("surname"));
                                sutoupdatemodel.setUsername(jobjMember.getString("username"));
                                sutoupdatemodel.setPhoto(jobjMember.getString("photo"));

                                String username = jobjMember.getString("username");
                                String name = jobjMember.getString("name");
                                String surname = jobjMember.getString("surname");

                                //String name_surname = name+" "+surname+" "+username;
                                String name_surname = name + " " + surname + " ";
                                updatememberslist.add(sutoupdatemodel);
                                updatemembersliststring.add(name_surname);

                                String gm_ids = jobjMember.getString("id");
                                update_gm_ids.add(gm_ids);
                            }

                            //autoComplete_members.setText(updatemembersliststring.toString());
                            group_member_txt.setText(updatemembersliststring.toString());
                            /*selected_gmIds = update_gm_ids.toString();
                            Log.d("SELECTEDIDS",selected_gmIds);*/
                        }

                        if (editSuccessResponceCode.equals("10200")) {
                            progressDialog.dismiss();
                            //   Toast.makeText(getApplicationContext(), "Something wrong", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

            };
            strupdate.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(GroupCreateActivityDialog.this);
            requestQueue.add(strupdate);
        } else {

        }
    }

}

