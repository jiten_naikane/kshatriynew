package in.innasoft.kshatriyayouth.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.adapters.SurnameListAdapter;
import in.innasoft.kshatriyayouth.models.SurnameListModel;
import in.innasoft.kshatriyayouth.utilities.AppUrls;
import in.innasoft.kshatriyayouth.utilities.NetworkChecking;

public class RegisterPage extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    Toolbar toolbar;
    TextView reg_txt, iam_txt, membership_txt, more_txt, reg_haveaccount_txt, login_here_txt, surname_txt, blood_grp_btn_txt, dob_txt;
    EditText firstname_edt, email_edt, password_edt, adhaar_edt, mobile_edt, add_surname_edt;
    CheckBox reg_view;

    RadioGroup gender_group, membership_group;
    RadioButton male_radio, female_radio, business_radio, association_radio, normal_radio, genderStatus, memberstatus;
    int gunder_status = 0;
    int member_init_status = 0;

    Button reg_btn;
    //    Typeface typeface;
    private boolean checkInternet;
    private int mYear, mMonth, mDay;
    String send_gender, send_username, send_mobile, send_email, send_password, send_adhaar, send_member, sendDOB;


    AlertDialog dialog;
    ProgressDialog progressDialog;
    SurnameListAdapter surnameListAdapter;
    ArrayList<SurnameListModel> surnameModels = new ArrayList<SurnameListModel>();
    ArrayList<String> surname_list = new ArrayList<String>();

    private static final int PERMISSION_REQUEST_ID = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_activity_register_page);

//        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.montserrat_light));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        reg_txt = (TextView) findViewById(R.id.reg_txt);
//        reg_txt.setTypeface(typeface);

        surname_txt = (TextView) findViewById(R.id.surname_txt);
//        surname_txt.setTypeface(typeface);
        surname_txt.setOnClickListener(this);

        blood_grp_btn_txt = (TextView) findViewById(R.id.blood_grp_btn_txt);
//        blood_grp_btn_txt.setTypeface(typeface);
        blood_grp_btn_txt.setOnClickListener(this);

        dob_txt = (TextView) findViewById(R.id.dob_txt);
//        dob_txt.setTypeface(typeface);
        dob_txt.setOnClickListener(this);

        iam_txt = (TextView) findViewById(R.id.iam_txt);
//        iam_txt.setTypeface(typeface);

        membership_txt = (TextView) findViewById(R.id.membership_txt);
//        membership_txt.setTypeface(typeface);

        more_txt = (TextView) findViewById(R.id.more_txt);
//        more_txt.setTypeface(typeface);

        more_txt.setOnClickListener(this);
        reg_haveaccount_txt = (TextView) findViewById(R.id.reg_haveaccount_txt);
//        reg_haveaccount_txt.setTypeface(typeface);

        login_here_txt = (TextView) findViewById(R.id.login_here_txt);
//        login_here_txt.setTypeface(typeface);
        login_here_txt.setOnClickListener(this);

        firstname_edt = (EditText) findViewById(R.id.firstname_edt);
//        firstname_edt.setTypeface(typeface);

        add_surname_edt = (EditText) findViewById(R.id.add_surname_edt);
//        add_surname_edt.setTypeface(typeface);


        email_edt = (EditText) findViewById(R.id.email_edt);
//         email_edt.setTypeface(typeface);
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
//        mobile_edt.setTypeface(typeface);


        password_edt = (EditText) findViewById(R.id.password_edt);
//        password_edt.setTypeface(typeface);

        adhaar_edt = (EditText) findViewById(R.id.adhaar_edt);
//        adhaar_edt.setTypeface(typeface);

        reg_view = (CheckBox) findViewById(R.id.reg_view);
//        reg_view.setTypeface(typeface);
        reg_view.setOnCheckedChangeListener(this);

        gender_group = (RadioGroup) findViewById(R.id.gender_group);
        membership_group = (RadioGroup) findViewById(R.id.membership_group);

        male_radio = (RadioButton) findViewById(R.id.male_radio);
//        male_radio.setTypeface(typeface);

        female_radio = (RadioButton) findViewById(R.id.female_radio);
//        female_radio.setTypeface(typeface);

        business_radio = (RadioButton) findViewById(R.id.business_radio);
//        business_radio.setTypeface(typeface);

        association_radio = (RadioButton) findViewById(R.id.association_radio);
//        association_radio.setTypeface(typeface);

        normal_radio = (RadioButton) findViewById(R.id.normal_radio);
//        normal_radio.setTypeface(typeface);

        reg_btn = (Button) findViewById(R.id.reg_btn);
//        reg_btn.setTypeface(typeface);
        reg_btn.setOnClickListener(this);

        requestRuntimePermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS);

    }

    private void requestRuntimePermissions(String... permissions) {
        for (String perm : permissions) {

            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{perm}, PERMISSION_REQUEST_ID);

            }
        }
    }

    private void getSurnameList() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            surname_list.clear();
            surnameModels.clear();
            progressDialog.show();
            String url = AppUrls.BASE_URL + AppUrls.SURNAME;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("SURNAMERESPONSE:", response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progressDialog.dismiss();
                                    //   JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        SurnameListModel surnameModel = new SurnameListModel();
                                        surnameModel.setId(jsonObject2.getString("id"));
                                        String surname_name = jsonObject2.getString("name");
                                        surnameModel.setSurname(surname_name);

                                        surname_list.add(surname_name);
                                        surnameModels.add(surnameModel);
                                    }
                                    getSurnameListDialog();
                                } else {
                                    progressDialog.dismiss();
                                }

                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progressDialog.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(RegisterPage.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void getSurnameListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.surname_list_dialog, null);

        TextView surName_list_txt = (TextView) dialog_layout.findViewById(R.id.surName_list_txt);
//        surName_list_txt.setTypeface(typeface);

        final SearchView surName_search = (SearchView) dialog_layout.findViewById(R.id.surName_search);
        EditText searchEditText = (EditText) surName_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setTypeface(typeface);

        RecyclerView surName_recycler_view = (RecyclerView) dialog_layout.findViewById(R.id.surName_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        surName_recycler_view.setLayoutManager(layoutManager);

       /* for (int i = 0; i < surname_list.size(); i++){
            SurnameListModel surnameListModel = new SurnameListModel();
            surnameListModel.setSurname(surname_list.get(i));
            surnameModels.add(surnameListModel);
        }*/

        surnameListAdapter = new SurnameListAdapter(surnameModels, RegisterPage.this, R.layout.row_surname_list);

        surName_recycler_view.setAdapter(surnameListAdapter);

        surName_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                surName_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        surName_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                surnameListAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        if (reg_view == compoundButton) {
            if (isChecked) {
                password_edt.setInputType(InputType.TYPE_CLASS_TEXT);
                password_edt.setSelection(password_edt.getText().length());
            } else {
                password_edt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                password_edt.setSelection(password_edt.getText().length());
            }
        }
    }

    private boolean validate() {

        boolean result = true;

        String firstName = firstname_edt.getText().toString().trim();
        if ((firstName == null || firstName.equals("") || firstName.length() < 3) || firstName == "^[a-zA-Z\\\\s]+") {
            firstname_edt.setError("Invalid FirstName");
            result = false;
        }

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_edt.setError("Invalid Email");
            result = false;
        }

        String MOBILE_REGEX = "^[789]\\d{9}$";

        String mobile = mobile_edt.getText().toString().trim();
        if (!mobile.matches(MOBILE_REGEX)) {
            mobile_edt.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_edt.setError(null);
        }

        String password = password_edt.getText().toString().trim();
        if (password.length() < 3) {
            password_edt.setError("Invalid Password");
            result = false;
        }

        String surName = surname_txt.getText().toString().trim();
        if (surName == null || surName.equals("")) {
            surname_txt.setError("Invalid Surname");
            result = false;
        }

        String dob = dob_txt.getText().toString().trim();
        if (dob == null || dob.equals("")) {
            dob_txt.setError("Invalid DOB");
            result = false;
        }

        return result;
    }

    public void setSurName(String surname) {

        dialog.dismiss();
        surnameListAdapter.notifyDataSetChanged();
        surname_txt.setError(null);
        surname_txt.setText(surname);

        if (surname_txt.getText().toString().equals("Add Surname")) {
            // add_surname_edt.setVisibility(View.VISIBLE);
            surname_txt.setText("");
            LayoutInflater li = LayoutInflater.from(RegisterPage.this);
            View dialogView = li.inflate(R.layout.custom_dailog_add_surname, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterPage.this);
            // alertDialogBuilder.setTitle("nameEnter Sur :");
            alertDialogBuilder.setView(dialogView);


            final EditText et_surname_input = (EditText) dialogView.findViewById(R.id.et_surname_input);

            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {//ADD_SURNAME
                                public void onClick(DialogInterface dialog, int id) {
                                    String surname_value = et_surname_input.getText().toString();
                                    if (surname_value.equals("")) {
                                        Toast.makeText(RegisterPage.this, "You are not Added the Surname", Toast.LENGTH_LONG).show();

                                    } else {
                                        sendNewSurname(surname_value);
                                    }

                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });

            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();
        }


    }

    private void sendNewSurname(final String surname_value) {

        StringRequest strRegReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADD_SURNAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d("SURNAME:", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String editSuccessResponceCode = jsonObject.getString("status");
                    if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                        Toast.makeText(getApplicationContext(), "Surname Succefullay Added ", Toast.LENGTH_SHORT).show();

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String message = jsonObject1.getString("msg");


                        progressDialog.dismiss();


                    }
                    if (editSuccessResponceCode.equals("10300")) {
                        Toast.makeText(getApplicationContext(), "SurName already exits", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", surname_value);

                Log.d("SURNAMEPARAM", params.toString());

                return params;
            }
        };
        strRegReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RegisterPage.this);
        requestQueue.add(strRegReq);
    }


    @Override
    public void onClick(View v) {
        if (v == login_here_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar
                        .make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == blood_grp_btn_txt) {
            final String names[] = {"A+", "O+", "B+", "AB+", "A-", "O-", "B-", "AB-"};
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterPage.this);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.custom_blood_dialog, null);
            alertDialog.setCancelable(false);
            alertDialog.setView(convertView);
            alertDialog.setTitle("List");
            final ListView lv_blood = (ListView) convertView.findViewById(R.id.lv_blood);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
            lv_blood.setAdapter(adapter);
            final AlertDialog dialog = alertDialog.show();
            lv_blood.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    blood_grp_btn_txt.setText(names[arg2]);
                    dialog.dismiss();

                }
            });
        }

        if (v == dob_txt) {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    String strMonth = null, strDay = null;

                    int month = monthOfYear + 1;
                    if (month < 10) {

                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {

                        strDay = "0" + dayOfMonth;
                    } else {
                        strDay = dayOfMonth + "";
                    }
                    sendDOB = String.valueOf(strDay + "-" + strMonth + "-" + year);
                    dob_txt.setText(String.valueOf(strDay + " - " + strMonth + " - " + year));
                    dob_txt.setError(null);
                }
            }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }

        if (v == surname_txt) {
            surnameModels.clear();
            getSurnameList();
        }
        if (v == reg_btn) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                if (male_radio.isChecked() || female_radio.isChecked()) {
                    genderStatus = (RadioButton) findViewById(gender_group.getCheckedRadioButtonId());

                    if (genderStatus.getId() == male_radio.getId()) {
                        gunder_status = 1;
                    }
                    if (genderStatus.getId() == female_radio.getId()) {
                        gunder_status = 1;
                    }
                }
                if (business_radio.isChecked() || association_radio.isChecked() || normal_radio.isChecked()) {
                    memberstatus = (RadioButton) findViewById(membership_group.getCheckedRadioButtonId());

                    if (memberstatus.getId() == business_radio.getId()) {
                        member_init_status = 1;
                    }
                    if (memberstatus.getId() == association_radio.getId()) {
                        member_init_status = 1;
                    }
                    if (memberstatus.getId() == normal_radio.getId()) {
                        member_init_status = 1;
                    }
                }
                if (validate()) {
                    if (dob_txt.getText().toString().trim().length() != 0 && !dob_txt.getText().toString().equals(null)) {
                        progressDialog.show();

                        if ((gunder_status == 1) && (firstname_edt.getText().toString().trim().length() > 0)
                                && (mobile_edt.getText().toString().trim().length() > 0)
                                && (email_edt.getText().toString().trim().length() > 0)
                                && (password_edt.getText().toString().trim().length() > 0)
                                && (adhaar_edt.getText().toString().trim().length() > 0)) {

                            if (checkInternet) {
                                send_username = firstname_edt.getText().toString().trim();
                                send_email = email_edt.getText().toString().trim();
                                send_mobile = mobile_edt.getText().toString().trim();
                                send_password = password_edt.getText().toString().trim();
                                send_adhaar = adhaar_edt.getText().toString().trim();

                                if (male_radio.isChecked() || female_radio.isChecked()) {
                                    genderStatus = (RadioButton) findViewById(gender_group.getCheckedRadioButtonId());

                                    if (genderStatus.getId() == male_radio.getId()) {
                                        send_gender = "male";

                                    }
                                    if (genderStatus.getId() == female_radio.getId()) {
                                        send_gender = "female";

                                    }
                                }

                                if (business_radio.isChecked() || association_radio.isChecked() || normal_radio.isChecked()) {
                                    memberstatus = (RadioButton) findViewById(membership_group.getCheckedRadioButtonId());

                                    if (memberstatus.getId() == business_radio.getId()) {
                                        send_member = "Business";

                                    }
                                    if (memberstatus.getId() == association_radio.getId()) {
                                        send_member = "Association";

                                    }
                                    if (memberstatus.getId() == normal_radio.getId()) {
                                        send_member = "Normal";

                                    }
                                }


                                StringRequest strRegReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        progressDialog.dismiss();
                                        Log.d("REG-RESP", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String editSuccessResponceCode = jsonObject.getString("status");
                                            if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                                String message = jsonObject1.getString("msg");
                                                String userId = jsonObject1.getString("user_id");
                                                String mobile = jsonObject1.getString("mobile");
                                                String otp = jsonObject1.getString("otp");
                                                progressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "OTP has been sent to your registered Email Id...! ", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(RegisterPage.this, AccountVerificationPage.class);
                                                intent.putExtra("userId", userId);
                                                intent.putExtra("mobile", mobile);
                                                intent.putExtra("email", send_email);
                                                intent.putExtra("otp", otp);
                                                startActivity(intent);
                                            }
                                            if (editSuccessResponceCode.equals("10200")) {
                                                Toast.makeText(getApplicationContext(), "Email already exits", Toast.LENGTH_SHORT).show();
                                            }
                                            if (editSuccessResponceCode.equals("10300")) {
                                                Toast.makeText(getApplicationContext(), "Mobile already exits", Toast.LENGTH_SHORT).show();
                                            }
                                            if (editSuccessResponceCode.equals("10500")) {
                                                Toast.makeText(getApplicationContext(), "All fields required....!", Toast.LENGTH_SHORT).show();
                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }
                                },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("name", send_username);
                                        //params.put("dob", dob_txt.getText().toString());
                                        params.put("dob", sendDOB);
                                        params.put("gender", send_gender);
                                        params.put("email_id", send_email);
                                        params.put("mobile", send_mobile);
                                        params.put("password", send_password);
                                        params.put("surname", surname_txt.getText().toString());
                                           /* if(surname_txt.getText().toString().equals("Add Surname"))
                                            {
                                                params.put("surname", add_surname_edt.getText().toString());
                                            }
                                            else
                                            {
                                                params.put("surname", surname_txt.getText().toString());
                                            }*/
                                        params.put("adhaar", send_adhaar);
                                        params.put("blood_group", blood_grp_btn_txt.getText().toString());
                                        params.put("membership", send_member);
                                        Log.d("REG-PARAMS", params.toString());
                                        //jite@gmail.com
                                        //1234567

                                        return params;
                                    }
                                };
                                strRegReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                RequestQueue requestQueue = Volley.newRequestQueue(RegisterPage.this);
                                requestQueue.add(strRegReq);

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();

                            }

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Please provide all the details", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Please provide Date of birth..!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    progressDialog.dismiss();
                    //  Toast.makeText(getApplicationContext(), "Please provide all the details", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();


            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_ID) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(RegisterPage.this, "permission granted", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(RegisterPage.this, "permission not granted", Toast.LENGTH_LONG).show();

            }
        }
    }
}
