package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupPhotoListItemClickListener;


public class GroupPhotoListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public ImageView photo_image_iv;
    GroupPhotoListItemClickListener groupPhotoListItemClickListener;


    public GroupPhotoListHolder(View itemView) {

        super(itemView);


        photo_image_iv = (ImageView) itemView.findViewById(R.id.photo_image_iv);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.groupPhotoListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GroupPhotoListItemClickListener ic) {
        this.groupPhotoListItemClickListener = ic;
    }
}
