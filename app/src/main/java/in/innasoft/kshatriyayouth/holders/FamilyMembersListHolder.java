package in.innasoft.kshatriyayouth.holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FamilyMembersListItemClickListener;

public class FamilyMembersListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView family_name, family_gender, family_age, family_education, family_contact, family_occupation;
    public Activity context;
    FamilyMembersListItemClickListener familyMembersListItemClickListener;

    public FamilyMembersListHolder(View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        family_name = (TextView) itemView.findViewById(R.id.family_name);
        family_gender = (TextView) itemView.findViewById(R.id.family_gender);
        family_age = (TextView) itemView.findViewById(R.id.family_age);
        family_education = (TextView) itemView.findViewById(R.id.family_education);
        family_contact = (TextView) itemView.findViewById(R.id.family_contact);
        family_occupation = (TextView) itemView.findViewById(R.id.family_occupation);

    }

    @Override
    public void onClick(View view) {

        this.familyMembersListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FamilyMembersListItemClickListener ic) {
        this.familyMembersListItemClickListener = ic;
    }
}
