package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.MultipleSelctionMemberItemClcikListener;


public class MultiSelectMemberHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView auto_tv;
    public ImageView imge_multiselect;
    MultipleSelctionMemberItemClcikListener multiSelctionMemberItemClcikListener;


    public MultiSelectMemberHolder(View itemView) {

        super(itemView);

        auto_tv = (TextView) itemView.findViewById(R.id.auto_tv);

        imge_multiselect = (ImageView) itemView.findViewById(R.id.imge_multiselect);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.multiSelctionMemberItemClcikListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MultipleSelctionMemberItemClcikListener ic) {
        this.multiSelctionMemberItemClcikListener = ic;
    }
}
