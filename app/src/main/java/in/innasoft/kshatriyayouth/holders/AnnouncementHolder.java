package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class AnnouncementHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView title, description, createddateandtime, vierw_more_text;
    public ImageView image;
    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public AnnouncementHolder(View itemView) {

        super(itemView);
        createddateandtime = (TextView) itemView.findViewById(R.id.createddateandtime);
        vierw_more_text = (TextView) itemView.findViewById(R.id.vierw_more_text);
        description = (TextView) itemView.findViewById(R.id.description);
        title = (TextView) itemView.findViewById(R.id.title);
        image = (ImageView) itemView.findViewById(R.id.image);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
