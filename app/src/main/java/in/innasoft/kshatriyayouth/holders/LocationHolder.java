package in.innasoft.kshatriyayouth.holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.SurnameListItemClickListener;

public class LocationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView row_surname_txt;
    public Activity context;
    SurnameListItemClickListener surnameListItemClickListener;

    public LocationHolder(View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        row_surname_txt = (TextView) itemView.findViewById(R.id.row_surname_txt);

    }

    @Override
    public void onClick(View view) {

        this.surnameListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SurnameListItemClickListener ic) {
        this.surnameListItemClickListener = ic;
    }
}
