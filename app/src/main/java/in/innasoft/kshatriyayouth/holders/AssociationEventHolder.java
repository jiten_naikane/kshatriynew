package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.AssociationEventItemClcikListener;

/**
 * Created by Devolper on 22-Aug-17.
 */


public class AssociationEventHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView asso_event_img;
    public TextView asso_event_title, asso_event_descrption, asso_event_venue, asso_event_start_date, asso_event_end_date;


    AssociationEventItemClcikListener associatneventItemClickListener;

    public AssociationEventHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        asso_event_title = (TextView) itemView.findViewById(R.id.asso_event_title);

        asso_event_img = (ImageView) itemView.findViewById(R.id.asso_event_img);

        asso_event_descrption = (TextView) itemView.findViewById(R.id.asso_event_descrption);

        asso_event_venue = (TextView) itemView.findViewById(R.id.asso_event_venue);
        asso_event_start_date = (TextView) itemView.findViewById(R.id.asso_event_start_date);
        asso_event_end_date = (TextView) itemView.findViewById(R.id.asso_event_end_date);

    }

    @Override
    public void onClick(View view) {

        this.associatneventItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AssociationEventItemClcikListener ic) {
        this.associatneventItemClickListener = ic;
    }
}

