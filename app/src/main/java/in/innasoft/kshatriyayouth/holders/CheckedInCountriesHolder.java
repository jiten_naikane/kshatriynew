package in.innasoft.kshatriyayouth.holders;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.CheckedInCountriesClickListener;


/**
 * Created by purushotham on 27/1/17.
 */

public class CheckedInCountriesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView checkin_country_name_tv;
    public Fragment context;
    CheckedInCountriesClickListener checkedInCountriesClickListener;

    public CheckedInCountriesHolder(View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        checkin_country_name_tv = (TextView) itemView.findViewById(R.id.checkin_country_name_tv);
    }

    @Override
    public void onClick(View view) {

        this.checkedInCountriesClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CheckedInCountriesClickListener ic) {
        this.checkedInCountriesClickListener = ic;
    }
}
