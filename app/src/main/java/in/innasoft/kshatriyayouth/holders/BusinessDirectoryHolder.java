package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.BusinessDirectoryItemClcikListener;


public class BusinessDirectoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView bisiness_title, bisiness_location, createddateandtime;
    public ImageView business_image;
    BusinessDirectoryItemClcikListener businessDirectoryItemClcikListener;


    public BusinessDirectoryHolder(View itemView) {

        super(itemView);

        createddateandtime = (TextView) itemView.findViewById(R.id.createddateandtime);
        bisiness_location = (TextView) itemView.findViewById(R.id.bisiness_location);
        bisiness_title = (TextView) itemView.findViewById(R.id.bisiness_title);
        business_image = (ImageView) itemView.findViewById(R.id.business_image);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.businessDirectoryItemClcikListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BusinessDirectoryItemClcikListener ic) {
        this.businessDirectoryItemClcikListener = ic;
    }
}
