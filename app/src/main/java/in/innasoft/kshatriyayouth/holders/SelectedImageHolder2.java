package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.SelectedImageItemClickListener2;


public class SelectedImageHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView selected_image_delete_tv;
    public ImageView selected_image_iv;
    SelectedImageItemClickListener2 selectedImageItemClickListener2;

    public SelectedImageHolder2(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        selected_image_delete_tv = (TextView) itemView.findViewById(R.id.selected_image_delete_tv);
        selected_image_iv = (ImageView) itemView.findViewById(R.id.selected_image_iv);
    }

    @Override
    public void onClick(View view) {
        this.selectedImageItemClickListener2.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SelectedImageItemClickListener2 ic) {
        this.selectedImageItemClickListener2 = ic;
    }
}
