package in.innasoft.kshatriyayouth.holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.DOBFriendsListItemClickListener;

public class DOBFriendsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name_dob_friends;
    public ImageView imge_dob_friends;
    public Activity context;
    DOBFriendsListItemClickListener dobFriendsListItemClickListener;

    public DOBFriendsListHolder(View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        name_dob_friends = (TextView) itemView.findViewById(R.id.name_dob_friends);
        imge_dob_friends = (ImageView) itemView.findViewById(R.id.imge_dob_friends);

    }

    @Override
    public void onClick(View view) {

        this.dobFriendsListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(DOBFriendsListItemClickListener ic) {
        this.dobFriendsListItemClickListener = ic;
    }
}
