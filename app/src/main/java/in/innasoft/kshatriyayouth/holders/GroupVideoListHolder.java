package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupVideoListItemClickListener;


public class GroupVideoListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public ImageView img_defult_videothumbnail;
    public TextView video_title;
    GroupVideoListItemClickListener groupVideoListItemClickListener;


    public GroupVideoListHolder(View itemView) {

        super(itemView);

        img_defult_videothumbnail = (ImageView) itemView.findViewById(R.id.img_defult_videothumbnail);
        video_title = (TextView) itemView.findViewById(R.id.video_title);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.groupVideoListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GroupVideoListItemClickListener ic) {
        this.groupVideoListItemClickListener = ic;
    }
}
