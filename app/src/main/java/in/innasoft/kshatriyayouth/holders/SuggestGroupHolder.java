package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.SuggestGroupListItemClickListener;


public class SuggestGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView suggest_group_name, suggest_group_members_count, textDelete;
    public Button suggesst_join_button;
    public ImageView suggest_group_list_img_view;


    SuggestGroupListItemClickListener suggestedGroupItemClickListener;


    public SuggestGroupHolder(View itemView) {

        super(itemView);
        suggest_group_name = (TextView) itemView.findViewById(R.id.suggest_group_name);
        suggest_group_members_count = (TextView) itemView.findViewById(R.id.suggest_group_members_count);
        textDelete = (TextView) itemView.findViewById(R.id.textDelete);
        suggesst_join_button = (Button) itemView.findViewById(R.id.suggesst_join_button);

        suggest_group_list_img_view = (ImageView) itemView.findViewById(R.id.suggest_group_list_img_view);

        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.suggestedGroupItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SuggestGroupListItemClickListener ic) {
        this.suggestedGroupItemClickListener = ic;
    }
}
