package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class JobsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView job_title_text, job_experience_text, job_skills_text, postedby_text, job_salary_text, postedby_title, createddateandtime_text;
    public ImageView job_image_iv;

    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;

    public JobsHolder(View itemView) {

        super(itemView);

        job_title_text = (TextView) itemView.findViewById(R.id.job_title_text);
        postedby_title = (TextView) itemView.findViewById(R.id.postedby_title);
        job_experience_text = (TextView) itemView.findViewById(R.id.job_experience_text);
        job_skills_text = (TextView) itemView.findViewById(R.id.job_skills_text);
        postedby_text = (TextView) itemView.findViewById(R.id.postedby_text);
        job_salary_text = (TextView) itemView.findViewById(R.id.job_salary_text);
        createddateandtime_text = (TextView) itemView.findViewById(R.id.createddateandtime_text);
        job_image_iv = (ImageView) itemView.findViewById(R.id.job_image_iv);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
