package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.AssociationPostItemClcikListener;

/**
 * Created by Devolper on 22-Aug-17.
 */


public class AssociationPostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView asso_post_img;
    public TextView asso_post_title, asso_post_descrption;


    AssociationPostItemClcikListener associatnpostItemClickListener;

    public AssociationPostHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        asso_post_title = (TextView) itemView.findViewById(R.id.asso_post_title);
        asso_post_img = (ImageView) itemView.findViewById(R.id.asso_post_img);
        asso_post_descrption = (TextView) itemView.findViewById(R.id.asso_post_descrption);

    }

    @Override
    public void onClick(View view) {

        this.associatnpostItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AssociationPostItemClcikListener ic) {
        this.associatnpostItemClickListener = ic;
    }
}

