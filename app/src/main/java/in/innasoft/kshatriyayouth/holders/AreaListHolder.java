package in.innasoft.kshatriyayouth.holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.AreaListItemClickListener;

public class AreaListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView row_area_txt;
    public Activity context;
    AreaListItemClickListener areaListItemClickListener;

    public AreaListHolder(View itemView) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        row_area_txt = (TextView) itemView.findViewById(R.id.row_area_txt);

    }

    @Override
    public void onClick(View view) {

        this.areaListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AreaListItemClickListener ic) {
        this.areaListItemClickListener = ic;
    }
}
