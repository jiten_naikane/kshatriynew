package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupMemberListItemClickListener;


public class GroupMemberListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView member_name_text;
    public ImageView member_image_iv;
    GroupMemberListItemClickListener groupMemberListItemClickListener;


    public GroupMemberListHolder(View itemView) {

        super(itemView);

        member_name_text = (TextView) itemView.findViewById(R.id.member_name_text);
        member_image_iv = (ImageView) itemView.findViewById(R.id.member_image_iv);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.groupMemberListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GroupMemberListItemClickListener ic) {
        this.groupMemberListItemClickListener = ic;
    }
}
