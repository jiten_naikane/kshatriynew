package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.GroupListItemClickListener;


public class GroupListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView group_name, members_count;
    public ImageView group_list_img_view;
    public Button button_join_group;
    GroupListItemClickListener groupListItemClickListener;


    public GroupListHolder(View itemView) {

        super(itemView);
        group_name = (TextView) itemView.findViewById(R.id.group_name);
        members_count = (TextView) itemView.findViewById(R.id.members_count);

        button_join_group = (Button) itemView.findViewById(R.id.button_join_group);

        group_list_img_view = (ImageView) itemView.findViewById(R.id.group_list_img_view);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.groupListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GroupListItemClickListener ic) {
        this.groupListItemClickListener = ic;
    }
}
