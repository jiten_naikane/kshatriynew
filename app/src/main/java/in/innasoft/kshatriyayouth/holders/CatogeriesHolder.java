package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class CatogeriesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView title, description, createddateandtime, postedby, jobpreferences;
    public ImageView image;
    public View titleline;
    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public CatogeriesHolder(View itemView) {

        super(itemView);

        title = (TextView) itemView.findViewById(R.id.title);
        titleline = (View) itemView.findViewById(R.id.tab_line);


        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
