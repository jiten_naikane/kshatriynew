package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;
import in.innasoft.kshatriyayouth.utilities.FeedImageView;


public class FeedHomeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView after_reg_university_name_tv, after_reg_county_name_tv, after_reg_price_limit_tv, pricetype, fee_txt;
    public ImageView after_reg_university_iv;

    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;

/////////////////////////////////

    ///////////////////
    public TextView name = (TextView) itemView.findViewById(R.id.name);
    public ImageView post_hideorvisible = (ImageView) itemView.findViewById(R.id.post_hideorvisible);
    public TextView timestamp = (TextView) itemView
            .findViewById(R.id.timestamp);
    public TextView hideshow_text = (TextView) itemView
            .findViewById(R.id.hideshow_text);


    public TextView likes = (TextView) itemView
            .findViewById(R.id.likes);
    public TextView shares = (TextView) itemView
            .findViewById(R.id.shares);
    public TextView post_status = (TextView) itemView
            .findViewById(R.id.post_status);
    public TextView shared_post = (TextView) itemView
            .findViewById(R.id.shared_post);


    public TextView like_text = (TextView) itemView
            .findViewById(R.id.like_text);

    public TextView coment_text = (TextView) itemView.findViewById(R.id.coment_text);
    public TextView share_text = (TextView) itemView.findViewById(R.id.share_text);
    public TextView edited = (TextView) itemView.findViewById(R.id.edited);
    public TextView unhide = (TextView) itemView.findViewById(R.id.unhide);


    public TextView statusMsg = (TextView) itemView
            .findViewById(R.id.txtStatusMsg);
    public TextView shared_name = (TextView) itemView
            .findViewById(R.id.shared_name);
    public TextView shared_display_text = (TextView) itemView
            .findViewById(R.id.shared_display_text);
    public TextView seemore = (TextView) itemView
            .findViewById(R.id.seemore);
    public RelativeLayout chatlayout = (RelativeLayout) itemView
            .findViewById(R.id.chatlayout);
    public RelativeLayout feed_view_layout = (RelativeLayout) itemView
            .findViewById(R.id.feed_view_layout);
    public RelativeLayout sharelayout = (RelativeLayout) itemView
            .findViewById(R.id.sharelayout);
    public RelativeLayout like_layout = (RelativeLayout) itemView
            .findViewById(R.id.like_layout);
    public LinearLayout like_catogery_layout = (LinearLayout) itemView
            .findViewById(R.id.like_catogery_layout);
    public LinearLayout likecommentsharelayout = (LinearLayout) itemView
            .findViewById(R.id.likecommentsharelayout);
    public LinearLayout shared_post_layout = (LinearLayout) itemView
            .findViewById(R.id.shared_post_layout);
    public RelativeLayout main_feed_layout = (RelativeLayout) itemView
            .findViewById(R.id.main_feed_layout);
    public TextView url = (TextView) itemView.findViewById(R.id.txtUrl);
    public NetworkImageView profilePic = (NetworkImageView) itemView
            .findViewById(R.id.profilePic);
    public NetworkImageView shared_profilePic = (NetworkImageView) itemView
            .findViewById(R.id.shared_profilePic);
    public FeedImageView feedImageView = (FeedImageView) itemView
            .findViewById(R.id.feedImage1);
    public FeedImageView feedImageView1 = (FeedImageView) itemView
            .findViewById(R.id.feedImage2);
    public FeedImageView feedImageView2 = (FeedImageView) itemView
            .findViewById(R.id.feedImage3);
    public WebView webView = (WebView) itemView
            .findViewById(R.id.webView);
    public ImageView thumbnail_image = (ImageView) itemView
            .findViewById(R.id.thumbnail_image);
    public ImageView like_image = (ImageView) itemView
            .findViewById(R.id.like_image);


    public ImageView like_icon = (ImageView) itemView
            .findViewById(R.id.like_icon);
    public ImageView love_icon = (ImageView) itemView
            .findViewById(R.id.love_icon);

    public ImageView haha_icon = (ImageView) itemView
            .findViewById(R.id.haha_icon);

    public ImageView wow_icon = (ImageView) itemView
            .findViewById(R.id.wow_icon);
    public ImageView sad_icon = (ImageView) itemView
            .findViewById(R.id.sad_icon);
    public ImageView angry_icon = (ImageView) itemView
            .findViewById(R.id.angry_icon);


    public FeedHomeHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
