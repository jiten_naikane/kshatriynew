package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.EditProfileCityItemClickListener;


public class EditProfileCityHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView city_name_txt;
    EditProfileCityItemClickListener cityItemClickListener;

    public EditProfileCityHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        city_name_txt = (TextView) itemView.findViewById(R.id.city_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.cityItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(EditProfileCityItemClickListener ic) {
        this.cityItemClickListener = ic;
    }
}
