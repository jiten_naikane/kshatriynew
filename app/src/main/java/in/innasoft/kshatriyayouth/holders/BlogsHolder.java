package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class BlogsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView title, description, createddateandtime, postedby, jobpreferences;
    public ImageView image;
    public View blog_view;
    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public BlogsHolder(View itemView) {

        super(itemView);
        createddateandtime = (TextView) itemView.findViewById(R.id.createddateandtime);
        description = (TextView) itemView.findViewById(R.id.description);
        title = (TextView) itemView.findViewById(R.id.title);
        blog_view = (View) itemView.findViewById(R.id.blog_view);

        jobpreferences = (TextView) itemView.findViewById(R.id.jobpreferences);
        image = (ImageView) itemView.findViewById(R.id.image);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
