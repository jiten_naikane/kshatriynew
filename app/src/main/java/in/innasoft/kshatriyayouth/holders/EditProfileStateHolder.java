package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.EditProfileStateItemClickListener;


public class EditProfileStateHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView state_name_txt;
    EditProfileStateItemClickListener stateItemClickListener;

    public EditProfileStateHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        state_name_txt = (TextView) itemView.findViewById(R.id.state_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.stateItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(EditProfileStateItemClickListener ic) {
        this.stateItemClickListener = ic;
    }
}
