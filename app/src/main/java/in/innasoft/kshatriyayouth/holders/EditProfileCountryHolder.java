package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.EditProfileCountryItemClickListener;


public class EditProfileCountryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView country_name_txt;
    EditProfileCountryItemClickListener countryItemClickListener;

    public EditProfileCountryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        country_name_txt = (TextView) itemView.findViewById(R.id.country_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.countryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(EditProfileCountryItemClickListener ic) {
        this.countryItemClickListener = ic;
    }
}
