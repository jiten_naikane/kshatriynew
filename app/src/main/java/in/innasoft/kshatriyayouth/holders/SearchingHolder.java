package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class SearchingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name;
    public ImageView profilePic;
    public Button delete, confirm;


    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public SearchingHolder(View itemView) {

        super(itemView);
        name = (TextView) itemView.findViewById(R.id.name);
        profilePic = (ImageView) itemView.findViewById(R.id.profilePic);
        delete = (Button) itemView.findViewById(R.id.delete);
        confirm = (Button) itemView.findViewById(R.id.confirm);

        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
