package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.InformationDirectoryItemClickListener;


public class InformationDirectoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView info_directory_title, info_directory_occupation;
    public ImageView info_image_iv;
    InformationDirectoryItemClickListener informatuion_directoryItemClickListener;

    public InformationDirectoryHolder(View itemView) {

        super(itemView);

        info_directory_occupation = (TextView) itemView.findViewById(R.id.info_directory_occupation);
        info_directory_title = (TextView) itemView.findViewById(R.id.info_directory_title);
        info_image_iv = (ImageView) itemView.findViewById(R.id.info_image_iv);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.informatuion_directoryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(InformationDirectoryItemClickListener ic) {
        this.informatuion_directoryItemClickListener = ic;
    }
}
