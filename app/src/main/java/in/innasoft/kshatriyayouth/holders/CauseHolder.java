package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class CauseHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView description, createddataandtime, charity_name, donation_amount, location, postedby, textView2, dot_txt;
    public ImageView image;
    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public CauseHolder(View itemView) {

        super(itemView);

        description = (TextView) itemView.findViewById(R.id.description);
        createddataandtime = (TextView) itemView.findViewById(R.id.createddataandtime);
        charity_name = (TextView) itemView.findViewById(R.id.charity_name);
        donation_amount = (TextView) itemView.findViewById(R.id.donation_amount);
        location = (TextView) itemView.findViewById(R.id.location);
        postedby = (TextView) itemView.findViewById(R.id.postedby);
        textView2 = (TextView) itemView.findViewById(R.id.textView2);
        dot_txt = (TextView) itemView.findViewById(R.id.dot_txt);
        image = (ImageView) itemView.findViewById(R.id.image);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
