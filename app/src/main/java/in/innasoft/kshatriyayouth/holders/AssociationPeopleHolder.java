package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.AssociationPeopleItemClcikListener;

/**
 * Created by Devolper on 22-Aug-17.
 */


public class AssociationPeopleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView people_profile_img;
    public TextView people_name_text, people_city_state_text;


    AssociationPeopleItemClcikListener associatnpeopleItemClickListener;

    public AssociationPeopleHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        people_name_text = (TextView) itemView.findViewById(R.id.people_name_text);
        people_profile_img = (ImageView) itemView.findViewById(R.id.people_profile_img);
        people_city_state_text = (TextView) itemView.findViewById(R.id.people_city_state_text);


    }

    @Override
    public void onClick(View view) {

        this.associatnpeopleItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AssociationPeopleItemClcikListener ic) {
        this.associatnpeopleItemClickListener = ic;
    }
}

