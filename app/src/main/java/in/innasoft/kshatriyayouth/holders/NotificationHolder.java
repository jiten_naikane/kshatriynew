package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class NotificationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name_surname, item_name, timestamp, group_name, post_or_reply, comment_name, post_name, resolve_group;
    public Button btn_accept_request, btn_deny_request;
    public ImageView profilePic, like_button;
    public LinearLayout comment_like_reply, accepted_deny_ll;

    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public NotificationHolder(View itemView) {

        super(itemView);


        name_surname = (TextView) itemView.findViewById(R.id.name_surname);
        item_name = (TextView) itemView.findViewById(R.id.item_name);
        comment_name = (TextView) itemView.findViewById(R.id.comment_name);
        post_name = (TextView) itemView.findViewById(R.id.post_name);
        resolve_group = (TextView) itemView.findViewById(R.id.resolve_group);
        post_or_reply = (TextView) itemView.findViewById(R.id.post_or_reply);
        group_name = (TextView) itemView.findViewById(R.id.group_name);
        timestamp = (TextView) itemView.findViewById(R.id.timestamp);
        profilePic = (ImageView) itemView.findViewById(R.id.profilePic);
        like_button = (ImageView) itemView.findViewById(R.id.like_button);

        btn_accept_request = (Button) itemView.findViewById(R.id.btn_accept_request);
        btn_deny_request = (Button) itemView.findViewById(R.id.btn_deny_request);

        comment_like_reply = (LinearLayout) itemView.findViewById(R.id.comment_like_reply);
        accepted_deny_ll = (LinearLayout) itemView.findViewById(R.id.accepted_deny_ll);

        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
