package in.innasoft.kshatriyayouth.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.kshatriyayouth.R;
import in.innasoft.kshatriyayouth.itemClickListerners.FeedHomeClickListner;


public class AllFriendsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name, timestamp, createddateandtime, vierw_more_text;
    public ImageView profilePic;
    public Button confirm, delete;
    FeedHomeClickListner afterRegCPListofUniversitiesItemClickListener;


    public AllFriendsHolder(View itemView) {

        super(itemView);
        name = (TextView) itemView.findViewById(R.id.name);
        confirm = (Button) itemView.findViewById(R.id.confirm);
        delete = (Button) itemView.findViewById(R.id.delete);
        timestamp = (TextView) itemView
                .findViewById(R.id.timestamp);

        profilePic = (ImageView) itemView
                .findViewById(R.id.profilePic);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        this.afterRegCPListofUniversitiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(FeedHomeClickListner ic) {
        this.afterRegCPListofUniversitiesItemClickListener = ic;
    }
}
